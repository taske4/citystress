<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Женская одежда в интернет-магазине CityStress,【Низкие цены】☛ в наличии большой ассортимент моделей.  ☛ Доставка по всей России: ☎ 8 800 700 46 00");
$APPLICATION->SetPageProperty("title", "Интернет-магазин женской одежды Сити Стресс — официальный сайт CityStress");

/**
 * @global $APPLICATION
 */

$APPLICATION->SetTitle("Интернет-магазин женской одежды CityStress");
?><?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"homeslider",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "slider",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("LINK",""),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?> <?
    global $sectionsFilter;

    $sectionsFilter['=UF_SHOW_ON_HOME_PAGE'] = true;
?> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"home",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "Y",
		"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
		"FILTER_NAME" => "sectionsFilter",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array("",""),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "/catalog/#SECTION_CODE#/",
		"SECTION_USER_FIELDS" => array("UF_MOBILE_PICTURE","UF_TABLET_PICTURE","UF_DESKTOP_PICTURE",""),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "5",
		"VIEW_MODE" => "LINE"
	)
);?> <?

        global $newProductsFilter;

        $cache = \Bitrix\Main\Data\Cache::createInstance(); // получаем экземпляр класса

        if ($cache->initCache(7200, "cache_key2")) { // проверяем кеш и задаём настройки
            /**
             * @var $newHas
             * @var $newProductsIds
             */
            extract($cache->getVars()); // достаем переменные из кеша

            if ($_REQUEST['clean_cache'] === 'Y') {
                $cache->abortDataCache();
            }
        } elseif ($cache->startDataCache()) {
            $blockNewProducts = \Citystress\ProjectSettings::getInstance()->getBlockNewProducts();
            $newHas         = count($blockNewProducts);
            $newProductsIds = array_keys($blockNewProducts);
            $preActiveOffer = array_values($blockNewProducts);

            $cache->endDataCache(['newHas' => $newHas, 'newProductsIds' => $newProductsIds, 'preActiveOffer' => $preActiveOffer]); // записываем в кеш
        }

        if ($newHas) {
            $newProductsFilter['ID'] = $newProductsIds;
        }
    ?> <? if ($newHas) { ?>
<div class="homenew_container container">
	<div class="homenew_block block">
		<div class="block_top">
			<div class="block_title">
				Новинки
			</div>
 <a href="/catalog/odezhda/filter/new-is-d355b0a02d11c2a119477e7dd3d299a6/apply/" class="go_section">Перейти в коллекцию </a>
		</div>
		<div class="products_sliderwrap">
			<div class="custom_slarrows">
				<div class="custom_slarrow custom_prev">
				</div>
				<div class="custom_slarrow custom_next">
				</div>
			</div>
			<div class="products_slider">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"catalog_items",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "ID",
		"ELEMENT_SORT_ORDER" => $newProductsIds,
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "newProductsFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array("SOSTAV"),
		"LABEL_PROP_MOBILE" => array(),
		"LABEL_PROP_POSITION" => "top-left",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_LAZY_LOAD" => "Показать ещё",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_FIELD_CODE" => array("",""),
		"OFFERS_LIMIT" => 0,
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGENAV_TMPL" => "",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "12",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRE_ACTIVE_OFFER" => $preActiveOffer,
		"PRICE_CODE" => array("Розничная", 'oldprice'),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER" => true,
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N"
	)
);?>
			</div>
		</div>
	</div>
</div>
 <? } else { ?> <br>
<br>
 <? } ?>


<h1>CityStress - стильная&nbsp;женская одежда</h1>

<?
$homeBanner = \Citystress\ProjectSettings::getInstance()->getHomeBanner();

if ($homeBanner['LINK']) {
	echo '<a href="'.$homeBanner['LINK'].'" class="banner banner_fullw">';
} else {
	echo '<div class="banner banner_fullw">';
}
?>
	<div class="back_img back_img__relative">
		<picture>
			<source type="image/jpg" media="(max-width: 750px)"
					srcset="<?= $homeBanner['MOBILE'] ?> 1x, <?= $homeBanner['MOBILE'] ?> 2x">
			<source type="image/jpg" media="(min-width: 751px) and (max-width: 1000px)"
					srcset="<?= $homeBanner['TABLET'] ?> 1x, <?= $homeBanner['TABLET'] ?> 2x">
			<source type="image/jpg" media="(min-width: 1001px)"
					srcset="<?= $homeBanner['DESKTOP'] ?> 1x, <?= $homeBanner['DESKTOP'] ?> 2x">
			<img data-imgmob="<?= $homeBanner['DESKTOP'] ?>"
				 data-imgtab="<?= $homeBanner['DESKTOP'] ?>"
				 data-img1x="<?= $homeBanner['DESKTOP'] ?>"
				 data-img2x="<?= $homeBanner['DESKTOP'] ?>"
				 src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png" alt="">
		</picture>
	</div>
<?
echo $homeBanner['LINK'] ? '</a>' : '</div>';
?>

<?
    if ($cache->initCache(7200, "cache_key")) { // проверяем кеш и задаём настройки
        /**
         * @var $hitsHas
         * @var $newProductsIds
         */
        extract($cache->getVars()); // достаем переменные из кеша

        if ($_REQUEST['test']) {
            global $APPLICATION;
            $APPLICATION->RestartBuffer();
            echo '<pre>';
            var_dump($newProductsIds2);
            var_dump($preActiveOffer);
            die;
        }

        if ($_REQUEST['clean_cache'] === 'Y') {
            $cache->abortDataCache();
        }
    } elseif ($cache->startDataCache()) {
        $blockNewProducts = \Citystress\ProjectSettings::getInstance()->getBlockHitProducts();
        $hitsHas         = count($blockNewProducts);
        $newProductsIds2  = array_keys($blockNewProducts);
        $preActiveOffer  = array_values($blockNewProducts);

        $cache->endDataCache(['hitsHas' => $hitsHas, 'newProductsIds2' => $newProductsIds2, 'preActiveOffer' =>
            $preActiveOffer]); // записываем в кеш
    }

    if ($hitsHas) {
        global $bestsellerFilter;
        $bestsellerFilter['ID'] = $newProductsIds2;
    }


?> <? if ($hitsHas) { ?>
<div class="homehits_container container">
	<div class="homehits_block block">
		<div class="block_top">
			<div class="block_title">
				Хиты продаж
			</div>
 <a href="/catalog/odezhda/filter/bestseller-is-4c7b7f88644409e52e3e06e263410090/apply/" class="go_section">Перейти в коллекцию </a>
		</div>
		<div class="products_sliderwrap">
			<div class="custom_slarrows">
				<div class="custom_slarrow custom_prev">
				</div>
				<div class="custom_slarrow custom_next">
				</div>
			</div>
			<div class="products_slider">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"catalog_items",
	Array(
		'TEST2' => 'Y',
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "ID",
		"ELEMENT_SORT_ORDER" => $newProductsIds2,
		"ELEMENT_SORT_FIELD2" => "ID",
		"ELEMENT_SORT_ORDER2" => $newProductsIds2,
		"ENLARGE_PRODUCT" => "STRICT",
		"FILTER_NAME" => "bestsellerFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array("SOSTAV"),
		"LABEL_PROP_MOBILE" => array(),
		"LABEL_PROP_POSITION" => "top-left",
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "100",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_LAZY_LOAD" => "Показать ещё",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_FIELD_CODE" => array("",""),
		"OFFERS_LIMIT" => 0,
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGENAV_TMPL" => "more",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "100",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRE_ACTIVE_OFFER" => $preActiveOffer,
		"PRICE_CODE" => array("Розничная", 'oldprice'),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER" => true,
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N"
	)
);?>
			</div>
		</div>
	</div>
</div>
 <? } else { ?> <br>
<br>
    <? } ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>