<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle(" Для вас подарок...");
$APPLICATION->SetPageProperty("title", 'О бренде - интернет-магазин «CityStress»');
$APPLICATION->SetPageProperty("description", 'Информация о нашем бренде женской одежды CityStress.');
?><div class="block taske-block">
	<div class="container taske-container">
	</div>
	<p class="block taske-block">
	</p>
	<div class="container taske-container">
	</div>
	<p style="color: #242424;">
 <br>
 <br>
 <br>
 <img width="179" alt="unnamed.png" src="/upload/medialibrary/8c5/j34g0d1jw9iltnco50633e6q9kxvetca.png" height="39" title="unnamed.png"><br>
	</p>
	<p style="color: #242424;">
 <span style="font-weight: bold;">Создавай образы без ограничений</span>
	</p>
	<p style="color: #242424;">
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">Не надо ограничивать себя вещью, сочетанием или образом.</span>
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">С&nbsp;</span><span style="color: #333333;">CityStress</span><span style="color: #333333;"> </span><span style="color: #333333;">каждая может себе позволить обновлять гардероб и не испытывать дискомфорт и неудобства</span><span style="color: #333333;">.</span>
	</p>
	<p style="color: #242424;">
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">CityStress</span><span style="color: #333333;"> </span><span style="color: #333333;">дает свободу самовыражения через доступность и инклюзивность</span>
	</p>
 <img width="308" alt="09014.jpg" src="https://citystress.ru/upload/medialibrary/567/v2i6hvl66msq02cqmhyce0474x5spi6p.jpg" height="386" title="09014.jpg" style="color: #242424;"><br>
 <br>
	<p style="color: #242424;">
 <span style="font-weight: bold;">Универсальность</span>
	</p>
	<p style="color: #242424;">
		 Мы создаем капсульные коллекции,&nbsp;которые формируют базовый&nbsp;гардероб на все случаи жизни&nbsp;<br>
 <br>
 <span style="font-weight: bold;">Качество</span>
	</p>
	<p style="color: #242424;">
		 Тщательный контроль производства моделей собственной разработки&nbsp;и опыт&nbsp;более 20 лет на рынке обеспечивают&nbsp;надежность и долговечность одежды
	</p>
	<p style="color: #242424;">
	</p>
	<p style="color: #242424;">
 <span style="font-weight: bold;">Доступность</span>
	</p>
	<p style="color: #242424;">
		 Мы делаем стиль доступным каждой,&nbsp;не надо выкраивать и считать, будь&nbsp;свободна в выборе
	</p>
	<p style="color: #242424;">
 <img width="273" alt="горы1853 копия.jpg" src="https://citystress.ru/upload/medialibrary/2f3/1z1mri7zyq7oyek442uu0karxvk1pc9p.jpg" height="410" title="горы1853 копия.jpg"><br>
 <br>
	</p>
	<p style="color: #242424;">
 <span style="font-weight: bold;">Наш стиль - отражение идей</span> <br>
 <br>
		 Одежда CityStress дает комфорт&nbsp;и уверенность в любой ситуации.
	</p>
	<p style="color: #242424;">
		 Ты можешь полностью отдаться моменту&nbsp;и жить его на полную катушку.
	</p>
	<p style="color: #242424;">
		 Идеальный стиль для современной городской девушки.
	</p>
	<p style="color: #242424;">
		 В своих коллекциях мы отражаем потребность&nbsp;в универсальности образов, в которых легко сначала закрыть&nbsp;важные сделки в офисе, потом прогуляться
	</p>
	<p style="color: #242424;">
	</p>
	<p style="color: #242424;">
		 с друзьями и активно провести время вместе&nbsp;
	</p>
	<p style="color: #242424;">
 <br>
	</p>
	<p style="color: #242424;">
 <img width="273" alt="00106.jpg" src="https://citystress.ru/upload/medialibrary/d8b/01fxjhunye312ex692f6ad3oeyc31wgo.jpg" height="410" title="00106.jpg"><br>
 <br>
	</p>
	<p style="color: #242424;">
 <span style="font-weight: bold;">Почему</span>&nbsp;<span style="font-family: var(--ui-font-family-primary, var(--ui-font-family-helvetica)); font-weight: bold;">CityStress</span><span style="font-family: var(--ui-font-family-primary, var(--ui-font-family-helvetica)); font-weight: bold;">?</span>
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">СТРЕСС - драйв, в котором живет девушка сегодня.</span>
	</p>
	<p style="color: #242424;">
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">Это то, что дает энергию и яркие запоминающиеся эмоции: поиск себя, любовь, отношения,</span>&nbsp;<span style="color: #333333;">преодоление трудностей и - как результат - успех!</span>
	</p>
	<p style="color: #242424;">
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">Девушка&nbsp;</span><span style="color: #333333;">CityStress</span><span style="color: #333333;"> </span><span style="color: #333333;">живет в режиме</span>&nbsp;<span style="color: #333333;">многозадачности и без проблем создает модный&nbsp;</span><span style="color: #333333;">образ, легко стилизует его, чтобы выглядеть&nbsp;</span><span style="color: #333333;">уместно в любой ситуации</span><span style="color: #333333;">.</span>
	</p>
	<p style="color: #242424;">
 <span style="color: #333333;">Понятная, адаптированная под городскую среду мода дает чувство уверенности и защищенности,</span>&nbsp;<span style="color: #333333;">повышая стрессоустойчивость жительницы города-миллионника</span>
	</p>
 <br>
 <img width="273" alt="012203 1.jpg" src="https://citystress.ru/upload/medialibrary/e3e/r9qf58i3et7kgohar90fmkzoxei9bykm.jpg" height="410" title="012203 1.jpg" style="color: #242424;"><br>
 <br>
 <b><br>
 </b>
	<p>
 <b> <span style="font-weight: bold;">Сотрудничество с CityStress</span> </b>
	</p>
	<p>
		 Приглашаем компании и индивидуальных предпринимателей к сотрудничеству по открытию магазинов бренда CityStress: <a href="mailto:booker@citystress.ru"><span style="font-weight: normal; line-height: 160%; font-size: 14px; color: #8293ca;">booker@citystress.ru</a>
	</p>
 <br>
 <br>
 <br>
 <br>
 <br>
	<p>
	</p>
 <br>
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>