<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
$APPLICATION->SetPageProperty("keywords",  '');
?>

<?

global $arrFilterFavorites, $USER;

$arrFilterFavorites['ID'] = \Citystress\Favorites::getInstance()->get((int)$USER->GetID());

?>

    <div class="favorites_container container pagetype1">
        <div class="favorites_block block">

            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <i>/</i>
                <span>Избранное</span>
            </div>

            <h1>Избранное</h1>

            <? if (count($arrFilterFavorites['ID'] ?: [])) { ?>

                <div class="filters">

                    <div class="filters_top">

                        <div class="filters_in">
                            <span>Фильтр</span>
                            <div class="filters_in__icon">
                                <svg class="filters_in__close" fill="none" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg"><g stroke="#242424" stroke-linecap="round" stroke-width="1.5"><path d="m3 12.2566 9.5131-9.51316"></path><path d="m12.5137 12.2566-9.51318-9.51316"></path></g></svg>
                                <svg class="filters_in__open" fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><g stroke="#242424" stroke-linecap="round" stroke-width="1.5"><path d="m7.7915 5.66663h6.375"></path><path d="m2.8335 11.3334h7.08333"></path><ellipse cx="4.9585" cy="5.66663" rx="2.125" ry="2.125" transform="matrix(0 1 -1 0 10.62513 .70813)"></ellipse><ellipse cx="12.0415" cy="11.3334" rx="2.125" ry="2.125" transform="matrix(0 1 -1 0 23.3749 -.7081)"></ellipse></g></svg>
                                <svg class="filters_in__open2" fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m14.1665 11.3334h-7.08333" stroke="#242424" stroke-linecap="round" stroke-width="1.5"></path><ellipse cx="2.125" cy="2.125" rx="2.125" ry="2.125" stroke="#242424" stroke-linecap="round" stroke-width="1.5" transform="matrix(.00000004 1 1 -.00000004 2.8335 9.20825)"></ellipse><g fill="#242424"><path clip-rule="evenodd" d="m8.3148 6.75c-.17581-.4705-.28279-.97459-.30866-1.5h-5.00614c-.41421 0-.75.33579-.75.75s.33579.75.75.75z" fill-rule="evenodd"></path><ellipse cx="13" cy="5" rx="3" ry="3" transform="matrix(0 1 -1 0 18 -8)"></ellipse></g></svg>
                            </div>
                        </div>

                        <div class="filters_out__back modal_close"></div>

                        <div class="filters_out" style="height: auto;">
                            <div class="filters_mobhead">
                                <span class="filters_mobhead__title">Фильтр</span>
                                <span class="filters_mobhead__reset">Сбросить</span>
                                <svg class="filters_mobhead__close modal_close" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"></rect><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"></rect></g></svg>
                            </div>

                            <div class="filters_customscroll_main">

                                <div class="filters_item filters_item__sort">
                                    <div class="filters_item__label">Сортировать</div>
                                    <div class="filters_item__chosen"></div>
                                    <div class="filters_item__out">
                                        <ul>
                                            <li><span>Сначала новинки</span></li>
                                            <li><span>Сначала акции</span></li>
                                            <li><span>По низкой цене</span></li>
                                            <li><span>По высокой цене</span></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="filters_item filters_item__resetfav">
                                    <div class="filters_item__label">Очистить избранное</div>
                                    <div class="filters_item__chosen"></div>
                                </div>

                            </div>

                            <button class="button button_black button_apply">Применить</button>
                            <button class="button button_black show_results">Показать <span>152</span> товара</button>
                        </div>

                        <div class="filters_options">

                            <div class="sortby">
                                <span>Сортировка</span>
                                <svg fill="none" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="m3 6 4.5 4 4.5-4" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"></path></svg>
                                <ul>
                                    <li><a rel="nofollow noopener" href="?sort=PRICE&order=asc"
                                           <? if ($_GET['sort'] === 'PRICE' && $_GET['order'] === 'asc') { ?>class="active"<? } ?>>По низкой
                                            цене</a></li>
                                    <li><a rel="nofollow noopener" href="?sort=PRICE&order=desc"
                                           <? if ($_GET['sort'] === 'PRICE' && $_GET['order'] === 'desc') { ?>class="active"<? } ?>>По высокой
                                            цене</a></li>
                                </ul>
                            </div>

                            <div class="cards_cols">
                                <strong>Вид:</strong>
                                <span class="by3 active" data-type="by3">3</span>
                                <i>/</i>
                                <span class="by4" data-type="by4">4</span>
                                <span class="by2" data-type="by2">2</span>
                                <u>/</u>
                                <span class="by1" data-type="by1">1</span>
                            </div>

                        </div>

                    </div>

                    <div class="favorites_reset">Очистить избранное</div>

                </div>

<!--                <div class="filters">-->
<!---->
<!--                    <div class="filters_top">-->
<!---->
<!--                        --><?// $APPLICATION->IncludeComponent("bitrix:catalog.smart.filter",
//                            "main_favorites",
//                            array(
//                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
//                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
//                                "SECTION_ID" => $arCurSection['ID'],
//                                "FILTER_NAME" => $arParams["FILTER_NAME"],
//                                "PRICE_CODE" => $arParams["~PRICE_CODE"],
//                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
//                                "CACHE_TIME" => $arParams["CACHE_TIME"],
//                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
//                                "SAVE_IN_SESSION" => "N",
//                                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
//                                "XML_EXPORT" => "N",
//                                "SECTION_TITLE" => "NAME",
//                                "SECTION_DESCRIPTION" => "DESCRIPTION",
//                                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
//                                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
//                                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
//                                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
//                                "SEF_MODE" => $arParams["SEF_MODE"],
//                                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
//                                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
//                                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
//                                "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
//                            ),
//                            $component,
//                            array('HIDE_ICONS' => 'Y')
//                        );
//                        ?>
<!---->
<!--                    </div>-->
<!---->
<!--                    <div class="favorites_reset">Очистить избранное</div>-->
<!---->
<!--                </div>-->
<!---->
                <div class="catalog_items by4">
                    <?
                    if (isset($_GET["sort"]) && isset($_GET["order"]) && ($_GET["sort"] == "PRICE")) {

                        if ($_GET['sort'] === 'PRICE') {
                            $_GET['sort'] = 'catalog_PRICE_3';
                        }
                        $arParams["ELEMENT_SORT_ORDER"] = $_GET["order"];
                    }
                    ?>

                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "catalog_items",
                        array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => $_GET["sort"] ?? 'sort',
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => $_GET["order"] ?? 'asc',
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrFilterFavorites",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "6",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array("SOSTAV"),
                            "LABEL_PROP_MOBILE" => array(),
                            "LABEL_PROP_POSITION" => "top-left",
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "12",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("Розничная"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_ID" => "",
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "N",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "Y",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N",
                            'PAGENAV_TMPL' => 'more',
                        )
                    ); ?>
                </div>



            <? } else { ?>
                    <div class="favorites_hint">Чтобы добавить товар в «Избранное»‎, отметьте его <svg fill="none" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="m7.5 2.9439c.00271.00306.00265.00311.00265.00311l.00447-.00404c.00423-.00381.01125-.01006.02095-.0185.01942-.01689.04954-.04251.08956-.07486.08012-.06478.19935-.15606.35128-.25801.30538-.20492.73485-.44754 1.23841-.60809.98808-.315 2.26438-.32184 3.50968.9498l.0038.00394.0031.00293.0022.00217c.0023.00238.0066.00668.0126.01287.0119.01239.0307.03229.0548.05937.0483.05424.1178.13679.1971.24493.1594.21715.3549.53229.501.92399.2871.76956.3934 1.853-.3922 3.12438l-.0322.0521-.0104.03996c-.0009.00199-.0021.00441-.0034.00727-.0111.02336-.0371.07346-.09.15577-.1062.16529-.3146.45106-.7122.89952-.7472.84294-2.1465 2.24239-4.7512 4.47429-2.60468-2.2319-4.00397-3.63135-4.75125-4.47429-.39757-.44846-.60595-.73423-.71213-.89952-.05288-.08231-.07891-.13241-.09-.15577-.00136-.00286-.00248-.00528-.00338-.00727l-.01039-.03996-.0322-.0521c-.78569-1.27138-.67934-2.35482-.39225-3.12438.14613-.3917.3416-.70684.50097-.92399.07936-.10814.14883-.19069.19714-.24493.02413-.02708.04288-.04698.05483-.05937.00597-.00619.01023-.01049.0126-.01287l.00201-.00199.00321-.00311.00387-.00394c1.24529-1.27164 2.52162-1.2648 3.50965-.9498.50356.16055.93303.40317 1.23841.60809.15193.10195.27116.19323.35128.25801.04002.03235.07014.05797.08956.07486.0097.00844.01672.01469.02095.0185l.00447.00404s-.00005-.00005.00265-.00311zm0-1.00565c-.01015-.00691-.02044-.01385-.03085-.02084-.34677-.2327-.84396-.51521-1.43849-.70476-1.20736-.384928-2.8116-.378356-4.30821 1.14777l-.00613.00609c-.00569.00569-.01335.01345-.02277.02323-.01885.01954-.04482.0472-.07639.08263-.06307.07079-.14887.17307-.24507.30414-.19176.26129-.428559.64212-.607059 1.12061-.359357.96328-.472675 2.30361.440819 3.80528.00678.01664.01495.03525.02481.05602.0286.06021.07245.14066.14027.24623.13521.21047.37153.52978.78718.99863.82445.92999 2.37268 2.46862 5.2836 4.93842l.04782.0583c.00349-.003.00698-.0059.01047-.0089.00349.003.00698.0059.01047.0089l.04782-.0583c2.91091-2.4698 4.45911-4.00843 5.28361-4.93842.4156-.46885.652-.78816.7872-.99863.0678-.10557.1116-.18602.1402-.24623.0099-.02077.0181-.03938.0249-.05602.9134-1.50167.8001-2.84199.4408-3.80528-.1785-.47849-.4153-.85932-.6071-1.12061-.0962-.13107-.182-.23335-.2451-.30414-.0315-.03543-.0575-.06309-.0763-.08263-.0095-.00978-.0171-.01754-.0228-.02323l-.0062-.00609c-1.4966-1.526126-3.1008-1.532698-4.30816-1.14777-.59453.18955-1.09172.47206-1.43849.70476-.01041.00699-.0207.01393-.03085.02084z" style="fill-rule:evenodd;clip-rule:evenodd;fill:#242424;stroke:#242424;stroke-miterlimit:10;stroke-linejoin:round"/></svg><br> на страницах каталога или в самой карточке.</div>

                    <div class="button_wrap">
                        <button class="button button_black favorites_gocatalog"><a href="/catalog/odezhda/"><span>Перейти в каталог</span></a></button>
                    </div>
            <? } ?>

<!--            <div class="button_wrap">-->
<!--                <button class="button button_black favorites_more"><a href="/"><span>Показать еще</span></a></button>-->
<!--            </div>-->


        </div>
    </div>

    <? if (!count($arrFilterFavorites['ID'] ?: [])) { ?>
        <div class="recently_container container favorites_recently">
            <div class="recently_block block">

                <div class="block_top">
                    <div class="block_title">Вы <span>недавно</span> смотрели</div>
                    <a href="/" class="go_section">Смотреть все
                        <svg fill="none" height="11" viewBox="0 0 19 11" width="19" xmlns="http://www.w3.org/2000/svg">
                            <path d="m14.3262 1.25 4.132 4.25m0 0-4.132 4.25m4.132-4.25h-17.9582" stroke="#242424"
                                  stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </a>
                </div>

                <?
                $arViewed = [];
                $basketUserId = (int) CSaleBasket::GetBasketUserID(false);
                if ($basketUserId > 0):
                    $viewedIterator = \Bitrix\Catalog\CatalogViewedProductTable::getList([
                        'select' => ['PRODUCT_ID', 'ELEMENT_ID'],
                        'filter' => ['=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID],
                        'order' => ['DATE_VISIT' => 'DESC'],
                        'limit' => 4
                    ]);
                    while ($arFields = $viewedIterator->fetch()) {
                        $arViewed[] = $arFields['ELEMENT_ID'];
                    }
                    // if end after products_sliderwrap

                    global $viewedFilter;

                    $viewedFilter = [
                        'ID' => $viewedFilter,
                    ];
                    ?>

                    <div class="products_sliderwrap">
                        <div class="custom_slarrows">
                            <div class="custom_slarrow custom_prev">
                                <svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2"/>
                                </svg>
                            </div>
                            <div class="custom_slarrow custom_next">
                                <svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2"/>
                                </svg>
                            </div>
                        </div>

                        <div class="products_slider">

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:catalog.section",
                                "catalog_items",
                                array(
                                    "ACTION_VARIABLE" => "action",
                                    "ADD_PICT_PROP" => "-",
                                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "ADD_TO_BASKET_ACTION" => "ADD",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "BACKGROUND_IMAGE" => "-",
                                    "BASKET_URL" => "/personal/basket.php",
                                    "BROWSER_TITLE" => "-",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "COMPATIBLE_MODE" => "Y",
                                    "CONVERT_CURRENCY" => "N",
                                    "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                                    "DETAIL_URL" => "",
                                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "DISPLAY_COMPARE" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "ELEMENT_SORT_FIELD" => $_GET["sort"] ?? 'sort',
                                    "ELEMENT_SORT_FIELD2" => "id",
                                    "ELEMENT_SORT_ORDER" => $_GET["order"] ?? 'asc',
                                    "ELEMENT_SORT_ORDER2" => "desc",
                                    "ENLARGE_PRODUCT" => "STRICT",
                                    "FILTER_NAME" => "viewedFilter",
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                                    "IBLOCK_ID" => "6",
                                    "IBLOCK_TYPE" => "catalog",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "LABEL_PROP" => array("SOSTAV"),
                                    "LABEL_PROP_MOBILE" => array(),
                                    "LABEL_PROP_POSITION" => "top-left",
                                    "LAZY_LOAD" => "N",
                                    "LINE_ELEMENT_COUNT" => "3",
                                    "LOAD_ON_SCROLL" => "N",
                                    "MESSAGE_404" => "",
                                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                    "MESS_BTN_BUY" => "Купить",
                                    "MESS_BTN_DETAIL" => "Подробнее",
                                    "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                    "META_DESCRIPTION" => "-",
                                    "META_KEYWORDS" => "-",
                                    "OFFERS_FIELD_CODE" => array("", ""),
                                    "OFFERS_LIMIT" => "5",
                                    "OFFERS_SORT_FIELD" => "sort",
                                    "OFFERS_SORT_FIELD2" => "id",
                                    "OFFERS_SORT_ORDER" => "asc",
                                    "OFFERS_SORT_ORDER2" => "desc",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "PAGER_TITLE" => "Товары",
                                    "PAGE_ELEMENT_COUNT" => "12",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                    "PRICE_CODE" => array("Розничная"),
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                                    "PRODUCT_DISPLAY_MODE" => "N",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                                    "PRODUCT_SUBSCRIPTION" => "Y",
                                    "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                                    "RCM_TYPE" => "personal",
                                    "SECTION_CODE" => "",
                                    "SECTION_ID" => "",
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "SECTION_URL" => "",
                                    "SECTION_USER_FIELDS" => array("", ""),
                                    "SEF_MODE" => "N",
                                    "SET_BROWSER_TITLE" => "Y",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "Y",
                                    "SET_META_KEYWORDS" => "Y",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "Y",
                                    "SHOW_404" => "N",
                                    "SHOW_ALL_WO_SECTION" => "N",
                                    "SHOW_CLOSE_POPUP" => "N",
                                    "SHOW_DISCOUNT_PERCENT" => "N",
                                    "SHOW_FROM_SECTION" => "N",
                                    "SHOW_MAX_QUANTITY" => "N",
                                    "SHOW_OLD_PRICE" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "SHOW_SLIDER" => "Y",
                                    "SLIDER_INTERVAL" => "3000",
                                    "SLIDER_PROGRESS" => "N",
                                    "TEMPLATE_THEME" => "blue",
                                    "USE_ENHANCED_ECOMMERCE" => "N",
                                    "USE_MAIN_ELEMENT_SECTION" => "N",
                                    "USE_PRICE_COUNT" => "N",
                                    "USE_PRODUCT_QUANTITY" => "N",
                                    'PAGENAV_TMPL' => 'more',
                                    'SLIDER' => true,
                                )
                            ); ?>

                        </div>
                    </div>

                <? endif; ?>

            </div>
        </div>
    <? } ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>