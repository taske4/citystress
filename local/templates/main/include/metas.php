<?php 
$date = new DateTime();
$timestamp = $date->getTimestamp();
// $timestamp = 'ver=1.0';?>

<meta id="viewport" name="viewport" content="">

<script>
var m=document.getElementById("viewport"),w=screen.width,h=screen.height,c="content",u="user-scalable=0",v="width=";w<1300?window.innerWidth>window.innerHeight?m.setAttribute(c,v+"1024,"+u):w<500?m.setAttribute(c,v+"360,"+u):m.setAttribute(c,v+"768,"+u):m.setAttribute(c,v+"device-width,"+u),window.addEventListener("resize",function(){var t=screen.width;screen.height;t<1300?window.innerWidth>window.innerHeight?m.setAttribute(c,v+"1024,"+u):t<500?m.setAttribute(c,v+"360,"+u):m.setAttribute(c,v+"768,"+u):m.setAttribute(c,v+"device-width,"+u)});
</script>

<meta name="format-detection" content="telephone=no">

<link rel="icon" href="<?=SITE_MAIN_TEMPLATE_PATH?>/img/favicon.png" type="image/png">
<link rel="shortcut icon" href="<?=SITE_MAIN_TEMPLATE_PATH?>/img/favicon.png" type="image/png">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="UTF-8" />

<? require_once $_SERVER['DOCUMENT_ROOT'] . SITE_MAIN_TEMPLATE_PATH . '/include/micromarkup.php'; ?>

<style>
@font-face {
    font-family: 'Montserrat';
    src: url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Regular.eot');
    src: local('Montserrat Regular'), local('Montserrat-Regular'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Regular.eot?#iefix') format('embedded-opentype'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Regular.woff2') format('woff2'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Regular.woff') format('woff'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
} 

@font-face {
    font-family: 'Montserrat';
    src: url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Medium.eot');
    src: local('Montserrat Medium'), local('Montserrat-Medium'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Medium.eot?#iefix') format('embedded-opentype'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Medium.woff2') format('woff2'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Medium.woff') format('woff'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Medium.ttf') format('truetype');
    font-weight: 500;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: 'Montserrat';
    src: url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-SemiBold.eot');
    src: local('Montserrat SemiBold'), local('Montserrat-SemiBold'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-SemiBold.eot?#iefix') format('embedded-opentype'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-SemiBold.woff2') format('woff2'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-SemiBold.woff') format('woff'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-SemiBold.ttf') format('truetype');
    font-weight: 600;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: 'Montserrat';
    src: url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Bold.eot');
    src: local('Montserrat Bold'), local('Montserrat-Bold'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Bold.eot?#iefix') format('embedded-opentype'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Bold.woff2') format('woff2'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-Bold.woff') format('woff'),
        url('/fonts/Montserrat-Bold.ttf') format('truetype');
    font-weight: 700;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: 'Montserrat';
    src: url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-ExtraBold.eot');
    src: local('Montserrat ExtraBold'), local('Montserrat-ExtraBold'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-ExtraBold.eot?#iefix') format('embedded-opentype'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-ExtraBold.woff2') format('woff2'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-ExtraBold.woff') format('woff'),
        url('<?=SITE_MAIN_TEMPLATE_PATH?>/assets/fonts/Montserrat-ExtraBold.ttf') format('truetype');
    font-weight: 800;
    font-style: normal;
    font-display: swap;
}
</style>