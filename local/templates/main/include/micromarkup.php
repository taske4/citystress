<?php

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$Uri     = new \Bitrix\Main\Web\Uri($request->getRequestUri());

$uri = rawurldecode($Uri->getPath());

\Bitrix\Main\Loader::IncludeModule('iblock');


$micromarkupRes = \Bitrix\Iblock\Iblock::wakeUp(MICROMARKUP_IBLOCK_ID)->getEntityDataClass()::query()
    ->setSelect([
        'NAME', 'IMAGE' => 'PREVIEW_PICTURE', 'DESC' => 'PREVIEW_TEXT',
        'OG_TYPE_' => 'OG_TYPE', 'LINK_' => 'LINK', 'FOOTER_TEXT_' => 'FOOTER_TEXT'
    ])
    ->setFilter(['LINK_VALUE' => $uri])
    ->fetch();

$micromarkupData = [];

if ($micromarkupRes) {
    global $footerText;
    $footerText = unserialize($micromarkupRes['FOOTER_TEXT_VALUE'])['TEXT'];

    if ($micromarkupRes['IMAGE']) {
        $image = \CFile::GetPath($micromarkupRes['IMAGE']);
    }

    $micromarkupData['og']['url'] = "https://citystress.ru".$Uri->getUri().$Uri->getQuery();
    $micromarkupData['og']['title']    = $micromarkupRes['NAME'];
    $micromarkupData['og']['description']    = $micromarkupRes['DESC'];
    $micromarkupData['og']['type'] = $micromarkupRes['OG_TYPE_VALUE'];

    $micromarkupData['twitter']['url'] = "https://citystress.ru".$Uri->getUri().$Uri->getQuery();
    $micromarkupData['twitter']['title']    = $micromarkupRes['NAME'];
    $micromarkupData['twitter']['description']    = $micromarkupRes['DESC'];
    $micromarkupData['twitter']['type'] = $micromarkupRes['OG_TYPE_VALUE'];

    if ($image) {
        $micromarkupData['og']['image'] =
        $micromarkupData['twitter']['image'] = \CFile::GetPath($micromarkupRes['IMAGE']);
    }
}

echo '<meta property="og:logo" content="https://citystress.ru'.SITE_MAIN_TEMPLATE_PATH.'/img/logo.png" />';

$customMicroData = false;

foreach ($micromarkupData['og'] as $key => $value) {
    $customMicroData = true;
    echo "<meta property=\"og:$key\" content=\"$value\"/>";
}
foreach ($micromarkupData['twitter'] as $key => $value) {
    $customMicroData = true;
    echo "<meta property=\"twitter:$key\" content=\"$value\"/>";
}

if (!$customMicroData) {
    $APPLICATION->ShowViewContent('microdata');
}