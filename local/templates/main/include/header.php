<?
/**
 * @global $APPLICATION ;
 */
$APPLICATION->ShowPanel();

global $submenuCatalogNewFilter;

$res = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_IBLOCK_ID)->getEntityDataClass()::query()
    ->setSelect(['ID', 'IBLOCK_SECTION', 'NEW_' => 'NEW'])
    ->setFilter([
        'ACTIVE' => 'Y',
        '!NEW_VALUE' => null,
    ])
    ->fetchAll();

$newSectionIds = array_column($res, 'IBLOCK_ELEMENTS_ELEMENT_CATALOG_IBLOCK_SECTION_ID');
$newSectionIds = array_unique($newSectionIds);

$submenuCatalogNewFilter['ID'] = $newSectionIds;

$res = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_IBLOCK_ID)->getEntityDataClass()::query()
    ->setSelect(['ID', 'IBLOCK_SECTION', 'SALE_' => 'SALE'])
    ->setFilter([
        'ACTIVE' => 'Y',
        '!SALE_VALUE' => null,
    ])
    ->fetchAll();

$saleSectionIds = array_column($res, 'IBLOCK_ELEMENTS_ELEMENT_CATALOG_IBLOCK_SECTION_ID');
$saleSectionIds = array_unique($saleSectionIds);

$submenuCatalogSaleFilter['ID'] = $saleSectionIds;
?>

<div class="breadcrumbs-detail hiddenpro">
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        ".default",
        Array(
            "PATH" => "",
            "SITE_ID" => "s1",
            "START_FROM" => "0"
        )
    );?>
</div>

<div class="main_layout">
    <header>
        <div class="header_container container"<?= !\Citystress\ProjectSettings::getInstance()->isShowHeaderAction() ? ' style="top:0;"' : ''; ?>>

            <?= \Citystress\ProjectSettings::getInstance()->printHeaderAction()?>

            <div class="header_block">
                <div class="header_submenu_close"></div>

                <div class="mobmenu_in">
                    <div class="mobmenu_in__line mobmenu_in__line1"></div>
                    <div class="mobmenu_in__line mobmenu_in__line2"></div>
                    <div class="mobmenu_in__line mobmenu_in__line3"></div>
                </div>

                <?
                if ($APPLICATION->GetCurPage(false) === '/') {
                    echo '<div class="header_logo">';
                } else {
                    echo '<a href="/" class="header_logo">';
                }
                ?>
                    <img src="<?= SITE_MAIN_TEMPLATE_PATH?>/img/logo.png" alt="" class="header_logo__light">
                    <img src="<?= SITE_MAIN_TEMPLATE_PATH?>/img/logo_dark.png" alt="" class="header_logo__dark">
                <?
                if ($APPLICATION->GetCurPage(false) === '/') {
                    echo '</div>';
                } else {
                    echo '</a>';
                }
                ?>


                <ul class="header_menu">
                    <li data-submenu="catalog">
                        <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/odezhda/') { ?>
                            Каталог
                        <? } else { ?>
                            <a href="/catalog/odezhda/">Каталог</a>
                        <? } ?>
                    </li>
                    <? if ($newSectionIds) { ?>
                        <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/odezhda/') { ?>
                            <li data-submenu="new">Новинки</li>
                        <? } else { ?>
                            <li data-submenu="new"><a href="/catalog/odezhda/filter/new-is-d355b0a02d11c2a119477e7dd3d299a6/apply/">Новинки</a></li>
                        <? } ?>
                    <? } ?>
                    <? if ($saleSectionIds) { ?>
                        <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/odezhda/') { ?>
                            <li data-submenu="sale">Sale</li>
                        <? } else { ?>
                            <li data-submenu="sale"><a href="/catalog/odezhda/filter/sale-is-9750bfc234420f1e1c7feabcbf1e9bfe/apply/">Sale</a></li>
                        <? } ?>
                    <? } ?>
                    <li>
                        <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/shops/') { ?>
                            Магазины
                        <? } else { ?>
                            <a href="/shops/">Магазины</a>
                        <? } ?>
                    </li>
                    <li data-submenu="clients">Покупателям</li>
                </ul>


                <div class="header_right">

                    <div class="search_closeicon">
                        <svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg">
                            <g fill="#242424" opacity=".25">
                                <rect height="1.66376" rx=".831881"
                                      transform="matrix(.7071 .707114 -.7071 .707114 1.17676 .000061)" width="26.6202"/>
                                <rect height="1.66376" rx=".831881"
                                      transform="matrix(.707099 -.707114 .7071 .707114 0 18.8235)" width="26.6202"/>
                            </g>
                        </svg>
                    </div>

                    <div class="header_search">
                        <form class="search_form">
                            <svg class="search_form__loupe1" fill="none" height="16" viewBox="0 0 16 16" width="16"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="m11.2845 11.36 2.3154 2.24m-.7467-5.97334c0 2.88664-2.34 5.22664-5.22663 5.22664-2.88661 0-5.22667-2.34-5.22667-5.22664 0-2.88661 2.34006-5.22667 5.22667-5.22667 2.88663 0 5.22663 2.34006 5.22663 5.22667z"
                                    stroke="#242424" stroke-linecap="round" stroke-width="2"/>
                            </svg>
                            <svg class="search_form__loupe2" fill="none" height="24" viewBox="0 0 24 24" width="24"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="m16.9268 17.04 3.4732 3.36m-1.12-8.96c0 4.3299-3.5101 7.84-7.84 7.84-4.32994 0-7.84002-3.5101-7.84002-7.84 0-4.32994 3.51008-7.84002 7.84002-7.84002 4.3299 0 7.84 3.51008 7.84 7.84002z"
                                    stroke="#242424" stroke-linecap="round"/>
                            </svg>
                            <input class="search_form__input" placeholder="Поиск...">
                        </form>
                    </div>

                    <div class="search_block">
                        <div class="search_close"></div>
                        <div class="customscroll">
                            <div class="search_cols">
                                <div class="search_col search_col1">
                                    <div class="search_col__label">Вы недавно искали</div>
                                    <ul>
                                        <?
                                            $arRecently = \Citystress\SearchRecently::getInstance()->get();
                                            $arRecently = array_unique($arRecently);
                                        ?>
                                        <? foreach($arRecently as $text) { ?>
                                            <li><a href="/search/?q=<?=$text?>&how=r"><?=$text?></a></li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="search_products__title">Вам могут понравиться</div>
                            <div class="search_products">
                                <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.search", 
	"header", 
	array(
		"ACTION_VARIABLE" => "action",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/cart/",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "catalog",
		"LINE_ELEMENT_COUNT" => "3",
		"NO_WORD_LOGIC" => "N",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "SIZE",
			1 => "COLOR",
			2 => "CML2_LINK",
			3 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "6",
        "PRICE_CODE" => array('Розничная', 'oldprice'),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => "",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"RESTART" => "Y",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SHOW_PRICE_COUNT" => "1",
		"USE_LANGUAGE_GUESS" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_SEARCH_RESULT_ORDER" => "N",
		"USE_TITLE_RANK" => "N",
		"COMPONENT_TEMPLATE" => "header"
	),
	false
);?>
                            </div>
                        </div>
                    </div>

                    <ul class="header_links">
                        <li class="header_links__chat">
                            <a href="" rel=”nofollow”>
                                <svg fill="none" height="24" viewBox="0 0 24 24" width="24"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="m4.32698 6.63803.89101.45399zm.44122 13.59377-.70711-.7071zm13.5938-3.5588.454.891zm1.311-1.311.891.454zm0-8.72397.891-.45399zm-1.311-1.31105.454-.89101zm-12.72397 0 .45399.89101zm2.06908 11.96592-.70711-.7071zm-2.70711-7.4929c0-.85658.00078-1.43887.03755-1.88896.03582-.43842.10075-.66262.18044-.81902l-1.78202-.90798c-.24729.48533-.34585 1.00204-.39178 1.56413-.04497.55043-.04419 1.22825-.04419 2.05183zm0 2.2v-2.2h-2v2.2zm-2 0v5h2v-5zm0 5v2.9136h2v-2.9136zm0 2.9136c0 1.2918 1.56185 1.9388 2.4753 1.0253l-1.41421-1.4142c.34648-.3465.93891-.1011.93891.3889zm2.4753 1.0253 2.93891-2.9389-1.41421-1.4142-2.93891 2.9389zm9.7247-4.9389h-6.78579v2h6.78579zm2.708-.218c-.1564.0797-.3806.1446-.819.1804-.4501.0368-1.0324.0376-1.889.0376v2c.8236 0 1.5014.0008 2.0518-.0442.5621-.0459 1.0788-.1445 1.5642-.3918zm.874-.874c-.1917.3763-.4977.6823-.874.874l.908 1.782c.7526-.3835 1.3645-.9954 1.748-1.748zm.218-2.708c0 .8566-.0008 1.4389-.0376 1.889-.0358.4384-.1007.6626-.1804.819l1.782.908c.2473-.4854.3459-1.0021.3918-1.5642.045-.5504.0442-1.2282.0442-2.0518zm0-2.4v2.4h2v-2.4zm-.218-2.70798c.0797.1564.1446.3806.1804.81902.0368.45009.0376 1.03238.0376 1.88896h2c0-.82358.0008-1.5014-.0442-2.05183-.0459-.56209-.1445-1.0788-.3918-1.56413zm-.874-.87403c.3763.19174.6823.4977.874.87403l1.782-.90798c-.3835-.75265-.9954-1.36457-1.748-1.74807zm-2.708-.21799c.8566 0 1.4389.00078 1.889.03755.4384.03582.6626.10075.819.18044l.908-1.78202c-.4854-.24729-1.0021-.34585-1.5642-.39178-.5504-.04497-1.2282-.04419-2.0518-.04419zm-6.4 0h6.4v-2h-6.4zm-2.70798.21799c.1564-.07969.3806-.14462.81902-.18044.45009-.03677 1.03238-.03755 1.88896-.03755v-2c-.82358 0-1.5014-.00078-2.05183.04419-.56209.04593-1.0788.14449-1.56413.39178zm-.87403.87403c.19174-.37633.4977-.68229.87403-.87403l-.90798-1.78202c-.75265.3835-1.36457.99542-1.74807 1.74807zm3.19622 10.90798v-2c-.53043 0-1.03914.2107-1.41421.5858z"
                                        fill="#fff"/>
                                    <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                        <path d="m8 9h8"/>
                                        <path d="m8 13h5"/>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li class="header_links__profile">
                            <a href="" rel=”nofollow” class="auth_form">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M3.23779 19.5C4.5632 17.2892 7.46807 15.7762 12.0001 15.7762C16.5321 15.7762 19.4369 17.2892 20.7623 19.5M15.6001 8.1C15.6001 10.0882 13.9883 11.7 12.0001 11.7C10.0118 11.7 8.40007 10.0882 8.40007 8.1C8.40007 6.11177 10.0118 4.5 12.0001 4.5C13.9883 4.5 15.6001 6.11177 15.6001 8.1Z"
                                        stroke="white" stroke-width="2" stroke-linecap="round"/>
                                </svg>
                            </a>
                        </li>

                        <?
                            global $USER;

                            $favActive = count(\Citystress\Favorites::getInstance()->get($USER->GetID()) ?: []);
                        ?>
                        <li class="header_links__fav<? if ($favActive) { ?> active<? } ?>">
                            <a href="/favorites/" rel=”nofollow”>
                                <img class="header_links__icon1" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/fav1.svg" alt="">
                                <img class="header_links__icon2" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/fav2.svg" alt="">
                                <img class="header_links__icon3" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/fav3.svg" alt="">
                                <img class="header_links__icon4" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/fav4.svg" alt="">
                                <img class="header_links__icon5" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/fav5.svg" alt="">
                                <img class="header_links__icon6" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/fav6.svg" alt="">
                            </a>
                        </li>
                        <?
                        \Bitrix\Main\Loader::includeModule('sale');

                        $baskeEmpty = true;

                        if (\Bitrix\Sale\Internals\BasketTable::query()
                            ->addSelect('ID')
                            ->addFilter('FUSER_ID', \Bitrix\Sale\Fuser::getId())
                            ->setLimit(1)
                            ->fetch()) {
                            $baskeEmpty = false;
                        }?>
                        <li class="header_links__cart<?=$baskeEmpty ?: ' active'; ?>">
                            <a href="/cart/" rel=”nofollow”>
                                <img class="header_links__icon1" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/cart1.svg" alt="">
                                <img class="header_links__icon2" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/cart2.svg" alt="">
                                <img class="header_links__icon3" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/cart3.svg" alt="">
                                <img class="header_links__icon4" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/cart4.svg" alt="">
                                <img class="header_links__icon5" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/cart5.svg" alt="">
                                <img class="header_links__icon6" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/cart6.svg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>

            </div>


            <div class="submenu submenu_clients">
                <div class="submenu_close"></div>
                <div class="submenu_content">
                    <div class="submenu_nav">
                        <ul>
<!--                            <li data-cat="3_1" class="active"><span>Помощь</span></li>-->
                            <li>
                                <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/news-sales/') { ?>
                                    Акции и новости
                                <? } else { ?>
                                    <a href="/news-sales/">Акции и новости</a>
                                <? } ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <? if ($newSectionIds) { ?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "submenu_new",
                Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "N",
                    "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                    "FILTER_NAME" => "submenuCatalogNewFilter",
                    "IBLOCK_ID" => "6",
                    "IBLOCK_TYPE" => "catalog",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array("", "NAME", ""),
                    "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                    "SECTION_USER_FIELDS" => array("UF_MOBILE_PICTURE", "UF_TABLET_PICTURE", "UF_DESKTOP_PICTURE", ""),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "10",
                    "VIEW_MODE" => "TREE"
                )
            );?>

            <? } ?>

            <? if ($saleSectionIds) { ?>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list",
                    "submenu_sale",
                    Array(
                        "ADD_SECTIONS_CHAIN" => "N",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "COUNT_ELEMENTS" => "N",
                        "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                        "FILTER_NAME" => "submenuCatalogSaleFilter",
                        "IBLOCK_ID" => "6",
                        "IBLOCK_TYPE" => "catalog",
                        "SECTION_CODE" => "",
                        "SECTION_FIELDS" => array("", "NAME", ""),
                        "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                        "SECTION_USER_FIELDS" => array("UF_MOBILE_PICTURE", "UF_TABLET_PICTURE", "UF_DESKTOP_PICTURE", ""),
                        "SHOW_PARENT_NAME" => "Y",
                        "TOP_DEPTH" => "10",
                        "VIEW_MODE" => "TREE"
                    )
                );?>

            <? } ?>

            <?
            global $submenuCatalogFilter;
            $submenuCatalogFilter['!ID'] = 5;
            ?>

            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "submenu_catalog",
                Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "N",
                    "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                    "FILTER_NAME" => "submenuCatalogFilter",
                    "IBLOCK_ID" => "6",
                    "IBLOCK_TYPE" => "catalog",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array("", "NAME", ""),
                    "SECTION_ID" => $_REQUEST["SECTION_ID"],
                    "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                    "SECTION_USER_FIELDS" => array("UF_MOBILE_PICTURE", "UF_TABLET_PICTURE", "UF_DESKTOP_PICTURE", ""),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "TREE"
                )
            );?>

        </div>
    </header>


    <!--noindex-->
    <div class="mobmenu">
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "mobmenu",
            Array(
                "ADD_SECTIONS_CHAIN" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "N",
                "COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
                "FILTER_NAME" => "sectionsFilter",
                "IBLOCK_ID" => "6",
                "IBLOCK_TYPE" => "catalog",
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => array("", "NAME", ""),
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                "SECTION_USER_FIELDS" => array("UF_MOBILE_PICTURE", "UF_TABLET_PICTURE", "UF_DESKTOP_PICTURE", ""),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "2",
                "VIEW_MODE" => "TREE"
            )
        );?>

        <div class="mob_contacts">
            <ul class="mob_contacts__top">
                <li><span><svg fill="none" height="24" viewBox="0 0 24 24" width="24"
                               xmlns="http://www.w3.org/2000/svg"><path
                                d="m4.32698 6.63803.4455.22699zm.44122 13.59377-.35356-.3536v.0001zm13.5938-3.5588.227.4455zm1.311-1.311.4455.227zm0-8.72397.4455-.227zm-1.311-1.31105.227-.4455zm-12.72397 0 .22699.4455zm2.06908 11.96592-.35356-.3536zm-3.20711-7.4929c0-.84833.00039-1.45451.03921-1.92968.03835-.46933.11168-.76666.23327-1.0053l-.891-.45399c-.20539.4031-.29555.84668-.33895 1.37786-.04292.52534-.04253 1.17928-.04253 2.01111zm0 2.2v-2.2h-1v2.2zm-1 0v5h1v-5zm0 5v2.9136h1v-2.9136zm0 2.9136c0 .8464 1.02328 1.2702 1.62175.6718l-.70711-.7071c.0029-.0029.01348-.0103.02859-.0129.01237-.0021.02098 0 .0259.002.00493.0021.01251.0066.01975.0169.00883.0125.01112.0252.01112.0293zm1.62175.6718 2.93891-2.939-.70711-.7071-2.93891 2.9389zm10.07825-4.0854h-6.78579v1h6.78579zm2.935-.2725c-.2387.1216-.536.1949-1.0053.2333-.4752.0388-1.0814.0392-1.9297.0392v1c.8318 0 1.4858.0004 2.0111-.0425.5312-.0434.9748-.1336 1.3779-.339zm1.0925-1.0925c-.2397.4704-.6221.8528-1.0925 1.0925l.454.891c.6585-.3355 1.194-.871 1.5295-1.5295zm.2725-2.935c0 .8483-.0004 1.4545-.0392 1.9297-.0384.4693-.1117.7666-.2333 1.0053l.891.454c.2054-.4031.2956-.8467.339-1.3779.0429-.5253.0425-1.1793.0425-2.0111zm0-2.4v2.4h1v-2.4zm-.2725-2.93498c.1216.23864.1949.53597.2333 1.0053.0388.47517.0392 1.08135.0392 1.92968h1c0-.83183.0004-1.48577-.0425-2.01111-.0434-.53118-.1336-.97475-.339-1.37786zm-1.0925-1.09254c.4704.23969.8528.62214 1.0925 1.09254l.891-.45399c-.3355-.65856-.871-1.194-1.5295-1.52955zm-2.935-.27248c.8483 0 1.4545.00039 1.9297.03921.4693.03835.7666.11168 1.0053.23327l.454-.891c-.4031-.20539-.8467-.29555-1.3779-.33895-.5253-.04292-1.1793-.04253-2.0111-.04253zm-6.4 0h6.4v-1h-6.4zm-2.93498.27248c.23864-.12159.53597-.19492 1.0053-.23327.47517-.03882 1.08135-.03921 1.92968-.03921v-1c-.83183 0-1.48577-.00039-2.01111.04253-.53118.0434-.97476.13356-1.37786.33895zm-1.09254 1.09254c.23969-.4704.62214-.85285 1.09254-1.09254l-.45399-.891c-.65856.33555-1.194.87099-1.52955 1.52955zm3.28818 10.78138c.09377-.0937.22095-.1464.35355-.1464v-1c-.39782 0-.77935.158-1.06066.4393z"
                                fill="#fff"/><g stroke="#fff" stroke-linecap="round" stroke-linejoin="round"><path
                                    d="m8 9h8"/><path d="m8 13h5"/></g></svg></span></li>
                <li><a href="tel:88007004600">
                        <svg fill="none" height="22" viewBox="0 0 22 22" width="22" xmlns="http://www.w3.org/2000/svg">
                            <path clip-rule="evenodd"
                                  d="m6.4397 3.38359c.2139-.39396.21376-.39404.21361-.39412l-.00102-.00055-.00163-.00088-.00424-.00226c-.00325-.00172-.00735-.00386-.01226-.00639-.00982-.00503-.02293-.0116-.03902-.0193-.03211-.01537-.07659-.03549-.131-.05718-.10748-.04284-.26057-.0945-.4381-.12553-.3454-.06037-.86901-.05334-1.27792.36586l-1.4202 1.42256-.02617.04093.37706.24181c-.37706-.24181-.37687-.24209-.37706-.24181l-.00076.0012-.0009.00142-.00222.00354c-.00166.00267-.00369.00596-.00605.00985-.00473.0078-.01082.01804-.0181.03071-.01454.02532-.03382.06036-.05641.10487-.04518.089-.10365.21604-.16387.3792-.12049.32646-.24798.79765-.2895 1.39719-.08333 1.20312.18189 2.88851 1.48336 4.92369l.00388.0072c.00839.0156.02056.038.03646.0667.03182.0574.0786.14.14.244.12277.2079.30427.5017.54176.8499.4692.6879 1.16081 1.5939 2.05437 2.4711.84424.8394 1.70647 1.4941 2.36706 1.9444.34749.2368.64067.418.84817.5407.1037.0614.1862.1081.2434.1399.0286.0159.0509.0281.0665.0365l.0077.0041c2.0322 1.2963 3.7149 1.5579 4.9157 1.4724.5984-.0426 1.0686-.1708 1.3944-.2917.1628-.0604.2896-.119.3784-.1643.0444-.0226.0794-.0419.1047-.0565.0126-.0073.0228-.0134.0306-.0181.0039-.0024.0071-.0044.0098-.006l.0035-.0023.0014-.0009.0007-.0004c.0003-.0001.0005-.0003-.2409-.378l.2409.378.0414-.0265 1.4202-1.4226c.4185-.4096.4255-.9341.3653-1.28-.031-.1778-.0826-.3312-.1254-.4388-.0216-.0545-.0417-.0991-.057-.1313-.0077-.0161-.0143-.0292-.0193-.039-.0025-.005-.0047-.0091-.0064-.0123l-.0022-.0043-.0009-.0016-.0006-.001c-.0001-.0002-.0001-.0003-.3934.2139l.3934-.2139-.0311-.0573-2.9029-2.9078-.007-.0064c-.4969-.4571-1.0216-.4791-1.4166-.3584-.1853.0566-.3343.1412-.4355.2096-.0514.0346-.0926.0665-.1223.0911-.0149.0123-.0271.0229-.0364.0312-.0047.0042-.0086.0078-.0118.0107l-.0043.004-.0017.0017-.0008.0007c-.0002.0002-.0007.0007.3088.3247l-.3095-.324-.0036.0034-1.5501 1.5526c-.1165-.0572-.2669-.146-.4481-.2703-.2972-.204-.6248-.4661-.9335-.7294-.3075-.2623-.5898-.5204-.7957-.7132-.1028-.0963-.1861-.176-.2435-.2314-.0035-.0034-.0069-.0066-.0101-.0098-.003-.0031-.0061-.0063-.0092-.0095-.0554-.0576-.13505-.1411-.23134-.2441-.19286-.2063-.45105-.4892-.71396-.7974-.26396-.3094-.52728-.6379-.73328-.93612-.12502-.18101-.21547-.3322-.2745-.45012l1.54715-1.54973.00344-.0036-.32353-.30998c.32353.30998.32337.31015.32353.30998l.00066-.00069.00073-.00077.00163-.00173.004-.00429c.00298-.00322.00657-.00718.01073-.01184.00829-.00932.01886-.02152.03115-.03646.02449-.02978.05634-.07103.09096-.12248.06824-.10145.15271-.25065.20922-.43628.12051-.39567.09851-.92116-.35778-1.41888l-.00684-.00746-2.91801-2.90761-.05665-.03087zm2.98226 9.21021c.01932.02.03507.0362.04682.0483l.01811.0185.00483.005.00182.0018c.00001 0 .00002 0 .00244-.0023.05438.0521.12232.1166.20115.1905.21246.199.50557.467.82677.741.32.2729.6742.5574 1.008.7865.3156.2165.6735.4272.9865.4877l.2333.0451 1.9063-1.9094c.0017-.0014.004-.0034.0069-.0058.0107-.0088.0288-.023.0532-.0395.0503-.034.1187-.0718.1964-.0955.1299-.0397.3119-.0542.5432.1552l2.7893 2.7939c.0056.0126.0118.0274.0184.0439.0271.0685.0576.1609.0752.2615.0366.2101.0064.3719-.1098.4852l-.0023.0022-1.3488 1.3509c-.011.0061-.0246.0133-.0406.0214-.0604.0308-.1555.0752-.2835.1227-.2557.0949-.6427.2022-1.1468.2381-1.0011.0712-2.4993-.1365-4.3853-1.3433l-.0162-.0103-.0169-.0089-.0032-.0017c-.0027-.0015-.0073-.0039-.0135-.0073-.0124-.0067-.0315-.0171-.0569-.0312-.0506-.0281-.1262-.0709-.2227-.128-.1931-.1142-.4699-.2852-.79954-.5099-.62291-.4245-1.4296-1.0373-2.21728-1.8169-.00833-.0081-.01648-.0164-.02477-.0246-.03923-.039-.0784-.0784-.11749-.1182l-.00081.0008c-.78485-.7891-1.39996-1.5971-1.82546-2.221-.22515-.33-.39623-.6071-.51037-.8005-.05705-.0966-.09982-.1722-.12793-.2229-.01406-.0253-.02445-.0445-.03113-.0569-.00334-.0062-.00575-.0107-.00723-.0135l-.00163-.0031-.00919-.0175-.01069-.0167c-1.21203-1.88929-1.4228-3.38987-1.35337-4.39232.03496-.50478.14159-.89219.2361-1.14825.04728-.1281.09151-.2233.12219-.28373.00812-.01601.01529-.02957.0213-.04064l1.34872-1.35095.0022-.00227c.11307-.11649.27457-.14668.4844-.11.10042.01755.19265.0481.26099.07534.01674.00667.03162.01297.04437.01859l2.80405 2.79407c.20872.23149.19415.41374.15458.54372-.0237.07784-.06142.14632-.09531.1967-.01645.02446-.03066.04265-.03945.05334-.00241.00293-.00437.00524-.00581.00691l-1.90971 1.91288.04996.23772c.06516.31002.27685.66609.49525.98239.23052.3337.51565.6882.78894 1.0085.27435.3216.54221.6151.7411.8278.07376.0789.13822.1469.19025.2013-.00235.0024-.00234.0024-.00233.0024l.00678.0067.01854.0182c.01194.0116.02793.0272.04764.0463zm7.49414 5.344-.0009.0005c.0007-.0004.0009-.0005.0009-.0005z"
                                  fill="#fff" fill-rule="evenodd"/>
                            <path
                                d="m6.65331 2.98947c.00015.00008.00029.00016-.21361.39412zm0 0-.00102-.00055-.00163-.00088-.00424-.00226c-.00325-.00172-.00735-.00386-.01226-.00639-.00982-.00503-.02293-.0116-.03902-.0193-.03211-.01537-.07659-.03549-.131-.05718-.10748-.04284-.26057-.0945-.4381-.12553-.3454-.06037-.86901-.05334-1.27792.36586l-1.4202 1.42256-.02617.04093m3.35156-1.61726.05665.03087 2.91801 2.90761.00684.00746c.45629.49772.47829 1.02321.35778 1.41888-.05651.18563-.14098.33483-.20922.43628-.03462.05145-.06647.0927-.09096.12248-.01229.01494-.02286.02714-.03115.03646-.00416.00466-.00775.00862-.01073.01184l-.004.00429-.00163.00173-.00073.00077-.00066.00069m-6.34176-3.3621.37706.24181c-.37706-.24181-.37687-.24209-.37706-.24181zm0 0-.00076.0012-.0009.00142-.00222.00354c-.00166.00267-.00369.00596-.00605.00985-.00473.0078-.01082.01804-.0181.03071-.01454.02532-.03382.06036-.05641.10487-.04517.089-.10365.21604-.16387.3792-.12049.32646-.24798.79765-.2895 1.39719-.08333 1.20312.18189 2.88851 1.48336 4.92369l.00388.0072c.00839.0156.02056.038.03647.0667.0318.0574.07859.14.13999.244.12277.2079.30427.5017.54176.8499.4692.6879 1.16081 1.5939 2.05437 2.4711.84424.8394 1.70647 1.4941 2.36706 1.9444.34749.2368.64067.418.84817.5407.1037.0614.1862.1081.2434.1399.0286.0159.0509.0281.0665.0365l.0077.0041c2.0322 1.2963 3.7149 1.5579 4.9157 1.4724.5984-.0426 1.0686-.1708 1.3944-.2917.1628-.0604.2896-.119.3784-.1643.0444-.0226.0794-.0419.1047-.0565.0126-.0073.0228-.0134.0306-.0181.0039-.0024.0071-.0044.0098-.006l.0035-.0023.0014-.0009.0007-.0004m0 0c.0003-.0001.0005-.0003-.2409-.378zm0 0 .0414-.0265 1.4202-1.4226c.4185-.4096.4255-.9341.3653-1.28-.031-.1778-.0826-.3312-.1254-.4388-.0216-.0545-.0417-.0991-.057-.1313-.0077-.0161-.0143-.0292-.0193-.039-.0025-.005-.0047-.0091-.0064-.0123l-.0022-.0043-.0009-.0016-.0006-.001m0 0c-.0001-.0002-.0001-.0003-.3934.2139zm0 0-.0311-.0573-2.9029-2.9078-.007-.0064c-.4969-.4571-1.0216-.4791-1.4166-.3584-.1853.0566-.3343.1412-.4355.2096-.0514.0346-.0926.0665-.1223.0911-.0149.0123-.0271.0229-.0364.0312-.0047.0042-.0086.0078-.0118.0107l-.0043.004-.0017.0017-.0008.0007c-.0002.0002-.0007.0007.3088.3247l-.3095-.324-.0036.0034-1.5501 1.5526c-.1165-.0572-.2669-.146-.4481-.2703-.2972-.204-.6248-.4661-.9335-.7294-.3075-.2623-.5898-.5204-.7957-.7132-.1028-.0963-.1861-.176-.2435-.2314-.0035-.0034-.0069-.0066-.0101-.0098-.003-.0031-.0061-.0063-.0092-.0095-.0554-.0576-.13505-.1411-.23134-.2441-.19286-.2063-.45105-.4892-.71396-.7974-.26396-.3094-.52728-.6379-.73328-.93612-.12502-.18101-.21547-.3322-.2745-.45012l1.54715-1.54973.00344-.0036m0 0-.32353-.30998c.32353.30998.32337.31015.32353.30998zm-.17473 4.67327c-.01175-.0121-.0275-.0283-.04682-.0483-.01971-.0191-.0357-.0347-.04764-.0463l-.01854-.0182-.00678-.0067c-.00001 0-.00002 0 .00233-.0024-.05202-.0544-.11649-.1224-.19025-.2013-.19889-.2127-.46675-.5062-.7411-.8278-.27329-.3203-.55842-.6748-.78894-1.0085-.2184-.3163-.43009-.67237-.49525-.98239l-.04996-.23772 1.90971-1.91288c.00144-.00167.0034-.00398.00581-.00691.00879-.01069.023-.02888.03945-.05334.03389-.05038.07161-.11886.09531-.1967.03957-.12998.05414-.31223-.15458-.54372l-2.80405-2.79407c-.01275-.00562-.02763-.01192-.04437-.01859-.06834-.02724-.16057-.05779-.26099-.07534-.20983-.03668-.37133-.00649-.4844.11l-.0022.00227-1.34872 1.35095c-.00601.01107-.01318.02463-.0213.04064-.03068.06043-.07491.15562-.12219.28373-.09451.25606-.20114.64347-.2361 1.14825-.06943 1.00245.14134 2.50303 1.35337 4.39232l.01069.0167.00919.0175.00163.0031c.00148.0028.00389.0073.00723.0135.00668.0124.01707.0316.03113.0569.02811.0507.07088.1263.12793.2229.11414.1934.28522.4705.51037.8005.4255.6239 1.04061 1.4319 1.82546 2.221l.00081-.0008c.03909.0398.07826.0792.11749.1182.00829.0082.01644.0165.02477.0246.78768.7796 1.59437 1.3924 2.21728 1.8169.32964.2247.60644.3957.79954.5099.0965.0571.1721.0999.2227.128.0254.0141.0445.0245.0569.0312.0062.0034.0108.0058.0135.0073l.0032.0017.0169.0089.0162.0103c1.886 1.2068 3.3842 1.4145 4.3853 1.3433.5041-.0359.8911-.1432 1.1468-.2381.128-.0475.2231-.0919.2835-.1227.016-.0081.0296-.0153.0406-.0214l1.3488-1.3509.0022-.0022c.1163-.1133.1465-.2751.1099-.4852-.0176-.1006-.0481-.193-.0752-.2615-.0066-.0165-.0128-.0313-.0184-.0439l-2.7893-2.7939c-.2313-.2094-.4133-.1949-.5432-.1552-.0777.0237-.1461.0615-.1964.0955-.0244.0165-.0425.0307-.0532.0395-.0029.0024-.0052.0044-.0069.0058l-1.9063 1.9094-.2333-.0451c-.313-.0605-.6709-.2712-.9865-.4877-.3338-.2291-.688-.5136-1.008-.7865-.3212-.274-.61431-.542-.82677-.741-.07883-.0739-.14677-.1384-.20115-.1905-.00242.0023-.00243.0023-.00244.0023l-.00182-.0018-.00483-.005zm7.44732 5.2957-.0009.0005c.0007-.0004.0009-.0005.0009-.0005z"
                                stroke="#fff" stroke-miterlimit="10" stroke-width=".1"/>
                        </svg>
                    </a></li>
                <li class="mob_v_open_auth_form"><span><svg fill="none" height="24" viewBox="0 0 24 24" width="24"
                               xmlns="http://www.w3.org/2000/svg"><path
                                d="m3.23779 19.5c1.32541-2.2108 4.23028-3.7238 8.76231-3.7238 4.532 0 7.4368 1.513 8.7622 3.7238m-5.1622-11.4c0 1.9882-1.6118 3.6-3.6 3.6-1.9883 0-3.60003-1.6118-3.60003-3.6 0-1.98823 1.61173-3.6 3.60003-3.6 1.9882 0 3.6 1.61177 3.6 3.6z"
                                stroke="#fff" stroke-linecap="round"/></svg></span></li>
            </ul>
            <div class="mob_contacts__city">
                Ваш город: <span data-modalname="cityselect" class="universal_modal__in not_from_mapmodal"><?= regionality()->getCurrentCity()['NAME']; ?></span>
            </div>
        </div>
    </div>
    <!--/noindex-->