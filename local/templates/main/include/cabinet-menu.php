<?php

?>

<div class="account_nav">
    <div class="account_nav__over">
        <ul class="account_nav__list">
            <li class="account_nav__item my_data first"><a href="/cabinet/private/">Мои данные</a> </li>
            <li class="account_nav__item my_orders"><a href="/cabinet/orders/">Заказы</a></li>
<!--            <li class="account_nav__item"><a href="/returns">Возвраты</a></li>-->
<!--            <li class="account_nav__item"><a href="/reviews">Отзывы и вопросы</a></li>-->
<!--            <li class="account_nav__item"><a href="/addresses">Адреса</a></li>-->
<!--            <li class="account_nav__item last"><a href="/cards">Карты</a> </li>-->
        </ul>
    </div>
    <svg class="account_nav__arrow account_nav__prev disabled" fill="none" height="60" viewBox="0 0 30 60" width="30" xmlns="http://www.w3.org/2000/svg"><path d="m0 0h30v60h-30z" fill="#fff" fill-opacity=".9"/><path d="m13 24 4 4.5-4 4.5" opacity=".5" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
    <svg class="account_nav__arrow account_nav__next" fill="none" height="60" viewBox="0 0 30 60" width="30" xmlns="http://www.w3.org/2000/svg"><path d="m0 0h30v60h-30z" fill="#fff" fill-opacity=".9"/><path d="m13 24 4 4.5-4 4.5" opacity=".5" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
</div>