<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


if ($arResult['AUTHORIZED']) {
    echo Loc::getMessage('MAIN_AUTH_CHD_SUCCESS');
    return;
}

$fields = $arResult['FIELDS'];
?>

    <? if ($arResult['ERRORS']): ?>
        <div class="alert alert-danger">
            <? foreach ($arResult['ERRORS'] as $error) {
                echo $error;
            }
            ?>
        </div>
        <br>
    <? elseif ($arResult['SUCCESS']): ?>
        <script>
            $('.restore_step').css('display','none');
            $('.restore_step3').fadeIn(200);
        </script>
    <? endif; ?>

    <? if (!$arResult['SUCCESS']) { ?>
    <form class="form_v1" name="bform" method="post" target="_top" action="<?= POST_FORM_ACTION_URI; ?>">
        <div class="fields">
            <input hidden="hidden" type="text" name="<?= $fields['login']; ?>" maxlength="255"
                   value="<?= \htmlspecialcharsbx($arResult['LAST_LOGIN']); ?>"/>
            <input hidden="hidden" type="text" name="<?= $fields['checkword']; ?>" maxlength="255"
                   value="<?= \htmlspecialcharsbx($arResult[$fields['checkword']]); ?>"/>

            <div class="field_wrap">
                    <input type="password" name="<?= $fields['password']; ?>"
                           value="<?= \htmlspecialcharsbx($arResult[$fields['password']]); ?>" maxlength="255"
                           autocomplete="off"  class="field" placeholder="Новый пароль" />
            </div>

            <div class="field_wrap">
                    <input type="password" name="<?= $fields['confirm_password']; ?>"
                           value="<?= \htmlspecialcharsbx($arResult[$fields['confirm_password']]); ?>" maxlength="255"
                           autocomplete="off" class="field" placeholder="Повторите пароль"/>
            </div>

            <div class="button_wrap">
                <input hidden="hidden" type="text" name="<?= $fields['action']; ?>"
                       value="<?= Loc::getMessage('MAIN_AUTH_CHD_FIELD_SUBMIT'); ?>"/>
                <input type="submit" class="button button_black"/>
            </div>

            <? if ($arResult['AUTH_AUTH_URL'] || $arResult['AUTH_REGISTER_URL']): ?>
                <hr class="bxe-light">
                <noindex>
                    <? if ($arResult['AUTH_AUTH_URL']): ?>
                        <div class="bx-authform-link-container">
                            <a href="<?= $arResult['AUTH_AUTH_URL']; ?>" rel="nofollow">
                                <?= Loc::getMessage('MAIN_AUTH_CHD_URL_AUTH_URL'); ?>
                            </a>
                        </div>
                    <? endif; ?>
                    <? if ($arResult['AUTH_REGISTER_URL']): ?>
                        <div class="bx-authform-link-container">
                            <a href="<?= $arResult['AUTH_REGISTER_URL']; ?>" rel="nofollow">
                                <?= Loc::getMessage('MAIN_AUTH_CHD_URL_REGISTER_URL'); ?>
                            </a>
                        </div>
                    <? endif; ?>
                </noindex>
            <? endif; ?>
        </div>
    </form>
<? } ?>