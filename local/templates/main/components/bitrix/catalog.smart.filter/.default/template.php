<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="filters">

	<div class="filters_top">

		<div class="categories_mobin">
			<span>Категории</span>
			<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 13H15M1 8.5H15M1 4H15" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"></path></svg>
		</div>

		<div class="categories">
			<div class="categories_menu">
				<div class="categories_over">
					<ul class="categories_list">
						<li data-catid="0" class="categories_item all active" data-rightpos="0"><span>Все</span></li>
						<li data-catid="1" class="categories_item" data-rightpos="0"><span>Топы и блузы</span></li>
						<li data-catid="2" class="categories_item" data-rightpos="0"><span>Джемперы и блузы</span></li>
						<li data-catid="3" class="categories_item" data-rightpos="0"><span>Штаны и брюки</span></li>
						<li data-catid="4" class="categories_item" data-rightpos="0"><span>Деним</span></li>
						<li data-catid="5" class="categories_item" data-rightpos="0"><span>Свитшоты и Худи</span></li>
						<li data-catid="6" class="categories_item" data-rightpos="0"><span>Рубашки</span></li>
						<li data-catid="7" class="categories_item" data-rightpos="0"><span>Футболки и лонгсливы</span></li>
						<li data-catid="8" class="categories_item" data-rightpos="0"><span>Sale</span></li>
						<li data-catid="9" class="categories_item" data-rightpos="0"><span>Верхняя одежда</span></li>
						<li data-catid="10" class="categories_item" data-rightpos="0"><span>Комбинезоны</span></li>
						<li data-catid="11" class="categories_item" data-rightpos="0"><span>Юбки</span></li>
						<li data-catid="12" class="categories_item" data-rightpos="0"><span>Жакеты</span></li>
						<li data-catid="13" class="categories_item last" data-rightpos="0"><span>Платья</span></li>
					</ul>
				</div>
				<ul class="subcats subcats_1">
					<li class="subcat_item"><span>С длинным рукавом</span></li>
					<li class="subcat_item"><span>По фигуре</span></li>
					<li class="subcat_item"><span>Без рукава</span></li>
				</ul>
				<ul class="subcats subcats_2">
					<li class="subcat_item"><span>Подкатегория 1</span></li>
					<li class="subcat_item"><span>Подкатегория 2</span></li>
					<li class="subcat_item"><span>Подкатегория 3</span></li>
				</ul>
				<div class="categories_arrows">
					<div class="categories_arrow categories_prev disabled"><svg fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m11.2198 3.06006-5.44001 5.44 5.44001 5.44004" stroke="#242424" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path></svg></div>
					<div class="categories_arrow categories_next"><svg fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m11.2198 3.06006-5.44001 5.44 5.44001 5.44004" stroke="#242424" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path></svg></div>
				</div>
			</div>
		</div>

		<div class="filters_in">
			<span>Фильтр</span>
			<div class="filters_in__icon">
				<svg class="filters_in__close" fill="none" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg"><g stroke="#242424" stroke-linecap="round" stroke-width="1.5"><path d="m3 12.2566 9.5131-9.51316"/><path d="m12.5137 12.2566-9.51318-9.51316"/></g></svg>
				<svg class="filters_in__open" fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><g stroke="#242424" stroke-linecap="round" stroke-width="1.5"><path d="m7.7915 5.66663h6.375"/><path d="m2.8335 11.3334h7.08333"/><ellipse cx="4.9585" cy="5.66663" rx="2.125" ry="2.125" transform="matrix(0 1 -1 0 10.62513 .70813)"/><ellipse cx="12.0415" cy="11.3334" rx="2.125" ry="2.125" transform="matrix(0 1 -1 0 23.3749 -.7081)"/></g></svg>
				<svg class="filters_in__open2" fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m14.1665 11.3334h-7.08333" stroke="#242424" stroke-linecap="round" stroke-width="1.5"/><ellipse cx="2.125" cy="2.125" rx="2.125" ry="2.125" stroke="#242424" stroke-linecap="round" stroke-width="1.5" transform="matrix(.00000004 1 1 -.00000004 2.8335 9.20825)"/><g fill="#242424"><path clip-rule="evenodd" d="m8.3148 6.75c-.17581-.4705-.28279-.97459-.30866-1.5h-5.00614c-.41421 0-.75.33579-.75.75s.33579.75.75.75z" fill-rule="evenodd"/><ellipse cx="13" cy="5" rx="3" ry="3" transform="matrix(0 1 -1 0 18 -8)"/></g></svg>
			</div>
		</div>

		<div class="filters_out__back modal_close"></div>

		<div class="filters_out" style="height: auto;">
			<div class="filters_mobhead">
				<span class="filters_mobhead__title">Фильтр</span>
				<span class="filters_mobhead__reset">Сбросить</span>
				<svg class="filters_mobhead__close" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg>
			</div>

			<div class="filters_customscroll_main">
				<? foreach ($arResult['ITEMS'] as $arItem) { ?>
					<? if ($arItem['CODE'] === 'Розничная') { ?>
						<div class="filters_item filters_item__price" data-control-id="<?=$arItem['VALUES']['MIN']['CONTROL_ID']?>:<?=$arItem['VALUES']['MAX']['CONTROL_ID']?>">
							<div class="filters_item__label">Цена, руб</div>
							<div class="filters_item__out">
								<div
									class="pricerange_slider_wrap"
									data-min="<?= $arItem['VALUES']['MIN']['VALUE'] ?>"
									data-max="<?= $arItem['VALUES']['MAX']['VALUE'] ?>"
									data-min-html="<?= $arItem['VALUES']['MIN']['HTML_VALUE'] ?? $arItem['VALUES']['MIN']['VALUE'] ?>"
									data-max-html="<?= $arItem['VALUES']['MAX']['HTML_VALUE'] ?? $arItem['VALUES']['MAX']['VALUE'] ?>"
								>
									<div class="pricerange_slider"></div>
								</div>
							</div>
						</div>
					<? }elseif ($arItem['CODE'] === 'COMPOUND') { ?>
						<div class="filters_item filters_item__fabric">
							<div class="filters_item__label"><?=$arItem['NAME']?></div>
							<div class="filters_item__chosen"></div>
							<div class="filters_item__out">
								<ul>
									<? foreach ($arItem['VALUES'] as $value) { ?>
										<li class="<?= $value['DISABLED'] ? 'disabled' : ''; ?><?= $value['CHECKED'] ? ' active' : ''; ?>"
											data-control-id="<?= $value['CONTROL_ID'] ?>">
											<span><?= $value['VALUE']; ?></span></li>
									<? } ?>
								</ul>
								<button class="button button_black_tr filters_item__show"><span>Показать товары</span></button>
							</div>
						</div>
					<? } elseif ($arItem['CODE'] === 'SIZE') { ?>
						<div class="filters_item filters_item__size ">
							<div class="filters_item__label">Размер</div>
							<div class="filters_item__chosen"></div>
							<div class="filters_item__out">
								<ul>
									<? foreach ($arItem['VALUES'] as $value) { ?>
										<li class="<?= $value['DISABLED'] ? 'disabled' : ''; ?><?= $value['CHECKED'] ? ' active' : ''; ?>"
											data-control-id="<?= $value['CONTROL_ID'] ?>">
											<span><?= $value['VALUE']; ?></span></li>
									<? } ?>
								</ul>
								<button class="button button_black_tr filters_item__show"><span>Показать товары</span></button>
							</div>
						</div>
					<? } elseif ($arItem['CODE'] === 'COLOR') { ?>
						<div class="filters_item filters_item__color">
							<div class="filters_item__label">Цвет</div>
							<div class="filters_item__chosen"></div>
							<div class="filters_item__out">
								<ul>
									<? foreach ($arItem['VALUES'] as $value) { ?>
										<li class="<?= $value['DISABLED'] ? 'disabled' : ''; ?><?= $value['CHECKED'] ? ' active' : ''; ?>"
											data-control-id="<?= $value['CONTROL_ID'] ?>">
											<div
												style="
													background: <?= $arResult['COLORS'][$value['VALUE']]; ?>;
												<?= strtolower($arResult['COLORS'][$value['VALUE']]) === '#fff' ? 'border: 1px solid #A4A4A4;' : '' ?>
													"
											></div>
											<span><?= $value['VALUE'] ?></span>
										</li>
									<? } ?>
								</ul>
								<button class="button button_black_tr filters_item__show"><span>Показать товары</span></button>
							</div>
						</div>
					<? } ?>
				<? } ?>
			</div>
		</div>


		<div class="filters_options">

			<div class="sortby">
				<span>Сортировка</span>
				<svg fill="none" height="15" viewBox="0 0 15 15" width="15" xmlns="http://www.w3.org/2000/svg"><path d="m3 6 4.5 4 4.5-4" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
				<ul>
					<!--                                <li><a href="">Сначала новинки</a></li>-->
					<!--                                <li><a href="">Сначала акции</a></li>-->
					<li><a rel="nofollow noopener" href="?sort=PRICE&order=asc" <?if ($_GET['sort'] === 'PRICE' && $_GET['order'] === 'asc') { ?>class="active"<? } ?>>По низкой цене</a></li>
					<li><a rel="nofollow noopener" href="?sort=PRICE&order=desc" <?if ($_GET['sort'] === 'PRICE' && $_GET['order'] === 'desc') { ?>class="active"<? } ?>>По высокой цене</a></li>
				</ul>
			</div>

			<div class="cards_cols">
				<strong>Вид:</strong>
				<span class="by3" data-type="by3">3</span>
				<i>/</i>
				<span class="by4 active" data-type="by4">4</span>
				<span class="by2" data-type="by2">2</span>
				<u>/</u>
				<span class="by1" data-type="by1">1</span>
			</div>

		</div>

	</div>

</div>

