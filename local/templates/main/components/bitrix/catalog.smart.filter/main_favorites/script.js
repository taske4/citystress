function applyFilter()
{
	let data = {
		'ajax': 'y',
	};

	$('.filters_item [data-control-id].active, .filters_item__price').each(function( index, item ) {
		let controlId = $(this).attr('data-control-id');

		if ($(item).hasClass('filters_item__price')) {
			let priceControls = controlId.split(':');
			data[priceControls[0]] = $('[data-min-html]').attr('data-min-html');
			data[priceControls[1]] = $('[data-min-html]').attr('data-max-html');
		} else {
			data[controlId] = 'Y';
		}
	});

	$.ajax({
		type: 'GET',
		data: data,
		dataType: 'json',
		headers: {
			'Bx-ajax': 'true'
		},
		success: function(data) {
			$('.filters_item__show span').html(data.ELEMENT_COUNT+'&nbsp;');
			$('.filters_item__show a').attr('href', data.FILTER_URL);

			if (data && data.ITEMS) {
				for(let i in data.ITEMS) {

					if (data.ITEMS[i]['CODE'] === 'test') {
						console.log(data.ITEMS[i]);
					} else {
						if (data.ITEMS[i]['VALUES']) {
							for (let c in data.ITEMS[i]['VALUES']) {
								updateItem(data.ITEMS[i]['VALUES'][c]);
							}
						}
					}
				}
			} else {
				console.log('Смарт фильтр больше не работает!');
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus);
			console.log(errorThrown);
			// Обработка ошибок
		}
	});
}

function updateItem(item)
{
	// console.log(item);
	if (item.DISABLED) {
		$('.filters_item [data-control-id='+item.CONTROL_ID+']').addClass('disabled');
	} else {
		$('.filters_item [data-control-id='+item.CONTROL_ID+']').removeClass('disabled');
	}
}

$(function (){
	$(document).on('mouseup', '.filters_item__price .ui-slider-handle', function (){
		setTimeout(applyFilter, 100);
	})

	$(document).on('click','.filters_item [data-control-id]:not(.disabled), .filters_reset, .filters_mobhead__reset, .filters_applied__remove', function (){
		setTimeout(applyFilter, 100);
	});

	$(document).on('click', '.filters_mobhead__close', function (){
		setTimeout(function(){
			applyFilter();

			window.location.href = $('.filters_item__show a').attr('href');
		}, 100);
	});

	$('.filters_item .active').each(function(index, item){
		var this_text = $(this).text();

		if ( !$('.filters_applied__item[data-text="'+this_text+'"]').length ) {
			$('.filters_applied').append('<button class="filters_applied__item" data-item-control-id="'+$(item).attr('data-control-id')+'" data-text="'+this_text+'"><span>'+this_text+'</span><img src="/local/templates/main/img/close.svg" alt="" class="filters_applied__remove"></button>');
			$('.filters_reset').appendTo('.filters_applied');
			$('.filters_applied .filters_reset').show();
		};
	});
});
