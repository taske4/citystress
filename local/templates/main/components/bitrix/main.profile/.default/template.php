<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

        $subscribeEntity = \Bitrix\Iblock\Iblock::wakeUp(SUBSCRIBE_IBLOCK_ID)->getEntityDataClass();
        $subscribeFound = $subscribeEntity::query()
            ->setSelect(['ID', 'EMAIL_' => 'EMAIL'])
            ->setFilter(['=EMAIL_VALUE' => $USER->GetEmail()])
            ->fetch();
?>

<div class="profile_container container pagetype2">
    <div class="profile_block block">

        <? require_once $_SERVER['DOCUMENT_ROOT'] . SITE_MAIN_TEMPLATE_PATH . "/include/cabinet-menu.php" ?>
        <script>
            $('.account_nav__item.my_data').addClass('active');
        </script>

        <div class="account_head">
            <div class="account_head__title">Мои данные</div>
        </div>


        <div class="profile_content">
            <form class="profile_form" method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>" enctype="multipart/form-data">

                <?= $arResult["BX_SESSION_CHECK"] ?>

                <input type="hidden" name="SUBSCRIBE_ON_STRESS" value="Y"/>
                <input type="hidden" name="lang" value="<?= LANG ?>"/>
                <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>

                <div class="profile_grouplabel">Персональная информация</div>

                <?
                ShowError($arResult["strProfileError"]);

                if ($arResult['DATA_SAVED'] == 'Y') {
                    if (\Bitrix\Main\Application::getInstance()->getSession()->get("is_user_change_email")) {
                        echo "Ваш логин для входа на сайт был изменен.<br>Данные для входа на сайт были отправлены вам на новую почту.";

                        ?>
                        <script>
                            $(function(){
                                if (screen.width <= 1250) {
                                    $('.mobadd_back').show();
                                    $('.mob_login_data_exchange').removeClass('hiddenpro');
                                }
                            });
                        </script>
                        <?

                        \Bitrix\Main\Application::getInstance()->getSession()->set("is_user_change_email", false);
                    }
                    ShowNote(GetMessage('PROFILE_DATA_SAVED'));

                }
                ?>

                <div class="fields_wrap group1">

                    <div class="field_wrap filled">
                        <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
                        <input value="<?= $arResult["arUser"]["LAST_NAME"] ?>" maxlength="50" name="LAST_NAME" type="text" class="field required">
                    </div>

                    <div class="field_wrap">
                        <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
                        <input value="<?= $arResult["arUser"]["NAME"] ?>" maxlength="50" name="NAME" type="text" class="field required" placeholder="Имя*">
                    </div>

                    <div class="field_wrap">
                        <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
                        <input value="<?= $arResult["arUser"]["SECOND_NAME"] ?>" maxlength="50" name="SECOND_NAME" type="text" class="field required" placeholder="Отчество">
                    </div>

                    <div class="field_wrap">
                        <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
                        <input value="<?= $arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" name="PERSONAL_BIRTHDAY" type="text" class="field required" placeholder="Дата рождения">
                    </div>

                    <select name="PERSONAL_GENDER" class="hiddenpro">
                        <option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
                        <option
                            value="M"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
                        <option
                            value="F"<?= $arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " SELECTED=\"SELECTED\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
                    </select>

                    <div id="user-gender-select" class="field_wrap">
                        <div class="checkboxes single">
                            <div class="checkbox checkbox_v2 <?= $arResult['arUser']['PERSONAL_GENDER'] === "F" ? "checked" : "" ?>">
                                <input name="gender-female" type="checkbox" class="checkbox_input">
                                <div class="checkbox_icon"></div>
                                <span class="checkbox_label">Женщина</span>
                            </div>
                            <div class="checkbox checkbox_v2 <?= $arResult['arUser']['PERSONAL_GENDER'] === "M" ? "checked" : "" ?>">
                                <input name="gender-male" type="checkbox" class="checkbox_input">
                                <div class="checkbox_icon"></div>
                                <span class="checkbox_label">Мужчина</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="fields_wrap group2">

                    <div class="field_wrap">
                        <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
                        <input maxlength="255" value="<?= $arResult["arUser"]["PERSONAL_MOBILE"] ?>" name="PERSONAL_MOBILE" type="text" class="field field_phone required" placeholder="Телефон*">
                    </div>

                    <div class="field_wrap">
                        <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
                        <input value="<?= $arResult["arUser"]["EMAIL"] ?>" name="EMAIL" type="text" class="field field_email required" placeholder="Email*">
                    </div>

                </div>

                <div class="fields_wrap profile_subscribe">
                    <div class="profile_grouplabel">Управление подпиской</div>
                    <div class="subscribe_on_stress checkbox checkbox_v1 <?= $subscribeFound ? 'checked' : ''?>">
                        <input type="checkbox" checked="<?= $subscribeFound ? 'true' : 'false'?>">
                        <div class="checkbox_icon">
                            <svg fill="none" height="5" viewBox="0 0 8 5" width="8" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="m7.85356.125357c.09376.080366.14644.189315.14644.302904s-.05268.222532-.14644.302904l-4.84116 4.143505c-.0939.08024-.22119.12533-.3539.12533-.13272 0-.26001-.04509-.35391-.12533l-2.170175-1.85744c-.0884632-.08125-.13662103-.18872-.13433734-.29977.00229037-.11105.05484864-.217.14661034-.29554.091755-.07853.215549-.12351.345292-.12547s.255306.03926.350246.11498l1.816274 1.55452 4.48725-3.840593c.09389-.0802701.22116-.125357.35391-.125357.13268 0 .26002.0450869.3539.125357z" fill="#242424" fill-rule="evenodd"/></svg>
                        </div>
                        <span>Подписаться на новости и скидки</span>
                    </div>
                </div>

                <button value="Сохранить" type="submit" name="save" class="button button_black profile_submit"><span>Сохранить изменения</span></button>

                <div class="profile_deletewrap">
                    <? if (\Bitrix\Main\Application::getInstance()->getSession()->get("request_on_remove_user")) { ?>
                        <p>Ваш запрос на удаление аккаунта направлен.</p>
                    <? } else { ?>
                        <a href="" class="profile_delete">Отправить запрос на <span>удаление аккаунта</span></a>
                    <? } ?>
                </div>

            </form>

        </div>

    </div>
</div>

<script>
    $('.checkbox').on('click', function() {
        var checkboxes = $(this).closest('.checkboxes');

        if ( checkboxes.hasClass('single') ) {

            checkboxes.find('.checkbox').removeClass('checked');
            $(this).addClass('checked');
            $(this).find('input').prop('checked',true);

        } else {

            if ( $(this).hasClass('checked') ) {
                $(this).find('input').prop('checked',false);
                $(this).removeClass('checked');
            } else {
                $(this).find('input').prop('checked',true);
                $(this).addClass('checked');
            };

        };

        cart_check();
    });
</script>