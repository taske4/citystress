<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

?>
<form class="form_v1" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
<?
if ($arResult["BACKURL"] <> '')
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">

	<div class="fields">
		<div class="field_wrap">
			<input type="text" class="field" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>" placeholder="Email или номер телефона" />
			<input type="hidden" name="USER_EMAIL" />
		</div>
	</div>

<?/*if($arResult["PHONE_REGISTRATION"]):?>

	<div style="margin-top: 16px">
		<div><b><?=GetMessage("sys_forgot_pass_phone")?></b></div>
		<div><input type="text" name="USER_PHONE_NUMBER" value="<?=$arResult["USER_PHONE_NUMBER"]?>" /></div>
		<div><?echo GetMessage("sys_forgot_pass_note_phone")?></div>
	</div>
<?endif;*/?>

	<div class="button_wrap">
		<input type="submit" class="button button_black" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
	</div>
</form>

<script type="text/javascript">
document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
</script>
