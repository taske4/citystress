<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->createFrame()->begin("Загрузка навигации");
?>

<?if($arResult["NavPageCount"] > 1):?>

	<?if ($arResult["NavPageNomer"]+1 <= $arResult["nEndPage"]):?>
		<?
		$plus = $arResult["NavPageNomer"]+1;
		$url = $arResult["sUrlPathParams"] . "PAGEN_".$arResult["NavNum"]."=".$plus;

		?>

			<button class="button button_black pagenav_button_more orders_more" data-url="<?=$url?>"><span>Показать еще</span></button>

	<?else:?>

			<button class="button button_black pagenav_button_more orders_more"><span>Загружено все</span></button>

	<?endif?>

<?endif?>