<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */


$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

Loader::includeModule("highloadblock");

$colors = HL\HighloadBlockTable::getById(HL_COLORS)->fetch();
$colors = HL\HighloadBlockTable::compileEntity($colors);
$colors_data_class = $colors->getDataClass();
$colors = $colors_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_HASH'],
))->fetchAll();

$allColors = [];
foreach ($colors as $color) {
    $allColors[$color['UF_NAME']] = $color;
}

$sizes = HL\HighloadBlockTable::getById(HL_SIZES)->fetch();
$sizes = HL\HighloadBlockTable::compileEntity($sizes);
$sizes_data_class = $sizes->getDataClass();
$sizes = $sizes_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_SORT'],
))->fetchAll();

/**
 * @var $arResult
 */

Loader::IncludeModule('catalog');

$arResult['sizesSorting'] = [];
foreach ($sizes as $size) {
    $arResult['sizesSorting'][$size['UF_NAME']] = $size['UF_SORT'];
}

foreach ($arResult['ITEMS'] as &$arItem) {
    $arItem['PRODUCT_SETS'] = \Bitrix\Catalog\ProductSetsTable::query()
        ->setSelect(['ITEM_ID'])
        ->setFilter([
            '=OWNER_ID' => $arItem['ID'],
            '!ITEM_ID' => $arItem['ID'],
        ])
        ->fetchAll();

    $arItem['sizes'] = [];
    $arItem['colors'] = [];

    foreach ($arItem['OFFERS'] as $arOffer) {
        if (!$arItem['PHOTO_BY_COLORS'][$arOffer['PROPERTIES']['COLOR']['~VALUE']]) {
            $arItem['PHOTO_BY_COLORS'][$arOffer['PROPERTIES']['COLOR']['~VALUE']] = current($arOffer['PROPERTIES']['MORE_PHOTO']['VALUE']);
        }

        $arItem['prices'][$arOffer['PROPERTIES']['COLOR']['~VALUE']] = [
            'price'          => $arOffer['PRICES']['Розничная']['PRINT_VALUE'],
            'oldprice'       => $arOffer['PRICES']['oldprice']['PRINT_VALUE'],
            'priceNumber'    => $arOffer['PRICES']['Розничная']['VALUE'],
            'oldpriceNumber' => $arOffer['PRICES']['oldprice']['VALUE'],
        ];

        $arItem['colors'][$allColors[$arOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']]['UF_HASH']] = $arOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE'];

        if ($arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']) {
            $arItem['sizes']
            [$arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']]
            [$arResult['sizesSorting'][$arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']].'_'.$arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']] = $arOffer['ID'];
        }
    }

    $arItemSizes = [];

    foreach($arItem['sizes'] as $color => $sizes) {

        ksort($sizes);

        $tmp = [];
        foreach($sizes as $key => $value) {
            if (preg_match('/\d+_(\w+)/', $key, $matches) && $matches[1]) {
                $tmp[$matches[1]] = $value;
            }
        }

        $arItemSizes[count($tmp ?: []).'_'.$color] = $tmp;
    }

    $arItem['sizes'] = $arItemSizes;

    ksort($arItem['sizes']);


    $arItemSizes = [];
    foreach($arItem['sizes'] as $color => $sizes) {
        if (preg_match('/\d+_(.+)/', $color, $matches) && $matches[1]) {
            $arItemSizes[$matches[1]] = $sizes;
        }
    }

    $arItem['sizes'] = array_reverse($arItemSizes);

    unset($arItemSizes);
}

$arResult['favorites'] = \Citystress\Favorites::getInstance()->get($GLOBALS['USER']->GetID());