<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Bitrix\Currency\CurrencyLang;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
?>


<div class="look_side" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <?
        $totalOldPrice = $totalPrice = 0;
    ?>
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <?
        $productData = [
            'id' => $arItem['ID'],
            'quantity' => 1,
        ];
        $activeOffer = null;
        $productName = $arItem['NAME'];
        $priceString = null;//$arItem['PRICES'];
        $sizes = [];
        $allSizes = [];

        foreach ($arItem['sizes'] as $color => $sizes) {
            $allSizes = array_merge($allSizes, array_keys($sizes));
        }

        $allSizes = array_unique($allSizes);

        $activeOfferId = null;

        foreach ($arItem['OFFERS'] as $index => $arOffer) {
            if (
                (
                    (isset($arParams['PRE_ACTIVE_OFFER']) && in_array($arOffer['ID'], $arParams['PRE_ACTIVE_OFFER']))  &&
                    is_null($activeOfferId)
                )
            ) {
                $activeOfferId = current($arItem['sizes'][$arOffer['PROPERTIES']['COLOR']['VALUE']]);
            }
        }

        foreach ($arItem['OFFERS'] as $index => $arOffer) {
            if (!$activeOffer) {
                if ($arOffer['ID'] === $activeOfferId) {
                    $activeOffer = $arOffer;
                    $priceString = $arOffer['PRICES']['Розничная']['PRINT_VALUE'];//['PRINT_DISCOUNT_VALUE'];
                    $productName = $arOffer['NAME'] ?? $productName;
                    $sizes = $arItem['sizes'][$arOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']];

                    $totalPrice += (float)$arOffer['PRICES']['Розничная']['VALUE'];
                    $totalOldPrice += ((float)$arOffer['PRICES']['oldprice']['VALUE'] ?: (float)$arOffer['PRICES']['Розничная']['VALUE']);
                }
            }
        }

        if ($activeOffer) {
            $productData = [
                'id' => $activeOffer['ID'],
                'quantity' => 1,
            ];
        }

        ?>
        <div
            class="product_mini"
            data-product-id="<?=$arItem['ID']?>"
            data-product='<?=json_encode(['quantity' => 1, 'id' => $activeOffer['ID']])?>'
            data-product-all-sizes='<?=json_encode($allSizes)?>'
        >
            <div class="product_mini__img">
                <div class="product_mini__imgbox">

                    <? foreach ($arItem['PHOTO_BY_COLORS'] as $color => $pictureId) { ?>
                        <?
                        $pictureUrl = CFile::GetPath($pictureId);
                        $class = '';
                        if ($activeOffer['PROPERTIES']['COLOR']['VALUE'] !== $color) {
                            $class = ' hiddenpro';
                        }
                        ?>
                        <div class="back_img<?=$class?>" data-color="<?=$color?>">
                            <a href="<?=$arItem['DETAIL_PAGE_URL'].'?offerId='.$activeOffer['ID']?>" data-href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$pictureUrl?>" alt=""></a>
                        </div>
                    <? } ?>

                </div>
            </div>
            <div class="product_mini__box">
                <div class="product_mini__row">
                    <div class="product_mini__article"><?=$activeOffer['PROPERTIES']['CML2_ARTICLE']['VALUE']?></div>
                    <div class="product_mini__priceold"><?= $activeOffer['PRICES']['oldprice']['VALUE'] ? $activeOffer['PRICES']['oldprice']['PRINT_VALUE'] : '' ?></div>
                </div>
                <div class="product_mini__row">
                    <div class="product_mini__title"><a href="<?=$arItem['DETAIL_PAGE_URL'].'?offerId='.$activeOffer['ID']?>" data-href="<?=$arItem['DETAIL_PAGE_URL']?>"><?= $arItem['NAME'] ?></a></div>
                    <div class="product_mini__price"><?= $activeOffer['PRICES']['Розничная']['PRINT_VALUE']?></div>
                </div>
                <div class="product_mini__color">
                    <div class="product_mini__colorchosen">
                        <span style="background: <?= array_search($activeOffer['PROPERTIES']['COLOR']['VALUE'], $arItem['colors']); ?>;"></span>
                        <i><?= $activeOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']; ?></i>
                    </div>
                    <ul class="product_mini__colorlist" >
                        <?
                        foreach ($arItem['colors'] as $hex => $color) {
                            $sizesColor = $arItem['sizes'][$color] ?: [];
                            $classes = '';
                            $dataOfferId = current($sizesColor);

                            if (array_search($activeOffer['ID'], $sizesColor)) {
                                $classes .= ' active';
                                $dataOfferId = $activeOfferId;
                            }

                            ?>
                            <li data-offer-id="<?=$dataOfferId?>" class="<?=$classes?>">
                                <div
                                    data-prices='<?=json_encode($arItem['prices'][$color])?>'
                                    data-color-value="<?=$color?>"
                                    data-color-offer='<?=json_encode($arItem['sizes'][$color])?>'
                                    data-color="<?=preg_replace('/#/', '', $hex);?>"
                                    style="background: <?=$hex;?>;"
                                ></div>
                                <span><?=$color?></span>
                            </li>
                        <? } ?>
                    </ul>
                </div>
                <div class="product_mini__options">
                    <div class="product_mini__sizes">
<!--                        --><?// foreach ($sizes as $size => $offerId) { ?>
                        <? foreach ($allSizes as $sizeValue) { ?>
                            <?

                            $classes = '';


                            if (!($offerId = $sizes[$sizeValue])) {
                                $classes .= ' disabled';
                            } elseif ($activeOffer['PROPERTIES']['SIZE']['VALUE'] === $sizeValue) {
                                $classes .= ' active';
                            }

                            ?>
                            <button
                                class="product_mini__size<?=$classes?>"
                                data-offer-id="<?= $offerId ?>"
                            >
                                <?= preg_replace("/\d+/", '', $sizeValue) ?>
                            </button>
                        <? } ?>
                    </div>
                    <div class="product_mini__actions">
                        <button class="product_mini__fav<? if (array_search($arItem['ID'], $arResult['favorites']?: [])
                            !==
                        false) { ?> active<? } ?>"> <!-- добавляем класс active, если в избранном -->
                            <svg fill="none" height="16" viewBox="0 0 18 16" width="18"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="m9 2.74296c.00309.00353.00303.00358.00303.00358l.0051-.00465c.00484-.00439.01286-.01161.02396-.02135.02219-.01949.05661-.04905.10234-.08638.09157-.07474.22783-.18006.40147-.2977.349-.23645.8398-.5164 1.4153-.70164 1.1292-.36347 2.5879-.37135 4.0111 1.09592l.0044.00455.0034.00338.0025.00251c.0028.00273.0076.0077.0144.01484.0137.0143.0351.03727.0627.06851.0552.06258.1346.15783.2253.28261.1821.25055.4055.61418.5725 1.06614.3281.88796.4497 2.13808-.4482 3.60505l-.0368.06012-.0119.0461c-.001.0023-.0023.00509-.0039.00839-.0127.02696-.0424.08477-.1028.17974-.1214.19072-.3595.52046-.8139 1.03791-.854.97261-2.4532 2.58731-5.43 5.16271-2.97678-2.5754-4.57596-4.1901-5.43-5.16271-.45436-.51745-.69251-.84719-.81386-1.03791-.06044-.09497-.09018-.15278-.10286-.17974-.00156-.0033-.00283-.00609-.00386-.00839l-.01188-.0461-.0368-.06012c-.89793-1.46697-.77638-2.71709-.44828-3.60505.167-.45196.3904-.81559.57253-1.06614.0907-.12478.17009-.22003.22531-.28261.02757-.03124.049-.05421.06265-.06851.00683-.00714.0117-.01211.01441-.01484l.00229-.0023.00368-.00359.00441-.00455c1.4232-1.46727 2.88186-1.45939 4.01104-1.09592.57549.18524 1.06632.46519 1.41532.70164.17364.11764.3099.22296.40147.2977.04573.03733.08015.06689.10234.08638.0111.00974.01912.01696.02396.02135l.0051.00465s-.00006-.00005.00303-.00358zm0-1.16037c-.0116-.00796-.02335-.01598-.03525-.02404-.39631-.2685-.96454-.594474-1.644-.813184-1.37984-.444149-3.21325-.436565-4.92366 1.324354l-.007.00702c-.00651.00657-.01526.01552-.02604.0268-.02154.02255-.05122.05446-.08729.09535-.07208.08168-.17014.19969-.28008.35093-.21916.30149-.48979.74091-.69379 1.29301-.410692 1.11148-.540197 2.65801.50379 4.3907.00775.01921.01709.04068.02836.06464.03269.06948.0828.1623.16031.28411.15452.24286.42461.6113.89963 1.15227.94223 1.07305 2.71163 2.84845 6.0384 5.69815l.05466.0673c.00399-.0034.00798-.0068.01196-.0102.00399.0034.00797.0068.01196.0102l.05466-.0673c3.32678-2.8497 5.09618-4.6251 6.03838-5.69815.475-.54097.7451-.90941.8997-1.15227.0775-.12181.1276-.21463.1603-.28411.0112-.02396.0206-.04543.0283-.06463 1.044-1.7327.9145-3.27923.5038-4.39071-.204-.5521-.4746-.99152-.6938-1.29301-.1099-.15124-.208-.26925-.2801-.35093-.036-.04089-.0657-.0728-.0873-.09535-.0107-.01128-.0195-.02023-.026-.0268l-.007-.00702c-1.7104-1.760919-3.5438-1.768503-4.9236-1.324354-.67951.21871-1.24774.544684-1.64405.813184-.01189.00806-.02365.01608-.03525.02404z"
                                      style="fill-rule:evenodd;clip-rule:evenodd;fill:#242424;stroke:#242424;stroke-width:.5;stroke-miterlimit:10;stroke-linejoin:round"/>
                            </svg>
                            <svg class="fav_active" fill="none" height="15" viewBox="0 0 16 15" width="16"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="m7.96475 1.05855.03525.02404.03525-.02404c.39631-.268496.96454-.594474 1.644-.813184 1.37985-.444149 3.21325-.436565 4.92365 1.324354l.007.00702c.0065.00657.0153.01552.026.0268.0216.02255.0513.05446.0873.09535.0721.08168.1702.19969.2801.35093.2192.30149.4898.74091.6938 1.29301.4107 1.11148.5402 2.65801-.5038 4.39071-.0077.0192-.0171.04067-.0283.06463-.0327.06948-.0828.1623-.1603.28411-.1546.24286-.4247.6113-.8997 1.15227-.9422 1.07305-2.7116 2.84845-6.03838 5.69815l-.05466.0673-.01196-.0102-.01196.0102-.05466-.0673c-3.32677-2.8497-5.09617-4.6251-6.0384-5.69815-.47502-.54097-.74511-.90941-.899634-1.15227-.077509-.12181-.127616-.21463-.160304-.28411-.011272-.02396-.020609-.04543-.02836-.06464-1.043989-1.73269-.914484-3.27922-.503789-4.3907.203999-.5521.47463-.99152.693784-1.29301.109943-.15124.208003-.26925.280083-.35093.03607-.04089.06575-.0728.08729-.09535.01078-.01128.01953-.02023.02604-.0268l.007-.00702c1.71041-1.760919 3.54382-1.768503 4.92366-1.324354.67946.21871 1.24769.544688 1.644.813184z"
                                      fill="#242424"/>
                            </svg>
                        </button>
                        <button class="buttin button_black product_mini__tocart">В корзину</button>
                    </div>
                </div>
<!--                <div class="product_mini__info">Обхват талии, см: <strong>66-72</strong>. Обхват бедер, см:-->
<!--                    <strong>94-98</strong></div>-->
<!--                <div class="product_mini__tablein">Таблица размеров</div>-->
            </div>
        </div>

    <? } ?>
    <?

    if ($totalOldPrice <= $totalPrice) {
        $totalOldPrice = null;
    }

    $currencyCode = "RUB"; // Код валюты
    $formattedPrice = FormatCurrency(
        $totalPrice,
        $currencyCode
    );

    if ($totalOldPrice) {
        $formattedPriceold = FormatCurrency(
            $totalOldPrice,
            $currencyCode
        );
    } else {
        $formattedPriceold = null;
    }

    ?>
    <script>
        $(function (){
            $('.look_prices .look_price').html('<?=$formattedPrice?>');
            $('.look_prices .look_priceold').html('<?=$formattedPriceold?>');
        });
    </script>
</div>