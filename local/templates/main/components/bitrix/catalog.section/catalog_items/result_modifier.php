<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

Loader::includeModule("highloadblock");

$favorites = \Citystress\Favorites::getInstance()->get($GLOBALS['USER']->GetID());

$colors = HL\HighloadBlockTable::getById(HL_COLORS)->fetch();
$colors = HL\HighloadBlockTable::compileEntity($colors);
$colors_data_class = $colors->getDataClass();
$colors = $colors_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_HASH', 'UF_NEED_SHADOW'],
))->fetchAll();

$allColors = [];
foreach($colors as $color) {
    $allColors[trim(mb_ucfirst($color['UF_NAME']))] = $color;
}

$sizes = HL\HighloadBlockTable::getById(HL_SIZES)->fetch();
$sizes = HL\HighloadBlockTable::compileEntity($sizes);
$sizes_data_class = $sizes->getDataClass();
$sizes = $sizes_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_SORT'],
))->fetchAll();

$arResult['sizesSorting'] = [];
foreach ($sizes as $size) {
    $arResult['sizesSorting'][$size['UF_NAME']] = $size['UF_SORT'];
}

$allColors = [];
foreach($colors as $color) {
    $allColors[mb_ucfirst($color['UF_NAME'])] = $color;
}

/**
 * @var $arResult
 */

Loader::IncludeModule('catalog');

$salesIds = [];
foreach ($arResult['ITEMS'] as $arItem) {
    foreach($arItem['OFFERS'] as $arOffer) {
        if ($saleId = $arOffer['PROPERTIES']['LINK_SALE']['VALUE']) {
            $salesIds[] = $saleId;
        }
    }
}


$sales = [];

if ($salesIds) {
    $sales = \Bitrix\Iblock\Iblock::wakeUp(SALES_IBLOCK_ID)->getEntityDataClass()::query()
        ->setSelect(['ID', 'NAME', 'PERCENT_' => 'PERCENT', 'COUPON_' => 'COUPON'])
        ->setFilter(['ID' => $salesIds, 'ACTIVE' => 'Y'])
        ->fetchAll();
}



foreach($arResult['ITEMS'] as &$arItem) {

    /*
     * Таблица размеров доступна только в разделе "Одежда"
     */
    $arItem['tableSizeAvailable'] = false;

    if ($arItem['IBLOCK_SECTION_ID']) {
        $list = CIBlockSection::GetNavChain(false, $arItem['IBLOCK_SECTION_ID'], array('ID'), true);
        foreach ($list as $ar_group) {
            if (intval($ar_group["ID"]) === 59) {
                $arItem['tableSizeAvailable'] = true;
                break;
            }
        }
    }

    $arItem['in_fav'] = in_array($arItem['ID'], $favorites ?: []);

    $arItem['PRODUCT_SETS'] = \Bitrix\Catalog\ProductSetsTable::query()
        ->setSelect(['ITEM_ID'])
        ->setFilter([
            '=OWNER_ID' => $arItem['ID'],
            '!ITEM_ID'  => $arItem['ID'],
        ])
        ->fetchAll();

    $arItem['sizes']  = [];
    $arItem['colors'] = [];

    // =====================================

    // =====================================

    foreach ($arItem['OFFERS'] as &$arOffer) {
        if ($saleId = $arOffer['PROPERTIES']['LINK_SALE']['VALUE']) {
            if (($saleFoundKey = array_search($saleId, array_column($sales, 'ID'))) !== false) {
                $arOffer['SALE'] = $sales[$saleFoundKey];
            }
        }

        $arItem['prices'][$arOffer['ID']]['BASE'] = $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['PRINT_DISCOUNT_VALUE'];
        $arItem['prices'][$arOffer['ID']]['OLD']  = '';

        $basePrice = $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['VALUE'];
        $oldPrice  = $arOffer['PRICES'][$arParams['PRICE_CODE'][1]]['VALUE'];

        if ($arOffer['SALE']['PERCENT_VALUE']) {
            $discounted = $basePrice-(($basePrice*$arOffer['SALE']['PERCENT_VALUE'])/100);
            $discounted = \Bitrix\Catalog\Product\Price::roundPrice(3, $discounted, 'RUB');
            $arItem['prices'][$arOffer['ID']]['DISCOUNTED'] = FormatCurrency($discounted, 'RUB');
            $arItem['tags'][$arOffer['ID']]['SALE'] = $arOffer['SALE']['NAME'];
        } else {
            $discounted = null;
        }

        // Рассчет price diff
        if (
            ($discounted && $oldPrice) &&
            ($discounted < $oldPrice)
        ) {
            $arItem['prices'][$arOffer['ID']]['OLD'] = $arOffer['PRICES'][$arParams['PRICE_CODE'][1]]['PRINT_DISCOUNT_VALUE'];
            $arItem['prices'][$arOffer['ID']]['DIFF_PERCENT'] = null;
//                (int)(($priceDiff = ($oldPrice - $discounted) / $oldPrice) * 100);
        } elseif (
            ($discounted && $basePrice) &&
            ($discounted < $basePrice)
        ) {
            $arItem['prices'][$arOffer['ID']]['DIFF_PERCENT'] = null;
                //(int)(($priceDiff = ($basePrice - $discounted) / $basePrice) * 100);
        } elseif ($basePrice < $oldPrice) {
            $arItem['prices'][$arOffer['ID']]['OLD'] = $arOffer['PRICES'][$arParams['PRICE_CODE'][1]]['PRINT_DISCOUNT_VALUE'];
            $arItem['prices'][$arOffer['ID']]['DIFF_PERCENT'] = (int)((($oldPrice - $basePrice) / $oldPrice) * 100);
        }

        $arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE'] = mb_ucfirst($arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']);

        $colorProps = array(
            'color_name' => $arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE'],
            'need_shadow' => $allColors[$arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']]['UF_NEED_SHADOW']
        );
        $arItem['colors'][$allColors[$arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']]['UF_HASH']] = $colorProps;

        if ($arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']) {
            $arItem['sizes']
            [$arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']]
            [$arResult['sizesSorting'][$arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']].'_'.$arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']] = $arOffer['ID'];
        }

    }

    $arItemSizes = [];

    foreach($arItem['sizes'] as $color => $sizes) {

        ksort($sizes);



        $tmp = [];
        foreach($sizes as $key => $value) {
            if (preg_match('/\d+_([\w ]+)/', $key, $matches) && $matches[1]) {
                $tmp[$matches[1]] = $value;
            }
        }

        $arItemSizes[count($tmp ?: []).'_'.$color] = $tmp;
    }

    $arItem['sizes'] = $arItemSizes;

    ksort($arItem['sizes']);


    $arItemSizes = [];
    foreach($arItem['sizes'] as $color => $sizes) {
        if (preg_match('/\d+_(.+)/', $color, $matches) && $matches[1]) {
            $arItemSizes[$matches[1]] = $sizes;
        }
    }


    $arItem['sizes'] = array_reverse($arItemSizes);

    unset($arItemSizes);

    $offersSets = [];
    foreach($arItem['OFFERS'] as $offer) {
        if ($offer['PROPERTIES']['SET_LINK']['VALUE']) {
            $offersSets[$offer['PROPERTIES']['COLOR']['VALUE']] = ['setId' => $offer['PROPERTIES']['SET_LINK']['VALUE']];
        }
    }

    foreach($offersSets as $color => $value) {
        try {
            $offerSet = \Citystress\Entity\Sets::getById(intval($value['setId']));
        } catch (\Error | \Exception $e) {
            continue;
        }

        $setOffersIds = $offerSet['PROPERTIES']['OFFERS_LINK'];
        if (!$setOffersIds) {
            continue;
        }

        $setOffers = \Citystress\Entity\Offer::getList(['ID' => $setOffersIds]);

        foreach ($setOffers as $setOffer) {
            if ($setOffer['ACTIVE'] !== 'Y') {
                $setOffer['ID'] = $setOffer['VARIANTS'][0]['ID'];

                if (!$setOffer['ID']) {
                    continue;
                }
            }

            $offersSets[$color]['offerId'] = $setOffer['ID'];
            $offersSets[$color]['gambarMini'][] = CFile::GetPath($setOffer['PROPERTIES']['MORE_PHOTO'][0]);
            $offersSets[$color]['productIds'][] = [
                'ID'              => $setOffer['PROPERTIES']['CML2_LINK'],
                'ACTIVE_OFFER_ID' => $setOffer['ID'], // для предвыбора в catalog.section
            ];
        }
    }

    $arItem['offersSets'] = $offersSets;
}
