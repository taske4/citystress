<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */
;
$this->setFrameMode(true);
?>
<? if ($arResult['ITEMS']) { ?>
<div class="submenu_sugg">
    <div class="submenu_suggbox">

        <div class="sugg_sliderwrap sliderwrap">

            <div class="custom_slarrows">
                <div class="custom_slarrow2 custom_prev"><svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg></div>
                <div class="custom_slarrow2 custom_next"><svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg></div>
            </div>

            <div class="sugg_slider slider">

                <? foreach($arResult['ITEMS'] as $arItem) { ?>
                <?
                    $n = rand(0, count($arItem['PROPERTIES']['PHOTO_GALLERY']['VALUE'] ?: []));
                    if ($offer = $arItem['OFFERS'][0]) {
                        $n = rand(0, count($offer['PROPERTIES']['MORE_PHOTO']['VALUE'] ?: []));
                        $picId = $offer['PROPERTIES']['MORE_PHOTO']['VALUE'][$n];
                    }

                    if (!$picId) {
                        continue;
                    }

                    $picPath = \CFile::GetPath($picId);

                    if (!$picPath) {
                        continue;
                    }
                ?>
                <div class="slide">
                    <div class="sugg_item">
                        <div class="sugg_item__content">
                            <div class="back_img">
                                <picture>
                                    <source type="image/jpg" media="(max-width: 750px)" srcset="<?=$picPath;?>">
                                    <source type="image/jpg" media="(min-width: 751px)" srcset="<?=$picPath;?> 1x, <?=$picPath;?> 2x">
                                    <img data-img1x="<?=$picPath?>" data-img2x="<?=$picPath?>" src="<?=$picPath?>" alt="">
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
                <? } ?>

            </div>

        </div>

    </div>
</div>
<? } ?>