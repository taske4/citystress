<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


$productsKeys = [];

foreach ($arResult['BASKET'] as $key => $basketItem) {
    $productsKeys[$basketItem['PRODUCT_ID']] = $key;
}

$res = \Citystress\Entity\Offer::getEntity()::query()
    ->setSelect(['ID', 'MORE_PHOTO'])
    ->setFilter(['=ID' => array_keys($productsKeys)])
    ->fetchCollection();

foreach($res as $value) {
    $key = $productsKeys[$value->getId()];

    if ($key) {
        foreach($value->getMorePhoto()->getAll() as $photo) {
            $arResult['BASKET'][$key]['MORE_PHOTO_FIRST'] = \CFile::GetPath($photo->getValue());
            break;
        }
    }
}