<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @global CMain $APPLICATION */
/** @var array $arParams */
/** @var array $arResult */

/** @var string $templateFolder */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

\Bitrix\Main\Loader::includeModule("highloadblock");

?>


<? foreach($arResult['BASKET'] as $key => $basketItem) { ?>
    <?
        $colorHash = '#fff';

        try {
            $offer   = (new \Citystress\Entity\Offer($basketItem['PRODUCT_ID'], false))->offer;
            $article = $offer['PROPERTIES']['CML2_ARTICLE'];
            $color   = $offer['PROPERTIES']['COLOR'];
            $size    = $offer['PROPERTIES']['SIZE'];

            if ($color) {
                $colors = \Bitrix\Highloadblock\HighloadBlockTable::getById(HL_COLORS)->fetch();
                $colors = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($colors)->getDataClass();
                $colorData = $colors::query()
                    ->setSelect(['UF_HASH'])
                    ->setFilter(['=UF_NAME' => $color])
                    ->fetch();

                if ($colorData['UF_HASH']) {
                    $colorHash = $colorData['UF_HASH'];
                }
            }
        } catch (\Error | \Exception $e) {
            $article = '';
            $color   = '';
            $size    = '';
        }
    ?>
    <div class="order_item">
        <div class="order_item__col order_item__col1">
            <div class="order_product">
                <div class="order_product__img">
                    <div class="back_img">
                        <a href="<?=$basketItem['DETAIL_PAGE_URL']?>">111
                            <img src="<?=$basketItem['MORE_PHOTO_FIRST']?>" alt="<?=$basketItem['NAME']?>">
                        </a>
                    </div>
                </div>
                <div class="order_product__title"><a href="<?=$basketItem['DETAIL_PAGE_URL']?>"><?=$basketItem['NAME']?></a></div>
                <div class="order_product__article"><?=$article?></div>
                <? if ($color && $size) { ?>
                    <div class="order_product__variant"><span style="background:<?=$colorHash?>;"></span> <?=$color?> <em>/</em> <?=$size?></div>
                <? } ?>
            </div>
        </div>
        <div class="order_item__col order_item__col2">
            <div class="order_item__collabel">Товар</div>
            <strong><?=$basketItem['PRICE_FORMATED']?></strong>
        </div>
        <div class="order_item__col order_item__col3">
            <div class="order_item__collabel">Количество</div>
            <strong><?=$basketItem['QUANTITY']?></strong>
        </div>
        <div class="order_item__col order_item__col4">
            <div class="order_item__collabel">Сумма</div>
            <strong><?=$basketItem['FORMATED_SUM']?></strong>
        </div>
    </div>
<? } ?>