<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

Loader::includeModule("highloadblock");

$favorites = \Citystress\Favorites::getInstance()->get($GLOBALS['USER']->GetID());

$colors = HL\HighloadBlockTable::getById(HL_COLORS)->fetch();
$colors = HL\HighloadBlockTable::compileEntity($colors);
$colors_data_class = $colors->getDataClass();
$colors = $colors_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_HASH'],
))->fetchAll();

$allColors = [];
foreach($colors as $color) {
    $allColors[mb_ucfirst($color['UF_NAME'])] = $color;
}

/**
 * @var $arResult
 */

Loader::IncludeModule('catalog');


foreach($arResult['ITEMS'] as &$arItem) {
    $arItem['in_fav'] = in_array($arItem['ID'], $favorites);

    $arItem['PRODUCT_SETS'] = \Bitrix\Catalog\ProductSetsTable::query()
        ->setSelect(['ITEM_ID'])
        ->setFilter([
            '=OWNER_ID' => $arItem['ID'],
            '!ITEM_ID'  => $arItem['ID'],
        ])
        ->fetchAll();

    $arItem['sizes']  = [];
    $arItem['colors'] = [];

    foreach ($arItem['OFFERS'] as &$arOffer) {
        $arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE'] = mb_ucfirst($arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']);

        $arItem['colors'][$allColors[$arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']]['UF_HASH']] = $arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE'];
        $arItem['sizes'][$arOffer['DISPLAY_PROPERTIES']['COLOR']['VALUE']][$arOffer['DISPLAY_PROPERTIES']['SIZE']['VALUE']] = $arOffer['ID'];
    }
}