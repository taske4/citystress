$(function(){

    let pagination = $('.product').parent().find('.pagination_box');

    if (!pagination.length) {
        pagination = $('.product').parent().find('.button_wrap');
    }

    if (pagination) {
        let paginationHtml = $(pagination);

        $(pagination).remove();

        $('.product').parent().after(paginationHtml);
    }
});