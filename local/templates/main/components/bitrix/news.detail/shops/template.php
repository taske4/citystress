<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $APPLICATION;
?>
<?
if ($shopName = $arResult["PROPERTIES"]['SHOP_NAME']["VALUE"]) {
    $APPLICATION->SetTitle($shopName);
    $APPLICATION->SetPageProperty('title', $shopName);
}

?>
<div class="shops_container container pagetype1">
  <div class="shops_block block">
    <div class="breadcrumbs"></div>
  <h1>
      <span><?= $arResult["NAME"]; ?></span>
    </h1>
	    <div class="shops_nav">
      <a href="/shops" data-index="CityStress" class="shops_nav__item active">Магазины</a>
      <a href="/shops?shops_nav=Cdek" data-index="Cdek" class="shops_nav__item">Пункты выдачи</a>
    </div>
	
	    <div class="shops_out mappoints_box option_view" style="display: block;">
      <div class="shops_flex">

        <div class="shops_content">
          <div class="nomob_customscroll">
            <div class="shops_content__inside">

              <a href="/shops/?shops_nav=<?=$arResult["PROPERTIES"]['TYPE']["VALUE"]?>" class="mappoints_goback">
                <svg fill="none" height="13" viewBox="0 0 25 13" width="25" xmlns="http://www.w3.org/2000/svg"><path d="m7.67377 2.25-4.13194 4.25m0 0 4.13194 4.25m-4.13194-4.25h17.95817" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
                <span>Назад к списку</span>
              </a>
 
              <div class="shops_mobmapbox">
                <div data-zoom="10" data-center=[<?=$_SESSION["LOCATION_LATITUDE"]?>,<?=$_SESSION["LOCATION_LONGITUDE"]?>] class="mappoints_mobmap_in"></div> <!-- мобильная версия карты со своими настройками zoom и center -->
              </div> 

              <div class="shoppickup_item optionview_item mappoints_data__item checked" data-coos="[<?=$arResult["PROPERTIES"]["MAP_COORDS"]["VALUE"]?>]">
                <div class="shoppickup_item__title"><?if($arResult["PROPERTIES"]['SHOP_NAME']["VALUE"]!=''):?><?=$arResult["PROPERTIES"]['SHOP_NAME']["VALUE"]?><?else:?><?=$arResult["NAME"]?><?endif;?></div>
				<?if($arResult["PROPERTIES"]['TYPE']["VALUE"]!='Cdek'):?>
				  <ul class="pick_options">
				  <?foreach($arResult['OPTIONS'] as $option):?>
				   <li><img src="<?=SITE_MAIN_TEMPLATE_PATH?><?if(in_array($option,$arResult["PROPERTIES"]['OPTIONS']["VALUE"])):?>/img/pickoption_yes.svg<?else:?>/img/pickoption_no.svg<?endif;?>" alt=""><span><?=$option?></span></li>
				  <?endforeach;?>
                  </ul>
 <?endif;?>
                <ul class="pick_chars">
                  <li>
                    <div class="pick_chars__label">Адрес</div>
                    <p><?=$arResult["NAME"]?></p>
                  </li>
		<?if($arResult["PROPERTIES"]['SCHEDULE']["VALUE"]!=''):?><div class="shoppickup_item__worktime"><li>
                    <div class="pick_chars__label">Время работы</div>  <p><?=$arResult["PROPERTIES"]['SCHEDULE']["VALUE"]?></p> </li><?endif;?>
 		<?if($arResult["PROPERTIES"]['PHONE']["VALUE"]!=''):?><div class="shoppickup_item__worktime"><li>
                    <div class="pick_chars__label">Телефон</div>  <p><?=$arResult["PROPERTIES"]['PHONE']["VALUE"]?></p> </li><?endif;?>
                   <li>
                    <div class="pick_chars__label">По вопросам сотрудничества</div>
                    <p><a href="mailto:ecommerce@citystress.ru">ecommerce@citystress.ru</a></p>
                  </li>
                </ul>
<?if(count($arResult['PHOTOS'] ?: [])>0):?>
  <div class="pick_photos">
  <?foreach($arResult['PHOTOS'] as $photo):?>
   <div class="pick_photo"><div style="background-image: url(<?=$photo['src']?>)"></div></div>
  <?endforeach;?>
                </div>
<?endif;?>
 
                 

              </div>

            </div>
          </div>
        </div>


        <div class="shops_mapbox">
          <div data-zoom="12" data-center=[<?=$_SESSION["LOCATION_LATITUDE"]?>,<?=$_SESSION["LOCATION_LONGITUDE"]?>] class="mappoints_map_in"></div>
        </div>
      
        <div class="mappoints_map"></div>
      </div>
    </div>
   </div>
    </div>
	