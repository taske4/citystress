<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
$arResult['PHOTOS'] = [];
$arResult['OPTIONS'] = [];
 
if(count($arResult["PROPERTIES"]['MORE_PHOTO']['VALUE'] ?: [])>0)
{
	foreach($arResult["PROPERTIES"]['MORE_PHOTO']['VALUE'] as $file)
{
	$arResult['PHOTOS'][] = CFile::ResizeImageGet($file, array('width'=>113, 'height'=>113), BX_RESIZE_IMAGE_PROPORTIONAL, true);         
}
}
 $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "CODE"=>"OPTIONS"));
while($enum_fields = $property_enums->GetNext())
{
	$arResult['OPTIONS'][] = $enum_fields["VALUE"];

}?>
 
 