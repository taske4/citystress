<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
	die();
}

/**
 * @global $arResult
 */

if ($arResult['PROPERTIES']['DATE_FROM']['VALUE'] && $arResult['PROPERTIES']['DATE_TO']['VALUE']) {
    $now        = new DateTime();
    $activeFrom = new DateTime($arResult['PROPERTIES']['DATE_FROM']['VALUE']);
    $activeTo   = new DateTime($arResult['PROPERTIES']['DATE_TO']['VALUE']);

    $arResult['ACTIVE_FROM'] = $activeFrom->format('d.m.Y');
    $arResult['ACTIVE_TO'] = $activeTo->format('d.m.Y');
    $arResult['ACTIVE_DAYS'] = $now->diff($activeTo)->d;
}

$whereList = $arResult['PROPERTIES']['WHERE']['VALUE'] ?: [];

$imageForWhere = [
    'Интернет-магазин' => SITE_MAIN_TEMPLATE_PATH."/img/bag2.svg",
    'Розничные магазины' => SITE_MAIN_TEMPLATE_PATH."/img/shop.svg",
];

foreach($whereList as $where) {
    $arResult['WHERE'][] = [
        'value' => $where,
        'img'   => $imageForWhere[$where],
    ];
}


//
//$arResult['salesPeriod'] = \lib\Helper::elementActivityModify($activeFrom, $activeTo);