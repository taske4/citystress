<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddChainItem($arResult['NAME']);

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$uriString = $request->getRequestUri();

$image = 'https://citystress.ru'.$arResult['PREVIEW_PICTURE']['SRC'];

$micromarkupData['twitter']['url'] = $micromarkupData['og']['url'] = 'https://citystress.ru' . $uriString;
$micromarkupData['twitter']['title'] = $micromarkupData['og']['title'] = $arResult['NAME'];
$micromarkupData['twitter']['description'] = $micromarkupData['og']['description'] = $arResult['PREVIEW_TEXT'];
$micromarkupData['twitter']['type'] = $micromarkupData['og']['type'] = 'SaleCard';
$micromarkupData['og']['image'] = $micromarkupData['twitter']['image'] = $image;

$this->SetViewTarget('microdata');

foreach ($micromarkupData['og'] as $key => $value) {
    echo "<meta property=\"og:$key\" content=\"$value\"/>";
}
foreach ($micromarkupData['twitter'] as $key => $value) {
    echo "<meta property=\"twitter:$key\" content=\"$value\"/>";
}

$this->EndViewTarget();

$productWithOffersLinkToSale = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_IBLOCK_ID)->getEntityDataClass()::query()
    ->setSelect(['IBLOCK_SECTION_ID', 'OFFERS_SALES_' => 'OFFERS_SALES'])
    ->setFilter([
        'OFFERS_SALES_VALUE' => [$arResult['ID']],
    ])
    ->fetchAll();

?>

<script>
    let actiondetailspop_data = <?= json_encode([
        'name' => $arResult['NAME'],
        'detailtext' => $arResult['DETAIL_TEXT'] ?: $arResult['PREVIEW_TEXT'],
    ]) ?>;
</script>

<?$APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    ".default",
    Array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0"
    )
);?>


<div class="action_content">
    <div class="action_content__main">
        <h1 class="action_title">
            <?
            if ($percent = $arResult['PROPERTIES']['PERCENT']['VALUE']) {
                echo 'Скидка ' . $percent . '%';
            } else {
                echo $arResult['NAME'];
            }
            ?>
        </h1>

        <? if ($percent) { ?>
            <div class="action_subtitle">
                Скидка <?= $percent ?> <? if ($coupon = $arResult['PROPERTIES']['COUPON']['VALUE']) { ?>по промокоду «<?= $coupon ?>»<? } ?>
            </div>
        <? } ?>

        <button
            class="button button_black action_details universal_modal__in not_from_mapmodal"
            data-modalname="actiondetails"
        >
            <span>
                <? if ($arResult['PROPERTIES']['TYPE']['VALUE'] === 'Sales') { ?>Условия акции
                <? } elseif ($arResult['PROPERTIES']['TYPE']['VALUE'] === 'News') { ?>Подробнее
                <? } ?>
            </span>
        </button>
    </div>
    <div class="action_content__side">
        <div class="action_period">
            <div class="action_period__title"><span>Период проведения</span><strong>Когда:</strong></div>
            <div class="action_period__date"><?= $arResult['ACTIVE_FROM'] ?> - <?= $arResult['ACTIVE_TO'] ?></div>
            <?
                $nowDate    = new \DateTime();
                $saleDateTo = \DateTime::createFromFormat('d.m.Y', $arResult['ACTIVE_TO']);

                if ($saleDateTo > $nowDate) {
            ?>
                <div class="action_period__left">Осталось <?= $arResult['ACTIVE_DAYS'] ?> дня</div>
            <? } elseif ($arResult['PROPERTIES']['TYPE']['VALUE'] === 'Sales') { ?>
                <div class="action_period__left">Акция завершилась</div>
            <? } ?>
        </div>
        <? if ($arResult['WHERE']) { ?>
            <div class="action_where">
                <div class="action_where__title"><span>Где действует</span><strong>Где:</strong></div>
                <ul>
                    <? foreach ($arResult['WHERE'] as $where) { ?>
                        <li><span><?= $where['value'] ?></span><img src="<?= $where['img'] ?>"
                                                                    alt="<?= $where['value'] ?>"></li>
                    <? } ?>
                    <!--                        <li><span>Розничные магазины</span><img src="-->
                    <?php //=SITE_MAIN_TEMPLATE_PATH?><!--/img/shop.svg" alt=""></li>-->
                </ul>
            </div>
        <? } ?>
    </div>
</div>

<? if ($arResult['PROPERTIES']['TYPE']['VALUE'] !== 'News') { ?>
    <? if ($productWithOffersLinkToSale) { ?>
    <div class="filters">
        <?= '<div class="filters_top">'; ?>
        <?
        global $searchFilter;
        $searchFilter['ACTIVE'] = 'Y';
        $searchFilter['PROPERTY_OFFERS_SALES'] = $arResult['ID'];

        $newsPreFilterSectionId = $_REQUEST['newsfilter']['section'] ?: '';
        ?>

        <? echo '<div class="mobcats_back"></div>'; ?>

        <? $APPLICATION->ShowViewContent('catalogSection'); ?>

        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "main",
            array(
                "SHOW_ALL_WO_SECTION" => 'Y',
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CONVERT_CURRENCY" => "N",
                "DISPLAY_ELEMENT_COUNT" => "Y",
                "FILTER_NAME" => "searchFilter2",
                "FILTER_VIEW_MODE" => "vertical",
                "HIDE_NOT_AVAILABLE" => "N",
                "IBLOCK_ID" => "6",
                "IBLOCK_TYPE" => "catalog",
                "PAGER_PARAMS_NAME" => "arrPager",
                "POPUP_POSITION" => "left",
                "PREFILTER_NAME" => "searchFilter",
                "PRICE_CODE" => ["Розничная", 'oldprice'],
                "SAVE_IN_SESSION" => "N",
                "SECTION_ID" => $newsPreFilterSectionId,
                "SECTION_DESCRIPTION" => "-",
                "SECTION_TITLE" => "-",
                "SEF_MODE" => "N",
                "TEMPLATE_THEME" => "blue",
                "XML_EXPORT" => "N"
            )
        ); ?>
    </div>

    <div class="catalog_items by4">
        <?
        if (isset($_GET["sort"]) && isset($_GET["order"]) && in_array(strtolower($_GET['order']), ['asc', 'desc'])) {
            $sort = true;

            if ($_GET['sort'] === 'PRICE') {
                $_GET['sort'] = 'catalog_PRICE_3';
            } elseif (in_array($_GET['sort'], ['NEW', 'SALE'])) {
                $_GET['sort'] = 'PROPERTY_' . $_GET['sort'];
            } else {
                $sort = false;
            }

            if ($sort) {
                $arParams["ELEMENT_SORT_FIELD"] = $_GET["sort"];
                $arParams["ELEMENT_SORT_ORDER"] = $_GET["order"];
            }
        }

        $arParams["ELEMENT_SORT_FIELD"] = $_GET['sort'];
        $arParams["ELEMENT_SORT_ORDER"] = $_GET['order'];

        ?>

        <? global $sectionsIds;
        $sectionsIds = []; ?>

        <?


            global $sectionsIds2;
            $sectionsIds2 = [];

            foreach($productWithOffersLinkToSale as $value) {
                $section = \Bitrix\Iblock\SectionTable::query()
                    ->setSelect(['ID',])
                    ->setFilter(['ID' => $value['IBLOCK_SECTION_ID']])
                    ->fetch();

                $sectionsIds2[] = $section['ID'];
            }

        ?>

        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "catalog_items",
            array(
                "ACTION_VARIABLE" => "action",
                "ADD_PICT_PROP" => "-",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BACKGROUND_IMAGE" => "-",
                "BASKET_URL" => "/personal/basket.php",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COMPATIBLE_MODE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                "DETAIL_URL" => "",
                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_COMPARE" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                "ENLARGE_PRODUCT" => "STRICT",
                "FILTER_NAME" => "searchFilter2",
                "HIDE_NOT_AVAILABLE" => "N",
                "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                "IBLOCK_ID" => "6",
                "IBLOCK_TYPE" => "catalog",
                "INCLUDE_SUBSECTIONS" => "Y",
                "LABEL_PROP" => array("SOSTAV"),
                "LABEL_PROP_MOBILE" => array(),
                "LABEL_PROP_POSITION" => "top-left",
                "LAZY_LOAD" => "N",
                "LINE_ELEMENT_COUNT" => "3",
                "LOAD_ON_SCROLL" => "N",
                "MESSAGE_404" => "",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "META_DESCRIPTION" => "-",
                "META_KEYWORDS" => "-",
                "OFFERS_FIELD_CODE" => array("", ""),
                "OFFERS_LIMIT" => "0",
                "OFFERS_SORT_FIELD" => "sort",
                "OFFERS_SORT_FIELD2" => "id",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_ORDER2" => "desc",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Товары",
                "PAGE_ELEMENT_COUNT" => "24",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRICE_CODE" => ["Розничная", 'oldprice'],
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                "PRODUCT_DISPLAY_MODE" => "N",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                "PRODUCT_SUBSCRIPTION" => "Y",
                "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                "RCM_TYPE" => "personal",
                "SECTION_ID" => $newsPreFilterSectionId,
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array("", ""),
                "SEF_MODE" => "N",
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SHOW_ALL_WO_SECTION" => "N",
                "SHOW_CLOSE_POPUP" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_FROM_SECTION" => "N",
                "SHOW_MAX_QUANTITY" => "N",
                "SHOW_OLD_PRICE" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "SHOW_SLIDER" => "Y",
                "SLIDER_INTERVAL" => "3000",
                "SLIDER_PROGRESS" => "N",
                "TEMPLATE_THEME" => "blue",
                "USE_ENHANCED_ECOMMERCE" => "N",
                "USE_MAIN_ELEMENT_SECTION" => "N",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "N",
                'PAGENAV_TMPL' => 'news-detail',
                'INCLUDE_FROM' => 'sales',
                'SALES_SALE_ID' => $arResult['ID'],
            )
        ); ?>

        <? echo '<div class="mobcats_back"></div>'; ?>
    </div>

    <? $APPLICATION->IncludeComponent(
        "bitrix:system.pagenavigation",
        ".default",
        array(
            "NAV_RESULT" => $arResult["NAV_RESULT"],
            "SEF_MODE" => "N",
            "SHOW_NAV_CHAIN" => "N",
        ),
        false
    ); ?>

    <!--        <div class="button_wrap">-->
    <!--            <button class="button button_black show_more"><a href="/"><span>Показать еще</span></a></button>-->
    <!--        </div>-->
    <? } ?>

    <?
    $this->SetViewTarget('catalogSection');
    require_once __DIR__.'/catalogsection.php';
    $this->EndViewTarget();
    ?>

<? } ?>