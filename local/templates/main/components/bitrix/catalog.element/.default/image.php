<?
/**
 * @var $arResult;
 * @var $activeOffer;
 */

$filterNum = 0;
foreach($arResult['offersSets'] as $color => $value) {
    $filterNum++;
    $setId      = $value['setId'];
    $productIds = array_column($value['productIds'], 'ID');

    if (!$productIds) {
        continue;
    }

    $GLOBALS['filterSet'.$filterNum]['ID'] = $productIds;

    $class = '';
    if ($activeOffer['PROPERTIES']['COLOR']['VALUE'] !== $color) {
        $class = ' hiddenpro';
    }
?>
    <div class="look_container container<?=$class?>" data-set-for-color="<?=$color?>">
        <div class="look_block block">

            <div class="block_top">
                <div class="block_title">Образ целиком</div>
            </div>

            <div class="look_content">

                <div class="clook_sliderwrap">
                    <div class="custom_slarrows">
                        <div class="custom_slarrow custom_prev"><svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg></div>
                        <div class="custom_slarrow custom_next"><svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg></div>
                    </div>

                    <div class="clook_slider">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:catalog.section",
                            "catalog_items",
                            array(
                                'INCLUDE_FROM' => 'look',
                                'PRE_ACTIVE_OFFER' => array_column($value['productIds'], 'ACTIVE_OFFER_ID'),
                                "ACTION_VARIABLE" => "action",
                                "ADD_PICT_PROP" => "-",
                                "ADD_PROPERTIES_TO_BASKET" => "Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "ADD_TO_BASKET_ACTION" => "ADD",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "BACKGROUND_IMAGE" => "-",
                                "BASKET_URL" => "/personal/basket.php",
                                "BROWSER_TITLE" => "-",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "COMPATIBLE_MODE" => "Y",
                                "CONVERT_CURRENCY" => "N",
                                "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                                "DETAIL_URL" => "",
                                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_COMPARE" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "ELEMENT_SORT_FIELD" => $_GET["sort"] ?? 'sort',
                                "ELEMENT_SORT_FIELD2" => "id",
                                "ELEMENT_SORT_ORDER" => $_GET["order"] ?? 'asc',
                                "ELEMENT_SORT_ORDER2" => "desc",
                                "ENLARGE_PRODUCT" => "STRICT",
                                "FILTER_NAME" => "filterSet".$filterNum,
                                "HIDE_NOT_AVAILABLE" => "N",
                                "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                                "IBLOCK_ID" => "6",
                                "IBLOCK_TYPE" => "catalog",
                                "INCLUDE_SUBSECTIONS" => "Y",
                                "LABEL_PROP" => array("SOSTAV"),
                                "LABEL_PROP_MOBILE" => array(),
                                "LABEL_PROP_POSITION" => "top-left",
                                "LAZY_LOAD" => "N",
                                "LINE_ELEMENT_COUNT" => "3",
                                "LOAD_ON_SCROLL" => "N",
                                "MESSAGE_404" => "",
                                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                "MESS_BTN_BUY" => "Купить",
                                "MESS_BTN_DETAIL" => "Подробнее",
                                "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                "META_DESCRIPTION" => "-",
                                "META_KEYWORDS" => "-",
                                "OFFERS_FIELD_CODE" => array("", ""),
                                "OFFERS_LIMIT" => "0",
                                "OFFERS_SORT_FIELD" => "sort",
                                "OFFERS_SORT_FIELD2" => "id",
                                "OFFERS_SORT_ORDER" => "asc",
                                "OFFERS_SORT_ORDER2" => "desc",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Товары",
                                "PAGE_ELEMENT_COUNT" => "12",
                                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                "PRICE_CODE" => array("Розничная"),
                                "PRICE_VAT_INCLUDE" => "Y",
                                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                                "PRODUCT_DISPLAY_MODE" => "N",
                                "PRODUCT_ID_VARIABLE" => "id",
                                "PRODUCT_PROPS_VARIABLE" => "prop",
                                "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                                "PRODUCT_SUBSCRIPTION" => "Y",
                                "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                                "RCM_TYPE" => "personal",
                                "SECTION_CODE" => "",
                                "SECTION_ID" => "",
                                "SECTION_ID_VARIABLE" => "SECTION_ID",
                                "SECTION_URL" => "",
                                "SECTION_USER_FIELDS" => array("", ""),
                                "SEF_MODE" => "N",
                                "SET_BROWSER_TITLE" => "Y",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "Y",
                                "SET_META_KEYWORDS" => "Y",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "Y",
                                "SHOW_404" => "N",
                                "SHOW_ALL_WO_SECTION" => "N",
                                "SHOW_CLOSE_POPUP" => "N",
                                "SHOW_DISCOUNT_PERCENT" => "N",
                                "SHOW_FROM_SECTION" => "N",
                                "SHOW_MAX_QUANTITY" => "N",
                                "SHOW_OLD_PRICE" => "N",
                                "SHOW_PRICE_COUNT" => "1",
                                "SHOW_SLIDER" => "Y",
                                "SLIDER_INTERVAL" => "3000",
                                "SLIDER_PROGRESS" => "N",
                                "TEMPLATE_THEME" => "blue",
                                "USE_ENHANCED_ECOMMERCE" => "N",
                                "USE_MAIN_ELEMENT_SECTION" => "N",
                                "USE_PRICE_COUNT" => "N",
                                "USE_PRODUCT_QUANTITY" => "N",
                                'PAGENAV_TMPL' => '',
                                'SLIDER' => true,
                            )
                        ); ?>
                    </div>
                </div>


                <div class="look_allbox" data-set=<?=json_encode(['offerId' => $value['offerId'], 'setId' => $setId, 'productIds' => $productIds])?>>
                    <div class="look_allbox__wrap">
                        <div class="look_allbox__back back_img">
                            <picture>
                                <source type="image/jpg" media="(max-width: 750px)" srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/product2_1x.jpg">
                                <source type="image/jpg" media="(min-width: 751px)" srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/product2_1x.jpg 1x, <?=SITE_MAIN_TEMPLATE_PATH?>/img/product2.jpg 2x">
                                <img data-img1x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/product2_1x.jpg" data-img2x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/product2.jpg" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/product2.jpg" alt="">
                            </picture>
                        </div>
                        <div class="look_allbox__content">
                            <div class="look_allbox__title">Вся коллекция</div>
                            <?
                                $class = '';
                                if (count($productIds) === 3) {
                                    $class = 'margin-left-3';
                                } elseif (count($productIds) === 2) {
                                    $class = 'margin-left-2';
                                } elseif (count($productIds) === 1) {
                                    $class = 'margin-left-1';
                                }
                            ?>
                            <div class="look_allbox__imgs <?=$class?>">
                                <? foreach($value['gambarMini'] as $picturePath) { ?>
                                <div class="look_allbox__img">
                                    <div class="back_img">
                                        <picture>
                                            <source type="image/jpg" srcset="<?=$picturePath?>">
                                            <img data-img1x="<?=$picturePath?>" data-img2x="<?=$picturePath?>" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/blank.png" alt="">
                                        </picture>
                                    </div>
                                </div>
                                <? } ?>
                                <? if (count($productIds) > 3) { ?>
                                    <div class="look_allbox__plus">+<?=count($productIds)-3?></div>
                                <? } ?>
                            </div>
                            <div class="look_allbox__mobtitle complete_look universal_modal__in" data-modalname="look_1" data-lookid="1">Смотреть Весь образ</div>
                            <div class="button_wrap">
                                <button class="button_white_tr complete_look universal_modal__in" data-modalname="look_1" data-lookid="1">Собери весь образ</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
<? } ?>