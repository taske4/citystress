<?php
global $details_svg;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Diag;


/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

?>

<? require 'common.php'?>

<?
/**
 * @global $activeOffer
 * @global $prices
 * @global $priceString
 */

	$offers = [];

	foreach($arResult['OFFERS'] as $offer) {
		$offers[] = [
			'@type' 	    => 'Offer',
			'availability'  => 'http://schema.org/InStock',
			'price'		    => $prices[$offer['ID']]['BASE'],
			'priceCurrency' => 'RUB',
		];
	}

	$chemaOrgProduct = [
		'@context'    => 'https://schema.org',
		'@type'       => 'Product',
		'description' => $arResult['DETAIL_TEXT'],
		'name' 		  => $arResult['NAME'],
		'image'       => 'https://citystress.ru'.\CFile::GetPath($activeOffer['PROPERTIES']['MORE_PHOTO']['VALUE'][0]),
		'offers'      =>  $offers
	];

	$gtmViewItemEvent = [
		'item_id' 		=> $activeOffer['ID'],
		'item_name' 	=> $activeOffer['NAME'],
		'item_category' => $arResult['SECTION']['NAME'],
		'price' 		=> $activeOffer['PRICES']['Розничная']['VALUE'],
	];

	$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$uriString = $request->getRequestUri();

	$image = 'https://citystress.ru'.\CFile::GetPath($activeOffer['PROPERTIES']['MORE_PHOTO']['VALUE'][0]);

	$micromarkupData['twitter']['url'] = $micromarkupData['og']['url'] = 'https://citystress.ru' . $uriString;
	$micromarkupData['twitter']['title'] = $micromarkupData['og']['title'] = $arResult['NAME'];
	$micromarkupData['twitter']['description'] = $micromarkupData['og']['description'] = $arResult['DETAIL_TEXT'];
	$micromarkupData['twitter']['type'] = $micromarkupData['og']['type'] = 'ProductCard';
	$micromarkupData['og']['image'] =
	$micromarkupData['twitter']['image'] = $image;

	$this->SetViewTarget('microdata');

	foreach ($micromarkupData['og'] as $key => $value) {
		echo "<meta property=\"og:$key\" content=\"$value\"/>";
	}
	foreach ($micromarkupData['twitter'] as $key => $value) {
		echo "<meta property=\"twitter:$key\" content=\"$value\"/>";
	}

	$this->EndViewTarget();
?>

<script type="application/ld+json">
	<?=json_encode($chemaOrgProduct);?>
</script>

<script>
	window.dataLayer.push({
		'event': 'view_item',
		'ecommerce': {
			'items': [
				<?=json_encode($gtmViewItemEvent)?>
			]
		}
	});
</script>

<div class="product_container container product_detail_page">
	<div
		class="product_block block fixbox"
		data-all-sizes='<?= json_encode($arResult['allSizes']); ?>'
        data-sizes='<?= json_encode($arResult['sizes']); ?>'
		data-product-id="<?= $arResult['ID'] ?>"
		data-product='<?= json_encode($productData) ?>'
		data-prices='<?=json_encode($prices)?>'
	>

	<div class="product_gallery">

			<div class="breadcrumbs">
			</div>


			<div class="product_gallery__sliderwrap">
				<a href="/catalog/<?=$arResult['SECTION']['CODE'];?>/" class="product_goback">
					<svg fill="none" height="18" viewBox="0 0 23 18" width="23" xmlns="http://www.w3.org/2000/svg"><path d="m.8125 9-.353553-.35355-.353554.35355.353554.35355zm21.3281.5c.2762 0 .5-.22386.5-.5s-.2238-.5-.5-.5zm-13.1504-9.384803-8.531253 8.531253.707103.7071 8.53125-8.531247zm-8.531253 9.238353 8.531253 8.53125.7071-.7071-8.53125-8.53125zm.353553.14645h21.3281v-1h-21.3281z" fill="#242424"/></svg>
				</a>

				<? if ($arResult['tags']) { ?>
				<div class="product_gallery__tags product_gallery__tags2">
					<? foreach($arResult['tags'] as $tag) { ?>
						<?= $tag; ?>
					<? } ?>
				</div>
				<? } ?>

                <? if (count($arResult['OFFERS'] ?: [])) { ?>
                    <? foreach ($arResult['OFFERS'] as $arOffer) { ?>
                        <?
                        $show = $activeOffer['ID'] === $arOffer['ID'];
                        ?>
                        <div class="product_gallery__slider<? if (!$show) { echo ' hiddenpro'; } ?>" data-offer-id="<?= $arOffer['ID'] ?>">
                            <? foreach ($arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
                                <?
                                $pictureUrl = \CFile::GetPath($pictureId);
                                ?>
                                <div class="product_gallery__item">
                                    <a href="<?=$pictureUrl?>"
                                       class="product_gallery__itembox colorbox">
                                        <div class="back_img">
                                            <picture>
                                                <source type="image/jpg" media="(max-width: 750px)"
                                                        srcset="<?=$pictureUrl?>">
                                                <source type="image/jpg" media="(min-width: 751px)"
                                                        srcset="<?=$pictureUrl?> 2x">
                                                <img data-img1x="<?=$pictureUrl?>"
                                                     data-img2x="<?=$pictureUrl?>"
                                                     src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png" alt="">
                                            </picture>
                                        </div>
                                    </a>
                                </div>
                            <? } ?>
                        </div>

                    <? } ?>
                <? } else { ?>
                    <div class="product_gallery__slider">

                        <? foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
                            <?
                            $pictureUrl = CFile::GetPath($pictureId);
                            ?>
                            <div class="product_gallery__item">
                                <a href="<?=$pictureUrl?>"
                                   class="product_gallery__itembox colorbox">
                                    <div class="back_img">
                                        <picture>
                                            <source type="image/jpg" media="(max-width: 750px)"
                                                    srcset="<?=$pictureUrl?>">
                                            <source type="image/jpg" media="(min-width: 751px)"
                                                    srcset="<?=$pictureUrl?> 2x">
                                            <img data-img1x="<?=$pictureUrl?>"
                                                 data-img2x="<?=$pictureUrl?>"
                                                 src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png" alt="">
                                        </picture>
                                    </div>
                                </a>
                            </div>
                        <? } ?>
                    </div>
                <? } ?>


				<div class="product_gallery__nav">
					<div data-index="0" class="product_gallery__navitem active"></div>
					<div data-index="1" class="product_gallery__navitem"></div>
					<div data-index="2" class="product_gallery__navitem"></div>
					<div data-index="3" class="product_gallery__navitem"></div>
					<div data-index="4" class="product_gallery__navitem"></div>
					<div data-index="5" class="product_gallery__navitem"></div>
					<div data-index="6" class="product_gallery__navitem"></div>
				</div>
			</div>


			<? if (count($arResult['OFFERS'] ?: [])) { ?>
				<? foreach ($arResult['OFFERS'] as $arOffer) { ?>
					<?
					$show = $activeOffer['ID'] === $arOffer['ID'];
					$inRow = $i = 0;
					$maxInRow = (count($arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] ?: []) <= 6) ? 2 : 3;
					?>
					<div class="product_gallery__items<? if (!$show) { echo ' hiddenpro'; } ?>" data-offer-id="<?= $arOffer['ID'] ?>">
						<? if ($arOffer['SALE']['NAME']) { ?>
						<div class="product_gallery__tag" style="display: block!important; z-index: 2; position: absolute; top: 1em; right: 1em; background: #FE7779; color: #FFF"><?=$arOffer['SALE']['NAME']?></div>
						<? } ?>
						<? foreach ($arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
							<?
							$x = ($i <= 2) ? 2 : $maxInRow;

							$pictureUrl = \CFile::GetPath($pictureId);
							if ($inRow === 0) {
								$x = ($i === 0) ? 2 : $maxInRow;
								echo '<div class="product_gallery__row cols_'.$x.'">';
							}

							?>
							<div class="product_gallery__item">
								<a href="<?=$pictureUrl?>"
								   class="product_gallery__itembox colorbox">
									<div class="back_img">
										<picture>
											<source type="image/jpg" media="(max-width: 750px)"
													srcset="<?=$pictureUrl?>">
											<source type="image/jpg" media="(min-width: 751px)"
													srcset="<?=$pictureUrl?> 2x">
											<img data-img1x="<?=$pictureUrl?>"
												 data-img2x="<?=$pictureUrl?>"
												 src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png" alt="">
										</picture>
									</div>
								</a>
							</div>
							<?
								$inRow++;

								$x = ($i <= 2) ? 2 : $maxInRow;

								if ($inRow === $x || (($i + 1) === count($arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] ?: []))) {
									$inRow = 0;
									?>
									</div>
									<?
								}

								$i++;
							?>
						<? } ?>
					</div>

				<? } ?>
			<? } else {/* ?>
				<div class="product_gallery__items">

					<? foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
						<?
						$pictureUrl = CFile::GetPath($pictureId);
						?>
						<div class="product_gallery__item">
							<a href="<?=$pictureUrl?>"
							   class="product_gallery__itembox colorbox">
								<div class="back_img">
									<picture>
										<source type="image/jpg" media="(max-width: 750px)"
												srcset="<?=$pictureUrl?>">
										<source type="image/jpg" media="(min-width: 751px)"
												srcset="<?=$pictureUrl?> 2x">
										<img data-img1x="<?=$pictureUrl?>"
											 data-img2x="<?=$pictureUrl?>"
											 src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png" alt="">
									</picture>
								</div>
							</a>
						</div>
					<? } ?>
				</div>
			<? */} ?>

		</div>

		<div class="product_info">
			<div class="product_mobpanel">
				<div class="product_titlebox">
					<h1><?=$productName?></h1>
					<div class="product_mobtitle"><?=$productName?></div> <!-- средствами css тут не получится обрезать длину, учитывая другие элементы. нужно делать движком -->
					<div class="product_article"><?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?></div>
				</div>

				<div class="product_pricebox">
					<? $discounted = $prices[$activeOffer['ID']]['DISCOUNTED']; ?>
					<span class="product_price" style="padding: 0.25em"><?=$prices[$activeOffer['ID']]['BASE']?></span>
					<? if ($prices[$activeOffer['ID']]['OLD']) { ?>
						<span class="product_priceold" style="<?if ($prices[$activeOffer['ID']]['OLD']) {?>padding: 0.25em<? } ?>"><?=$prices[$activeOffer['ID']]['OLD']?></span>
					<? } ?>
					<? if ($prices[$activeOffer['ID']]['DISCOUNTED']) {?>
						<span class="product_price_discounted" style="<?if ($prices[$activeOffer['ID']]['DISCOUNTED']) {?>padding: 0.25em<? } ?>"><?=$discounted?></span>
					<? } ?>

					<ul class="product_tags">
						<? if (($diffPercent = $prices[$activeOffer['ID']]['DIFF_PERCENT'])) { ?>
							<li class="price_diff_percent">-<?=$diffPercent?>%</li>
						<? } ?>
<!--						<li>YEAR 2024</li>-->

						<?
							if ($arResult['PROPERTIES']['NEW']['VALUE']) { ?>
								<li class="dark">NEW</li>
							<? }
						?>
					</ul>
				</div>

				<div class="product_mob__actions">
					<div class="product_mob__chosensize">
						<span><?=$activeOffer['PROPERTIES']['SIZE']['VALUE']?></span>
						<svg fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m14 6-5.5 5-5.5-5" stroke="#a8a8a8" stroke-linecap="round" stroke-linejoin="round"/></svg>
					</div>
					<button class="button button_black">В корзин<i>у</i><em>е</em></button>
				</div>
			</div>


			<div class="product_mobsizesback modal_close"></div>

            <? if ($allSizes) { ?>
			<div class="product_sizes">
				<div class="product_sizes__label">Размеры</div>
                <span class="product_sizes_tablein universal_modal__in" data-modalname="sizetable">Таблица размеров</span>


                <svg class="product_mobsizes__close modal_close" fill="none" height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m2.22468.811082c-.39052-.390525-1.02368-.390525-1.414209 0-.390524.390528-.390524 1.023688 0 1.414218l5.774739 5.77473-5.774679 5.77467c-.390524.3905-.390524 1.0237 0 1.4142.390529.3905 1.023689.3905 1.414209 0l5.77468-5.77466 5.77468 5.77466c.3905.3905 1.0237.3905 1.4142 0s.3905-1.0237 0-1.4142l-5.77467-5.77467 5.77477-5.77473c.3905-.39053.3905-1.02369 0-1.414218-.3906-.390525-1.0237-.390525-1.4142 0l-5.77478 5.774738z" fill="#fff"/></svg>

				<div class="sizes">
                    <? foreach ($allSizes as $index => $size) { ?>
                    <?
                        $active = $available = false;
                        $offerId = null;

                        if ($offerId = $sizes[$size]) {
                            $available = true;
                        }

                        if ($activeOffer['PROPERTIES']['SIZE']['VALUE'] === $size) {
                            $active = true;
                        }
                    ?>
						<div
							class="product_size<? if ($active) { ?> active<? } ?><? if (!$available) { ?> disabled<? } ?>"
							data-offer-id="<?= $offerId ?>"
							data-title="<?= $size ?>"
							data-subtitle="<?= $size ?>"
						><?= $size ?></div>
					<? } ?>
				</div>

<!--				<div class="product_sizes__info">-->
<!--					Обхват талии, см: <strong>66-72</strong>.<br>-->
<!--					Обхват бедер, см: <strong>94-98</strong>-->
<!--				</div>-->

				<button class="button button_black modal_close">Выбрать размеры</button>
			</div>


            <? } ?>

			<? if ($arResult['tableSizeAvailable']) { ?>
				<span class="product_sizes_tablein universal_modal__in" data-modalname="sizetable">Таблица размеров</span>
			<? } ?>

            <? if ($arResult['colors']) { ?>
			<div class="product_color">
				<div class="product_color__label">Цвет</div>
                <? foreach ($arResult['colors'] as $hex => $color) { ?>
                    <?
                    $offerId = array_search(current($arResult['sizes'][$color]), array_column($arResult['OFFERS'], 'ID'));
                    $picture = current($arResult['OFFERS'][$offerId]['PROPERTIES']['MORE_PHOTO']['VALUE'] ?: []);

                    $pictureUrl = $picture ? \CFile::GetPath($picture) : null;
                    ?>
                    <div data-color="<?=$color?>" data-offer-id="<?=$arResult['OFFERS'][$offerId]['ID']?>" class="product_color__item<?= (array_search($activeOffer['ID'], $arResult['sizes'][$color])) ? ' active' : '' ?>">
                        <div class="back_img">
                            <picture>
                                <source type="image/jpg" srcset="<?=$pictureUrl?>">
                                <img data-img1x="<?=$pictureUrl?>" data-img2x="<?=$pictureUrl?>" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/blank.png" alt="">
                            </picture>
                        </div>
                        <span><?=$color?></span>
                    </div>
                <? } ?>
			</div>
            <? } ?>


			<div class="product_actions">

				<div class="product_add">
					<a href="" class="product_tocart">
						<button type="button" class="button button_black">Добавить в корзину</button>
					</a>

					<div class="product_tofav">
						<svg class="product_tofav__icon1" fill="none" height="23" viewBox="0 0 23 23" width="23" xmlns="http://www.w3.org/2000/svg"><path d="m11.8306 5.87322c.0034.00376.0033.00382.0033.00382l.0056-.00496c.0053-.00468.014-.01236.0261-.02273.0243-.02076.0619-.05225.1119-.092.1-.0796.2489-.19177.4387-.31706.3813-.25181.9177-.54997 1.5465-.74725 1.2339-.3871 2.8278-.3955 4.3829 1.16717l.0049.00485.0037.0036.0028.00266c.0029.00292.0083.00821.0157.01582.0149.01522.0383.03968.0685.07296.0603.06664.1471.16809.2462.30098.199.26684.4431.65411.6256 1.13546.3585.94568.4913 2.27708-.4899 3.83946l-.0402.064-.0129.0491c-.0012.0024-.0026.0054-.0043.0089-.0138.0287-.0463.0903-.1124.1915-.1326.2031-.3928.5543-.8893 1.1054-.9332 1.0358-2.6806 2.7555-5.9334 5.4983-3.25279-2.7428-5.00024-4.4625-5.93345-5.4983-.4965-.5511-.75672-.9023-.88933-1.1054-.06603-.1012-.09854-.1628-.1124-.1915-.00169-.0035-.00308-.0065-.00421-.0089l-.01298-.0491-.04021-.064c-.98118-1.56238-.84836-2.89378-.48984-3.83946.18248-.48135.42659-.86862.62561-1.13546.09911-.13289.18586-.23434.2462-.30098.03013-.03328.05354-.05774.06846-.07296.00746-.00761.01278-.0129.01574-.01582l.00251-.00245.00401-.00381.00483-.00485c1.55515-1.56267 3.14904-1.55427 4.38291-1.16717.62885.19728 1.16515.49544 1.54655.74725.1897.12529.3386.23746.4387.31706.05.03975.0876.07124.1118.092.0121.01037.0209.01805.0262.02273l.0056.00496s-.0001-.00006.0033-.00382zm0-1.23581c-.0127-.00848-.0255-.01702-.0385-.0256-.4331-.28595-1.054-.63313-1.79646-.86606-1.50777-.47302-3.51117-.46494-5.38015 1.41046l-.00766.00748c-.00711.007-.01667.01653-.02844.02854-.02354.02402-.05597.058-.09539.10155-.07876.08699-.18592.21268-.30605.37375-.23947.32108-.53519.78907-.75811 1.37707-.44877 1.18375-.59028 2.83083.5505 4.6762.00847.0204.01867.0433.03099.0688.03572.074.09047.1729.17517.3026.16885.2586.46398.651.98304 1.2272 1.02959 1.1428 2.96304 3.0336 6.59826 6.0686l.0597.0717c.0044-.0037.0087-.0073.0131-.0109.0043.0036.0087.0072.0131.0109l.0597-.0717c3.6352-3.035 5.5686-4.9258 6.5982-6.0686.5191-.5762.8142-.9686.9831-1.2272.0847-.1297.1394-.2286.1751-.3026.0124-.0255.0226-.0484.031-.0688 1.1408-1.84537.9993-3.49245.5505-4.6762-.2229-.588-.5186-1.05599-.7581-1.37707-.1201-.16107-.2273-.28676-.306-.37375-.0395-.04355-.0719-.07753-.0954-.10155-.0118-.01201-.0214-.02154-.0285-.02854l-.0076-.00748c-1.869-1.8754-3.8724-1.88348-5.3802-1.41046-.7424.23293-1.3633.58011-1.7964.86606-.013.00858-.0258.01712-.0385.0256z" style="fill-rule:evenodd;clip-rule:evenodd; stroke-miterlimit:10;stroke-linejoin:round"/></svg>
						<svg class="product_tofav__icon2" fill="none" height="16" viewBox="0 0 17 16" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m8.46254 1.12912.03746.02564.03746-.02564c.42108-.286396 1.02481-.634106 1.74674-.867396 1.4661-.473759 3.4141-.46567 5.2314 1.412646l.0074.00749c.007.00701.0162.01655.0277.02858.0229.02406.0544.0581.0927.10171.0766.08713.1808.213.2976.37433.2329.32158.5204.7903.7372 1.3792.4363 1.18559.5739 2.83522-.5353 4.68342-.0082.02049-.0182.04339-.0301.06895-.0348.07412-.088.17312-.1704.30305-.1641.25905-.4511.65205-.9558 1.22909-1.0011 1.14461-2.8811 3.03831-6.41582 6.07811l-.05807.0717-.01271-.0109-.01271.0109-.05807-.0717c-3.53469-3.0398-5.41468-4.9335-6.4158-6.07811-.50471-.57704-.79168-.97004-.95586-1.22909-.082358-.12993-.135597-.22893-.170328-.30305-.011977-.02556-.021897-.04846-.030132-.06895-1.109239-1.8482-.971639-3.49783-.535276-4.68342.216749-.5889.504294-1.05762.737146-1.3792.11681-.16133.221-.2872.29759-.37433.03833-.04361.06986-.07765.09275-.10171.01144-.01203.02074-.02157.02766-.02858l.00744-.0075c1.8173-1.878306 3.76531-1.886395 5.23139-1.412636.72193.23329 1.32566.581 1.74674.867396z" fill="#fff"/></svg>
					</div>

					<button type="button" class="button button_black_tr product_oneclick universal_modal__in" data-modalname="oneclick">Купить в 1 клик</button>

					<div class="product_share">
						<svg fill="none" height="27" viewBox="0 0 27 27" width="27" xmlns="http://www.w3.org/2000/svg"><path d="m9.51512 7.96572 9.58728-.00014m0 0v9.45092m0-9.45092-11.13697 11.13692" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></svg>
					</div>
				</div>

				<div class="product_sidetags">
					<? foreach($arResult['OFFERS'] as $arOffer) { ?>
						<?
							if (!$arOffer['SALE']) {
								continue;
							}

							$show = $activeOffer['ID'] === $arOffer['ID'];
						?>
						<div class="in_action item_w_hint <?= $show ?: 'hiddenpro' ?>" data-offer-id="<?=$arOffer['ID']?>">
							<div class="item_w_hint__title">Участвует в акции <svg fill="none" height="14" viewBox="0 0 14 14" width="14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><clipPath id="a"><path d="m0 0h14v14h-14z" transform="matrix(-1 0 0 1 14 0)"></path></clipPath><g clip-path="url(#a)" fill="#242424"><path clip-rule="evenodd" d="m7 0c3.866 0 7 3.13401 7 7 0 3.866-3.134 7-7 7-3.86599 0-7-3.134-7-7 0-3.86599 3.13401-7 7-7zm5.6 7c0 3.0928-2.5072 5.6-5.6 5.6-3.09279 0-5.6-2.5072-5.6-5.6 0-3.09279 2.50721-5.6 5.6-5.6 3.0928 0 5.6 2.50721 5.6 5.6z" fill-rule="evenodd"></path><path d="m6.62384 7.90833c0-.28889.0571-.52777.17132-.71666.11918-.18889.2905-.39723.51396-.625.16388-.16667.28306-.30556.35755-.41667.07945-.11667.11918-.24722.11918-.39167 0-.20555-.07449-.36666-.22347-.48333-.14401-.12222-.33767-.18333-.581-.18333-.2334 0-.44196.05555-.6257.16666-.17877.10556-.33023.25556-.45438.45l-.9013-.59166c.20857-.35556.48914-.63056.84171-.825.35755-.19445.77964-.29167 1.2663-.29167.57107 0 1.02793.13611 1.37057.40833.34762.27223.52142.65.52142 1.13334 0 .22777-.03476.42777-.10428.6-.06456.17222-.1465.31944-.24581.44166-.09435.11667-.2185.25278-.37244.40834-.18374.18333-.31782.33889-.40224.46666-.08442.12223-.12663.27223-.12663.45zm.5661 2.09167c-.2036 0-.37492-.07222-.51396-.21667-.13408-.15-.20112-.33055-.20112-.54166s.06704-.38611.20112-.525c.13408-.14445.3054-.21667.51396-.21667.20857 0 .37989.07222.51397.21667.13408.13889.20112.31389.20112.525s-.06952.39166-.20857.54166c-.13408.14445-.30292.21667-.50652.21667z"></path></g></svg></div>
							<div class="hint">
								<img class="hint_close" src="/img/close2.svg" alt="">
								<div class="hint_title">Товар участвует в акции</div>
								<div class="hint_text"><a href="/news-sales/<?=$arOffer['SALE']['CODE']?>/"><?=$arOffer['SALE']['NAME']?></a></div>
							</div>
						</div>
					<? } ?>
					<?
					if ($arResult['OFFERS']) {
						foreach ($arResult['OFFERS'] as $arOffer) {
							$show = $activeOffer['ID'] === $arOffer['ID'];
						}
					}
					?>

					<? if ($arResult['PROPERTIES']['ON_SALE_SOON']['VALUE']) { ?>
					<div class="online_only item_w_hint">
						<div class="item_w_hint__title">Скоро в продаже
							<? if ($arResult['PROPERTIES']['ON_SALE_SOON_INFO']['VALUE']) { ?>
							<?php echo $details_svg;?>
							<? } ?>
						</div>
						<? if ($arResult['PROPERTIES']['ON_SALE_SOON_INFO']['VALUE']) { ?>
						<div class="hint">
							<img class="hint_close" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/close2.svg" alt="">
							<div class="hint_title">Скоро в продаже</div>
							<div class="hint_text"><?=$arResult['PROPERTIES']['ON_SALE_SOON_INFO']['VALUE']?></div>
						</div>
						<? } ?>
					</div>
					<? } ?>

					<? if ($arResult['PROPERTIES']['ONLINE_ONLY']['VALUE']) { ?>
					<div class="online_only item_w_hint">
						<div class="item_w_hint__title">Только онлайн
							<? if ($arResult['PROPERTIES']['ONLINE_ONLY_INFO']['VALUE']) { ?>
							<?php echo $details_svg;?>
							<? } ?>
						</div>
						<? if ($arResult['PROPERTIES']['ONLINE_ONLY_INFO']['VALUE']) { ?>
						<div class="hint">
							<img class="hint_close" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/close2.svg" alt="">
							<div class="hint_title">Только онлайн</div>
							<div class="hint_text"><?=$arResult['PROPERTIES']['ONLINE_ONLY_INFO']['VALUE']?></div>
						</div>
						<? } ?>
					</div>
					<? } ?>

 			<?if($arResult['AMOUNT'][$activeOffer['ID']]>0):?>
<div class="product_instore universal_modal__in" data-modalname="instore">в <?=$arResult['AMOUNT'][$activeOffer['ID']]?> <?=plural($arResult['AMOUNT'][$activeOffer['ID']], 'магазине', 'магазинах', 'магазинах');?></div>
			<?endif;?>

				</div>

			</div>

            <?/*
			<div class="podeli">
				<img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/chyastyami.svg" alt="" class="podeli_logo">
				<div class="podeli_calc">4-мя платежами по<br> 749.75 ₽ без переплат <?php echo $details_svg;?></div>
				<div class="podeli_steps">
					<div class="podeli_step active">
						<div class="podeli_step__date">Сегодня</div>
						<div class="podeli_step__sum">749.75 ₽</div>
						<div class="podeli_step__line"></div>
					</div>
					<div class="podeli_step">
						<div class="podeli_step__date">27 апреля</div>
						<div class="podeli_step__sum">749.75 ₽</div>
						<div class="podeli_step__line"></div>
					</div>
					<div class="podeli_step">
						<div class="podeli_step__date">11 мая</div>
						<div class="podeli_step__sum">749.75 ₽</div>
						<div class="podeli_step__line"></div>
					</div>
					<div class="podeli_step">
						<div class="podeli_step__date">25 мая</div>
						<div class="podeli_step__sum">749.75 ₽</div>
						<div class="podeli_step__line"></div>
					</div>
				</div>
			</div>
            */?>

			<div class="product_menu">


				<div class="product_menuitem">
					<div class="product_menuitem__title">Доставка <?php echo $chevron_svg;?></div>
					<div class="product_menuitem__content product_delivery">
						<p>Бесплатная доставка в ПВЗ при заказе от 10 000 руб.<br> Возможные способы доставки товара:</p>
						<ul>
							<li>
								<img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/delivery1.svg" alt="">
								<strong>Курьерская доставка</strong>
								<span>Срок - от 2 дней</span>
							</li>
							<li>
								<img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/delivery2.svg" alt="">
								<strong>Доставка в пункты выдачи заказов и постаматы</strong>
								<span>Срок - от 2 дней</span>
							</li>
							<li>
								<img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/delivery3.svg" alt="">
								<strong>Самовывозом из магазина</strong>
								<span>Срок - от 1 дня</span>
							</li>
						</ul>
						<p>Более подробная информация в разделе Доставка</p>
					</div>
				</div>



				<div class="product_menuitem">
					<div class="product_menuitem__title">Оплата <?php echo $chevron_svg;?></div>
					<div class="product_menuitem__content product_payment">
						<p>Вы можете оплатить заказ удобным для вас способом:</p>
						<ul>
							<li>Банковской картой на сайте</li>
							<li>По СПБ (QR-коду)</li>
							<li>Наличными или картой при получении заказа</li>
							<li>Рассрочка от Халвы</li>
						</ul>
					</div>
				</div>


				<div class="product_menuitem">
					<div class="product_menuitem__title">О товаре <?php echo $chevron_svg;?></div>
                    <? if ($arResult['OFFERS']) { ?>
                        <? foreach($arResult['OFFERS'] as $arOffer) { ?>
                            <? $show = $activeOffer['ID'] === $arOffer['ID']; ?>
                            <div class="offer-content product_menuitem__content product_details<? if (!$show) { echo ' hiddenpro'; } ?>" data-offer-id="<?=$arOffer['ID']?>">
                                <ul>
                                    <li><strong>Артикул:</strong> <?=$arOffer['PROPERTIES']['CML2_ARTICLE']['VALUE'];?></li>
                                    <li><strong>Цвет:</strong> <?=$arOffer['PROPERTIES']['COLOR']['VALUE'];?></li>
									<? if ($season = $arResult['PROPERTIES']['COUNTRY']['VALUE']) { ?>
										<li><strong>Страна дизайна:</strong> <?=$arResult['PROPERTIES']['COUNTRY']['VALUE']?></li>
									<? } ?>
                                    <? if ($season = $arResult['PROPERTIES']['SEASON']['VALUE']) { ?>
                                        <li><strong>Сезон:</strong> <?=$season;?></li>
                                    <? } ?>
                                </ul>
                                <ul>
                                    <? if ($arOffer['PROPERTIES']['SIZE_ON_MODEL']['VALUE']) { ?>
                                        <li><strong>На модели
                                                размер:</strong> <?= $arOffer['PROPERTIES']['SIZE_ON_MODEL']['VALUE'] ?></li>
                                    <? } ?>
                                    <? if ($arOffer['PROPERTIES']['HEIGHT_MODEL_ON_PHOTO']['VALUE']) { ?>
                                        <li><strong>Рост модели на фото,
                                                см:</strong> <?= $arOffer['PROPERTIES']['HEIGHT_MODEL_ON_PHOTO']['VALUE'] ?></li>
                                    <? } ?>
                                    <? if ($arOffer['PROPERTIES']['PARAMS_MODEL_ON_PHOTO']['VALUE']) { ?>
                                        <li><strong>Параметры модели на фото,
                                                см:</strong> <?= implode(', ', $arOffer['PROPERTIES']['PARAMS_MODEL_ON_PHOTO']['VALUE']); ?></li>
                                    <? } ?>
                               </ul>

								<? if (isset($arOffer['PROPERTIES']['DESCRIPTION']['VALUE']['TEXT'])) { ?>
									<p><?=$arOffer['PROPERTIES']['DESCRIPTION']['VALUE']['TEXT']?></p>
								<? } ?>

								<? if (count($arOffer['PROPERTIES']['SEO_WORDS']['VALUE'] ?: [])) { ?>
									<div class="product_details__tags">
										<? foreach ($arOffer['PROPERTIES']['SEO_WORDS']['VALUE'] as $value) { ?>
											<span><?=trim($value);?></span>
										<? } ?>
									</div>
								<? } ?>

								<? if ($compound = $arResult['PROPERTIES']['SOSTAV']['VALUE']) { ?>
									<div class="product_madeof"><strong>Состав:</strong> <?=$compound?></div>
<!--								--><?// if ($compound = $arResult['PROPERTIES']['COMPOUND']['VALUE']) { ?>
<!--                                    <div class="product_madeof"><strong>Состав:</strong> --><?php //= implode(', ', $compound); ?><!--</div>-->
                                <? } ?>

								<? if ($care = $arResult['PROPERTIES']['CARE']['VALUE']) { ?>
									<?
										$carePictures = \Bitrix\Iblock\Elements\ElementCareTable::query()
											->setSelect(['PREVIEW_PICTURE'])
											->setFilter([
												'ID' => $care
											])
											->fetchAll();

										$carePictures = array_column($carePictures, 'PREVIEW_PICTURE');
									?>

									<? if ($carePictures) { ?>
										<div class="product_care__title">Уход</div>
										<div class="product_care">
											<? foreach ($carePictures as $pictureId) { ?>
												<img src="<?= CFile::GetPath($pictureId)?>" alt="">
											<? } ?>
										</div>
									<? } ?>
								<? } ?>
                            </div>
                        <? } ?>
                    <? } else { ?>
                        <div class="product_menuitem__content product_details">
                            <ul>
                                <li><strong>Артикул:</strong> <?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?></li>
                                <li><strong>Страна дизайна:</strong> <?=$arResult['PROPERTIES']['COUNTRY']['VALUE']?></li>
                                <? if ($season = $arResult['PROPERTIES']['SEASON']['VALUE']) { ?>
                                    <li><strong>Сезон:</strong> <?=$season;?></li>
                                <? } ?>
                            </ul>
                            </ul>
                            <ul>

                                <? if ($arResult['PROPERTIES']['MODEL_GROWTH']['VALUE']) { ?>
                                    <li><strong>Рост модели на фото,
                                            см:</strong> <?= $arResult['PROPERTIES']['MODEL_GROWTH']['VALUE'] ?></li>
                                <? } ?>
                                <? if ($arResult['PROPERTIES']['MODEL_PARAMS']['VALUE']) { ?>
                                    <li><strong>Параметры модели на фото,
                                            см:</strong> <?= $arResult['PROPERTIES']['MODEL_PARAMS']['VALUE'] ?></li>
                                <? } ?>
                            </ul>
                            <p>Удлиненное теплое пальто-халат с однотонным поясом. Есть два глубоких кармана в боковом шве. Рукав реглан, прямой крой, длинна миди. Классическое женское пальто сделает ваш образ лаконичным и женственным.</p>
                            <div class="product_details__tags">
                                <span>Женская</span>
                                <span>Купальники</span>
                                <span>Боди</span>
                                <span>С прорезью</span>
                                <span>Без рисунка</span>
                                <span>Синий</span>
                                <span>Черный</span>
                            </div>
                            <div class="product_madeof"><strong>Состав:</strong> 100% хлопок</div>
                            <? if ($compound = $arResult['PROPERTIES']['COMPOUND']['VALUE']) { ?>
                                <div class="product_madeof"><strong>Состав:</strong> <?= implode(', ', $compound); ?></div>
                            <? } ?>
                            <div class="product_care__title">Уход</div>
                            <div class="product_care">
                                <img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/care1.svg" alt="">
                                <img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/care2.svg" alt="">
                                <img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/care3.svg" alt="">
                                <img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/care4.svg" alt="">
                                <img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/care5.svg" alt="">
                                <img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/care6.svg" alt="">
                            </div>
                        </div>
                    <? } ?>
				</div>


			</div>
		</div>

	</div>
	<div class="instore mappoints_box modal universal_modal__instore"> <!-- класс store_view переводит модалку в режим уже выбранного магазина -->

  <div class="mappoints_head modal_head">
    <span class="mappoints_head__title">Наличие в магазинах</span>
    <span class="mappoints_head__storetitle">Заказать в магазине</span>
    <svg class="mappoints_mobclose modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg>
  </div>

  <div class="mappoints_close modal_close">
    <svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".75"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17578 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg>
  </div>
  <div class="mappoints_body">
    <div class="customscroll">

      <div class="mappoints_inside">
        <div class="mappoints_title">Наличие<br> в магазинах</div>

        <div class="mappoints_city universal_modal__in" data-modalname="cityselect">
          <div class="mappoints_city__chose">Выберите город</div>
          <span><?=$_SESSION["CITY"]["NAME"]?></span>
          <svg fill="none" height="16" viewBox="0 0 15 16" width="15" xmlns="http://www.w3.org/2000/svg"><path d="m3 6.5 4.5 4 4.5-4" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
        </div>

        <!-- В режиме магазина -->

        <div class="instore_storeinfo">
          <div class="instore_shoptitle">Заказать<br> в магазине</div>

          <ul class="store_info">
            <li class="store_title"></li>
            <li class="store_address"></li>
            <li class="store_worktime"></li>
            <li class="store_phone"></li>
          </ul>

          <div class="instore_pickother">
            <span>Выбрать другой магазин</span>
            <svg fill="none" height="16" viewBox="0 0 15 16" width="15" xmlns="http://www.w3.org/2000/svg"><path d="m3 6.5 4.5 4 4.5-4" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
          </div>
        </div>

        <!-- / В режиме магазина -->

        <div data-zoom="10" data-center="[<?=$_SESSION["LOCATION_LATITUDE"]?>,<?=$_SESSION["LOCATION_LONGITUDE"]?>]" class="mappoints_mobmap_in"></div> <!-- мобильная версия карты со своими настройками zoom и center -->

        <div class="instore_product product_mini" data-product-id="<?=$arResult['ID']?>" data-product='<?= json_encode($productData) ?>'>
         	<div class="product_mini__img">
		<div class="product_mini__imgbox">
			<? foreach ($arResult['OFFERS'] as $offer) { ?>
				<?
                    $picture = current($offer['PROPERTIES']['MORE_PHOTO']['VALUE'] ?: []);

                    if (!$picture) {
                        continue;
                    }

					$pictureUrl = CFile::GetPath($picture);
				?>
				<div class="back_img <?= ($offer['ID'] == $activeOffer['ID']) ? '' : ' hiddenpro' ?>" data-picture-offer="<?=$offer['ID']?>">
					<picture>
						<source type="image/jpg" media="(max-width: 750px)" srcset="<?=$pictureUrl?>">
						<source type="image/jpg" media="(min-width: 751px)" srcset="<?=$pictureUrl?> 1x, <?=$pictureUrl?> 2x">
						<img data-img1x="<?=$pictureUrl?>" data-img2x="<?=$pictureUrl?>" src="<?=$pictureUrl?>" alt="">
					</picture>
				</div>
			<? } ?>
		</div>
	</div>
          <div class="product_mini__box">
            <div class="product_mini__row">
              <div class="product_mini__article"><?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?></div>
              <!--div class="product_mini__priceold"><?=$priceString?></div-->
            </div>
            <div class="product_mini__row">
              <div class="product_mini__title"><?=$productName?></div>
              <div class="product_mini__price"><?=$priceString?></div>
            </div>
            <div class="product_mini__color">
			<? foreach ($arResult['colors'] as $hex => $color) { ?>
				<? if (array_search($activeOffer['ID'], $arResult['sizes'][$color])) { ?>
					<div class="product_mini__colorchosen"><span style="background: <?=$hex?>;"></span><i><?=$color?></i></div>
				<? } ?>
			<? } ?>
			<ul class="product_mini__colorlist">
				<? foreach ($arResult['colors'] as $hex => $color) { ?>
					<?
						$offerId = array_search(current($arResult['sizes'][$color]), array_column($arResult['OFFERS'], 'ID'));
                        $picture = current($arResult['OFFERS'][$offerId]['PROPERTIES']['MORE_PHOTO']['VALUE'] ?: []);

                        $pictureUrl = $picture ? CFile::GetPath($picture) : null;
					?>
					<li class="<?= (array_search($activeOffer['ID'], $arResult['sizes'][$color])) ? 'active' : '' ?>"><div data-color-offer='<?=json_encode($arResult['sizes'][$color])?>' data-color="<?=$hex?>" style="background: <?=$hex?>;"></div><span><?=$color?></span></li>
				<? } ?>
			</ul>
		</div>
  <div class="product_mini__options">
			<div class="product_mini__sizes">
				<? foreach ($arResult['colors'] as $hex => $color) { ?>
					<? if (array_search($activeOffer['ID'], $arResult['sizes'][$color])) { ?>
						<? foreach ($arResult['sizes'][$color] as $size => $offerId) { ?>
							<button class="instore_offer product_mini__size <?if($offerId==$activeOffer['ID']):?>active<?endif;?>" data-offer-id="<?=$offerId?>"><?=$size?></button>
						<? } ?>
					<? } ?>
				<? } ?>
			</div>
            </div>
          </div>
        </div>

        <!-- В режиме магазина -->

        <div class="instore_orderinfo">
          <ul class="instore_orderlist">
            <li><strong>Срок резерва:</strong> 2 дня</li>
            <li><strong>Способ оплаты:</strong> банковской картой онлайн</li>
            <li>Вы сможете забрать ваш заказ после получения смс в готовности заказа</li>
          </ul>

          <form class="form_v1 form_instock">
            <input type="hidden" class="form_v1__in">
            <div class="fields">
              <div class="field_wrap">
                <input type="text" name="instock_name" class="field required" placeholder="Ф.И.О.*">
              </div>
              <div class="field_wrap">
                <input type="text" name="instock_phone" class="field field_phone" placeholder="Номер телефона*">
              </div>
              <div class="field_wrap">
                <input type="text" name="instock_email" class="field field_email" placeholder="Введите ваш email*">
              </div>
              <div class="form_agreement">
                Нажимая на кнопку “Оформить заказ” я подтверждаю согласие с <a href="">Политикой конфиденциальности</a> и <a href="">Офертой</a>, а также даю согласие на обработку персональных данных
              </div>
              <div class="button_wrap">
                <button type="button" class="button button_black">
                  <span>Оплатить заказ</span>
                </button>
              </div>
            </div>
          </form>
        </div>

        <!-- / В режиме магазина -->

        <div class="instore_chosestore mappoints_data">
          <ul class="instore_stores">
   <?foreach( $arResult['STORES'][$activeOffer['ID']] as $store):?>
<li class="instore_store mappoints_data__item" data-coos="[<?=$store['COORDS'][0]?>,<?=$store['COORDS'][1]?>]" data-address="<?=$store['NAME']?>" data-worktime="<?=$store["SCHEDULE"]?>"  data-phone="<?=$store["PHONE"]?>">
              <div class="instore_store__icon"></div>
              <strong><?=$store['NAME']?></strong>
			  <?if($store['AMOUNT']>0){?>
              <p>Есть в наличии</p>
			  <?}else{?>
			  <p>Нет в наличии</p>
			  <?}?>
            </li>
   <?endforeach;?>
          </ul>
          <button class="button button_black instore_button">Выбрать магазин</button>
        </div>

      </div>
    </div>
  </div>

  <div data-zoom="12" data-center="[<?=$_SESSION["LOCATION_LATITUDE"]?>,<?=$_SESSION["LOCATION_LONGITUDE"]?>]" class="mappoints_map_in"></div>

  <div class="mappoints_map ya_map"></div>
</div>
</div>

<?
	require 'image.php';
?>

<div class="recently_container container">
	<div class="recently_block block">

		<div class="block_top">
			<div class="block_title">Вы <span>недавно</span> смотрели</div>
<!--			<a href="/" class="go_section">в категорию <svg fill="none" height="11" viewBox="0 0 19 11" width="19" xmlns="http://www.w3.org/2000/svg"><path d="m14.3262 1.25 4.132 4.25m0 0-4.132 4.25m4.132-4.25h-17.9582" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg></a>-->
		</div>

		<?
		$arViewed = [];
		$basketUserId = (int) CSaleBasket::GetBasketUserID(false);
		if ($basketUserId > 0):
			$viewedIterator = \Bitrix\Catalog\CatalogViewedProductTable::getList([
				'select' => ['PRODUCT_ID', 'ELEMENT_ID'],
				'filter' => ['=FUSER_ID' => $basketUserId, '=SITE_ID' => SITE_ID],
				'order' => ['DATE_VISIT' => 'DESC'],
				'limit' => 4
			]);
			while ($arFields = $viewedIterator->fetch()) {
				$arViewed[] = $arFields['ELEMENT_ID'];
			}
			// if end after products_sliderwrap

			global $viewedFilter;

			$viewedFilter = [
				'ID' => $viewedFilter,
			];
		?>

		<div class="products_sliderwrap">
			<div class="custom_slarrows">
				<div class="custom_slarrow custom_prev">
					<svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
						<path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
							  stroke-width="2"/>
					</svg>
				</div>
				<div class="custom_slarrow custom_next">
					<svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
						<path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
							  stroke-width="2"/>
					</svg>
				</div>
			</div>

			<div class="products_slider">

				<? $APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"catalog_items",
			array(
				"ACTION_VARIABLE" => "action",
				"ADD_PICT_PROP" => "-",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"ADD_SECTIONS_CHAIN" => "N",
				"ADD_TO_BASKET_ACTION" => "ADD",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"BACKGROUND_IMAGE" => "-",
				"BASKET_URL" => "/personal/basket.php",
				"BROWSER_TITLE" => "-",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COMPATIBLE_MODE" => "Y",
				"CONVERT_CURRENCY" => "N",
				"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
				"DETAIL_URL" => "",
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"DISPLAY_COMPARE" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"ELEMENT_SORT_FIELD" => $_GET["sort"] ?? 'sort',
				"ELEMENT_SORT_FIELD2" => "id",
				"ELEMENT_SORT_ORDER" => $_GET["order"] ?? 'asc',
				"ELEMENT_SORT_ORDER2" => "desc",
				"ENLARGE_PRODUCT" => "STRICT",
				"FILTER_NAME" => "viewedFilter",
				"HIDE_NOT_AVAILABLE" => "N",
				"HIDE_NOT_AVAILABLE_OFFERS" => "N",
				"IBLOCK_ID" => "6",
				"IBLOCK_TYPE" => "catalog",
				"INCLUDE_SUBSECTIONS" => "Y",
				"LABEL_PROP" => array("SOSTAV"),
				"LABEL_PROP_MOBILE" => array(),
				"LABEL_PROP_POSITION" => "top-left",
				"LAZY_LOAD" => "N",
				"LINE_ELEMENT_COUNT" => "3",
				"LOAD_ON_SCROLL" => "N",
				"MESSAGE_404" => "",
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",
				"MESS_BTN_BUY" => "Купить",
				"MESS_BTN_DETAIL" => "Подробнее",
				"MESS_BTN_LAZY_LOAD" => "Показать ещё",
				"MESS_BTN_SUBSCRIBE" => "Подписаться",
				"MESS_NOT_AVAILABLE" => "Нет в наличии",
				"META_DESCRIPTION" => "-",
				"META_KEYWORDS" => "-",
				"OFFERS_FIELD_CODE" => array("", ""),
				"OFFERS_LIMIT" => "5",
				"OFFERS_SORT_FIELD" => "sort",
				"OFFERS_SORT_FIELD2" => "id",
				"OFFERS_SORT_ORDER" => "asc",
				"OFFERS_SORT_ORDER2" => "desc",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => ".default",
				"PAGER_TITLE" => "Товары",
				"PAGE_ELEMENT_COUNT" => "12",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"PRICE_CODE" => array("Розничная"),
				"PRICE_VAT_INCLUDE" => "Y",
				"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
				"PRODUCT_DISPLAY_MODE" => "N",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
				"PRODUCT_SUBSCRIPTION" => "Y",
				"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
				"RCM_TYPE" => "personal",
				"SECTION_CODE" => "",
				"SECTION_ID" => "",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"SECTION_URL" => "",
				"SECTION_USER_FIELDS" => array("", ""),
				"SEF_MODE" => "N",
				"SET_BROWSER_TITLE" => "Y",
				"SET_LAST_MODIFIED" => "N",
				"SET_META_DESCRIPTION" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "Y",
				"SHOW_404" => "N",
				"SHOW_ALL_WO_SECTION" => "N",
				"SHOW_CLOSE_POPUP" => "N",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_FROM_SECTION" => "N",
				"SHOW_MAX_QUANTITY" => "N",
				"SHOW_OLD_PRICE" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"SHOW_SLIDER" => "Y",
				"SLIDER_INTERVAL" => "3000",
				"SLIDER_PROGRESS" => "N",
				"TEMPLATE_THEME" => "blue",
				"USE_ENHANCED_ECOMMERCE" => "N",
				"USE_MAIN_ELEMENT_SECTION" => "N",
				"USE_PRICE_COUNT" => "N",
				"USE_PRODUCT_QUANTITY" => "N",
				'PAGENAV_TMPL' => 'more',
				'SLIDER' => true,
			)
		); ?>

			</div>
		</div>

		<? endif; ?>

	</div>
</div>

<script>
	let oneclick = <?=json_encode([
	])?>;
</script>

<style>
	.podeli_widget.item {
		max-width: 100%!important;
	}
</style>