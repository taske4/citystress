$(function(){
	var url_string = location.href;
	var url = new URL(url_string);
	var offerId = url.searchParams.get("offerId");

	$('.product_block').on('click', '.product_color .product_color__item', function (e) {
		e.preventDefault();

		if (!$(this).hasClass('disabled')) {
			let productContainer = $(this).parents('[data-sizes]')
			if (!productContainer.attr('data-sizes')) {
				return;
			}

			let productSizes   = JSON.parse(productContainer.attr('data-sizes')),
				selectedColor  = $(this).attr('data-color'),
				sizes          = [];

			try {
				let allSizes     = JSON.parse(productContainer.attr('data-all-sizes'));

				for (size in allSizes) {
					sizes.push(allSizes[size]);
				}
			} catch (e) {
				for (color in productSizes) {
					for (size in productSizes[color]) {
						if (sizes.indexOf(size) === -1) {
							sizes.push(size);
						}
					}
				}
			}

			let htmlSizes = '',
				offerId = null,
				activeOfferId = null,
				sizeActive = '',
				activeColor = null

			for (key in sizes) {
				if (productSizes[selectedColor][sizes[key]]) {
					offerId = productSizes[selectedColor][sizes[key]];
					sizeActive = '';

					if (!activeOfferId) {
						activeOfferId = offerId;
						activeColor   = selectedColor;
						sizeActive    = ' active';
						$('.product_mob__chosensize span').html(sizes[key]);
					}

					htmlSizes += '<div class="product_size'+sizeActive+'" data-title="'+sizes[key]+'" data-subtitle="'+sizes[key]+'" data-offer-id="'+offerId+'">'+sizes[key]+'</div>';
				} else {
					htmlSizes += '<div class="product_size disabled" data-title="'+sizes[key]+'" data-subtitle="(RU 52)" data-offer-id="'+offerId+'">'+sizes[key]+'</div>';
				}
			}

			productContainer.find('.sizes').html(htmlSizes);

			// Теперь изменяем картикни
			// $(productContainer).find('.product_gal__img[data-offer-id]').each(function () {
			//     if ($(this).attr('data-offer-id') === offerId) {
			//         $(this).removeCl
			//     }
			// })
			$(productContainer).find('.in_action[data-offer-id='+offerId+']').removeClass('hiddenpro');
			$(productContainer).find('.in_action[data-offer-id!='+offerId+']').addClass('hiddenpro');

			$(productContainer).find('.product_gallery__sliderwrap .product_gallery__slider[data-offer-id='+offerId+'], .product_gallery__items[data-offer-id='+offerId+']').removeClass('hiddenpro');
			$(productContainer).find('.product_gallery__sliderwrap .product_gallery__slider[data-offer-id!='+offerId+'], .product_gallery__items[data-offer-id!='+offerId+']').addClass('hiddenpro');
			// $(productContainer).find('.product_gallery__slider[data-offer-id!='+offerId+']').each(function(index,el){
			// 	let attrClass = $(el).attr('class');
			// 	$(el).attr('class', attrClass+' hiddenpro');
			// });
			$(productContainer).find('.offer-content[data-offer-id='+offerId+'], .product_gallery__tag[data-offer-id='+offerId+']').removeClass('hiddenpro');
			$(productContainer).find('.offer-content[data-offer-id!='+offerId+'], .product_gallery__tag[data-offer-id!='+offerId+']').addClass('hiddenpro');

			let prices = $(productContainer).data('prices');

			let priceKey = parseInt(offerId);

			$(productContainer).find('.product_price').html(prices[priceKey]['BASE']);

			let oldPriceValue 		 = prices[priceKey]['OLD'] || '',
				discountedPriceValue = prices[priceKey]['DISCOUNTED'] || '';

			$(productContainer).find('.product_priceold').html(oldPriceValue);
			$(productContainer).find('.product_price_discounted').html(discountedPriceValue);

			if (discountedPriceValue) {
				if (!$(productContainer).find('.product_price_discounted').length) {
					if ($(productContainer).find('.product_pricebox .product_priceold').length) {
						$(productContainer).find('.product_pricebox .product_priceold').after('<span class="product_price_discounted"></span>');
					} else if ($(productContainer).find('.product_pricebox .product_price').length) {
						$(productContainer).find('.product_pricebox .product_price').after('<span class="product_price_discounted"></span>');
					}
				}

				$(productContainer).find('.product_price_discounted').html(discountedPriceValue);
			} else {
				if ($(productContainer).find('.product_price_discounted').length) {
					$(productContainer).find('.product_price_discounted').remove();
				}
			}

			if (prices[priceKey]['DIFF_PERCENT']) {
				if ($(productContainer).find('.product_tags').find('.price_diff_percent').length) {
					$(productContainer).find('.product_tags').find('.price_diff_percent').html('-' + prices[priceKey]['DIFF_PERCENT'] + '%');
				} else {
					$(productContainer).find('.product_tags').append('<li class="price_diff_percent">-' + prices[priceKey]['DIFF_PERCENT'] + '%</li>');
				}
			} else if ($(productContainer).find('.product_tags').find('.price_diff_percent').length) {
				$(productContainer).find('.product_tags').find('.price_diff_percent').remove();
			}

			/**
			 * Переключение образа
			 */
			$('.look_container[data-set-for-color="'+activeColor+'"]').removeClass('hiddenpro');
			$('.look_container[data-set-for-color!="'+activeColor+'"]').addClass('hiddenpro');
			$('.look_container[data-set-for-color="'+activeColor+'"]').find('.clook_slider').slick('refresh');

			try {
				$('.product_gallery__slider').slick('refresh');

			} catch (e) {

			}
		}
	});

	$('body').on('click', '.product_block .product_tocart .button, .product_container .product_mob__actions .button', function (e) {
		e.preventDefault();

		let productData = JSON.parse($('.product_block').attr('data-product')),
			offerId     = $('.product_block').find('.product_size.active').data('offer-id');

		if (offerId) {
			productData.id = offerId;
		}

		basketAddProduct(productData);
	})

	setTimeout(function (){
		console.log(offerId);
		$('.product_color .product_color__item[data-offer-id='+offerId+']').trigger('click');
	},100);

	$('.product_info').prepend($('.breadcrumbs-detail').html());
	$('.product_gallery .breadcrumbs').prepend($('.breadcrumbs-detail .breadcrumbs').html());
});

// v0.8 02.12.2022
// Обновленный дизайн

// v0.7 30.11.2022
// Legal формулировки
// Новый теглайн


//Определение экземпляра класса
try {
	const podeliCustom = new PodeliWidgetCustom();

	BX.ready(function () {
		//Инициализируем виджет для страницы Карточки товара
		console.log('test podeli js');

		podeliCustom
			.init({
				type: "item", //catalog || item || checkout
				priceNodeSelector: ".product_price",
				containerNodeSelector: ".product_container ",
				widgetContainerNodeSelector: ".product_actions",
				insertMethod: "after",
			})
			.then(() => {
				setTimeout(() => {
					podeliCustom.add();
				}, 1000);
			});

	});
} catch (e) {
	console.log(e.message);
}

// podeliCustom.init({
//   containerNodeSelector: "#bx-soa-order-form", // - селектор контейнера, в котором находятся элемент с ценой, а также в котором будет находиться виджет. Если виджет 1 на странице, то контейнером моджет быть body или вся страница. Если вилдетов несколько, как например, в каталоге товаров, то тут следует указать селектор контейнер каждой карточки. Обязательный параметр.
//   widgetContainerNodeSelector: "#bx-soa-paysystem .bx-soa-pp-desc-container", // - селектор контейнера, куда будет рендериться виджет. Обязательный параметр.
//   insertMethod: "append", // - способ вставки виджета относительно widgetContainerNodeSelector - append (добавяет элемент внутрь контейнера), replace (добавяет элемент внутрь контейнера, предварительно удалив содержимое контейнера), before (вставляет виджет перед контейнером), after (вставляет виджет после контейнера)
//   type: "checkout", // - тип виджета, примимает одно из 3 трех значений catalog (каталог товаров, маленький виджет с лого и ценой x4), item (карточка товара), checkout (страница оформления заказа)
//   priceNodeSelector: ".bx-soa-cart-total-line-total .bx-soa-cart-d", // - селектор, указывающий на элемент, в котором находится цена
// });
