<script type="text/javascript">
    var viewedCounter = {
        path: '/bitrix/components/bitrix/catalog.element/ajax.php',
        params: {
            AJAX: 'Y',
            SITE_ID: "<?= SITE_ID ?>",
            PRODUCT_ID: "<?= $arResult['ID'] ?>",
            PARENT_ID: "<?= $arResult['ID'] ?>"
        }
    };
    BX.ready(
        BX.defer(function(){
            BX.ajax.post(
                viewedCounter.path,
                viewedCounter.params
            );
        })
    );
</script>

<?php
$details_svg = '<svg fill="none" height="14" viewBox="0 0 14 14" width="14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><clipPath id="a"><path d="m0 0h14v14h-14z" transform="matrix(-1 0 0 1 14 0)"/></clipPath><g clip-path="url(#a)" fill="#242424"><path clip-rule="evenodd" d="m7 0c3.866 0 7 3.13401 7 7 0 3.866-3.134 7-7 7-3.86599 0-7-3.134-7-7 0-3.86599 3.13401-7 7-7zm5.6 7c0 3.0928-2.5072 5.6-5.6 5.6-3.09279 0-5.6-2.5072-5.6-5.6 0-3.09279 2.50721-5.6 5.6-5.6 3.0928 0 5.6 2.50721 5.6 5.6z" fill-rule="evenodd"/><path d="m6.62384 7.90833c0-.28889.0571-.52777.17132-.71666.11918-.18889.2905-.39723.51396-.625.16388-.16667.28306-.30556.35755-.41667.07945-.11667.11918-.24722.11918-.39167 0-.20555-.07449-.36666-.22347-.48333-.14401-.12222-.33767-.18333-.581-.18333-.2334 0-.44196.05555-.6257.16666-.17877.10556-.33023.25556-.45438.45l-.9013-.59166c.20857-.35556.48914-.63056.84171-.825.35755-.19445.77964-.29167 1.2663-.29167.57107 0 1.02793.13611 1.37057.40833.34762.27223.52142.65.52142 1.13334 0 .22777-.03476.42777-.10428.6-.06456.17222-.1465.31944-.24581.44166-.09435.11667-.2185.25278-.37244.40834-.18374.18333-.31782.33889-.40224.46666-.08442.12223-.12663.27223-.12663.45zm.5661 2.09167c-.2036 0-.37492-.07222-.51396-.21667-.13408-.15-.20112-.33055-.20112-.54166s.06704-.38611.20112-.525c.13408-.14445.3054-.21667.51396-.21667.20857 0 .37989.07222.51397.21667.13408.13889.20112.31389.20112.525s-.06952.39166-.20857.54166c-.13408.14445-.30292.21667-.50652.21667z"/></g></svg>';
$chevron_svg = '<svg fill="none" height="22" viewBox="0 0 22 22" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m18.1172 7.7647-7.1177 6.4706-7.11761-6.4706" stroke="#a8a8a8" stroke-linecap="round" stroke-linejoin="round"/></svg>';

$productData = [
    'productCode' => $arResult['CODE'],
    'quantity'    => 1,
];
$activeOffer  = null;
$productName = $arResult['NAME'];
$oldPrice = $priceString = null;//$arItem['PRICES'];
$sizes       = [];
$allSizes    = [];

$allSizes = $arResult['allSizes'];
$prices   = [];

foreach ($arResult['OFFERS'] as $arOffer) {
    $prices[$arOffer['ID']]['BASE'] = $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['PRINT_DISCOUNT_VALUE'];
    $prices[$arOffer['ID']]['OLD']  = '';


    $basePrice = $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['VALUE'];
    $oldPrice  = $arOffer['PRICES'][$arParams['PRICE_CODE'][1]]['VALUE'];

    if ($arOffer['SALE']['PERCENT_VALUE']) {
        $discounted = $basePrice-(($basePrice*$arOffer['SALE']['PERCENT_VALUE'])/100);
        $discounted = \Bitrix\Catalog\Product\Price::roundPrice(3, $discounted, 'RUB');
        $prices[$arOffer['ID']]['DISCOUNTED'] = FormatCurrency($discounted, 'RUB');
    } else {
        $discounted = null;
    }

    // Рассчет price diff
    if (
        ($discounted && $oldPrice) &&
        ($discounted < $oldPrice)
    ) {
        $prices[$arOffer['ID']]['OLD'] = $arOffer['PRICES'][$arParams['PRICE_CODE'][1]]['PRINT_DISCOUNT_VALUE'];
        $prices[$arOffer['ID']]['DIFF_PERCENT'] = (int)(($priceDiff = ($oldPrice - $discounted) / $oldPrice) * 100);
    } elseif (
        ($discounted && $basePrice) &&
        ($discounted < $basePrice)
    ) {
        $prices[$arOffer['ID']]['DIFF_PERCENT'] = (int)(($priceDiff = ($basePrice - $discounted) / $basePrice) * 100);
    } elseif ($basePrice < $oldPrice) {
        $prices[$arOffer['ID']]['OLD'] = $arOffer['PRICES'][$arParams['PRICE_CODE'][1]]['PRINT_DISCOUNT_VALUE'];
        $prices[$arOffer['ID']]['DIFF_PERCENT'] = (int)(($priceDiff = ($oldPrice - $basePrice) / $oldPrice) * 100);
    }


    if (!$activeOffer && ($arOffer['ID'] == $_GET['offerId'])) {
        $activeOffer = $arOffer;
        $sizes       = $arResult['sizes'][$arOffer['PROPERTIES']['COLOR']['VALUE']];
    }
}

if (!$activeOffer) {
    $activeOffer = $arResult['OFFERS'][0];
    $sizes       = $arResult['sizes'][$activeOffer['PROPERTIES']['COLOR']['VALUE']];
}

$productData['activeOfferId'] = $activeOffer['ID'];
$productData['id'] = $activeOffer['ID'];

?>
