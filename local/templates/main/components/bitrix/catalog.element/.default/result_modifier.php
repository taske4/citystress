<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$stores = \Citystress\Connector\tc\cdek\Adapter::getZpvdPvz($_SESSION["CITY"]["LOCATION_ID"], 1, 'CityStress');

Loader::includeModule("highloadblock");

$colors = HL\HighloadBlockTable::getById(HL_COLORS)->fetch();
$colors = HL\HighloadBlockTable::compileEntity($colors);
$colors_data_class = $colors->getDataClass();
$colors = $colors_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_HASH', 'ID'],
))->fetchAll();


$allColors = [];
foreach ($colors as $color) {
    $allColors[trim(mb_ucfirst($color['UF_NAME']))] = $color;
}
/**
 * @var $arResult
 */

$arResult['sidetags'] = $arResult['tags'] = [];

$salesEntity = \Bitrix\Iblock\Iblock::wakeUp(SALES_IBLOCK_ID)->getEntityDataClass();

$salesIds = [];

foreach ($arResult['OFFERS'] as $arOffer) {
    $salesIds[] = $arOffer['PROPERTIES']['LINK_SALE']['VALUE'];
}

$sales = $salesEntity::query()
    ->setSelect(['ID', 'CODE', 'NAME', 'PERCENT_' => 'PERCENT', 'COUPON_' => 'COUPON'])
    ->setFilter(['ID' => $salesIds, 'ACTIVE' => 'Y'])
    ->fetchAll();

$arResult['PRODUCT_SETS'] = \Bitrix\Catalog\ProductSetsTable::query()
    ->setSelect(['ITEM_ID'])
    ->setFilter([
        '=OWNER_ID' => $arResult['ID'],
        '!ITEM_ID' => $arResult['ID'],
    ])
    ->fetchAll();

$arResult['sizes'] = [];
$arResult['colors'] = [];

$activeOffer = null;

foreach ($arResult['OFFERS'] as &$arOffer) {
    if (!$activeOffer) {
        if ($_REQUEST['offerId']) {
            if ($_REQUEST['offerId'] == $arOffer['ID']) {
                $activeOffer = $arOffer;
            }
        } else {
            $activeOffer = $arOffer;
        }
    }

    $arOffer['PROPERTIES']['COLOR']['VALUE'] = mb_ucfirst($arOffer['PROPERTIES']['COLOR']['VALUE']);

    $saleId = $arOffer['PROPERTIES']['LINK_SALE']['VALUE'];
    if (($saleIndex = array_search($saleId, array_column($sales, 'ID'))) !== false) {
        $arOffer['SALE'] = $sales[$saleIndex];

        $hiddenpro = '';
        if ($activeOffer['ID'] !== $arOffer['ID']) {
            $hiddenpro = 'hiddenpro';
        }

        $arResult['tags'][] = '<div class="product_gallery__tag ' . $hiddenpro . '" data-offer-id="' . $arOffer['ID'] . '" style="background: #FE7779; color: #FFF">' . $arOffer['SALE']['NAME'] . '</div>';
    }

    if ($arOffer['PROPERTIES']['COLOR']['VALUE']) {

        $key = $allColors[$arOffer['PROPERTIES']['COLOR']['VALUE']]['UF_HASH'];


        if (!$key) {
            $futureHex = $allColors[$arOffer['PROPERTIES']['COLOR']['VALUE']]['ID'] . '000';
            $key = '#' . substr($futureHex, 0, 3);
        }

        $arResult['colors'][$key] = trim($allColors[trim($arOffer['PROPERTIES']['COLOR']['VALUE'])]['UF_NAME']);
    }

    $offerColor = trim($arOffer['PROPERTIES']['COLOR']['VALUE']);
    $sizesColor = trim($allColors[$offerColor]['UF_NAME']);

    $arResult['sizes'][$sizesColor][$arOffer['PROPERTIES']['SIZE']['VALUE']] = $arOffer['ID'];

    /*
     * Собираем слайдер
     */
    $arOffer['product_gallery'] = [
        'show' => $activeOffer['ID'] === $arOffer['ID'],
        'rows' => [],
    ];

    $items = $arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'];

    $amount = 0;
    foreach ($stores as $key => $store) {
        $rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList(array(
            'filter' => array('=PRODUCT_ID' => $arOffer["ID"], '=STORE.XML_ID' => $store['XML_ID']),
            'select' => array('AMOUNT', 'STORE_ID', 'XML_ID' => 'STORE.XML_ID'),
        ));
        while ($arStoreProduct = $rsStoreProduct->fetch()) {
            $stores[$key]['AMOUNT'] = $arStoreProduct['AMOUNT'];
            if ($arStoreProduct['AMOUNT'] > 0) {
                $amount++;
            }
        }
    }
    $arResult['STORES'][$arOffer["ID"]] = $stores;
    $arResult['AMOUNT'][$arOffer["ID"]] = $amount;
}

$allSizes = [];

foreach($arResult['sizes'] as $color => $sizes) {
    $allSizes = array_merge($allSizes, array_keys($sizes));
}

$allSizes = array_unique($allSizes);

$sizes = \Bitrix\Highloadblock\HighloadBlock::wakeUp(HL_SIZES)->getEntityDataClass()::query()
    ->setSelect(['*'])
    ->setfilter(['UF_NAME' => $allSizes])
    ->fetchCollection();

$allSizes = [];

foreach($sizes as $size) {
    $index = $size->getUfSort();

    while($allSizes[$index]) {
        $index++;
    }

    $allSizes[$index] = $size->getUfName();
}

ksort($allSizes);

$arResult['allSizes'] = $allSizes;


/*    <div class="product_gallery__items<? if (!$show) { echo ' hiddenpro'; } ?>" data-offer-id="<?= $arOffer['ID'] ?>">*/
//        <? foreach ($arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
    <!--            --><? //
//            $pictureUrl = \CFile::GetPath($pictureId);
//            ?>

<?

$offersSets = [];
foreach ($arResult['OFFERS'] as $offer) {
    if ($offer['PROPERTIES']['SET_LINK']['VALUE']) {
        $offersSets[$offer['PROPERTIES']['COLOR']['VALUE']] = ['setId' => $offer['PROPERTIES']['SET_LINK']['VALUE']];
    }
}

foreach ($offersSets as $color => $value) {
    try {
        $offerSet = \Citystress\Entity\Sets::getById(intval($value['setId']));
    } catch (\Error|\Exception $e) {
        continue;
    }

    $setOffersIds = $offerSet['PROPERTIES']['OFFERS_LINK'];
    if (!$setOffersIds) {
        continue;
    }

    $setOffers = \Citystress\Entity\Offer::getList(['ID' => $setOffersIds]);

    foreach ($setOffers as $setOffer) {
        if ($setOffer['ACTIVE'] !== 'Y') {
            $setOffer['ID'] = $setOffer['VARIANTS'][0]['ID'];

            if (!$setOffer['ID']) {
                continue;
            }
        }

        $offersSets[$color]['offerId'][] = $setOffer['ID'];
        $offersSets[$color]['gambarMini'][] = CFile::GetPath($setOffer['PROPERTIES']['MORE_PHOTO'][0]);
        $offersSets[$color]['productIds'][] = [
            'ID' => $setOffer['PROPERTIES']['CML2_LINK'],
            'ACTIVE_OFFER_ID' => $setOffer['ID'], // для предвыбора в catalog.section
        ];
    }
}

$arResult['offersSets'] = $offersSets;

/*
 * Таблица размеров доступна только в разделе "Одежда"
 */
$arResult['tableSizeAvailable'] = false;

if ($arResult['IBLOCK_SECTION_ID']) {
    $list = CIBlockSection::GetNavChain(false, $arResult['IBLOCK_SECTION_ID'], array('ID'), true);
    foreach ($list as $ar_group) {
        if (intval($ar_group["ID"]) === 59) {
            $arResult['tableSizeAvailable'] = true;
            break;
        }
    }
}