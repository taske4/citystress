<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
?>

<div class="look_top">
    <div class="look_title"><?= $arResult['NAME']; ?></div>
    <div class="look_prices">
        <div class="look_priceold"></div>
        <div class="look_price"></div>
    </div>
</div>

<?= '<div class="look_content">'; ?>

<div class="look_sliderwrap">
    <div class="custom_slarrows">
        <div class="custom_slarrow3 custom_prev"><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/arrow3.svg" alt=""></div>
        <div class="custom_slarrow3 custom_next"><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/arrow3.svg" alt=""></div>
    </div>
    <div class="look_slider">
        <? foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
            <?
            $pictureUrl = CFile::GetPath($pictureId);
            ?>
            <div class="slide">
                <div class="look_slide">
                    <div class="back_img">
                        <picture>
                            <source type="image/jpg" media="(max-width: 750px)"
                                    srcset="<?= $pictureUrl ?>">
                            <source type="image/jpg" media="(min-width: 751px)"
                                    srcset="<?= $pictureUrl ?> 1x, <?= $pictureUrl ?> 2x">
                            <img data-img1x="<?= $pictureUrl ?>"
                                 data-img2x="<?= $pictureUrl ?>"
                                 src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png" alt="">
                        </picture>

                    </div>
                </div>
            </div>
        <? } ?>

    </div>
</div>