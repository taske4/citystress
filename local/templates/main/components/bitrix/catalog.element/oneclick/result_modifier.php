<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();


use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

Loader::includeModule("highloadblock");

$colors = HL\HighloadBlockTable::getById(HL_COLORS)->fetch();
$colors = HL\HighloadBlockTable::compileEntity($colors);
$colors_data_class = $colors->getDataClass();
$colors = $colors_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_HASH', 'ID'],
))->fetchAll();

$allColors = [];
foreach($colors as $color) {
    $allColors[$color['UF_NAME']] = $color;
}
/**
 * @var $arResult
 */

Loader::IncludeModule('catalog');

$arResult['PRODUCT_SETS'] = \Bitrix\Catalog\ProductSetsTable::query()
    ->setSelect(['ITEM_ID'])
    ->setFilter([
        '=OWNER_ID' => $arResult['ID'],
        '!ITEM_ID'  => $arResult['ID'],
    ])
    ->fetchAll();

$arResult['sizes']  = [];
$arResult['colors'] = [];

$activeOffer = null;
foreach ($arResult['OFFERS'] as &$arOffer) {
    $arOffer['PROPERTIES']['COLOR']['VALUE'] = mb_ucfirst($arOffer['PROPERTIES']['COLOR']['VALUE']);

    if (!$activeOffer) {
        $activeOffer = $arOffer;
    }

    if ($arOffer['PROPERTIES']['COLOR']['VALUE']) {
        $key = $allColors[$arOffer['PROPERTIES']['COLOR']['VALUE']]['UF_HASH'];

        if (!$key) {
            $futureHex = (string)$allColors[$arOffer['PROPERTIES']['COLOR']['VALUE']]['ID'] . '000';
            $key = '#' . substr($futureHex, 0, 3);
        }

        $arResult['colors'][$key] = $allColors[$arOffer['PROPERTIES']['COLOR']['VALUE']]['UF_NAME'];
    }

    $arResult['sizes'][$allColors[$arOffer['PROPERTIES']['COLOR']['VALUE']]['UF_NAME']][$arOffer['PROPERTIES']['SIZE']['VALUE']] = $arOffer['ID'];

    /*
     * Собираем слайдер
     */
    $arOffer['product_gallery'] = [
        'show' => $activeOffer['ID'] === $arOffer['ID'],
        'rows' => [],
    ];

    $items = $arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'];
}

/*    <div class="product_gallery__items<? if (!$show) { echo ' hiddenpro'; } ?>" data-offer-id="<?= $arOffer['ID'] ?>">*/
//        <? foreach ($arOffer['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $index => $pictureId) { ?>
<!--            --><?//
//            $pictureUrl = \CFile::GetPath($pictureId);
//            ?>