<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Diag;


/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

?>

<? require $_SERVER['DOCUMENT_ROOT'].'/local/templates/main/components/bitrix/catalog.element/.default/common.php' ?>

<?php
//$details_svg = '<svg fill="none" height="14" viewBox="0 0 14 14" width="14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><clipPath id="a"><path d="m0 0h14v14h-14z" transform="matrix(-1 0 0 1 14 0)"/></clipPath><g clip-path="url(#a)" fill="#242424"><path clip-rule="evenodd" d="m7 0c3.866 0 7 3.13401 7 7 0 3.866-3.134 7-7 7-3.86599 0-7-3.134-7-7 0-3.86599 3.13401-7 7-7zm5.6 7c0 3.0928-2.5072 5.6-5.6 5.6-3.09279 0-5.6-2.5072-5.6-5.6 0-3.09279 2.50721-5.6 5.6-5.6 3.0928 0 5.6 2.50721 5.6 5.6z" fill-rule="evenodd"/><path d="m6.62384 7.90833c0-.28889.0571-.52777.17132-.71666.11918-.18889.2905-.39723.51396-.625.16388-.16667.28306-.30556.35755-.41667.07945-.11667.11918-.24722.11918-.39167 0-.20555-.07449-.36666-.22347-.48333-.14401-.12222-.33767-.18333-.581-.18333-.2334 0-.44196.05555-.6257.16666-.17877.10556-.33023.25556-.45438.45l-.9013-.59166c.20857-.35556.48914-.63056.84171-.825.35755-.19445.77964-.29167 1.2663-.29167.57107 0 1.02793.13611 1.37057.40833.34762.27223.52142.65.52142 1.13334 0 .22777-.03476.42777-.10428.6-.06456.17222-.1465.31944-.24581.44166-.09435.11667-.2185.25278-.37244.40834-.18374.18333-.31782.33889-.40224.46666-.08442.12223-.12663.27223-.12663.45zm.5661 2.09167c-.2036 0-.37492-.07222-.51396-.21667-.13408-.15-.20112-.33055-.20112-.54166s.06704-.38611.20112-.525c.13408-.14445.3054-.21667.51396-.21667.20857 0 .37989.07222.51397.21667.13408.13889.20112.31389.20112.525s-.06952.39166-.20857.54166c-.13408.14445-.30292.21667-.50652.21667z"/></g></svg>';
//$chevron_svg = '<svg fill="none" height="22" viewBox="0 0 22 22" width="22" xmlns="http://www.w3.org/2000/svg"><path d="m18.1172 7.7647-7.1177 6.4706-7.11761-6.4706" stroke="#a8a8a8" stroke-linecap="round" stroke-linejoin="round"/></svg>';
//
//$productData = [
//	'productCode' => $arResult['CODE'],
//	'quantity'    => 1,
//];
//$activeOffer  = null;
//$productName = $arResult['NAME'];
//$priceString = null;//$arItem['PRICES'];
//$sizes       = [];
//$allSizes    = [];
//
//foreach($arResult['sizes'] as $color => $sizes) {
//    $allSizes = array_merge($allSizes, array_keys($sizes));
//}
//
//$allSizes = array_unique($allSizes);
//
//foreach ($arResult['OFFERS'] as $arOffer) {
//
//    if (!$activeOffer && ($arOffer['ID'] == $_GET['offerId'])) {
//        $activeOffer = $arOffer;
//        $priceString = $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['PRINT_DISCOUNT_VALUE'];
////                $productName = $arOffer['NAME'];
//        $sizes = $arResult['sizes'][$arOffer['PROPERTIES']['COLOR']['VALUE']];
//    }
//}
//
//if(!$priceString) {
//    $priceString = $arResult['PRICES'][$arParams['PRICE_CODE'][0]]['PRINT_DISCOUNT_VALUE'];
//}
//
//if (!$activeOffer) {
//	$activeOffer = $arResult['OFFERS'][0];
//}
//
//?>

<div class="product_mini" data-product-id="<?=$arResult['ID']?>" data-product='<?= json_encode($productData) ?>'>
	<div class="product_mini__img">
		<div class="product_mini__imgbox">
			<? foreach ($arResult['OFFERS'] as $offer) { ?>
				<?
					$pictureUrl = CFile::GetPath(current($offer['PROPERTIES']['MORE_PHOTO']['VALUE']));
				?>
				<div class="back_img <?= ($offer['ID'] == $activeOffer['ID']) ? '' : ' hiddenpro' ?>" data-picture-offer="<?=$offer['ID']?>">
					<picture>
						<source type="image/jpg" media="(max-width: 750px)" srcset="<?=$pictureUrl?>">
						<source type="image/jpg" media="(min-width: 751px)" srcset="<?=$pictureUrl?> 1x, <?=$pictureUrl?> 2x">
						<img data-img1x="<?=$pictureUrl?>" data-img2x="<?=$pictureUrl?>" src="<?=$pictureUrl?>" alt="">
					</picture>
				</div>
			<? } ?>
		</div>
	</div>
	<div class="product_mini__box">
		<div class="product_mini__row">
			<div class="product_mini__article"><?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']?></div>
			<div class="product_mini__priceold">6 999 ₽</div>
		</div>
		<div class="product_mini__row">
			<div class="product_mini__title"><?=$arResult['NAME']?></div>
			<div class="product_mini__price"><?=$priceString?></div>
		</div>
		<div class="product_mini__color">
			<? foreach ($arResult['colors'] as $hex => $color) { ?>
				<? if (array_search($activeOffer['ID'], $arResult['sizes'][$color])) { ?>
					<div class="product_mini__colorchosen"><span style="background: <?=$hex?>;"></span><i><?=$color?></i></div>
				<? } ?>
			<? } ?>
			<ul class="product_mini__colorlist">
				<? foreach ($arResult['colors'] as $hex => $color) { ?>
					<?
						$offerId = array_search(current($arResult['sizes'][$color]), array_column($arResult['OFFERS'], 'ID'));
						$pictureUrl = CFile::GetPath(current($arResult['OFFERS'][$offerId]['PROPERTIES']['MORE_PHOTO']['VALUE']));
					?>
					<li class="<?= (array_search($activeOffer['ID'], $arResult['sizes'][$color])) ? 'active' : '' ?>"><div data-color-offer='<?=json_encode($arResult['sizes'][$color])?>' data-color="<?=$hex?>" style="background: <?=$hex?>;"></div><span><?=$color?></span></li>
				<? } ?>
			</ul>
		</div>
<!--		--><?// if ($arResult['colors']) { ?>
<!--			<div class="product_color">-->
<!--				<div class="product_color__label">Цвет</div>-->
<!---->
<!--				--><?// foreach ($arResult['colors'] as $hex => $color) { ?>
<!--					--><?//
//					$offerId = array_search(current($arResult['sizes'][$color]), array_column($arResult['OFFERS'], 'ID'));
//					$pictureUrl = CFile::GetPath(current($arResult['OFFERS'][$offerId]['PROPERTIES']['MORE_PHOTO']['VALUE']));
//					?>
<!--					<div data-color="--><?php //=$color?><!--" data-offer-id="--><?php //=$arResult['OFFERS'][$offerId]['ID']?><!--" class="product_color__item--><?php //= (array_search($activeOffer['ID'], $arResult['sizes'][$color])) ? ' active' : '' ?><!--">-->
<!--						<div class="back_img">-->
<!--							<picture>-->
<!--								<source type="image/jpg" srcset="--><?php //=$pictureUrl?><!--">-->
<!--								<img data-img1x="--><?php //=$pictureUrl?><!--" data-img2x="--><?php //=$pictureUrl?><!--" src="--><?php //=SITE_MAIN_TEMPLATE_PATH?><!--/img/blank.png" alt="">-->
<!--							</picture>-->
<!--						</div>-->
<!--						<span>--><?php //=$color?><!--</span>-->
<!--					</div>-->
<!--				--><?// } ?>
<!--			</div>-->
<!--		--><?// } ?>
		<div class="product_mini__options">
			<div class="product_mini__sizes">
				<? foreach ($arResult['colors'] as $hex => $color) { ?>
					<? if (array_search($activeOffer['ID'], $arResult['sizes'][$color])) { ?>
						<? foreach ($arResult['sizes'][$color] as $size => $offerId) { ?>
							<button class="product_mini__size" data-offer-id="<?=$offerId?>"><?=$size?></button>
						<? } ?>
					<? } ?>
				<? } ?>
			</div>
<!--			--><?// foreach ($allSizes as $index => $size) { ?>
<!--				--><?//
//				$available = false;
//				$offerId = null;
//
//				if ($offerId = $sizes[$size]) {
//					$available = true;
//				}
//				?>
<!--				<div class="product_size --><?// if (!$available) { ?><!--disabled--><?// } ?><!--" data-title="XXL" data-subtitle="(RU 52)">--><?php //=$size?><!--</div>-->
<!--			--><?// } ?>
			<div class="product_mini__actions">
				<button class="product_mini__fav "> <!-- добавляем класс active, если в избранном -->
					<svg fill="none" height="16" viewBox="0 0 18 16" width="18" xmlns="http://www.w3.org/2000/svg"><path d="m9 2.74296c.00309.00353.00303.00358.00303.00358l.0051-.00465c.00484-.00439.01286-.01161.02396-.02135.02219-.01949.05661-.04905.10234-.08638.09157-.07474.22783-.18006.40147-.2977.349-.23645.8398-.5164 1.4153-.70164 1.1292-.36347 2.5879-.37135 4.0111 1.09592l.0044.00455.0034.00338.0025.00251c.0028.00273.0076.0077.0144.01484.0137.0143.0351.03727.0627.06851.0552.06258.1346.15783.2253.28261.1821.25055.4055.61418.5725 1.06614.3281.88796.4497 2.13808-.4482 3.60505l-.0368.06012-.0119.0461c-.001.0023-.0023.00509-.0039.00839-.0127.02696-.0424.08477-.1028.17974-.1214.19072-.3595.52046-.8139 1.03791-.854.97261-2.4532 2.58731-5.43 5.16271-2.97678-2.5754-4.57596-4.1901-5.43-5.16271-.45436-.51745-.69251-.84719-.81386-1.03791-.06044-.09497-.09018-.15278-.10286-.17974-.00156-.0033-.00283-.00609-.00386-.00839l-.01188-.0461-.0368-.06012c-.89793-1.46697-.77638-2.71709-.44828-3.60505.167-.45196.3904-.81559.57253-1.06614.0907-.12478.17009-.22003.22531-.28261.02757-.03124.049-.05421.06265-.06851.00683-.00714.0117-.01211.01441-.01484l.00229-.0023.00368-.00359.00441-.00455c1.4232-1.46727 2.88186-1.45939 4.01104-1.09592.57549.18524 1.06632.46519 1.41532.70164.17364.11764.3099.22296.40147.2977.04573.03733.08015.06689.10234.08638.0111.00974.01912.01696.02396.02135l.0051.00465s-.00006-.00005.00303-.00358zm0-1.16037c-.0116-.00796-.02335-.01598-.03525-.02404-.39631-.2685-.96454-.594474-1.644-.813184-1.37984-.444149-3.21325-.436565-4.92366 1.324354l-.007.00702c-.00651.00657-.01526.01552-.02604.0268-.02154.02255-.05122.05446-.08729.09535-.07208.08168-.17014.19969-.28008.35093-.21916.30149-.48979.74091-.69379 1.29301-.410692 1.11148-.540197 2.65801.50379 4.3907.00775.01921.01709.04068.02836.06464.03269.06948.0828.1623.16031.28411.15452.24286.42461.6113.89963 1.15227.94223 1.07305 2.71163 2.84845 6.0384 5.69815l.05466.0673c.00399-.0034.00798-.0068.01196-.0102.00399.0034.00797.0068.01196.0102l.05466-.0673c3.32678-2.8497 5.09618-4.6251 6.03838-5.69815.475-.54097.7451-.90941.8997-1.15227.0775-.12181.1276-.21463.1603-.28411.0112-.02396.0206-.04543.0283-.06463 1.044-1.7327.9145-3.27923.5038-4.39071-.204-.5521-.4746-.99152-.6938-1.29301-.1099-.15124-.208-.26925-.2801-.35093-.036-.04089-.0657-.0728-.0873-.09535-.0107-.01128-.0195-.02023-.026-.0268l-.007-.00702c-1.7104-1.760919-3.5438-1.768503-4.9236-1.324354-.67951.21871-1.24774.544684-1.64405.813184-.01189.00806-.02365.01608-.03525.02404z" style="fill-rule:evenodd;clip-rule:evenodd;fill:#242424;stroke:#242424;stroke-width:.5;stroke-miterlimit:10;stroke-linejoin:round"/></svg>
					<svg class="fav_active" fill="none" height="15" viewBox="0 0 16 15" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m7.96475 1.05855.03525.02404.03525-.02404c.39631-.268496.96454-.594474 1.644-.813184 1.37985-.444149 3.21325-.436565 4.92365 1.324354l.007.00702c.0065.00657.0153.01552.026.0268.0216.02255.0513.05446.0873.09535.0721.08168.1702.19969.2801.35093.2192.30149.4898.74091.6938 1.29301.4107 1.11148.5402 2.65801-.5038 4.39071-.0077.0192-.0171.04067-.0283.06463-.0327.06948-.0828.1623-.1603.28411-.1546.24286-.4247.6113-.8997 1.15227-.9422 1.07305-2.7116 2.84845-6.03838 5.69815l-.05466.0673-.01196-.0102-.01196.0102-.05466-.0673c-3.32677-2.8497-5.09617-4.6251-6.0384-5.69815-.47502-.54097-.74511-.90941-.899634-1.15227-.077509-.12181-.127616-.21463-.160304-.28411-.011272-.02396-.020609-.04543-.02836-.06464-1.043989-1.73269-.914484-3.27922-.503789-4.3907.203999-.5521.47463-.99152.693784-1.29301.109943-.15124.208003-.26925.280083-.35093.03607-.04089.06575-.0728.08729-.09535.01078-.01128.01953-.02023.02604-.0268l.007-.00702c1.71041-1.760919 3.54382-1.768503 4.92366-1.324354.67946.21871 1.24769.544688 1.644.813184z" fill="#242424"/></svg>
				</button>
			</div>
		</div>

		<? foreach($arResult['OFFERS'] as $offer) { ?>
			<div class="product_mini__info<?= ($offer['ID'] == $activeOffer['ID']) ? '' : ' hiddenpro' ?>" data-info-offer="<?=$offer['ID']?>">
				<?= implode(', ', $offer['PROPERTIES']['PARAMS_MODEL_ON_PHOTO']['VALUE'] ?: []); ?>
			</div>
		<? } ?>
		<div class="product_mini__tablein universal_modal__in" data-modalname="sizetable">Таблица размеров</div>
	</div>
</div>