<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}
?>
<?if($USER->IsAuthorized()):?>

<? if ($_POST['ajax'] === 'yes') { ?>
		<?
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		echo 'ok';
		die;
		?>
<? } else { ?>
		<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
<? } ?>

<?else:?>
<?
if (count($arResult["ERRORS"] ?: []) > 0):
    foreach ($arResult["ERRORS"] as $key => $error) {
        if (intval($key) == 0 && $key !== 0) {
            $arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error);
        }
    }

    unset($arResult['ERRORS']['LOGIN']);

    if (preg_match('/Пользователь с логином ".+" уже существует/', $arResult['ERRORS'][0])) {
		echo '<div class="signup_error">Покупатель с таким адресом уже есть. Если это вы — <span data-modalname="auth" class="universal_modal__in go_login">войдите</span>, чтобы увидеть отложенные товары из корзины и применить скидку.</div>';
    } else {
		ShowError(implode("<br /><br />", $arResult["ERRORS"]));
	}


elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"):
?>
<p><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>

<?if($arResult["SHOW_SMS_FIELD"] == true):?>

<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>
<input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />
<table>
	<tbody>
		<tr>
			<td><?echo GetMessage("main_register_sms")?><span class="starrequired">*</span></td>
			<td><input size="30" type="text" name="SMS_CODE" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" /></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td><input type="submit" name="code_submit_button" value="<?echo GetMessage("main_register_sms_send")?>" /></td>
		</tr>
	</tfoot>
</table>
</form>

<script>
new BX.PhoneAuth({
	containerId: 'bx_register_resend',
	errorContainerId: 'bx_register_error',
	interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
	data:
		<?=CUtil::PhpToJSObject([
			'signedData' => $arResult["SIGNED_DATA"],
		])?>,
	onError:
		function(response)
		{
			var errorDiv = BX('bx_register_error');
			var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
			errorNode.innerHTML = '';
			for(var i = 0; i < response.errors.length; i++)
			{
				errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message);
			}
			errorDiv.style.display = '';
		}
});
</script>

<div id="bx_register_error" style="display:none"><?ShowError("error")?></div>

<div id="bx_register_resend"></div>

<?else:?>

	<form class="form_v1 login_form" method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform"
		  enctype="multipart/form-data">
		<?
		if ($arResult["BACKURL"] <> ''):
			?>
			<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
		<?
		endif;
		?>

		<div class="fields">
		<? foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
			<? if ($FIELD == "AUTO_TIME_ZONE" && $arResult["TIME_ZONE_ENABLED"] == true): ?>
			<? else: ?>
				<div class="field_wrap" style="<? if ($FIELD === 'EMAIL') { ?>display: none;<? } ?>">
					<? switch ($FIELD) {
					case "PERSONAL_BIRTHDAY": ?>
					<div class="field_wrap">
						<img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/edit.svg" alt="" class="field_editicon">
						<input value="<?= $arResult["arUser"]["PERSONAL_BIRTHDAY"]?>" name="PERSONAL_BIRTHDAY" type="text" class="field required" placeholder="Дата рождения">
					</div>
					<? break; case "PERSONAL_GENDER": ?>
						<div id="user-gender-select" class="field_wrap">
							<div class="checkboxes single">
								<div
									class="checkbox checkbox_v2 <?= $arResult['arUser']['PERSONAL_GENDER'] === "F" ? "checked" : "" ?>">
									<input name="gender-female" type="checkbox" class="checkbox_input">
									<div class="checkbox_icon"></div>
									<span class="checkbox_label">Женщина</span>
								</div>
								<div
									class="checkbox checkbox_v2 <?= $arResult['arUser']['PERSONAL_GENDER'] === "M" ? "checked" : "" ?>">
									<input name="gender-male" type="checkbox" class="checkbox_input">
									<div class="checkbox_icon"></div>
									<span class="checkbox_label">Мужчина</span>
								</div>
							</div>
						</div>
					<? break; case "PASSWORD": ?>
						<input size="30" type="password" name="REGISTER[<?= $FIELD ?>]"
							   value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off"
							   class="field required"
							   placeholder="Придумайте пароль*"
						/>
						<? if ($arResult["SECURE_AUTH"]): ?>
							<span class="bx-auth-secure" id="bx_auth_secure"
								  title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
							<div class="bx-auth-secure-icon"></div>
							</span>
							<noscript>
							<span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
							<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
							</span>
							</noscript>
							<script type="text/javascript">
								document.getElementById('bx_auth_secure').style.display = 'inline-block';
							</script>
						<? endif ?>
						<?
						break;
					case "CONFIRM_PASSWORD":
						?><input size="30" type="password" name="REGISTER[<?= $FIELD ?>]"
						value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off"
					class="field required"
								 placeholder="Повторите пароль*"
						/><?
						break;
					default: ?>
						<?
							$placeholder = null;
							$class       = null;

							if ($FIELD === 'NAME') {
								$placeholder = 'Введите ваше имя';
							} elseif ($FIELD === 'LOGIN') {
								$class = 'field field_email';
								$placeholder = 'Введите Email*';
							} elseif ($FIELD === 'EMAIL') {
								$class = 'orig_field_email';
							} elseif($FIELD === 'PERSONAL_PHONE') {
								$class = 'field field_phone required';
								$placeholder = 'Номер телефона*';
							} elseif($FIELD === 'SECOND_NAME') {
								$class = 'field';
								$placeholder = 'Отчество';
							} elseif($FIELD === 'LAST_NAME') {
								$class = 'field';
								$placeholder = 'Фамилия*';
							}
						?>
						<input
							class="<? if ($class) { echo $class; } else { ?>field<? } ?>"
							size="30"
							type="text"
							name="REGISTER[<?= $FIELD ?>]"
							value="<?= $arResult["VALUES"][$FIELD] ?>"
							placeholder="<?=$placeholder?>"
						/>
					<? } ?>
				</div>
			<? endif ?>
		<? endforeach ?>

			<div class="button_wrap">
				<div class="agreement_cover" style="display: block;"></div>
				<input class="button button_black" type="submit" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>"/>
			</div>

			<div class="form_agreement_v2 checked">
				<div class="form_agreement__icon">
					<svg fill="none" height="5" viewBox="0 0 8 5" width="8" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="m7.85356.125357c.09376.080366.14644.189315.14644.302904s-.05268.222532-.14644.302904l-4.84116 4.143505c-.0939.08024-.22119.12533-.3539.12533-.13272 0-.26001-.04509-.35391-.12533l-2.170175-1.85744c-.0884632-.08125-.13662103-.18872-.13433734-.29977.00229037-.11105.05484864-.217.14661034-.29554.091755-.07853.215549-.12351.345292-.12547s.255306.03926.350246.11498l1.816274 1.55452 4.48725-3.840593c.09389-.0802701.22116-.125357.35391-.125357.13268 0 .26002.0450869.3539.125357z" fill="#242424" fill-rule="evenodd"></path></svg>
				</div>
				<span>Подписаться на новости и скидки. <a href="">Согласие на обработку персональных данных</a></span>
			</div>

			<div class="form_agreement_v2 checked">
				<div class="form_agreement__icon">
					<svg fill="none" height="5" viewBox="0 0 8 5" width="8" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="m7.85356.125357c.09376.080366.14644.189315.14644.302904s-.05268.222532-.14644.302904l-4.84116 4.143505c-.0939.08024-.22119.12533-.3539.12533-.13272 0-.26001-.04509-.35391-.12533l-2.170175-1.85744c-.0884632-.08125-.13662103-.18872-.13433734-.29977.00229037-.11105.05484864-.217.14661034-.29554.091755-.07853.215549-.12351.345292-.12547s.255306.03926.350246.11498l1.816274 1.55452 4.48725-3.840593c.09389-.0802701.22116-.125357.35391-.125357.13268 0 .26002.0450869.3539.125357z" fill="#242424" fill-rule="evenodd"></path></svg>
				</div>
				<span>Вы согласны с <a href="">Правилами работы магазина</a> и ознакомлены с <a href="">Политикой конфиденциальности</a></span>
			</div>

		</div>


	</form>

<?endif //$arResult["SHOW_SMS_FIELD"] == true ?>

<?endif?>