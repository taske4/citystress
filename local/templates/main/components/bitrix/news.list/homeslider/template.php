<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if ($arResult['ITEMS']) { ?>

    <div class="hometop_container container">
        <div class="hometop_block">

            <div class="hometop_count"><span>01</span>/<?= (count($arResult['ITEMS'] ?: []) < 10) ? '0' : '' ?><?=count($arResult['ITEMS'])?></div>

            <div class="hometop_nav">
                <? foreach ($arResult['ITEMS'] as $index => $slide) { ?>
                    <div data-index="<?=$index+1?>" class="hometop_nav__item<?= !$index ? ' active' : '' ?>">
                        <div></div>
                    </div>
                <? } ?>
            </div>


            <div class="hometop_sliderwrap">
                <div class="hometop_slider">
                    <? $i = 1; ?>
                    <? foreach ($arResult['ITEMS'] as $slide) { ?>
                        <?
                        $link = $slide['PROPERTIES']['LINK']['VALUE'];

                        $mobilePicture = $tabletPicture = $desktopPicture
                            = $mobilePictureX2 = $tabletPictureX2 = $desktopPictureX2 = null;

                        $video = CFile::GetPath($slide['PROPERTIES']['VIDEO']['VALUE']);
                        $videoMobile = CFile::GetPath($slide['PROPERTIES']['VIDEO_MOBILE']['VALUE']) ?? $video;


                        if (!$video) {
                            if ($pictureId = $slide['PROPERTIES']['MOBILE_PICTURE']['VALUE']) {
                                $mobilePicture = CFile::GetPath($pictureId);
                            }

                            if ($pictureId = $slide['PROPERTIES']['MOBILE_PICTURE_X2']['VALUE']) {
                                $mobilePictureX2 = CFile::GetPath($pictureId);
                            }

                            if ($pictureId = $slide['PROPERTIES']['TABLET_PICTURE']['VALUE']) {
                                $tabletPicture = CFile::GetPath($pictureId);
                            }

                            if ($pictureId = $slide['PROPERTIES']['TABLED_PICTURE_X2']['VALUE']) {
                                $tabletPictureX2 = CFile::GetPath($pictureId);
                            }

                            if ($pictureId = $slide['PROPERTIES']['DESKTOP_PICTURE']['VALUE']) {
                                $desktopPicture = CFile::GetPath($pictureId);
                            }

                            if ($pictureId = $slide['PROPERTIES']['DESKTOP_PICTURE_X2']['VALUE']) {
                                $desktopPictureX2 = CFile::GetPath($pictureId);
                            }
                        }

                        if ($duration = intval($slide['PROPERTIES']['DURATION']['VALUE'])) {
                            $duration = $duration * 1000;
                        } else {
                            $duration = 3000;
                        }

                        ?>
                        <div class="slide slide_<?=$i++?>" data-duration="<?=$duration?>">
                            <?
                                if ($link) {
                                    echo '<a href="'.$link.'" class="hometop_item">';
                                } else {
                                    echo '<div class="hometop_item">';
                                }
                            ?>

                            <? if ($video) { ?>
                                <div data-video="<?=$video?>" data-videomob="<?=$videoMobile?>"
                                     class="video"></div>
                            <? } else { ?>
                                <div class="back_img back_img__relative">
                                    <picture>
                                        <source type="image/jpg" media="(max-width: 750px)"
                                                srcset="<?= $mobilePicture ?> 1x, <?= $mobilePictureX2 ?> 2x">
                                        <source type="image/jpg" media="(min-width: 751px) and (max-width: 1000px)"
                                                srcset="<?= $tabletPicture ?> 1x, <?= $tabletPictureX2 ?> 2x">
                                        <source type="image/jpg" media="(min-width: 1001px)"
                                                srcset="<?= $desktopPicture ?> 1x, <?= $desktopPictureX2 ?> 2x">

                                        <img data-imgmob="<?= $mobilePicture ?>" data-imgtab="<?= $tabletPicture ?>"
                                             data-img1x="<?= $desktopPicture ?>" data-img2x="<?= $desktopPictureX2 ?>"
                                             src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/blank.png"
                                             alt="<?= $slide['PREVIEW_TEXT'] ?>">
                                    </picture>
                                </div>
                            <? } ?>

                                <div class="hometop_item__content">
                                    <div class="hometop_item__title"><?= $slide['PREVIEW_TEXT'] ?></div>
                                    <div class="hometop_item__subtitle"><?= $slide['DETAIL_TEXT'] ?></div>
                                </div>

                            <?
                                echo $link ? '</a>' : '</div>';
                            ?>
                        </div>
                    <? } ?>

                </div>
            </div>

        </div>
    </div>
<? } ?>
