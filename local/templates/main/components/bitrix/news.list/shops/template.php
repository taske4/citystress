<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$shops_nav = $_GET['shops_nav'];
if($shops_nav=='') $shops_nav =  array_key_first($arResult['ITEMS']);

?>
<div class="shops_container container pagetype1">
  <div class="shops_block block">
  <div class="breadcrumbs">
    <?$APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        ".default",
        Array(
            "PATH" => "",
            "SITE_ID" => "s1",
            "START_FROM" => "0"
        )
    );?>
</div>
      <h1> <span><?php if ( $shops_nav != 'Cdek' ):?>Магазины CityStress<?php else:?>Пункты самовывоза<?php endif;?></span> 
      <div class="city_select universal_modal__in" data-modalname="cityselect">
        <span class="city_select__chose"><?=$_SESSION["CITY"]["NAME"]?></span>
        <svg fill="none" height="32" viewBox="0 0 32 32" width="32" xmlns="http://www.w3.org/2000/svg"><path d="m6.39844 14 9.59996 8.5333 9.6-8.5333" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
      </div>
    </h1>

<?
/*echo '<pre>';
var_dump($arResult['ITEMS']);*/
?>
<?if(count($arResult['ITEMS'] ?: [])>1):?>
    <div class="shops_nav">
		<?foreach($arResult['ITEMS'] as $key => $item):?>
		<div data-index="<?=$key?>" class="shops_nav__item <?php if ( $shops_nav == $key ):?>active<?php endif;?>"><?if($key=='CityStress'):?>Магазины<?endif;?><?if($key=='Cdek'):?>Пункты выдачи<?endif;?></div>
		<?endforeach;?>
      
    </div>
	<?endif;?>
 
	<?foreach($arResult['ITEMS'] as $key => $item):?>
 
	    <div class="shops_out shops_out_<?=$key?> mappoints_box" <?php if ( $shops_nav == $key ):?>style="display: block;"<?php endif;?>>
 
      <div class="shops_flex">

        <div class="shops_content">
          <div class="nomob_customscroll">
            <div class="shops_content__inside">

              <div class="mappoints_goback">
                <svg fill="none" height="13" viewBox="0 0 25 13" width="25" xmlns="http://www.w3.org/2000/svg"><path d="m7.67377 2.25-4.13194 4.25m0 0 4.13194 4.25m-4.13194-4.25h17.95817" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
                <span>Назад к списку</span>
              </div>

              <div class="mappoints_city universal_modal__in" data-modalname="cityselect">
                <div class="mappoints_city__chose">Ваш город</div>
                <span><?=$_SESSION["CITY"]["NAME"]?></span> 
                <svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><path d="m4 8.00004 6 5.33336 6-5.33336" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
              </div>

              <div class="shops_mobmapbox">
                <div data-zoom="10" data-center="[<?=$_SESSION["LOCATION_LATITUDE"]?>,<?=$_SESSION["LOCATION_LONGITUDE"]?>]" class="mappoints_mobmap_in"></div> <!-- мобильная версия карты со своими настройками zoom и center -->
              </div>

              <form class="form_v1">
                <div class="fields">
                  <div class="field_wrap">
                    <input type="text" class="field point_search" data-items="<?=$key?>" placeholder="Поиск пункта по адресу">
                  </div>
                </div>
              </form>

            
<?foreach($item as $map_point):?>
 
 <div data-page="/shops/<?=$map_point['ID']?>/" class="shoppickup_item optionview_item mappoints_data__item" data-coos="[<?=$map_point["PROPERTIES"]["MAP_COORDS"]["VALUE"]?>]" data-address="<?=$map_point['NAME']?>">
                <div class="shoppickup_item__title"><?if($map_point["PROPERTIES"]['SHOP_NAME']["VALUE"]!=''):?><?=$map_point["PROPERTIES"]['SHOP_NAME']["VALUE"]?><?else:?><?=$key?><?endif;?></div>
                <div class="shoppickup_item__address"><?=$map_point['NAME']?></div>
				<?if($map_point["PROPERTIES"]['SCHEDULE']["VALUE"]!=''):?><div class="shoppickup_item__worktime">Режим работы: <?=$map_point["PROPERTIES"]['SCHEDULE']["VALUE"]?></div><?endif;?>
       
              </div>
 <?endforeach;?>           
           


              <button type="submit" class="button button_black shops_showmore">
                <span>Показать еще</span>
              </button>
            </div>
          </div>
        </div>

	 <div class="shops_mapbox">
          <div data-zoom="12" data-center="[<?=$_SESSION["LOCATION_LATITUDE"]?>,<?=$_SESSION["LOCATION_LONGITUDE"]?>]" class="mappoints_map_in"></div>
        </div>
      
        <div class="mappoints_map"></div>
  </div>
  </div>
 	<?endforeach;?>
	</div>
	</div>