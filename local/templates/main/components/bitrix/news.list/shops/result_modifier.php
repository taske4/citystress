<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

 $newItemsList = [];
foreach ($arResult['ITEMS'] as $item)
{
 
	$newItemsList[$item["PROPERTIES"]['TYPE']["VALUE"]][] = $item;
	
}
$arResult['ITEMS'] = $newItemsList;
 
 