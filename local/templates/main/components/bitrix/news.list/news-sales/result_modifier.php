<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
	die();
}

/**
 * @global $arResult
 */

$arResult['NEWS_OUT_NUM'] = $arParams['ITEMS_TYPE'] === 'Sales' ? 1 : 2;

foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['PROPERTIES']['TYPE']['VALUE'] !== 'Sales') {
        continue;
    }

    if ($arItem['PROPERTIES']['DATE_FROM']['VALUE'] && $arItem['PROPERTIES']['DATE_TO']['VALUE']) {
        $now        = new DateTime();
        $activeFrom = new DateTime($arItem['PROPERTIES']['DATE_FROM']['VALUE']);
        $activeTo   = new DateTime($arItem['PROPERTIES']['DATE_TO']['VALUE']);
//        $arResult['ACTIVE_DAYS'] = $now->diff($activeTo)->d;
        $arResult['ITEMS'][$key]['salesPeriod'] = \lib\Helper::elementActivityModify($activeFrom, $activeTo);
    }
}