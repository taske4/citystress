<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

	<div class="news_out news_out_<?=$arResult['NEWS_OUT_NUM']?>" style="display: <?=($arResult['NEWS_OUT_NUM'] == '1') ? 'block' : 'none';?>">

		<div class="news_items">

			<? foreach($arResult['ITEMS'] as $arItem) {?>
				<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="news_item">
				<div class="news_item__box">
					<div class="back_img">
						<picture>
							<source type="image/jpg" media="(max-width: 750px)" srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
							<source type="image/jpg" media="(min-width: 751px)" srcset="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
							<img data-img1x="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" data-img2x="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" src="/img/blank.png" alt="">
						</picture>
					</div>
					<div class="news_item__content">
						<div class="news_item__title">
							<? if ($arItem['PROPERTIES']['PERCENT']['VALUE']) { ?>
							Скидка <?=$arItem['PROPERTIES']['PERCENT']['VALUE']?>%
							<? } else { ?>
								<?= $arItem['NAME']; ?>
							<? } ?>
						</div>
						<? if ($arItem['PROPERTIES']['COUPON']['VALUE']) { ?>
						<div>
							<div class="news_item__subtitle">промокод «<?=$arItem['PROPERTIES']['COUPON']['VALUE']?>»</div>
						</div>
						<? } ?>
						<div class="news_item__text"><? if ($arParams['ITEMS_TYPE'] === 'Sales') { ?>Акция действует <? } ?><?=$arItem['salesPeriod']?></div>
					</div>
				</div>
			</a>

			<? } ?>

		</div>

<!--		<div class="button_wrap">-->
<!--			<button class="button button_black news_more"><a href="/"><span>Показать еще</span></a></button>-->
<!--		</div>-->
		<? $APPLICATION->IncludeComponent(
			"bitrix:system.pagenavigation",
			($arParams['PAGENAV_TMPL'] ?? 'back'),
			array(
				"NAV_RESULT" => $arResult["NAV_RESULT"],
				"SEF_MODE" => "N",
				"SHOW_NAV_CHAIN" => "N",
				'BUTTON_CLASS' => 'news_more',
			),
			false
		); ?>

	</div>