<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
{
	die();
}

/** @var CBitrixPersonalOrderListComponent $component */
/** @var array $arParams */
/** @var array $arResult */

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

//\Bitrix\Main\UI\Extension::load([
//	'ui.design-tokens',
//	'ui.fonts.opensans',
//	'clipboard',
//	'fx',
//]);

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
//Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
//$this->addExternalCss("/bitrix/css/main/bootstrap.css");

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else
{
	$filterHistory = ($_REQUEST['filter_history'] ?? '');
	$filterShowCanceled = ($_REQUEST["show_canceled"] ?? '');

	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	if (empty($arResult['ORDERS']))
	{
		if ($filterHistory === 'Y')
		{
			if ($filterShowCanceled === 'Y')
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER')?></h3>
				<?
			}
			else
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST')?></h3>
				<?
			}
		}
		else
		{
			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST')?></h3>
			<?
		}
	}
	?>

	<div class="orders_content">
	<?
	{
		$orderHeaderStatus = null;
		foreach ($arResult['ORDERS'] as $key => $order)
		{
		?>
			<div class="order" data-order-id="<?=$order['ORDER']['ID']?>">
				<div class="order_top">
					<div class="order_top__col order_top__col1">
						<div class="order_top__title">#<?=$order['ORDER']['ID']?> <em>на <?=$order['PAYMENT'][0]['FORMATED_SUM']?></em></div>
						<div class="order_status <?=$order['ORDER']['STATUS_COLOR']?>"><?=$order['ORDER']['STATUS_NAME']?></div>
						<span><?=count($order['BASKET_ITEMS'])?> товара</span>
					</div>
					<div class="order_top__col order_top__col3">
						<div class="order_top__title">Дата оформления заказа</div>
						<div class="order_date"><?=$order['ORDER']['DATE_INSERT_FORMATED']?></div>
					</div>
					<div class="order_top__col order_top__col5">
						<div class="order_top__title">Оплата</div>
<!--						<div class="order_payment green">--><?php //=$order['PAYMENT'][0]['DATE_BILL_FORMATED']?><!--</div>-->
                        <div class="order_payment <?= $order['PAYMENT'][0]['PAID'] !== 'N' ? 'green' : 'red'?>">
                            <?= $order['PAYMENT'][0]['PAID'] !== 'N' ? 'Оплачен' : 'Не оплачен'?>
                        </div>
						<span><?=$order['PAYMENT'][0]['PAY_SYSTEM_NAME']?></span>
					</div>
					<div class="order_top__col order_top__col5">
						<div class="order_top__title">Доставка</div>
						<div class="order_deliverytype"><?=$order['SHIPMENT'][0]['DELIVERY_NAME']?></div>
						<span><?=$order['SHIPMENT']['DELIVERY_ADDRESS']?></span>
					</div>
					<div class="order_top__col order_top__col6">
						<div class="order_top__title">Сумма заказа</div>
						<div class="order_sum"><?=$order['PAYMENT'][0]['FORMATED_SUM']?></div>
					</div>
<!--					<div class="order_top__col order_top__col6">-->
<!--						<a href="" class="order_topaction">Повторить заказ</a>-->
<!--					</div>-->
				</div>

				<div class="order_content">

					<div class="order_content__head">
						<div class="order_content__label">Товар</div>
						<div class="order_content__label">Цена</div>
						<div class="order_content__label">Количество</div>
						<div class="order_content__label">Сумма</div>
					</div>


				</div>
				<div class="order_showdetails">
					<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
						<rect x="32" width="32" height="32" rx="16" transform="rotate(90 32 0)" fill="#F3F3F3"/>
						<path d="M21 14L16 18L11 14" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				</div>
			</div>
			<?
		}
	}
	?>

	</div>

	<div class="orders_morewrap">
		<? echo ($arResult['NAV_STRING']); ?>
	</div>

	<?


	if ($filterHistory !== 'Y')
	{
		$javascriptParams = array(
			"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
			"templateFolder" => CUtil::JSEscape($templateFolder),
			"templateName" => $this->__component->GetTemplateName(),
			"paymentList" => $paymentChangeData,
			"returnUrl" => CUtil::JSEscape($arResult["RETURN_URL"]),
		);
		$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
		?>
		<script>
			BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
		</script>
		<?
	}
}
