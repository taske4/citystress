<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

define('STATUS_COLORS', [
    'OJ' => 'red',
    'OP' => 'red',
    'OT' => 'red',
    'OV' => 'red',
    'VZ' => 'red',
]);

define('STATUS_NAME_ALIAS', [
    'отказ при выдаче' => 'Отмена',
    'возврат' => 'Отмена',
    'Обработка' => 'Принят новый заказ',
]);

$statusLangRes = \Bitrix\Sale\StatusLangTable::query()
    ->setSelect(['STATUS_ID', 'NAME'])
    ->setFilter([
        '=LID' => 'ru',
    ])
    ->fetchAll();

$statusLinks = [];

foreach($statusLangRes as $statusLang) {
    $statusColor = STATUS_COLORS[$statusLang['STATUS_ID']] ?: 'green';

    $alias = STATUS_NAME_ALIAS[strtolower($statusLang['NAME'])];

    if ($alias) {
        $statusLang['NAME'] = $alias;
    }

    $statusLinks[$statusLang['STATUS_ID']] = [
        'name'  => $statusLang['NAME'],
        'color' => $statusColor,
    ];
}

foreach($arResult['ORDERS'] as $key => $value) {
    $arResult['ORDERS'][$key]['ORDER']['STATUS_NAME']  = $statusLinks[$value['ORDER']['STATUS_ID']]['name'];
    $arResult['ORDERS'][$key]['ORDER']['STATUS_COLOR'] = $statusLinks[$value['ORDER']['STATUS_ID']]['color'];

    $order = \Bitrix\Sale\Order::load($value['ORDER']['ID']);
    $propsCollection = $order->getPropertyCollection();
//    $deliveryAddress = $propsCollection->getItemByOrderPropertyCode('DELIVERY_ADDRESS')->getValue();
//    $stressPvzId     = $propsCollection->getItemByOrderPropertyCode('PVZ_SITE_ID')->getValue();
//    $shopId          = $propsCollection->getItemByOrderPropertyCode('SHOP_ID')->getValue();
//    $cdekDeliveryAddress = $propsCollection->getItemByOrderPropertyCode('DELIVERY_CDEK')->getValue();
    $deliveryAddress = $propsCollection->getItemByOrderPropertyCode('DELIVERY_CDEK')->getValue();

//    if ($stressPvzId) {
//        $deliveryAddress = \Bitrix\Iblock\Iblock::wakeUp(SHOPS)->getEntityDataClass()::query()
//            ->setSelect(['NAME'])
//            ->setFilter(['=ID' => ($stressPvzId ?: $shopId)])
//            ->fetch()['NAME'];
//    }

    $arResult['ORDERS'][$key]['SHIPMENT']['DELIVERY_ADDRESS'] = $deliveryAddress;
}