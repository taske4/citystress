<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Loader;

$this->setFrameMode(true);

global $searchFilter;
global $searchFilter2;

$elementOrder = [];
if ($arParams['USE_SEARCH_RESULT_ORDER'] === 'N')
{
	$elementOrder = [
		"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
		"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
	];
}

if (Loader::includeModule('search'))
{
	$arElements = $APPLICATION->IncludeComponent(
		"bitrix:search.page",
		".default",
		[
			"RESTART" => $arParams["RESTART"],
			"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
			"USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
			"CHECK_DATES" => $arParams["CHECK_DATES"],
			"arrFILTER" => [
				"iblock_".$arParams["IBLOCK_TYPE"],
			],
			"arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => [
				$arParams["IBLOCK_ID"],
			]	,
			"USE_TITLE_RANK" => $arParams['USE_TITLE_RANK'],
			"DEFAULT_SORT" => "rank",
			"FILTER_NAME" => "",
			"SHOW_WHERE" => "N",
			"arrWHERE" => [],
			"SHOW_WHEN" => "N",
			"PAGE_RESULT_COUNT" => ($arParams["PAGE_RESULT_COUNT"] ?? 50),
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => "N",
		],
		$component,
		[
			'HIDE_ICONS' => 'Y',
		]
	);
	if (!empty($arElements) && is_array($arElements))
	{
		\Citystress\SearchRecently::getInstance()->record($_REQUEST['q']);

		$searchFilter = [
			"ID" => $arElements,
		];
		if ($arParams['USE_SEARCH_RESULT_ORDER'] === 'Y')
		{
			$elementOrder = [
				"ELEMENT_SORT_FIELD" => "ID",
				"ELEMENT_SORT_ORDER" => $arElements,
			];
		}
	}
}
else
{
	$searchQuery = '';
	if (isset($_REQUEST['q']) && is_string($_REQUEST['q']))
		$searchQuery = trim($_REQUEST['q']);
	if ($searchQuery !== '')
	{
		$searchFilter = [
			'*SEARCHABLE_CONTENT' => $searchQuery
		];
	}
	unset($searchQuery);
}

if (!empty($searchFilter) && is_array($searchFilter))
{
	$arParams['LINE_ELEMENT_COUNT'] = (int)($arParams['LINE_ELEMENT_COUNT'] ?? 3);
	if ($arParams['LINE_ELEMENT_COUNT'] < 2)
	{
		$arParams['LINE_ELEMENT_COUNT'] = 2;
	}
	elseif ($arParams['LINE_ELEMENT_COUNT'] > 4)
	{
		$arParams['LINE_ELEMENT_COUNT'] = 4;
	}

	$componentParams = [
		'COMPATIBLE_MODE' => 'Y',
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
		"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
		"PROPERTY_CODE_MOBILE" => ($arParams["PROPERTY_CODE_MOBILE"] ?? []),
		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => 0,
		"SECTION_URL" => $arParams["SECTION_URL"],
		"DETAIL_URL" => $arParams["DETAIL_URL"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
		"PRICE_CODE" => array('Розничная', 'oldprice'),
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
		"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
		"ADD_PROPERTIES_TO_BASKET" => ($arParams["ADD_PROPERTIES_TO_BASKET"] ?? ''),
		"PARTIAL_PRODUCT_PROPERTIES" => ($arParams["PARTIAL_PRODUCT_PROPERTIES"] ?? ''),
		"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
		"CURRENCY_ID" => $arParams["CURRENCY_ID"],
		"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
		"HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"LAZY_LOAD" => ($arParams["LAZY_LOAD"] ?? 'N'),
		"MESS_BTN_LAZY_LOAD" => ($arParams["~MESS_BTN_LAZY_LOAD"] ?? ''),
		"LOAD_ON_SCROLL" => ($arParams["LOAD_ON_SCROLL"] ?? 'N'),
		"FILTER_NAME" => "searchFilter2",
		"SECTION_USER_FIELDS" => [],
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"META_KEYWORDS" => "",
		"META_DESCRIPTION" => "",
		"BROWSER_TITLE" => "",
		"ADD_SECTIONS_CHAIN" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",

		'LABEL_PROP' => ($arParams['LABEL_PROP'] ?? ''),
		'LABEL_PROP_MOBILE' => ($arParams['LABEL_PROP_MOBILE'] ?? ''),
		'LABEL_PROP_POSITION' => ($arParams['LABEL_PROP_POSITION'] ?? ''),
		'ADD_PICT_PROP' => ($arParams['ADD_PICT_PROP'] ?? ''),
		'PRODUCT_DISPLAY_MODE' => ($arParams['PRODUCT_DISPLAY_MODE'] ?? ''),
		'PRODUCT_BLOCKS_ORDER' => ($arParams['PRODUCT_BLOCKS_ORDER'] ?? ''),
		'PRODUCT_ROW_VARIANTS' => ($arParams['PRODUCT_ROW_VARIANTS'] ?? ''),
		'ENLARGE_PRODUCT' => ($arParams['ENLARGE_PRODUCT'] ?? ''),
		'ENLARGE_PROP' => ($arParams['ENLARGE_PROP'] ?? ''),
		'SHOW_SLIDER' => ($arParams['SHOW_SLIDER'] ?? 'Y'),
		'SLIDER_INTERVAL' => ($arParams['SLIDER_INTERVAL'] ?? '3000'),
		'SLIDER_PROGRESS' => ($arParams['SLIDER_PROGRESS'] ?? 'N'),

		'OFFER_ADD_PICT_PROP' => ($arParams['OFFER_ADD_PICT_PROP'] ?? ''),
		'OFFER_TREE_PROPS' => ($arParams['OFFER_TREE_PROPS'] ?? []),
		'PRODUCT_SUBSCRIPTION' => ($arParams['PRODUCT_SUBSCRIPTION'] ?? ''),
		'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] ?? ''),
		'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] ?? ''),
		'SHOW_MAX_QUANTITY' => ($arParams['SHOW_MAX_QUANTITY'] ?? ''),
		'MESS_SHOW_MAX_QUANTITY' => ($arParams['~MESS_SHOW_MAX_QUANTITY'] ?? ''),
		'RELATIVE_QUANTITY_FACTOR' => ($arParams['RELATIVE_QUANTITY_FACTOR'] ?? ''),
		'MESS_RELATIVE_QUANTITY_MANY' => ($arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?? ''),
		'MESS_RELATIVE_QUANTITY_FEW' => ($arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?? ''),
		'MESS_BTN_BUY' => ($arParams['~MESS_BTN_BUY'] ?? ''),
		'MESS_BTN_ADD_TO_BASKET' => ($arParams['~MESS_BTN_ADD_TO_BASKET'] ?? ''),
		'MESS_BTN_SUBSCRIBE' => ($arParams['~MESS_BTN_SUBSCRIBE'] ?? ''),
		'MESS_BTN_DETAIL' => ($arParams['~MESS_BTN_DETAIL'] ?? ''),
		'MESS_NOT_AVAILABLE' => ($arParams['~MESS_NOT_AVAILABLE'] ?? ''),
		'MESS_BTN_COMPARE' => ($arParams['~MESS_BTN_COMPARE'] ?? ''),

		'USE_ENHANCED_ECOMMERCE' => ($arParams['USE_ENHANCED_ECOMMERCE'] ?? ''),
		'DATA_LAYER_NAME' => ($arParams['DATA_LAYER_NAME'] ?? ''),
		'BRAND_PROPERTY' => ($arParams['BRAND_PROPERTY'] ?? ''),

		'TEMPLATE_THEME' => ($arParams['TEMPLATE_THEME'] ?? ''),
		'ADD_TO_BASKET_ACTION' => ($arParams['ADD_TO_BASKET_ACTION'] ?? ''),
		'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] ?? ''),
		'COMPARE_PATH' => ($arParams['COMPARE_PATH'] ?? ''),
		'COMPARE_NAME' => ($arParams['COMPARE_NAME'] ?? ''),
		'USE_COMPARE_LIST' => ($arParams['USE_COMPARE_LIST'] ?? ''),
	] + $elementOrder;



}

?>

<div class="searchres_container container pagetype1<? if (!$GLOBALS['searchFilter']['ID']) { ?> no_results<? } ?>">
	<div class="searchres_block block">
		<div class="searchres_title">Результат поиска по запросу <span><?=$_GET['q']?></span></div>

			<? if ($GLOBALS['searchFilter']['ID']) { ?>

				<div class="filters">
					<?= '<div class="filters_top">'; ?>

					<? $APPLICATION->IncludeComponent(
						"bitrix:catalog.smart.filter",
						"main",
						array(
							"SHOW_ALL_WO_SECTION" => 'Y',
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"CONVERT_CURRENCY" => "N",
							"DISPLAY_ELEMENT_COUNT" => "Y",
							"FILTER_NAME" => "searchFilter2",
							"FILTER_VIEW_MODE" => "vertical",
							"HIDE_NOT_AVAILABLE" => "N",
							"IBLOCK_ID" => "6",
							"IBLOCK_TYPE" => "catalog",
							"PAGER_PARAMS_NAME" => "arrPager",
							"POPUP_POSITION" => "left",
							"PREFILTER_NAME" => "searchFilter",
							"PRICE_CODE" => ["Розничная"],
							"SAVE_IN_SESSION" => "N",
							"SECTION_CODE" => "",
							"SECTION_DESCRIPTION" => "-",
							"SECTION_TITLE" => "-",
							"SEF_MODE" => "N",
							"TEMPLATE_THEME" => "blue",
							"XML_EXPORT" => "N"
						)
					); ?>
				</div>

		<?

		file_put_contents($_SERVER["DOCUMENT_ROOT"].'/local/logs/smartFilterArrFilter.txt', json_encode($searchFilter2));

			if (!$searchFilter2) {
				$searchFilter2 = $searchFilter;
			}
		?>

		<div class="catalog_items by4">
					<? if ($componentParams) { ?>
						<? $APPLICATION->IncludeComponent(
							"bitrix:catalog.section",
							"catalog_items",
							$componentParams,
							$arResult["THEME_COMPONENT"],
							[
								'HIDE_ICONS' => 'Y',
							]
						); ?>
					<? } ?>
				</div>

		</div>

		<?$APPLICATION->IncludeComponent(
			"bitrix:system.pagenavigation",
			".default",
			array(
				"NAV_RESULT" => $arResult["NAV_RESULT"],
				"SEF_MODE" => "N",
				"SHOW_NAV_CHAIN" => "N",
			),
			false
		);?>
		<? } else { ?>
			<div class="searchres_notext">По вашему запросу ничего не найдено. <br> Попробуйте изменить параметры поиска или перейти к популярным запросам или товарам</div>
		<? } ?>

	</div>
</div>

<? if (!$GLOBALS['searchFilter']['ID']) { ?>

	<div class="homehits_container container searchres_related">
		<div class="homehits_block block">
			<div class="block_top">
				<div class="block_title">Хиты продаж</div>
				<a href="/catalog/odezhda/filter/bestseller-is-4c7b7f88644409e52e3e06e263410090/apply/" class="go_section"><span>Перейти</span> в коллекцию
					<svg fill="none" height="11" viewBox="0 0 19 11" width="19" xmlns="http://www.w3.org/2000/svg">
						<path d="m14.3262 1.25 4.132 4.25m0 0-4.132 4.25m4.132-4.25h-17.9582" stroke="#242424"
							  stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				</a>
			</div>

			<div class="products_sliderwrap">
				<div class="custom_slarrows">
					<div class="custom_slarrow custom_prev">
						<svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
							<path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
								  stroke-width="2"/>
						</svg>
					</div>
					<div class="custom_slarrow custom_next">
						<svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
							<path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
								  stroke-width="2"/>
						</svg>
					</div>
				</div>

				<div class="products_slider">

					<?
					global $bestsellerFilter;

					$bestsellerFilter['PROPERTY_BESTSELLER_VALUE'] = 'Да';
					?>

					<? $APPLICATION->IncludeComponent(
						"bitrix:catalog.section",
						"catalog_items",
						array(
							"ACTION_VARIABLE" => "action",
							"ADD_PICT_PROP" => "-",
							"ADD_PROPERTIES_TO_BASKET" => "Y",
							"ADD_SECTIONS_CHAIN" => "N",
							"ADD_TO_BASKET_ACTION" => "ADD",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"BACKGROUND_IMAGE" => "-",
							"BASKET_URL" => "/personal/basket.php",
							"BROWSER_TITLE" => "-",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"COMPATIBLE_MODE" => "Y",
							"CONVERT_CURRENCY" => "N",
							"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
							"DETAIL_URL" => "",
							"DISABLE_INIT_JS_IN_COMPONENT" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"DISPLAY_COMPARE" => "N",
							"DISPLAY_TOP_PAGER" => "N",
							"ELEMENT_SORT_FIELD" => $_GET["sort"] ?? 'sort',
							"ELEMENT_SORT_FIELD2" => "id",
							"ELEMENT_SORT_ORDER" => $_GET["order"] ?? 'asc',
							"ELEMENT_SORT_ORDER2" => "desc",
							"ENLARGE_PRODUCT" => "STRICT",
							"FILTER_NAME" => "bestsellerFilter",
							"HIDE_NOT_AVAILABLE" => "N",
							"HIDE_NOT_AVAILABLE_OFFERS" => "N",
							"IBLOCK_ID" => "6",
							"IBLOCK_TYPE" => "catalog",
							"INCLUDE_SUBSECTIONS" => "Y",
							"LABEL_PROP" => array("SOSTAV"),
							"LABEL_PROP_MOBILE" => array(),
							"LABEL_PROP_POSITION" => "top-left",
							"LAZY_LOAD" => "N",
							"LINE_ELEMENT_COUNT" => "3",
							"LOAD_ON_SCROLL" => "N",
							"MESSAGE_404" => "",
							"MESS_BTN_ADD_TO_BASKET" => "В корзину",
							"MESS_BTN_BUY" => "Купить",
							"MESS_BTN_DETAIL" => "Подробнее",
							"MESS_BTN_LAZY_LOAD" => "Показать ещё",
							"MESS_BTN_SUBSCRIBE" => "Подписаться",
							"MESS_NOT_AVAILABLE" => "Нет в наличии",
							"META_DESCRIPTION" => "-",
							"META_KEYWORDS" => "-",
							"OFFERS_FIELD_CODE" => array("", ""),
							"OFFERS_LIMIT" => "5",
							"OFFERS_SORT_FIELD" => "sort",
							"OFFERS_SORT_FIELD2" => "id",
							"OFFERS_SORT_ORDER" => "asc",
							"OFFERS_SORT_ORDER2" => "desc",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_TITLE" => "Товары",
							"PAGE_ELEMENT_COUNT" => "12",
							"PARTIAL_PRODUCT_PROPERTIES" => "N",
							"PRICE_CODE" => array("Розничная"),
							"PRICE_VAT_INCLUDE" => "Y",
							"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
							"PRODUCT_DISPLAY_MODE" => "N",
							"PRODUCT_ID_VARIABLE" => "id",
							"PRODUCT_PROPS_VARIABLE" => "prop",
							"PRODUCT_QUANTITY_VARIABLE" => "quantity",
							"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
							"PRODUCT_SUBSCRIPTION" => "Y",
							"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
							"RCM_TYPE" => "personal",
							"SECTION_CODE" => "",
							"SECTION_ID" => "",
							"SECTION_ID_VARIABLE" => "SECTION_ID",
							"SECTION_URL" => "",
							"SECTION_USER_FIELDS" => array("", ""),
							"SEF_MODE" => "N",
							"SET_BROWSER_TITLE" => "Y",
							"SET_LAST_MODIFIED" => "N",
							"SET_META_DESCRIPTION" => "Y",
							"SET_META_KEYWORDS" => "Y",
							"SET_STATUS_404" => "N",
							"SET_TITLE" => "Y",
							"SHOW_404" => "N",
							"SHOW_ALL_WO_SECTION" => "N",
							"SHOW_CLOSE_POPUP" => "N",
							"SHOW_DISCOUNT_PERCENT" => "N",
							"SHOW_FROM_SECTION" => "N",
							"SHOW_MAX_QUANTITY" => "N",
							"SHOW_OLD_PRICE" => "N",
							"SHOW_PRICE_COUNT" => "1",
							"SHOW_SLIDER" => "Y",
							"SLIDER_INTERVAL" => "3000",
							"SLIDER_PROGRESS" => "N",
							"TEMPLATE_THEME" => "blue",
							"USE_ENHANCED_ECOMMERCE" => "N",
							"USE_MAIN_ELEMENT_SECTION" => "N",
							"USE_PRICE_COUNT" => "N",
							"USE_PRODUCT_QUANTITY" => "N",
							'PAGENAV_TMPL' => 'more',
							'SLIDER' => true,
						)
					); ?>

				</div>
			</div>

		</div>
	</div>

<? } ?>

<script>
    $(function(){
        if ($('.catalog_items').find('.product').length) {
            $('.catalog_items').find('.product').each(function () {
                if ($(this).find('.product_gal__img').length) {
                    if (!$(this).find('.product_slider').hasClass('slick-initialized')) {
                        $(this).find('.product_slider').slick({dots: false, arrows: false, infinite: false, slidesToShow: 1, slidesToScroll: 1});
                    }
                }
            });
        }

        $('body').on('click', 'a.product_size', function (e) {
            e.preventDefault();

            if (!$(this).hasClass('disabled')) {
                let productContainer = $(this).parents('[data-product]')
                if (!productContainer.attr('data-product')) {
                    return;
                }

                let productData = JSON.parse(productContainer.attr('data-product'));

                let offerId = $(this).attr('data-offer-id');

                if (offerId) {
                    productData.id = offerId;
                }

                basketAddProduct(productData);
            }
        });
    });
</script>
