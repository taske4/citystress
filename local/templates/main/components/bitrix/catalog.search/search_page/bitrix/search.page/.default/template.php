<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<form action="" method="get" class="search search_bar">

	<button type="submit" value="<?=GetMessage("SEARCH_GO")?>" style="border: none;background: none;padding: 1em;cursor: pointer;">
		<svg class="search_form__loupe1" fill="none" height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m11.2845 11.36 2.3154 2.24m-.7467-5.97334c0 2.88664-2.34 5.22664-5.22663 5.22664-2.88661 0-5.22667-2.34-5.22667-5.22664 0-2.88661 2.34006-5.22667 5.22667-5.22667 2.88663 0 5.22663 2.34006 5.22663 5.22667z" stroke="#242424" stroke-linecap="round" stroke-width="2"/></svg>
	</button>
	<svg class="search_form__loupe2" fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="m16.9268 17.04 3.4732 3.36m-1.12-8.96c0 4.3299-3.5101 7.84-7.84 7.84-4.32994 0-7.84002-3.5101-7.84002-7.84 0-4.32994 3.51008-7.84002 7.84002-7.84002 4.3299 0 7.84 3.51008 7.84 7.84002z" stroke="#242424" stroke-linecap="round"/></svg>

	<input type="text" class="search_form__input" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" size="40" />
	<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />


	<div class="search_bar__close">
		<svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 .000061)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8235)" width="26.6202"/></g></svg>
	</div>
</form><br />

<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>