<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

$schema = [
    "@context"        => "https://schema.org",
    "@type"           => "BreadcrumbList",
    "itemListElement" => [
        [
            'position' => 1,
            '@type'    => 'ListItem',
            'item'     =>
            [
            'name' => 'Главная',
            '@id' => 'https://citystress.ru/',
            ],
        ]
    ]
];

$result = '<div class="breadcrumbs">
    <a href="/">Главная</a>';

$i = 2;

foreach ($arResult as $key => $chain) {
    if ($chain['TITLE'] === 'Каталог' || !$arResult[$key+1]) {
        continue;
    }

    if ($GLOBALS['APPLICATION']->GetCurPage(false) === $chain['LINK']) {
        $result .= '<i>/</i>
        <span>' . $chain['TITLE'] . '</span>';
    } else {
        $result .= '<i>/</i>
        <a href="' . $chain['LINK'] . '">' . $chain['TITLE'] . '</a>';
    }


    $schema['itemListElement'][] = [
        'position' => $i++,
        '@type'    => 'ListItem',
        'item'     => [
            'name' => $chain['TITLE'],
            '@id'  => 'https://citystress.ru'.$chain['LINK'],
        ]
    ];
}



$result .= '</div>';

if (!$GLOBALS['schemaBreadcrumb']) {
    $GLOBALS['schemaBreadcrumb'] = true;
    $result .= '<script type="application/ld+json">';
    $result .= json_encode($schema);
    $result .= '</script>';
}


return $result;