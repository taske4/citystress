;function applyFilter()
{
	let data = {
		'ajax': 'y',
	};

	$('.filters_item [data-control-id].active').each(function( index ) {
		let controlId = $(this).attr('data-control-id');
		data[controlId] = 'Y';
	});

	$.ajax({
		'type': 'GET',
		'data': data,
		'headers': {
			'Bx-ajax': true,
		},
		'success': function (res) {
			res.split('{')[0]; // 50ml
			console.log(res);
		}
	});
}

$(function (){
	$('.filters_item').on('click', function (){
		applyFilter();
	});
});