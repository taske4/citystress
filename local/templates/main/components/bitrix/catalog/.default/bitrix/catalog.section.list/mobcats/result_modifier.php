<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * @var $arResult
 */

global $arCurSection;

$mainSectionsLevel = $arCurSection['DEPTH_LEVEL']+1;

foreach($arResult['SECTIONS'] as $index => $section) {
    if ($section['DEPTH_LEVEL'] == $mainSectionsLevel) {
        $mainSections[$section['ID']] = $section;
    } elseif ($section['DEPTH_LEVEL'] == $mainSectionsLevel+1) {
        $subSections[$section['IBLOCK_SECTION_ID']][] = $section;
    }
}

$arResult['MAIN_SECTIONS'] = $mainSections;
$arResult['SUB_SECTIONS']  = $subSections;