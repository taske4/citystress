<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $arCurSection;

?>

<div class="mobcats" style="z-index: 9999999999999;">
    <div class="mobcats_head">
        <span>Категории</span>
        <svg class="mobcats_head__close" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"></rect><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"></rect></g></svg>
    </div>
    <!--noindex-->
    <div class="mobcats_body">
        <div class="customscroll">

            <ul class="mobcats_lvl1ul">
                <li class="mobcats_lvl1li w_sub">
                    <span>О1@!де111жда</span>
                    <ul class="mobcats_lvl2ul">
                        <li class="mobcats_lvl2li w_sub">
                            <span>Топы и блузы</span>
                            <ul class="mobcats_lvl3ul">
                                <li class="mobcats_lvl3li"><span>С длинным рукавом</span></li>
                                <li class="mobcats_lvl3li"><span>По фигуре</span></li>
                                <li class="mobcats_lvl3li"><span>Без рукава</span></li>
                                <li class="mobcats_lvl3li"><span>Свободные</span></li>
                                <li class="mobcats_lvl3li"><span>Базовые</span></li>
                                <li class="mobcats_lvl3li"><span>С длинным рукавом</span></li>
                                <li class="mobcats_lvl3li all"><span>Показать все</span></li>
                            </ul>
                        </li>
                        <li class="mobcats_lvl2li w_sub"><span>Джемперы и блузы</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Штаны и брюки</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Деним</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Свитшоты и Худи</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Рубашки</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Футболки и лонгсливы</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Sale</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Верхняя одежда</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Комбинезоны</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Юбки</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Жакеты</span></li>
                        <li class="mobcats_lvl2li w_sub"><span>Платья</span></li>
                        <li class="mobcats_lvl2li all"><span>Показать все</span></li>
                    </ul>
                </li>
                <li class="mobcats_lvl1li w_sub">
                    <span>Аксессуары</span>
                </li>
                <li class="mobcats_lvl1li w_sub">
                    <span>Коллекции</span>
                </li>
                <li class="mobcats_lvl1li w_sub red">
                    <span>CYber Monday</span>
                </li>
                <li class="mobcats_lvl1li all">
                    <span>Показать все</span>
                </li>
            </ul>


        </div>
    </div>
    <!--/noindex-->
</div>
