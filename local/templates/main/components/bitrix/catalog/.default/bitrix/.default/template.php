<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);

?>

<div class="catalog_items by4">

	<div class="product v2">
		<ul class="product_tags">
			<?php if ( $product_full == 1 ):?>
				<li class="dark">NEW</li>
				<li>Year 2024</li>
			<?php endif;?>
			<li>70%</li>
		</ul>
		<div class="product_top">
			<a href="" class="product_mobcart">
				<img src="/img/mob_addtocart.svg" alt="">
			</a>

			<div class="product_slider">
				<div class="product_gal__img product_gal__img1">
					<div class="back_img">
						<picture>
							<source type="image/jpg" media="(max-width: 750px)" srcset="/img/<?php echo $product_img;?>_1x.jpg">
							<source type="image/jpg" media="(min-width: 751px)" srcset="/img/<?php echo $product_img;?>_1x.jpg 1x, /img/<?php echo $product_img;?>.jpg 2x">
							<img data-img1x="/img/<?php echo $product_img;?>_1x.jpg" data-img2x="/img/<?php echo $product_img;?>.jpg" src="/img/blank.png" alt="">
						</picture>
					</div>
				</div>
				<div class="product_gal__img product_gal__img2">
					<div class="back_img">
						<picture>
							<source type="image/jpg" media="(max-width: 750px)" srcset="/img/6_1x.jpg">
							<source type="image/jpg" media="(min-width: 751px)" srcset="/img/6_1x.jpg 1x, /img/6.jpg 2x">
							<img data-img1x="/img/6_1x.jpg" data-img2x="/img/6.jpg" src="/img/blank.png" alt="">
						</picture>
					</div>
				</div>
				<div class="product_gal__img product_gal__img3">
					<div class="back_img">
						<picture>
							<source type="image/jpg" media="(max-width: 750px)" srcset="/img/7_1x.jpg">
							<source type="image/jpg" media="(min-width: 751px)" srcset="/img/7_1x.jpg 1x, /img/7.jpg 2x">
							<img data-img1x="/img/7_1x.jpg" data-img2x="/img/7.jpg" src="/img/blank.png" alt="">
						</picture>
					</div>
				</div>
				<?php if ( $product_full == 1 ):?>
					<div class="product_gal__img product_gal__img3">
						<div class="back_img">
							<picture>
								<source type="image/jpg" media="(max-width: 750px)" srcset="/img/8_1x.jpg">
								<source type="image/jpg" media="(min-width: 751px)" srcset="/img/8_1x.jpg 1x, /img/8.jpg 2x">
								<img data-img1x="/img/8_1x.jpg" data-img2x="/img/8.jpg" src="/img/blank.png" alt="">
							</picture>
						</div>
					</div>
				<?php endif;?>
			</div>

			<div class="product_body">
				<a href="/product" class="product_nav">
					<ul>
						<li data-index="1" class="product_nav__item active"></li>
						<li data-index="2" class="product_nav__item"></li>
						<li data-index="3" class="product_nav__item"></li>
						<?php if ( $product_full == 1 ):?>
							<li data-index="4" class="product_nav__item"></li>
						<?php endif;?>
					</ul>
				</a>
				<div class="product_add">
					<div class="product_add__title">Добавить в корзину</div>
					<div class="product_sizes">
						<a class="product_size" href="" data-title="XS" data-subtitle="(RU 42)">XS 42</a>
						<a class="product_size" href="" data-title="S" data-subtitle="(RU 44)">S 44</a>
						<a class="product_size <?php if ( $product_full == 1 ):?>disabled<?php endif;?>" href="" data-title="M" data-subtitle="(RU 46)">M 46</a>
						<a class="product_size" href="" data-title="L" data-subtitle="(RU 48)">L 48</a>
						<a class="product_size <?php if ( $product_full == 1 ):?>disabled<?php endif;?>" href="" data-title="XL" data-subtitle="(RU 50)">XL 50</a>
					</div>
				</div>
			</div>
		</div>

		<div class="product_bottom">
			<div class="product_title">
				<a href="">Хлопковый топ</a>
			</div>

			<div class="product_prices">
				<span class="product_price">699 ₽</span>
				<span class="product_priceold">2 999 ₽</span>
			</div>

			<?php if ( $product_nobottom != 1 ):?>

				<div class="product_variants">

					<a href="" class="product_measure">
						<svg fill="none" height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><g fill="#242424"><path d="m4.68936 15.5115c-.37076 0-.74151-.1853-1.01957-.3707l-2.966029-2.966c-.278064-.2781-.370753-.6489-.370753-1.0196 0-.3708.185377-.7415.370753-1.0196l9.268829-9.268803c.55611-.55613 1.48301-.55613 1.94641 0l2.9661 2.966023c.278.27807.3707.64882.3707 1.01957 0 .37076-.1854.74151-.3707 1.01957l-9.26886 9.26884c-.27806.1854-.55613.3707-.92688.3707zm6.30284-14.08857c-.0927 0-.2781.09268-.3708.09268l-9.26882 9.26879c-.09269.0927-.09269.1854-.09269.3708s.09269.2781.09269.3707l2.96602 2.9661c.18538.1853.46345.1853.64882 0l9.26878-9.26885c.0927-.09269.0927-.18538.0927-.37076 0-.18537-.0927-.27806-.0927-.37075l-2.966-2.96603s-.0927-.09268-.278-.09268z"/><path d="m8.21114 6.70615c-.09268 0-.27806 0-.37075-.09269l-1.39032-1.39032c-.18538-.18538-.18538-.46344 0-.64882.18537-.18538.46344-.18538.64882 0l1.39032 1.39032c.18538.18538.18538.46345 0 .64882-.09269 0-.18538.09269-.27807.09269z"/><path d="m6.0798 8.83799c-.09269 0-.27807 0-.37076-.09269l-1.39032-1.39033c-.18538-.18537-.18538-.46344 0-.64881.18538-.18538.46344-.18538.64882 0l1.39032 1.39032c.18538.18538.18538.46344 0 .64882 0 0-.18537.09269-.27806.09269z"/><path d="m3.94747 10.9698c-.09268 0-.27806 0-.37075-.0927l-1.39032-1.3903c-.18538-.18537-.18538-.46344 0-.64882.18537-.18537.46344-.18537.64881 0l1.39033 1.39032c.18538.1854.18538.4635 0 .6488 0 0-.18538.0927-.27807.0927z"/><path d="m10.3435 4.57433c-.0927 0-.2781 0-.37078-.09269l-1.39033-1.39032c-.18537-.18538-.18537-.46344 0-.64882.18538-.18538.46344-.18538.64882 0l1.39029 1.39032c.1854.18538.1854.46345 0 .64882-.0927.09269-.1853.09269-.278.09269z"/></g></svg>
					</a>

					<a href="" class="product_fav <?php if ( $product_full == 1 ):?>active<?php endif;?>"> <!-- добавляем класс active, если в избранном -->
						<svg fill="none" height="15" viewBox="0 0 16 15" width="16" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="m8 2.24296c.00309.00353.00303.00358.00303.00358l.0051-.00465c.00484-.00439.01286-.01161.02396-.02135.02219-.01949.05661-.04905.10234-.08638.09157-.07474.22783-.18006.40147-.2977.349-.23645.83983-.5164 1.41532-.70164 1.12918-.363466 2.58788-.37135 4.01108 1.09592l.0044.00455.0034.00338.0025.00251c.0028.00273.0076.0077.0144.01484.0137.0143.0351.03727.0627.06851.0552.06258.1346.15783.2253.28261.1821.25055.4055.61418.5725 1.06614.3281.88796.4497 2.13808-.4482 3.60505l-.0368.06012-.0119.0461c-.001.0023-.0023.00509-.0039.00839-.0127.02696-.0424.08477-.1028.17974-.1214.19072-.3595.52046-.8139 1.03791-.854.97262-2.4532 2.58731-5.43 5.16271-2.97678-2.5754-4.57596-4.19009-5.43-5.16271-.45436-.51745-.69251-.84719-.81386-1.03791-.06044-.09497-.09018-.15278-.10286-.17974-.00156-.0033-.00283-.00609-.00386-.00839l-.01188-.0461-.0368-.06012c-.897931-1.46697-.776381-2.71709-.44828-3.60505.167-.45196.3904-.81559.57253-1.06614.0907-.12478.17009-.22003.22531-.28261.02757-.03124.049-.05421.06265-.06851.00683-.00714.0117-.01211.01441-.01484l.00229-.0023.00368-.00359.00441-.00455c1.4232-1.46727 2.88186-1.459386 4.01104-1.09592.57549.18524 1.06632.46519 1.41532.70164.17364.11764.3099.22296.40147.2977.04573.03733.08015.06689.10234.08638.0111.00974.01912.01696.02396.02135l.0051.00465s-.00006-.00005.00303-.00358zm0-1.16037c-.0116-.00796-.02335-.01598-.03525-.02404-.39631-.268496-.96454-.594474-1.644-.813184-1.37984-.444149-3.21325-.436565-4.92366 1.324354l-.007.00702c-.00651.00657-.01526.01552-.02604.0268-.02154.02255-.05122.05446-.08729.09535-.07208.08168-.17014.19969-.280083.35093-.219154.30149-.489785.74091-.693784 1.29301-.410695 1.11148-.5402 2.65801.503789 4.3907.007751.01921.017088.04068.02836.06464.032688.06948.082795.1623.160304.28411.154524.24286.424614.6113.899634 1.15227.94223 1.07305 2.71163 2.84845 6.0384 5.69815l.05466.0673c.00399-.0034.00798-.0068.01196-.0102.00399.0034.00797.0068.01196.0102l.05466-.0673c3.32678-2.8497 5.09618-4.6251 6.03838-5.69815.475-.54097.7451-.90941.8997-1.15227.0775-.12181.1276-.21463.1603-.28411.0112-.02396.0206-.04543.0283-.06463 1.044-1.7327.9145-3.27923.5038-4.39071-.204-.5521-.4746-.99152-.6938-1.29301-.1099-.15124-.208-.26925-.2801-.35093-.036-.04089-.0657-.0728-.0873-.09535-.0107-.01128-.0195-.02023-.026-.0268l-.007-.00702c-1.7104-1.760919-3.5438-1.768503-4.92365-1.324354-.67946.21871-1.24769.544688-1.644.813184-.01189.00806-.02365.01608-.03525.02404z" fill="#242424" fill-rule="evenodd"/></svg>
						<svg class="fav_active" fill="none" height="15" viewBox="0 0 16 15" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m7.96475 1.05855.03525.02404.03525-.02404c.39631-.268496.96454-.594474 1.644-.813184 1.37985-.444149 3.21325-.436565 4.92365 1.324354l.007.00702c.0065.00657.0153.01552.026.0268.0216.02255.0513.05446.0873.09535.0721.08168.1702.19969.2801.35093.2192.30149.4898.74091.6938 1.29301.4107 1.11148.5402 2.65801-.5038 4.39071-.0077.0192-.0171.04067-.0283.06463-.0327.06948-.0828.1623-.1603.28411-.1546.24286-.4247.6113-.8997 1.15227-.9422 1.07305-2.7116 2.84845-6.03838 5.69815l-.05466.0673-.01196-.0102-.01196.0102-.05466-.0673c-3.32677-2.8497-5.09617-4.6251-6.0384-5.69815-.47502-.54097-.74511-.90941-.899634-1.15227-.077509-.12181-.127616-.21463-.160304-.28411-.011272-.02396-.020609-.04543-.02836-.06464-1.043989-1.73269-.914484-3.27922-.503789-4.3907.203999-.5521.47463-.99152.693784-1.29301.109943-.15124.208003-.26925.280083-.35093.03607-.04089.06575-.0728.08729-.09535.01078-.01128.01953-.02023.02604-.0268l.007-.00702c1.71041-1.760919 3.54382-1.768503 4.92366-1.324354.67946.21871 1.24769.544688 1.644.813184z" fill="#242424"/></svg>
					</a>

					<ul>
						<li><div style="background: #D62D30;"></div></li>
						<li class="active"><div style="background: #999999;"></div></li>
						<li><div style="background: #EEEEEE;"></div></li>
					</ul>
				</div>

				<?php if ( $product_full == 1 ):?>
					<div class="button_wrap">
						<button type="button" class="button button_tr complete_look" data-lookid="1">Смотреть Весь образ</button>
					</div>
				<?php endif;?>

			<?php endif;?>
		</div>
	</div>

</div>