<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
    global $openSectionId;
?>

<script>
    $(function(){
        let list = [];

        function collectTriggerElements(sectionId, list, parentSearch=false)
        {
            let element = '[data-section-id="'+sectionId+'"]';

            if ( $(element).length)
            {
                list.push($(element));

                if (parseInt($(element).data('parent-section-id')) > 0) {
                    collectTriggerElements($(element).data('parent-section-id'), list);
                }
            }
        }

        collectTriggerElements(<?=$openSectionId?>, list);

        window.collectionTriggerElements = list.reverse();

    });
</script>
