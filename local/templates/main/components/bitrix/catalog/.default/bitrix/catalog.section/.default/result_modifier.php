<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

Loader::includeModule("highloadblock");

$colors = HL\HighloadBlockTable::getById(HL_COLORS)->fetch();
$colors = HL\HighloadBlockTable::compileEntity($colors);
$colors_data_class = $colors->getDataClass();
$colors = $colors_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_HASH'],
))->fetchAll();

$allColors = [];
foreach($colors as $color) {
    $allColors[$color['UF_NAME']] = $color;
}

/**
 * @var $arResult
 */

Loader::IncludeModule('catalog');

foreach($arResult['ITEMS'] as &$arItem) {
    $arItem['PRODUCT_SETS'] = \Bitrix\Catalog\ProductSetsTable::query()
        ->setSelect(['ITEM_ID'])
        ->setFilter([
            '=OWNER_ID' => $arItem['ID'],
            '!ITEM_ID'  => $arItem['ID'],
        ])
        ->fetchAll();

    $arItem['sizes']  = [];
    $arItem['colors'] = [];

    foreach ($arItem['OFFERS'] as $arOffer) {
        $arItem['colors'][$allColors[$arOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']]['UF_HASH']] = $arOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE'];
        $arItem['sizes'][$arOffer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']][$arOffer['DISPLAY_PROPERTIES']['SIZE']['DISPLAY_VALUE']] = $arOffer['ID'];
    }
}