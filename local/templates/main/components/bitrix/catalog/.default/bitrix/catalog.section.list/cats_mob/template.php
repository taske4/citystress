<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $arCurSection, $openSectionId;

?>

<div class="mobcats">
    <div class="mobcats_head">
        <span>Категории</span>
        <svg class="mobcats_head__close" fill="none" height="20" viewBox="0 0 20 20" width="20"
             xmlns="http://www.w3.org/2000/svg">
            <g fill="#242424" opacity=".25">
                <rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)"
                      width="26.6202"></rect>
                <rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)"
                      width="26.6202"></rect>
            </g>
        </svg>
    </div>
    <!--noindex-->
    <div class="mobcats_body">
        <div class="customscroll">
            <?
            $WHEN_UL_OPEN_DEPTH_LEVEL = $CURRENT_DEPTH = $TOP_DEPTH = $arResult["SECTION"]["DEPTH_LEVEL"];
            $CHAIN = [];

            $curSectionDepthLevel = $arCurSection['DEPTH_LEVEL'];

            foreach ($arResult["SECTIONS"] as $sectionKey => $arSection) {

                if ($max < ($arSection["DEPTH_LEVEL"] - $TOP_DEPTH)) {
                    $max = ($arSection["DEPTH_LEVEL"] - $TOP_DEPTH);
                }

                if ($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"]) {
                    $WHEN_UL_OPEN_DEPTH_LEVEL = ($arSection["DEPTH_LEVEL"] - $TOP_DEPTH);
                    $CHAIN[$arResult['SECTIONS'][$sectionKey-1]['CODE']] = $WHEN_UL_OPEN_DEPTH_LEVEL;

                    echo "<ul class='mobcats_lvl" . ($arSection["DEPTH_LEVEL"] - $TOP_DEPTH) . "ul'>";

                    if ($CURRENT_DEPTH <= $curSectionDepthLevel) {
                        $link = '<span><div class="future_link" data-href="' . $arSection["SECTION_PAGE_URL"] . '">(Раньше не ссылка)' . $arSection["NAME"] . '</div></span>';
                    } else {
                        if ($GLOBALS['APPLICATION']->GetCurPage(false) === $arSection['SECTION_PAGE_URL']) {
                            $link = '<span>' . $arSection["NAME"] . '</span>';
                        } else {
                            $link = '<span><a href="' . $arSection["SECTION_PAGE_URL"] . '">' . $arSection["NAME"] . '</a></span>';
                        }
                    }
                } elseif ($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"]) {
                    echo "</li>";
                } else {
                    while ($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"]) {
                        echo "</li>";
                        ?>
                        <li class="mobcats_lvl<?= end($CHAIN) ?>li all">
                            <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/'.array_search(end($CHAIN), $CHAIN).'/') { ?>
                                <span><!--noindex-->Показать все<!--/noindex--></span>
                            <? } else { ?>
                                <span><a href="/catalog/<?= array_search(end($CHAIN), $CHAIN) ?>/"  rel="nofollow noopener"><!--noindex-->Показать все<!--/noindex--></a></span>
                            <? } ?>
                        </li>
                        <?
                        unset($CHAIN[array_search(end($CHAIN), $CHAIN)]);
                        echo "</ul>";
                        $CURRENT_DEPTH--;
                    }
                    echo "</li>";
                }

                //                if (!$link) {
                if ($CURRENT_DEPTH <= $curSectionDepthLevel) {
                    if ($GLOBALS['APPLICATION']->GetCurPage(false) === $arSection['SECTION_PAGE_URL']) {
                        $link = '<span>' . $arSection["NAME"] . '</span>';
                    } else {
                        $link = '<span><div class="future_link" data-href="' . $arSection["SECTION_PAGE_URL"] . '">' . $arSection["NAME"] . '</div></span>';
                    }
                } else {
                    if ($GLOBALS['APPLICATION']->GetCurPage(false) === $arSection['SECTION_PAGE_URL']) {
                        $link = '<span>' . $arSection["NAME"] . '</span>';
                    } else {
                        $link = '<span><a href="' . $arSection["SECTION_PAGE_URL"] . '">' . $arSection["NAME"] . '</a></span>';
                    }
                }
                //                }

                echo '<li data-parent-section-id="'.$arSection['IBLOCK_SECTION_ID'].'" data-section-id="'.$arSection['ID'].'" class="mobcats_lvl' . ($arSection["DEPTH_LEVEL"] - $TOP_DEPTH) . 'li">' . $link;

                $CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
            }

            while ($CURRENT_DEPTH > $TOP_DEPTH) {
                echo "</li>";

                ?>
                <li class="mobcats_lvl1li all">
                    <? if (isset($arCurSection['PARENT_SECTION'])) { ?>
                        <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/'.$arCurSection['PARENT_SECTION']['CODE']) { ?>
                            <span><!--noindex-->Показать все<!--/noindex--></span>
                        <? } else { ?>
                            <span><a href="/catalog/<?= $arCurSection['PARENT_SECTION']['CODE'] ?>/" rel="nofollow noopener"><!--noindex-->Показать все<!--/noindex--></a></span>
                        <? } ?>
                    <? } else { ?>
                        <span><!--noindex-->Показать все<!--/noindex--></span>
                    <? } ?>
                </li>
                <?

                echo "</ul>";
                $CURRENT_DEPTH--;
            }
            ?>
        </div>
    </div>
    <!--/noindex-->
</div>

<div class="categories_mobin">
    <span>Категории</span>
    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1 13H15M1 8.5H15M1 4H15" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
</div>