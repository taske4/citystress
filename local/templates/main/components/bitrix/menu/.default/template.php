<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul>

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;

	$attr = '';
	if ($arItem['TEXT'] === 'Политика конфиденциальности') {
		$attr = 'rel=nofollow';
	}
?>
	<li><a href="<?=$arItem["LINK"]?>" <?=$attr?>><?=$arItem["TEXT"]?></a></li>
<?endforeach?>

</ul>
<?endif?>