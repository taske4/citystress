<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>

<div class="mobcats" style="height: 641px;">
    <div class="mobcats_head">
        <span>Категории</span>
        <svg class="mobcats_head__close" fill="none" height="20" viewBox="0 0 20 20" width="20"
             xmlns="http://www.w3.org/2000/svg">
            <g fill="#242424" opacity=".25">
                <rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)"
                      width="26.6202"></rect>
                <rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)"
                      width="26.6202"></rect>
            </g>
        </svg>
    </div>

    <!--noindex-->
    <div class="mobcats_body">
        <div class="customscroll mCustomScrollbar _mCS_15 mCS_no_scrollbar">
            <div id="mCSB_15" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;"
                 tabindex="0">
                <div id="mCSB_15_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y"
                     style="position:relative; top:0; left:0;" dir="ltr">

                    <ul class="mobcats_lvl1ul">
                        <? foreach ($arResult['MAIN_SECTIONS'] as $sectionId => $section) { ?>
                            <? $subSections = $arResult['SUB_SECTIONS'][$sectionId]; ?>
                            <li class="mobcats_lvl1li">
                            <span><a
                                    href="?newsfilter[section]=<?= $section['ID'] ?>"><?= $section['NAME'] ?></a></span>

                            <? if ($subSections) { ?>
                                <ul class="mobcats_lvl2ul">
                                    <? foreach ($subSections as $subSection) { ?>
                                        <li class="mobcats_lvl2li" data-parent-section-id="<?=$sectionId?>" data-section-id="<?=$subSection['ID'];?>">
                                        <span><a
                                                href="?newsfilter[section]=<?= $subSection['ID'] ?>"><?= $subSection['NAME'] ?></a></span>
                                        </li>
                                    <? } ?>
                                    <li class="mobcats_lvl2li all"><span><a
                                                href="?newsfilter[section]">Показать все</a></span></li>
                                </ul>
                                </li>
                            <? } ?>
                            </li>
                        <? } ?>
                        <li class="mobcats_lvl1li all">
                            <span><a href="?newsfilter[section]">Показать все</a></span>
                        </li>
                    </ul>

                </div>
                <div id="mCSB_15_scrollbar_vertical"
                     class="mCSB_scrollTools mCSB_15_scrollbar mCS-light mCSB_scrollTools_vertical"
                     style="display: none;">
                    <div class="mCSB_draggerContainer">
                        <div id="mCSB_15_dragger_vertical" class="mCSB_dragger"
                             style="position: absolute; min-height: 0px; height: 0px; top: 0px;">
                            <div class="mCSB_dragger_bar" style="line-height: 0px;"></div>
                            <div class="mCSB_draggerRail"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/noindex-->
</div>

<div class="categories_mobin">
    <span>Категории</span>
    <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M1 13H15M1 8.5H15M1 4H15" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
</div>

<script>
    $(function () {
        $('.mobcats_lvl2ul').prev().each(function () {
            $(this).addClass('has_children');
            $(this).parent().addClass('w_sub');
        });

        $('.mobcats_lvl3ul').prev().each(function () {
            $(this).addClass('has_children');
            $(this).parent().addClass('w_sub');
        });
    });
</script>