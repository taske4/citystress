<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var $arResult
 */

$remove = [];

foreach($arResult['SECTIONS'] as $index => $section) {
    $scRes = \CIBlockSection::GetNavChain(
        CATALOG_IBLOCK_ID,
        $section['ID'],
        ["ID","DEPTH_LEVEL", 'NAME', 'CODE', 'DEPTH_LEVEL']
    );

    while ($scValue = $scRes->GetNext()) {
        if ($scValue['DEPTH_LEVEL'] < 3 && ($scValue['ID'] !== $section['ID'])) {
            $arResult['SECTIONS'][$scValue['ID']] = $scValue;
        }
    }

    if ($section['DEPTH_LEVEL'] == 1) {
        $remove[] = $index;
    }
}

foreach($remove as $index) {
    unset($arResult['SECTIONS'][$index]);
}

$mainSections = $subSections = [];

foreach($arResult['SECTIONS'] as $index => $section) {
    $mobSections[$section['IBLOCK_SECTION_ID']][] = $section;

    if (intval($section['DEPTH_LEVEL']) === 2) {
        $mainSections[$section['ID']] = $section;
    } elseif (intval($section['DEPTH_LEVEL']) === 3) {
        $subSections[$section['IBLOCK_SECTION_ID']][] = $section;
    }
}

$arResult['MAIN_SECTIONS'] = $mainSections;
$arResult['SUB_SECTIONS']  = $subSections;