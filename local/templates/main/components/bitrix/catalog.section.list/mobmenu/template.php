<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="customscroll">
    <ul class="mobmenu_lvl1">


        <? $mainSectionsIndex = 0; ?>
        <? foreach ($arResult['MAIN_SECTIONS'] as $sectionId => $section) { ?>
            <?
            $isActive = $mainSectionsIndex === 0;
            $mainSectionsIndex++;
            ?>
            <li class="mobmenu_lvl1li">
                <svg fill="none" height="10" viewBox="0 0 20 10" width="20" xmlns="http://www.w3.org/2000/svg">
                    <path d="m19.0588.882383-9.0588 8.235287-9.058809-8.235288" stroke="#333" stroke-linecap="round"
                          stroke-linejoin="round"/>
                </svg>
                <span><?=$section['NAME']?></span>
                <? if ($arResult['SUB_SECTIONS'][$section['ID']]) { ?>
                    <ul class="mobmenu_lvl2">
                        <? foreach ($arResult['SUB_SECTIONS'][$section['ID']] as $index => $subSection) { ?>
                                <li class="mobmenu_lvl2li">
                                    <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === $subSection['SECTION_PAGE_URL']) { ?>
                                        <span class="before-link"><?=$subSection['NAME'];?></span>
                                    <? } else { ?>
                                        <a href="<?=$subSection['SECTION_PAGE_URL'];?>"><?=$subSection['NAME'];?></a>
                                    <? } ?>
                                </li>
                        <? } ?>
                    </ul>
                <? } ?>
            </li>
        <? } ?>

        <li class="mobmenu_lvl1li">
            <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/odezhda/') { ?>
                <span>Новинки</span>
            <? } else { ?>
                <a href="/catalog/odezhda/filter/new-is-d355b0a02d11c2a119477e7dd3d299a6/apply/"><span>Новинки</span></a>
            <? } ?>
        </li>

        <li class="mobmenu_lvl1li">
            <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/catalog/odezhda/') { ?>
                <span>Sale</span>
            <? } else { ?>
                <a href="/catalog/odezhda/filter/sale-is-9750bfc234420f1e1c7feabcbf1e9bfe/apply/"><span>Sale</span></a>
            <? } ?>
        </li>

        <li class="mobmenu_lvl1li">
            <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === $section['SECTION_PAGE_URL']) { ?>
                <span>Магазины</span>
            <? } else { ?>
                <a href="/shops/"><span>Магазины</span></a>
            <? } ?>
        </li>
        <li class="mobmenu_lvl1li">
            <svg fill="none" height="10" viewBox="0 0 20 10" width="20" xmlns="http://www.w3.org/2000/svg">
                <path d="m19.0588.882383-9.0588 8.235287-9.058809-8.235288" stroke="#333" stroke-linecap="round"
                      stroke-linejoin="round"/>
            </svg>
            <span>Покупателям</span>
            <ul class="mobmenu_lvl2">
                <li class="mobmenu_lvl2li">
                    <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === '/news-sales/') { ?>
                        <span>АКЦИИ И НОВОСТИ</span>
                    <? } else { ?>
                        <a href="/news-sales/">АКЦИИ И НОВОСТИ</a>
                    <? } ?>
                </li>
            </ul>
        </li>
    </ul>
</div>