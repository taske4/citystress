<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="submenu submenu_sale">
    <div class="submenu_close"></div>
    <div class="submenu_content">
        <div class="submenu_nav">
            <ul>
                <? $mainSectionsIndex = 0; ?>
                <? foreach($arResult['MAIN_SECTIONS'] as $sectionId => $section) { ?>
                    <?
                        $isActive = $mainSectionsIndex === 0;
                        $mainSectionsIndex++;
                    ?>
                    <li data-cat="2_<?=$section['ID']?>"<?= $isActive ? ' class="active"' : ''; ?>>
                        <span>
                            <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === $section['SECTION_PAGE_URL']) { ?>
                                <span class="before-link"><?=$section['NAME']?></span>
                            <? } else { ?>
                                <a href="<?=$section['SECTION_PAGE_URL'];?>filter/sale-is-9750bfc234420f1e1c7feabcbf1e9bfe/apply/"><?= $section['NAME']; ?></a>
                            <? } ?>
                        </span>
                    </li>
                <? } ?>
            </ul>
        </div>

        <? $mainSectionsIndex = 0; ?>
        <? foreach ($arResult['MAIN_SECTIONS'] as $mainIndex => $mainSection) { ?>
            <?
                $isActive = $mainSectionsIndex === 0;
                $mainSectionsIndex++;
            ?>
            <div class="submenu_out submenu_out_2_<?= $mainSection['ID'] ?>"<?= $isActive ? 'style="display: block; "' : ''; ?>>
                <div class="submenu_out__flex">
                    <div class="submenu_main">
                        <ul>
                            <? foreach ($arResult['SUB_SECTIONS'][$mainSection['ID']] as $index => $section) { ?>
                                <li>
                                    <? if ($GLOBALS['APPLICATION']->GetCurPage(false) === $section['SECTION_PAGE_URL']) { ?>
                                        <span class="before-link"><?=$section['NAME'];?></span>
                                    <? } else { ?>
                                        <a href="<?= $section['SECTION_PAGE_URL']; ?>filter/sale-is-9750bfc234420f1e1c7feabcbf1e9bfe/apply/"><?= $section['NAME']; ?></a>
                                    <? } ?>
                                </li>
                            <? } ?>
                        </ul>
                    </div>
                    
                    <? // Слайдер раздела ?>
                    <?
                    $GLOBALS['CatalogMenuSliderFilter'] = [
                        'PROPERTY' => [
                            'SHOW_IN_NEW_MENU_VALUE' => 'Y',
                        ]
                    ];
                    ?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "catalog_menu_slider",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "CatalogMenuSliderFilter",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "6",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_FIELD_CODE" => array("",""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "18",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("test"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE_MOBILE" => array(),
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => $mainSection['ID'],
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("",""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "/catalog/product/#ELEMENT_CODE#/",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "Y",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    );?>
                    <? // end ?>

                </div>
            </div>
        <? } ?>

    </div>
</div>

