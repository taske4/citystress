<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $arCurSection;

?>

<div class="categories">
    <div class="categories_menu">
        <div class="categories_over">
            <ul class="categories_list">
                <li class="categories_item all active">
                    <? if (isset($arCurSection['PARENT_SECTION'])) { ?>
                        <span><a href="/catalog/<?= $arCurSection['PARENT_SECTION']['CODE'] ?>/">Все</a></span>
                    <? } else { ?>
                        <span>Все</span>
                    <? } ?>
                </li>
                <? foreach ($arResult['MAIN_SECTIONS'] as $arSection) { ?>
                    <li data-catid="<?= $arSection['ID'] ?>" class="categories_item">
                           <span>
                               <a href="<?= $arSection['SECTION_PAGE_URL']; ?>"><?= $arSection['NAME'] ?></a>
                           </span>
                    </li>
                <? } ?>
            </ul>
        </div>
        <? foreach ($arResult['MAIN_SECTIONS'] as $arSection) { ?>
            <? if ($arResult['SUB_SECTIONS'][$arSection['ID']]) { ?>
                <ul class="subcats subcats_<?= $arSection['ID'] ?>">
                    <? foreach ($arResult['SUB_SECTIONS'][$arSection['ID']] as $arSubSection) { ?>
                        <li class="subcat_item"><span><a
                                        href="<?= $arSubSection['SECTION_PAGE_URL'] ?>"><?= $arSubSection['NAME'] ?></a></span>
                        </li>
                    <? } ?>
                </ul>
            <? } ?>
        <? } ?>
        <div class="categories_arrows">
            <div class="categories_arrow categories_prev disabled">
                <svg fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg">
                    <path d="m11.2198 3.06006-5.44001 5.44 5.44001 5.44004" stroke="#242424" stroke-linecap="round"
                          stroke-linejoin="round" stroke-width="2"/>
                </svg>
            </div>
            <div class="categories_arrow categories_next">
                <svg fill="none" height="17" viewBox="0 0 17 17" width="17" xmlns="http://www.w3.org/2000/svg">
                    <path d="m11.2198 3.06006-5.44001 5.44 5.44001 5.44004" stroke="#242424" stroke-linecap="round"
                          stroke-linejoin="round" stroke-width="2"/>
                </svg>
            </div>
        </div>
    </div>
</div>