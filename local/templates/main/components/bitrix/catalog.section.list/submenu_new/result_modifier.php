<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$mainSections = $subSections = [];

/**
 * @var $arResult
 */

$sectionKeys = array_column($arResult['SECTIONS'], 'ID');
$arResult['SECTIONS'] = array_combine($sectionKeys, $arResult['SECTIONS']);

foreach ($arResult['SECTIONS'] as $sectionId => $section) {
    if ($section['DEPTH_LEVEL'] === 1) {
        continue;
    }

    $scRes = \CIBlockSection::GetNavChain(
        $arResult['IBLOCK_ID'],
        $section['ID'],
        ["ID","DEPTH_LEVEL", 'NAME', 'CODE', 'SECTION_PAGE_URL']
    );

    while ($scValue = $scRes->GetNext()) {
        if ($scValue['DEPTH_LEVEL'] < 3 && ($scValue['ID'] !== $section['ID'])) {
            $arResult['SECTIONS'][$scValue['ID']] = $scValue;
        }
    }
}



foreach($arResult['SECTIONS'] as $index => $section) {
    if ($section['DEPTH_LEVEL'] == 1) {
        $mainSections[$section['ID']] = $section;
    } elseif ($section['DEPTH_LEVEL'] == 2) {
        $subSections[$section['IBLOCK_SECTION_ID']][] = $section;
    }
}

$arResult['MAIN_SECTIONS'] = $mainSections;
$arResult['SUB_SECTIONS']  = $subSections;
