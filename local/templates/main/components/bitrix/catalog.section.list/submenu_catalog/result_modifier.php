<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$mainSections = $subSections = [];

/**
 * @var $arResult
 */


foreach($arResult['SECTIONS'] as $index => $section) {
    if ($section['DEPTH_LEVEL'] == 1) {
        $mainSections[$section['ID']] = $section;
    } elseif ($section['DEPTH_LEVEL'] == 2) {
        $subSections[$section['IBLOCK_SECTION_ID']][] = $section;
    }
}

$arResult['MAIN_SECTIONS'] = $mainSections;
$arResult['SUB_SECTIONS']  = $subSections;