<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="homecats_container container">
        <div class="homecats_block block">

            <div class="homecats_sliderwrap">
                <div class="custom_slarrows">
                    <div class="custom_slarrow custom_prev">
                        <svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
                            <path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2"/>
                        </svg>
                    </div>
                    <div class="custom_slarrow custom_next">
                        <svg fill="none" height="42" viewBox="0 0 22 42" width="22" xmlns="http://www.w3.org/2000/svg">
                            <path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                                  stroke-width="2"/>
                        </svg>
                    </div>
                </div>

                <div class="homecats_slider">

                    <? foreach($arResult['SECTIONS'] as $section) { ?>
                        <?
                        $mobilePicture = $tabletPicture = $desktopPicture = null;

                        if ($pictureId = $section['UF_MOBILE_PICTURE']) {
                            $mobilePicture = CFile::GetPath($pictureId);
                        }

                        if ($pictureId = $section['UF_TABLET_PICTURE']) {
                            $tabletPicture = CFile::GetPath($pictureId);
                        }

                        if ($pictureId = $section['UF_DESKTOP_PICTURE']) {
                            $desktopPicture = CFile::GetPath($pictureId);
                        }
                        ?>
                        <div class="slide">
                            <a href="<?=$section['SECTION_PAGE_URL']?>" class="homecats_item">
                                <div class="back_img">
                                    <picture>
                                        <source type="image/jpg" media="(max-width: 750px)" srcset="<?=$mobilePicture?>">
                                        <source type="image/jpg" media="(min-width: 751px)"
                                                srcset="<?=$tabletPicture?> 1x, <?=$tabletPicture?> 2x">
                                        <img data-img1x="<?=$desktopPicture?>" data-img2x="<?=$desktopPicture?>" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/blank.png" alt="<?=$section['NAME']?>">
                                    </picture>
                                </div>
                                <div class="homecats_item__content">
                                    <div class="homecats_item__title"><?=$section['NAME']?></div>
                                    <button class="button button_white_tr homecats_item__moblink">Смотреть все</button>
                                </div>
                            </a>
                        </div>
                    <? } ?>

                </div>
            </div>

        </div>
    </div>