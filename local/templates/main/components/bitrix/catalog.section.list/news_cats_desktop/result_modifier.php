<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * @var $arResult
 */

global $arCurSection;

foreach($arResult['SECTIONS'] as $index => $section) {
    $scRes = \CIBlockSection::GetNavChain(
        CATALOG_IBLOCK_ID,
        $section['ID'],
        ["ID","DEPTH_LEVEL", 'NAME', 'CODE', 'DEPTH_LEVEL']
    );

    while ($scValue = $scRes->GetNext()) {
        if ($scValue['DEPTH_LEVEL'] < 3 && ($scValue['ID'] !== $section['ID'])) {
            $arResult['SECTIONS'][$scValue['ID']] = $scValue;
        }
    }
}

$mainSections = $subSections = [];

foreach($arResult['SECTIONS'] as $index => $section) {
    $mobSections[$section['IBLOCK_SECTION_ID']][] = $section;

    if ($section['DEPTH_LEVEL'] == 2) {
        $mainSections[$section['ID']] = $section;
    } elseif ($section['DEPTH_LEVEL'] == 3) {
        $subSections[$section['IBLOCK_SECTION_ID']][] = $section;
    }
}

$arResult['MAIN_SECTIONS'] = $mainSections;
$arResult['SUB_SECTIONS']  = $subSections;
