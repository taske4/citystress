<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$containerClass = 'catalog_cats-'.strtolower($arParams['DEVICE_NAME']);

?>

<?
$TOP_DEPTH = $arParams['TOP_DEPTH'];
$CURRENT_DEPTH = $TOP_DEPTH;
$DOWN_DEPTH = $arResult['SECTIONS'][0]['DEPTH_LEVEL'];
$open = false;

echo '<ul class="mobcats_lvl1uli '.$containerClass.' hiddenpro">';

foreach ($arResult["SECTIONS"] as $arSection) {
    $arSection['DEPTH_LEVEL'] -= $DOWN_DEPTH - 1;

    /*
        $TOP_DEPTH 3
        $CURRENT_DEPTH 3
        $DOWN_DEPTH 2
        $arSection['DEPTH_LEVEL'] 1
    */

    if ($CURRENT_DEPTH < $arSection["DEPTH_LEVEL"]) {
        echo '<pre style="background: #eee">'.$arSection['DEPTH_LEVEL'].'</pre>';
        echo "<ul class='mobcats_lvl" . $arSection['DEPTH_LEVEL'] . "ul'>";
        $open = true;
    } elseif ($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"]) {
        echo "</li>";
    } elseif ($open) {
        while ($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"]) {
            echo "</li>";
            echo "</ul>";
            $CURRENT_DEPTH--;
        }
        echo "</li>";

        $open = false;
    }

    $count = $arParams["COUNT_ELEMENTS"] && $arSection["ELEMENT_CNT"] ? "&nbsp;(" . $arSection["ELEMENT_CNT"] . ")" : "";

    if ($GLOBALS['APPLICATION']->GetCurPage(false) === $arSection['SECTION_PAGE_URL']) {
        $link = '<span class="before-link">' . $arSection["NAME"] . '</span>';
    } else {
        $link = '<span><a href="' . $arSection['SECTION_PAGE_URL'] . '">' . $arSection["NAME"] . $count . '</a></span>';
    }

    echo '<li class="mobcats_lvl' . $arSection['DEPTH_LEVEL'] . 'li">' . $link;

    $CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
}

while ($CURRENT_DEPTH > $TOP_DEPTH) {
    echo "</li>";
    echo "</ul>";
    $CURRENT_DEPTH--;
}

echo '</ul>';