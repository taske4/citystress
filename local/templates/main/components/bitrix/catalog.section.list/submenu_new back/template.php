<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="submenu submenu_new">
    <div class="submenu_close"></div>
    <div class="submenu_content">
        <div class="submenu_side">
            <ul>
                <li data-cat="2_1" class="active"><span>Одежда</span></li>
                <li data-cat="2_2"><span>Аксессуары</span></li>
            </ul>
        </div>

        <div class="submenu_main">
            <div class="submenu_main__out submenu_main__out_2_1" style="display: block;">
                <ul>
                    <li><a href="">Ссылка 1</a></li>
                    <li><a href="">Ссылка 2</a></li>
                    <li><a href="">Ссылка 3</a></li>
                    <li><a href="">Ссылка 4</a></li>
                </ul>
            </div>
            <div class="submenu_main__out submenu_main__out_2_2">
                <ul>
                    <li><a href="">Ссылка 5</a></li>
                    <li><a href="">Ссылка 6</a></li>
                    <li><a href="">Ссылка 7</a></li>
                    <li><a href="">Ссылка 8</a></li>
                </ul>
            </div>
        </div>

        <div class="submenu_sugg">
            <div class="submenu_suggbox">
                <div class="sugg_sliderwrap">
                    <div class="custom_slarrows">
                        <div class="custom_slarrow2 custom_prev">
                            <svg fill="none" height="42" viewBox="0 0 22 42" width="22"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round"
                                      stroke-linejoin="round" stroke-width="2"/>
                            </svg>
                        </div>
                        <div class="custom_slarrow2 custom_next">
                            <svg fill="none" height="42" viewBox="0 0 22 42" width="22"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="m21 1-20 20 20 20" stroke="#fff" stroke-linecap="round"
                                      stroke-linejoin="round" stroke-width="2"/>
                            </svg>
                        </div>
                    </div>
                    <div class="sugg_slider">

                        <div class="slide">
                            <div class="sugg_item">
                                <div class="sugg_item__content">
                                    <div class="back_img">
                                        <picture>
                                            <source type="image/jpg" media="(max-width: 750px)"
                                                    srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/14_1x.jpg">
                                            <source type="image/jpg" media="(min-width: 751px)"
                                                    srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/14_1x.jpg 1x, /img/14.jpg 2x">
                                            <img data-img1x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/14_1x.jpg" data-img2x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/14.jpg"
                                                 src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/blank.png" alt="">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slide">
                            <div class="sugg_item">
                                <div class="sugg_item__content">
                                    <div class="back_img">
                                        <picture>
                                            <source type="image/jpg" media="(max-width: 750px)"
                                                    srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/15_1x.jpg">
                                            <source type="image/jpg" media="(min-width: 751px)"
                                                    srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/15_1x.jpg 1x, /img/15.jpg 2x">
                                            <img data-img1x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/15_1x.jpg" data-img2x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/15.jpg"
                                                 src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/blank.png" alt="">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slide">
                            <div class="sugg_item">
                                <div class="sugg_item__content">
                                    <div class="back_img">
                                        <picture>
                                            <source type="image/jpg" media="(max-width: 750px)"
                                                    srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/13_1x.jpg">
                                            <source type="image/jpg" media="(min-width: 751px)"
                                                    srcset="<?=SITE_MAIN_TEMPLATE_PATH?>/img/13_1x.jpg 1x, /img/13.jpg 2x">
                                            <img data-img1x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/13_1x.jpg" data-img2x="<?=SITE_MAIN_TEMPLATE_PATH?>/img/13.jpg"
                                                 src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/blank.png" alt="">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

