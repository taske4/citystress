<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

if (!empty($arResult["ERRORS"])):?>
	<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif;
if ($arResult["MESSAGE"] <> ''):?>
	<?ShowNote($arResult["MESSAGE"])?>
<?endif?>


<form class="footer_form" name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<div class="fields">
	<?=bitrix_sessid_post()?>
	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
		<?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
			<?foreach ($arResult["PROPERTY_LIST"] as $propertyID):?>


					<?
					if (intval($propertyID) > 0)
					{
						if (
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
							&&
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
						)
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
						elseif (
							(
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
								||
								$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
							)
							&&
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
						)
							$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
					} elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
						$arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

					if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
					{
						$inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"] ?: []) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
						$inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
					}
					else
					{
						$inputNum = 1;
					}

					for ($i = 0; $i<$inputNum; $i++)
					{
						if ($arParams["ID"] > 0 || count($arResult["ERRORS"] ?: []) > 0)
						{
							$value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
						}
						elseif ($i == 0)
						{
							$value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

						}
						else
						{
							$value = "";
						}

						$class = '';
						$isName = false;
						if ($propertyID ==='NAME') {
							$isName = true;
							$value = 'Новая подписка ';
							if ($GLOBALS['USER']->IsAuthorized()) {
								$value.='('.$GLOBALS['USER']->GetFullName().')';
							} else {
								$value.='(Не авторизованный пользователь)';
							}
							$class = 'hiddenpro';
						}
					?>
					<div class="field_wrap <?=$class?>">
						<input type="text" value="<?=$value?>"  size="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]; ?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" class="field <? if (!$isName) {?>field_email<?}?>" placeholder="Введите email...">
					</div>
					<? } ?>
			<?endforeach;?>
		<?endif?>

		<div class="button_wrap">
			<input type="submit" name="iblock_submit" class="hiddenpro">
			<button class="button">
				<span>Подписаться</span>
				<svg fill="none" height="11" viewBox="0 0 21 11" width="21"
					 xmlns="http://www.w3.org/2000/svg">
					<path d="m15.3262 1.25 4.132 4.25m0 0-4.132 4.25m4.132-4.25h-17.9582"
						  stroke="#242424" stroke-linecap="round" stroke-linejoin="round"
						  stroke-width="2"/>
				</svg>
			</button>

		</div>
			<?/*if ($arParams["LIST_URL"] <> ''):?>
				<input type="submit" name="iblock_apply" value="<?=GetMessage("IBLOCK_FORM_APPLY")?>" />
				<input
					type="button"
					name="iblock_cancel"
					value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
					onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"])?>';"
				>
			<?endif*/?>
	</div>

	<div class="form_agreement">
		Я подтверждаю согласие с <a rel="nofollow noopener" target="_blank" href="/page-politika-konfedentsialnosti/">Политикой конфиденциальности</a> и <a
			href="/pageoferta/"  rel="nofollow noopener" target="_blank" >Офертой</a>, а также даю согласие на обработку персональных данных
	</div>
</form>

<script defer>
	// $('.footer_form button').on('click', function (e){
	// 	$(this).closest('.footer_form').find('input[type=submit]').trigger('click');
	// });

	$('.footer_form').on('submit', function(e){
		e.preventDefault(); // avoid to execute the actual submit of the form.

		var form = $(this);
		var actionUrl = form.attr('action');

		var formData = new FormData(this);

		formData.append('iblock_submit', 'Отправить запрос');

		$(form).find('.mesag').remove();

		if (!$(form).find('input.field_email').val()) {
			$(form).prepend('<p class="mesag">Пожалуйста укажите email, это обязательное поле</p>');
		} else {
			$.ajax({
				type: "POST",
				url: actionUrl,
				data: formData, // serializes the form's elements.
				processData: false,
				contentType: false,
				success: function (data, textStatus, xhr) {
					if (xhr.status == 200) {
						$(form).prepend('<div class="mesag mesag-success">Вы успешно создали подписку</div>');
					} else {
						$(form).prepend('<p class="mesag">Что то пошло не так, попробуйте позже</p>');
					}
				}
			});
		}
	});
</script>
<style>
	.mesag.mesag-success {
		padding: 2em 1em;
		background: #3ba428;
		color: #fff;
		border-radius: 2px;
		text-align: center;
	}

	@media screen and (max-width: 750px) {
		.mesag.mesag-success {
			margin-bottom: .5em;
		}
	}
</style>