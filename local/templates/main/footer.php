<?php
/**
 * @global $APPLICATION;
 */
$date = new DateTime();
$timestamp = $date->getTimestamp();
// $timestamp = 'ver=1.0';

if (!$asset) {
    $asset = \Bitrix\Main\Page\Asset::getInstance();
}

?>

<footer>
    <div class="footer_container container">
        <div class="footer_block block">

            <div class="footer_top">


                <?
                if ($APPLICATION->GetCurPage(false) === '/') {
                    echo '<div class="footer_logo">';
                } else {
                    echo '<a href="/" class="footer_logo">';
                }
                ?>
                    <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/logo.svg" alt="" class="footer_logo__light">
                    <img src="<?= SITE_MAIN_TEMPLATE_PATH ?>/img/logo_dark.svg" alt="" class="footer_logo__dark">
                <?
                if ($APPLICATION->GetCurPage(false) === '/') {
                    echo '</div>';
                } else {
                    echo '</a>';
                }
                ?>

                <?
                global $Uri, $footerText;
                if ($Uri->getPath() === '/') { ?>
                    <div class="footer_top__text">
                        <?= \Citystress\ProjectSettings::getInstance()->getFooterDesc()?>
                    </div>
                <? } else { ?>
                    <div class="footer_top__text">
                        <?
                        if ($footerText) {
                            echo $footerText;
                        } else {
                            echo '<!--noindex-->'.
                                \Citystress\ProjectSettings::getInstance()->getFooterDesc().
                                '<!--/noindex-->';
                        }
                        ?>
                    </div>
                <? } ?>

            </div>


            <div class="footer_body">

                <div class="footer_col">
                    <svg class="footer_col__arrow" fill="none" height="8" viewBox="0 0 16 8" width="16"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="m15.1177.764765-7.11767 6.470585-7.117648-6.470586" stroke="#a8a8a8"
                              stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    <div class="footer_label">Больше о CITYSTRESS</div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "",
                        Array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "more_about_citystress",
                            "USE_EXT" => "N"
                        )
                    );?>
                </div>

                <div class="footer_col">

                    <svg class="footer_col__arrow" fill="none" height="8" viewBox="0 0 16 8" width="16"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="m15.1177.764765-7.11767 6.470585-7.117648-6.470586" stroke="#a8a8a8"
                              stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    <div class="footer_label">Помощь и поддержка</div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "",
                        Array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "main",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "help_and_support",
                            "USE_EXT" => "N"
                        )
                    );?>
                </div>

                <div class="footer_col">
                    <svg class="footer_col__arrow" fill="none" height="8" viewBox="0 0 16 8" width="16"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="m15.1177.764765-7.11767 6.470585-7.117648-6.470586" stroke="#a8a8a8"
                              stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    <div class="footer_label">Сотрудничество</div>

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "",
                        Array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(""),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "cooperation",
                            "USE_EXT" => "N"
                        )
                    );?>
                </div>


                <div class="footer_col">
                 <div class="footer_label">Узнать о новинках скидках</div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:iblock.element.add.form",
                        "emailSubscribe",
                        Array(
                            "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                            "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                            "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                            "CUSTOM_TITLE_DETAIL_TEXT" => "",
                            "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                            "CUSTOM_TITLE_NAME" => "",
                            "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                            "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                            "CUSTOM_TITLE_TAGS" => "",
                            "DEFAULT_INPUT_SIZE" => "30",
                            "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                            "ELEMENT_ASSOC" => "CREATED_BY",
                            "GROUPS" => array("2"),
                            "IBLOCK_ID" => "10",
                            "IBLOCK_TYPE" => "data",
                            "LEVEL_LAST" => "Y",
                            "LIST_URL" => "",
                            "MAX_FILE_SIZE" => "0",
                            "MAX_LEVELS" => "100000",
                            "MAX_USER_ENTRIES" => "100000",
                            "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                            "PROPERTY_CODES" => array("132", 'NAME'),
                            "PROPERTY_CODES_REQUIRED" => array("132", 'NAME'),
                            "RESIZE_IMAGES" => "N",
                            "SEF_MODE" => "N",
                            "STATUS" => "ANY",
                            "STATUS_NEW" => "N",
                            "USER_MESSAGE_ADD" => "Спасибо",
                            "USER_MESSAGE_EDIT" => "Спасибо",
                            "USE_CAPTCHA" => "N"
                        )
                    );?>
                </div>

            </div>


            <div class="footer_bottom">
                <div class="footer_copyright">&copy; CityStress — <?php echo date('Y'); ?></div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "sociallinks",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "socials",
                        "USE_EXT" => "N"
                    )
                );?>
                <ul class="footer_payment">
                    <li style="display: none;"><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/p_visa.svg"
                                                                                                alt="visa"></li>
                    <li><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/p_mastercard.svg" alt="mastercard">
                    </li>
                    <li><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/p_mir.svg" alt="mir"></li>
                    <li style="display: none;"><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/p_halva.svg"
                                                                                                 alt="halva"></li>
                </ul>
                <div class="footer_contacts">
                    <a href="tel:88007004600" class="phone">8 800 700 46-00</a>
                    <p>
                        Звонок по РФ бесплатный
                    </p>
                </div>
            </div>
            <div style="margin-top: 2em;">
                <p>&copy; 2011 - <?=date('Y')?> г. Все права защищены. Официальный сайт CityStress</p>
            </div>

        </div>
    </div>

    <!--noindex-->
    <? if (!$_SESSION['acept_cookies']) { ?>
    <div style="display: block" class="alert alert_cookie alert_bottom" id="alert_cookie">
        <p>
            Продолжая просмотр, вы даете согласие на<br>
            обработку файлов <span>cookies</span>
        </p>
        <button id="acept-cookie-button">ПРИНЯТЬ</button>
    </div>
    <? } ?>

    <div class="alert alert_tocart alert_bottom" id="alert_tocart">Товар добавлен в корзину.</div>

    <div class="universal_modal__backclose modal_back modal_close"></div>
    <div class="look_back"></div>

    <div class="look look_1">
    </div>

    <div class="auth modal universal_modal__auth">
        <div class="auth_head modal_head">
            <span class="go_login">Вход</span>
            <span class="go_signup">Регистрация</span>
            <svg class="auth_close modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg>
        </div>
        <div class="auth_body modal_body">
            <div class="customscroll">

                <div class="auth_content auth_content_1">
                    <?$APPLICATION->IncludeComponent("bitrix:system.auth.form","",Array(
                            "REGISTER_URL" => "register.php",
                            "FORGOT_PASSWORD_URL" => "",
                            "PROFILE_URL" => "profile.php",
                            "SHOW_ERRORS" => "Y"
                        )
                    );?>
                </div>

                <div class="auth_content auth_content_2">
                    <!--                    <div class="signup_error">Покупатель с таким адресом уже есть. Если это вы — <span data-modalname="auth" class="universal_modal__in go_login">войдите</span>, чтобы увидеть отложенные товары из корзины и применить скидку.</div>-->

                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.register",
                        "",
                        Array(
                            "AUTH" => "Y",
                            "REQUIRED_FIELDS" => array("EMAIL", "PERSONAL_GENDER", "LAST_NAME", "PERSONAL_PHONE"),
                            "SEF_FOLDER" => "/",
                            "SEF_MODE" => "Y",
                            "SET_TITLE" => "Y",
                            "SHOW_FIELDS" => array("EMAIL", "NAME", "LAST_NAME", "PERSONAL_PHONE"),
                            "SUCCESS_PAGE" => "",
                            "USER_PROPERTY" => array(),
                            "USER_PROPERTY_NAME" => "",
                            "USE_BACKURL" => "Y",
                            "VARIABLE_ALIASES" => Array()
                        )
                    );?>

                </div>

            </div>
        </div>
    </div>

    <div class="restore modal universal_modal__restore">
        <div class="restore_head modal_head">
            <span>Восстановление пароля</span>
            <svg class="restore_close modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg>
        </div>
        <div class="restore_body modal_body">
            <div class="customscroll">

                <div class="restore_step restore_step1">
                    <div class="restore_instruction">Введите почту или номер телефона, чтобы сменить старый пароль</div>
                    <div class="error" style="margin-bottom: 2em; text-align: center"></div>
                    <?$APPLICATION->IncludeComponent( "bitrix:system.auth.forgotpasswd",
                        ".default",
                        Array()
                    );?>
                    <!--                    <form class="form_v1">-->
                    <!--                        <div class="fields">-->
                    <!--                            <div class="field_wrap">-->
                    <!--                                <input type="text" class="field" placeholder="Email или номер телефона">-->
                    <!--                            </div>-->
                    <!--                            <div class="button_wrap">-->
                    <!--                                <button type="submit" class="button button_black">-->
                    <!--                                    <span>Восстановить пароль</span>-->
                    <!--                                </button>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </form>-->
                </div>


                <!--                change_password=yes&lang=ru&USER_CHECKWORD=13d481204c0e815e50ccb34d912e1add&USER_LOGIN=taske4%40yandex.ru-->
                <div class="restore_step restore_step2">
                    <? if ($_GET['change_password'] === 'yes') { ?>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.auth.changepasswd",
                            "",
                            Array(
                                "AUTH_AUTH_URL" => "",
                                "AUTH_REGISTER_URL" => ""
                            )
                        );?>
                    <? } else { ?>
                        <div class="restore_instruction">На ваш номер телефона мы выслали проверочный код. Введите его и придумайте новый пароль ниже</div>
                        <div class="error" style="margin-bottom: 2em; text-align: center"></div>
                        <form class="form_v1">
                            <div class="fields">
                                <div class="field_wrap">
                                    <input name="code" type="text" class="field" placeholder="Код подтверждения">
                                </div>
                                <div class="field_wrap">
                                    <input name="passwd" type="password" class="field" placeholder="Новый пароль">
                                </div>
                                <div class="field_wrap">
                                    <input name="passwd2" type="password" class="field" placeholder="Повторите пароль">
                                </div>
                                <div class="button_wrap">
                                    <button type="submit" class="button button_black">
                                        <span>Сменить пароль</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="restore_nocode">
                            <div class="restore_nocode__title">Не пришел код?</div>
                            Повторная отправка возможна через <strong>60</strong> сек
                        </div>
                        <div class="restore_nocode restore_nocode2">
                            <div class="restore_nocode__title">Не пришел код?</div>
                            <div class="restorepasswdagain"><span>Отправить</span> код еще раз</div>
                        </div>
                    <? } ?>
                </div>

                <div class="restore_step restore_step3">
                    <div class="restore_instruction">Ваш пароль успешно изменен!<br> Воспользуйтесь формой авторизации для входа в аккаунт.</div>
                    <button type="button" class="button button_black go_login universal_modal__in" data-modalname="auth">
                        <span>Войти в аккаунт</span>
                    </button>
                </div>

            </div>
        </div>
    </div>

    <div class="oneclick modal universal_modal__oneclick">
        <div class="oneclick_head modal_head">
            <span>Купить в 1 клик</span>
            <svg class="oneclick_close modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg>
        </div>
        <div class="oneclick_body modal_body">
            <div class="customscroll">

                <!--                Product mini -->

                <form class="form_v1 form_oneclick" action="">
                    <input type="hidden" class="form_v1__in">
                    <div class="fields">
                        <div class="field_wrap">
                            <input name="name" type="text" class="field required" placeholder="Введите ваше имя">
                        </div>
                        <div class="field_wrap">
                            <input name="phone" type="text" class="field field_phone" placeholder="Номер телефона">
                        </div>
                        <div class="form_agreement">
                            Я подтверждаю согласие с <a href="">Политикой конфиденциальности</a> и <a href="">Офертой</a>, а также даю согласие на обработку персональных данных
                        </div>
                        <div class="button_wrap">
                            <button type="button" class="button button_black">
                                <span>Оформить заказ</span>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/noindex-->

</footer>

<? require_once $_SERVER['DOCUMENT_ROOT'] . SITE_MAIN_TEMPLATE_PATH . '/include/popups.php'; ?>

</div>


<?
/**
 * @var $asset ;
 */

$asset->addString(
    "<link rel='stylesheet' type='text/css' href='" . SITE_MAIN_TEMPLATE_PATH . "/assets/css/podeli.widget.css?v=".uniqid()."'/>");
$asset->addString(
    "<link rel='stylesheet' type='text/css' href='" . SITE_MAIN_TEMPLATE_PATH . "/assets/css/other.css?v=".uniqid()."'/>");
$asset->addString(
    "<link rel='stylesheet' type='text/css' href='" . SITE_MAIN_TEMPLATE_PATH . "/assets/css/other_adaptive.css?v=".uniqid()."' media=\"(max-width: 1600px)\"/>"
);
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/jquery.min.js\"></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/colorbox.min.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/slick.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/jquery-ui.min.js\" defer></script>");
$asset->addString(
    "<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/jquery.ui.touch-punch.min.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/jquery.mask.min.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/jquery.inputmask.min.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/mcustomscrollbar.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/scripts.js?rand=".uniqid()."\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/maps.js\" defer></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/podeli.widget.js\"></script>");
$asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/custom.js\" defer></script>");
?>

<? if ($_GET['change_password'] === 'yes') { ?>
    <script>
        $(function(){
            setTimeout(function (){
                $('[data-modalname=restore]').trigger('click');
                $('.restore_step1').css('display','none');
                $('.restore_step2').fadeIn(200);
            }, 200);
        });
    </script>
<? } ?>


<script>
    (function(w,d,u){
        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
    })(window,document,'https://cdn-ru.bitrix24.ru/b26838004/crm/tag/call.tracker.js');
</script>

</body>
</html>