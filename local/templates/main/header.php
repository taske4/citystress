<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;

$asset = Asset::getInstance();

?>
    <!doctype html>
    <html style="--vh: 9.47px;" lang="ru">
    <head>
        <!-- Google Tag Manager -->
        <script>
            window.dataLayer = window.dataLayer || [];
        </script>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-P9T7VNHZ');</script>
        <!-- End Google Tag Manager -->
        
        <? $asset->addString("<script src=\"" . SITE_MAIN_TEMPLATE_PATH . "/assets/js/jquery.min.js\"></script>"); ?>

        <style>
            .search_block,
            .mobadd_back,
            .mobadd,
            .hidden,
            .popup_back,
            .popup,
            .mob_only,
            .mobmenu_in,
            .mobmenu,
            .nosize_back,
            .nosize,
            .look_back,
            .look,
            .mobcats,
            .mobcats_back,
            .categories_mobin {display: none;}
        </style>

        <?
        /**
         * @global $APPLICATION
         */
        $APPLICATION->ShowHead()
        ?>

        <? require_once $_SERVER['DOCUMENT_ROOT'] . SITE_MAIN_TEMPLATE_PATH . '/include/metas.php'; ?>

        <?
        $asset->addCss(SITE_MAIN_TEMPLATE_PATH . '/assets/css/top.css');
        $asset->addString("<link rel='stylesheet' type='text/css' href='" . SITE_MAIN_TEMPLATE_PATH . "/assets/css/top_adaptive.css' media=\"(max-width: 1600px)\" />");
        $asset->addCss(SITE_MAIN_TEMPLATE_PATH . '/assets/css/custom.css');
        ?>

        <title><? $APPLICATION->ShowTitle() ?></title>

        <?
        $APPLICATION->SetPageProperty('canonical', "https://citystress.ru".$APPLICATION->GetCurPage(false));
        $APPLICATION->SetPageProperty("keywords",  '');
        ?>

        <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date(); for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }} k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym"); ym(50233879, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, ecommerce:"dataLayer" }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/50233879" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

        <script type="text/javascript">
            try {
                dashamail = window.dashamail || function () {
                    dashamail.queue.push(arguments);
                };
                dashamail.queue = dashamail.queue || [];
                dashamail('create');
            } catch (err) {
                console.error(err.message);
            }
        </script>
        <script src="https://directcrm.dashamail.com/scripts/v2/tracker.js" async></script>

        <!-- Top.Mail.Ru counter -->
        <script type="text/javascript">
            var _tmr = window._tmr || (window._tmr = []);
            _tmr.push({id: "3548354", type: "pageView", start: (new Date()).getTime()});
            (function (d, w, id) {
                if (d.getElementById(id)) return;
                var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
                ts.src = "https://top-fwz1.mail.ru/js/code.js";
                var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
                if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
            })(document, window, "tmr-code");
        </script>
        <noscript><div><img src="https://top-fwz1.mail.ru/counter?id=3548354;js=na" style="position:absolute;left:-9999px;" alt="Top.Mail.Ru" /></div></noscript>
        <!-- /Top.Mail.Ru counter -->

    </head>

    <?
        $bodyClasses = [];

        if (defined('PAGE_404')) {
            $bodyClasses[] = 'error404';
        }
        if ($APPLICATION->GetCurPage(false) === '/') {
            $bodyClasses[] = 'home';
        }
        if ($APPLICATION->GetCurPage(false) === '/cart/') {
            $bodyClasses[] = 'cart';
        }
		  if ($APPLICATION->GetCurPage(false) === '/shops/') {
            $bodyClasses[] = 'shops';
        }
        if ($APPLICATION->GetCurPage(false) === '/search/') {
            $bodyClasses[] = 'search_results';
        }
        if (\Citystress\ProjectSettings::getInstance()->isShowHeaderAction()) {
            $bodyClasses[] = 'action_active';
        }
        if (preg_match('/\/shops\/\d+\//', $APPLICATION->GetCurPage(false))) {
            $bodyClasses[] = 'single_shop';
            $bodyClasses[] = 'single_shop__outlet';
        }
    ?>
    <body class="<?= implode(' ', $bodyClasses); ?> ">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9T7VNHZ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<? require_once $_SERVER['DOCUMENT_ROOT'] . SITE_MAIN_TEMPLATE_PATH . '/include/header.php'; ?>