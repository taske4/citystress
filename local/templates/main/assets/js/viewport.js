var m = document.getElementById('viewport'),
    w = screen.width,
    h = screen.height,
    c = 'content',
    u = 'user-scalable=0',
    v = 'width='; 
  
if ( w < 1300 ) { 

  if ( window.innerWidth > window.innerHeight ) {

    m.setAttribute(c,v+'1024,'+u); 
    
  } else {

    if ( w < 500 ) {
      m.setAttribute(c,v+'360,'+u);
    } else {
      m.setAttribute(c,v+'768,'+u);
    };

  };

} else {

  m.setAttribute(c,v+'device-width,'+u);

};

window.addEventListener("resize", function() {
var w = screen.width,
    h = screen.height;

if ( w < 1300 ) {

  if ( window.innerWidth > window.innerHeight ) {

    m.setAttribute(c,v+'1024,'+u);

  } else {

    if ( w < 500 ) {
      m.setAttribute(c,v+'360,'+u);
    } else {
      m.setAttribute(c,v+'768,'+u);
    };

  };

} else {

  m.setAttribute(c,v+'device-width,'+u);

};

});