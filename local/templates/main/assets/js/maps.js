
var mappage_var = 0,
		mapscreenw = $(window).width();


$('.mappoints_box').each(function(index) {
	var this_index = index + 1;
  $(this).find('.mappoints_data__item').each(function (index) {
  	var data_item_index = index + 1;
  	$(this).attr('data-markerid','marker_'+this_index+'_'+data_item_index);
  });
});



function map_position() {
	$('.mappoints_box').each(function () {
		var mappoints_map_in = $(this).find('.mappoints_map_in'),
				mappoints_mobmap_in = $(this).find('.mappoints_mobmap_in');

		if ( mapscreenw < 750 ) {
			$(this).find('.mappoints_map').appendTo(mappoints_mobmap_in);
		} else {
			$(this).find('.mappoints_map').appendTo(mappoints_map_in);
		};
	}); 
};

map_position();

$(window).on('resize', function(){
	mapscreenw = $(window).width();
  map_position();
});



function maps() {
  setTimeout(function() { 
    $.getScript("https://api-maps.yandex.ru/2.1/?apikey=fec3e0b3-c65c-47bf-aff7-96f4b3c8fabc&lang=ru_RU").done(function( script, textStatus ) {


    	if ( $('#contacts_body__map').length ) {

    		var cm_zoom = $('#contacts_body__map').data('zoom'),
    				cm_center = $('#contacts_body__map').data('center');

				ymaps.ready(function () {

					var cm_map = new ymaps.Map('contacts_body__map', {
						center: cm_center, 
						zoom: cm_zoom, 
						controls: ['zoomControl']}, { 
						searchControlProvider: 'yandex#search' 
					}); 

					cm_map.behaviors.disable('scrollZoom');
						
					var cm_markers_array = [];

					cm_markers_array.push({
					  type: 'Feature',
					  id: '#contacts_body__map',
					  geometry: {
					    type: 'Point',
					    coordinates: cm_center
					  }
					});

					var objectManager = new ymaps.ObjectManager({
					  clusterize: false,
					  gridSize: 128, 
					  groupByCoordinates: false,
					  hasBalloon: true,
					  hasHint: false,
					  margin: 10, 
					  minClusterSize: 2,
					  showInAlphabeticalOrder: false,
					  viewportMargin: 64,
					  zoomMargin: 40
					});
		
					objectManager.objects.options.set({
						openBalloonOnClick: false,
						iconLayout: 'default#image',
    		  	iconImageHref: '/local/templates/main/img/marker_citystress__active.svg',
    		  	iconImageSize: [40, 65],
    		  	iconImageOffset: [0, -65]
					});  
		
					objectManager.add(cm_markers_array);

					cm_map.geoObjects.add(objectManager); 

				}); 

    	};



  		$('.mappoints_box').each(function () {
  			
  			var mappoints_box = $(this),
  					map = mappoints_box.find('.mappoints_map'),
  					map_zoom = mappoints_box.find('.mappoints_map_in').data('zoom'),
  					map_center = mappoints_box.find('.mappoints_map_in').data('center'),
  					mob_map_zoom = mappoints_box.find('.mappoints_mobmap_in').data('zoom'),
  					mob_map_center = mappoints_box.find('.mappoints_mobmap_in').data('center');

				if ( mapscreenw < 750 ) {
				  var set_zoom = mob_map_zoom,
				 			set_center = mob_map_center;
				} else {
				  var set_zoom = map_zoom,
				 			set_center = map_center;
				};

				ymaps.ready(function () {

					if ( $('body').hasClass('single_shop') ) {
						set_center = mappoints_box.find('.mappoints_data__item:first').data('coos');
					};

					var map_var = new ymaps.Map(map[0], {
						center: set_center, 
						zoom: set_zoom, 
						controls: ['zoomControl']}, { 
						searchControlProvider: 'yandex#search' 
					}); 

					map_var.behaviors.disable('scrollZoom');

					if ( mappoints_box.find('.mappoints_data__item').length ) {

						var markers_array = [], 
								tryon_array = [],
								outlets_array = [], 
								last_opened_balooon = 0;
		
						mappoints_box.find('.mappoints_data__item').each(function() { 
							var marker_id = $(this).data('markerid'),
						  		marker_coos = $(this).data('coos'),
						  		hint_content = '-',
						  		balloon_content = '-'; 

						  if ( $(this).find('.shoppickup_item__outlet').length ) {
						  	outlets_array.push(marker_id);
						  };
	
						  if ( $(this).hasClass('tryon') ) {
						  	tryon_array.push(marker_id);
						  };

						  markers_array.push({
						    type: 'Feature',
						    id: marker_id,
						    geometry: {
						      type: 'Point',
						      coordinates: marker_coos
						    }
						  });
						});   
		
						var objectManager = new ymaps.ObjectManager({
						  clusterize: false,
						  gridSize: 128, 
						  groupByCoordinates: false,
						  hasBalloon: true,
						  hasHint: false,
						  margin: 10, 
						  minClusterSize: 2,
						  showInAlphabeticalOrder: false,
						  viewportMargin: 64,
						  zoomMargin: 40
						});
		
						objectManager.objects.options.set({
							openBalloonOnClick: false,
							iconLayout: 'default#image',
    		  		iconImageHref: '/local/templates/main/img/marker_citystress.svg',
    		  		iconImageSize: [40, 65],
    		  		iconImageOffset: [0, -65]
						});  

						if ( $('body').hasClass('single_shop') ) {
							if ( $('body').hasClass('single_shop__outlet') ) {
								objectManager.objects.options.set({
    		  				iconImageHref: '/local/templates/main/img/marker_outlet__active.svg'
								}); 
							} else {
								objectManager.objects.options.set({
    		  				iconImageHref: '/local/templates/main/img/marker_citystress__active.svg'
								}); 
							}; 
						};
  
						objectManager.add(markers_array);

						map_var.geoObjects.add(objectManager);
 
						if ( outlets_array ) {
							let i = 0;
							while (i < outlets_array.length) { 
								if ( $('body').hasClass('single_shop') ) {
     							objectManager.objects.setObjectOptions(outlets_array[i], {
    							  'iconImageHref': '/local/templates/main/img/marker_outlet__active.svg'
    							});
    						} else {
     							objectManager.objects.setObjectOptions(outlets_array[i], {
    							  'iconImageHref': '/local/templates/main/img/marker_outlet.svg'
    							});
    						};
								i++;
							};
						}; 

						objectManager.objects.events.add('click', function (e) {
						  var marker_id = e.get('objectId');
						  point_click(marker_id);
						});
	 
						mappoints_box.find('.mappoints_data__item').click(function() {
							var marker_id = $(this).data('markerid');
							point_click(marker_id);
						}); 

    				function point_click(marker_id) {
							var selected_item = mappoints_box.find('.mappoints_data__item[data-markerid="'+marker_id+'"]'),
									marker_coos = selected_item.data('coos');

						  if ( $('body').hasClass('shops') ) {

						  	var page_url = selected_item.data('page');
						  	window.location.href = page_url;

						  } else {

								mappoints_box.find('.mappoints_data__item').removeClass('checked');
								selected_item.addClass('checked');
	
								if ( selected_item.hasClass('optionview_item') ) {
									mappoints_box.addClass('option_view');
								};
	
								let i = 0;
								while (i < markers_array.length) { 
									if ( outlets_array.includes(markers_array[i]) ) {
     								objectManager.objects.setObjectOptions(markers_array[i], {'iconImageHref': '/local/templates/main/img/marker_outlet.svg'});
     							} else {
     								objectManager.objects.setObjectOptions(markers_array[i], {'iconImageHref': '/local/templates/main/img/marker_citystress.svg'});
     							};
									i++;
								};
	
								if ( last_opened_balooon != 0 ) {
     							if ( outlets_array.includes(last_opened_balooon) ) {
    								objectManager.objects.setObjectOptions(last_opened_balooon, {'iconImageHref': '/local/templates/main/img/marker_outlet.svg'});
									} else {
    								objectManager.objects.setObjectOptions(last_opened_balooon, {'iconImageHref': '/local/templates/main/img/marker_citystress.svg'});
									}; 						
     						};
	
								if ( outlets_array.includes(marker_id) ) {
    							objectManager.objects.setObjectOptions(marker_id, {'iconImageHref': '/local/templates/main/img/marker_outlet__active.svg'});
								} else {
    							objectManager.objects.setObjectOptions(marker_id, {'iconImageHref': '/local/templates/main/img/marker_citystress__active.svg'});
								}; 
	
								last_opened_balooon = marker_id;
	
    						map_var.setZoom(12, { smooth: true });	
						  	map_var.panTo(marker_coos);

						  }
    				}

						$('.mappoints_goback').click(function() { 
							if ( !$('body').hasClass('single_shop') ){
								mappoints_box = $(this).closest('.mappoints_box');
  							mappoints_box.removeClass('option_view'); 
  							mappoints_box.find('.mappoints_data__item').removeClass('checked');
  							$('.delivery_option').removeClass('checked'); 
     						objectManager.objects.setObjectOptions(last_opened_balooon, {
    						  'iconImageHref': '/local/templates/main/img/marker_citystress.svg'
    						});
    					};
						}); 

						$('.map_nav__item').click(function() { 
							mappoints_box = $(this).closest('.mappoints_box');
							mappoints_box.find('.map_nav__item').removeClass('active');
							$(this).addClass('active');
							var option = $(this).data('option');

							if ( option == 'tryon' ) {


								let i = 0;
								while (i < markers_array.length) { 
     							objectManager.objects.setObjectOptions(markers_array[i], {
    		  					iconImageHref: '/local/templates/main/img/blank.png',
    		  					iconImageSize: [1,1],
    		  					iconImageOffset: [0,0]
    		  				});
									i++;
								};
	
								if ( last_opened_balooon != 0 ) {
     							objectManager.objects.setObjectOptions(last_opened_balooon, {
    		  					iconImageHref: '/local/templates/main/img/blank.png',
    		  					iconImageSize: [1,1],
    		  					iconImageOffset: [0,0]
    		  				});		
     						};


							} else {

							};

						}); 



					} else {


						var myPlacemark;

						mappoints_box.find('.mappoints_map').addClass('pointless');

    				map_var.events.add('click', function (e) {
    					var coords = e.get('coords');
				
    					if (myPlacemark) {
    						myPlacemark.geometry.setCoordinates(coords);
    					} else {
    					 	myPlacemark = createPlacemark(coords);
    					 	map_var.geoObjects.add(myPlacemark);
    					 	myPlacemark.events.add('dragend', function () {
    					 	   getAddress(myPlacemark.geometry.getCoordinates());
    					 	});
    					}; 

    					getAddress(coords);
    				});
				
    				function createPlacemark(coords) {
    					return new ymaps.Placemark(coords, {
    					}, {
    						preset: 'islands#violetDotIconWithCaption',
    						draggable: true,
    						openBalloonOnClick: false,
								iconLayout: 'default#image',
    		  			iconImageHref: '/local/templates/main/img/marker_citystress.svg',
    		  			iconImageSize: [40, 65],
    		  			iconImageOffset: [0, -65]
    					});
    				}
				
    				function getAddress(coords) {  
    					ymaps.geocode(coords).then(function (res) {
    						var firstGeoObject = res.geoObjects.get(0);
    						myPlacemark.properties.set({
    							iconCaption: [
    							    firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
    							    firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
    							].filter(Boolean).join(', '),
    							balloonContent: firstGeoObject.getAddressLine()
    						});  
 	if ( firstGeoObject.getThoroughfare() !== undefined ) {
    						$('.courier .form_v1 .field_street').val(firstGeoObject.getLocalities()+', '+firstGeoObject.getThoroughfare());
    						if ( firstGeoObject.getPremiseNumber() !== undefined ) {
    							$('.courier .form_v1 .field_bld').val(firstGeoObject.getPremiseNumber());
							}
	}
    					});
    				} 

					};


					$('.citypop .cities_list li').click(function() { 
    				map_var.setZoom(14, { smooth: true });	
						map_var.panTo($(this).data('coos'));
					});

    		});

    	});
    }); 
  }, 0);
};



if (document.readyState === 'loading') {
} else {  
  mappage_var = 1;
  on_load();
};

$(window).on('load', function(){
  if ( mappage_var == 0 ) {
    on_load();
  };
});

function on_load() {
  if ( $('.mappoints_map').length || $('.contacts_body__map').length ) {
	  if(window.location.href.indexOf("/cart/") == -1){
  	maps();
	  }
  };
};

