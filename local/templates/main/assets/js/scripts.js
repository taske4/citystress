jQuery(function ($) {


    var screenw = $(window).width(),
        screenh = $(window).height(),
        scrolltop = $(window).scrollTop(),
        pageload_var = 0,
        last_screenw,
        last_scrolltop = 0,
        over_w = $('.categories_over').outerWidth(),
        last_map_modal = 0,
        modal_name;


    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('body').addClass('safari');
    }

    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    $(window).on('resize', function () {
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
    $('.customselect li').click(function () {
        var this_val = $(this).text();
        $(this).closest('.customselect').find('.customselect_chosen').val(this_val);
    });

    $('.customselect').click(function () {
        if ($(this).hasClass()) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        ;
    });

    $(document).click(function (e) {
        that = e.target;
        if ($(that).closest(".customselect").length < 1 || $(that).hasClass('customselect')) {
            $('.customselect').removeClass('active');
        }
        ;
        if ($(that).is('li')) {
            $('.customselect').removeClass('active');
        }
        ;
    });


    if (screenw < 750) {
        $('.shops_container .shoppickup_item').slice(4).addClass('hidden');
        $('.shops_container .pickuppoint_item').slice(5).addClass('hidden');
    }

    $('.shops_container .shops_showmore').click(function (e) {
        $(this).closest('.shops_content').find('.hidden').fadeIn(200);
        $(this).css('display', 'none');
    });


    $('.field').bind('input propertychange', function () {
        if ($(this).val() != '') {
            $(this).removeClass('error');
        }
        ;
    });


    function delivery_option_select() {
        var status = $('.delivery_option.checked .delivery_option__status strong').text();
        $('.cart_order__submit strong').text(status);
        $('.delivery_option').removeClass('error');
        $('.delivery_opts .cart_order__error').css('display', 'none');
        $('body').removeClass('universal_modal__out');
        $('.modal').css('transform', 'translateX(100%)');
    }


    $('.select_pickuppoint').click(function () {
        var pickuppoint_item = $('.pickuppoint_item.checked'),
            item__title = pickuppoint_item.find('.pickuppoint_item__title').text(),
            item__address = pickuppoint_item.find('.pickuppoint_item__address').text(),
            item__date = pickuppoint_item.find('.pickuppoint_item__date').text();

        $('.delivery_option__pickuppoint .new').remove();

        $('.delivery_option__pickuppoint .delivery_option__address').text(item__address);
        $('.delivery_option__pickuppoint ul').append('<li class="new">' + item__title + '</li><li class="new">' + item__date + '</li>');

        $('.delivery_option').removeClass('checked');
        $('.delivery_option__pickuppoint').addClass('checked');
        delivery_option_select();
    });


    $('.select_shop').click(function () {
        var shoppickup_item = $('.shoppickup_item.checked'),
            item__title = shoppickup_item.find('.shoppickup_item__title').text(),
            item__address = shoppickup_item.find('.shoppickup_item__address').text(),
            item__date = shoppickup_item.find('.pickuppoint_item__date').text(),
            item__worktime = shoppickup_item.find('.shoppickup_item__worktime').text();

        $('.delivery_option__shoppickup .metros, .delivery_option__shoppickup .new').remove();

        $('.delivery_option__shoppickup .delivery_option__address').text(item__address);
        $('.delivery_option__shoppickup ul').append('<li class="new">' + item__title + '</li><li class="new">' + item__date + '</li><li class="new">' + item__worktime + '</li>');
        shoppickup_item.find('.metros').clone().insertAfter('.delivery_option__shoppickup ul');

        $('.delivery_option').removeClass('checked');
        $('.delivery_option__shoppickup').addClass('checked');
        delivery_option_select();
    });


    $('div.shops_nav__item').click(function (e) {
        var this_index = $(this).data('index');

        $('.shops_nav__item').removeClass('active');
        $(this).addClass('active');

        $('.shops_out').css('display', 'none');
        $('.shops_out_' + this_index).fadeIn(200);
    });

    $('.citypop').on('click', '.cities_list li, .cities_found li', function () {
        if ($(this).parents('.cities_found').length) {
            var city_title = $(this).find('strong').text();
        } else {
            var city_title = $(this).text();
        }

        $('.mappoints_city span, .mob_contacts__city span, .submenu_city span, .city_select__chose').text(city_title);

        if ($('.cart_container').length) {
            $('#alert_region').fadeIn(200);
            setTimeout(function () {
                $('#alert_region').fadeOut(200);
            }, 3000);
        }
        ;

        close_all_popups();

        selectCity($(this).attr('data-location-id'));
    });

    $(document).on('click', '.universal_modal__in', function () {
        close_all_popups();
        $('body').addClass('universal_modal__out');

        modal_name = $(this).data('modalname');

        if ($(this).hasClass('not_from_mapmodal')) {

            last_map_modal = 0;

        } else {

            if (modal_name != 'cityselect') {
                if (modal_name == 'instore' || modal_name == 'courier' || modal_name == 'shoppickup' || modal_name == 'pickuppoint' || modal_name == 'postoffice') {
                    last_map_modal = $('.universal_modal__' + modal_name);
                } else {
                    last_map_modal = 0;
                }
                ;
            }
            ;

        }
        ;

        $('.universal_modal__' + modal_name).css('transform', 'translateX(0)');
        $('.universal_modal__' + modal_name).css('-webkit-transform', 'translateX(0)');
    });


    $('.modal_close').click(function () {
        close_all_popups();
    });


    cardexp_mask_callbacks = {
        onComplete: function (cep, event, currentField, options) {
            var this_val = currentField.val(),
                this_month = this_val.substring(0, 2),
                this_year = this_val.substring(3, 5);

            if (this_month > 12 || this_year < 23) {
                currentField.addClass('error');
            } else {
                currentField.addClass('filled');
            }
            ;
        },
        onKeyPress: function (cep, event, currentField, options) {
            currentField.removeClass('error filled');

            var this_val = currentField.val(),
                this_month = this_val.substring(0, 2),
                this_year = this_val.substring(3, 5);

            if (this_month > 12 || this_val.length > 4 && this_year < 23) {
                currentField.addClass('error');
            }
            ;
        }
    };

    $('.cardexp_field').mask('00/00', cardexp_mask_callbacks);

    card_mask_callbacks = {
        onComplete: function (cep, event, currentField, options) {
            currentField.addClass('filled');
        },
        onKeyPress: function (cep, event, currentField, options) {
            currentField.removeClass('filled');
        }
    };

    $(".card_field").mask('9999-9999-9999-9999', card_mask_callbacks);

    $('.cardcvc_field').mask('999', card_mask_callbacks);


    $('.cart_codes__show').click(function () {
        $(this).closest('.cart_codes__box').addClass('opened');
    });

    $('.cart_codes__change, .cart_codes__remove').click(function () {
        $(this).closest('.cart_codes__box').removeClass('applied').find('.cart_codes__input').val('');
    });

    $('.cart_codes__input').bind('input propertychange', function () {
        $(this).closest('.cart_codes__box').removeClass('error');
    });


    $('.payment_option').click(function () {
        $('.payment_option').removeClass('checked');
        $(this).addClass('checked');
        if ($(this).hasClass('payment_option__podeli')) {
            $('.payment_opts .podeli').fadeIn(200);
        } else {
            $('.payment_opts .podeli').css('display', 'none');
        }
        ;
    });


    $('.cart_item__remove').click(function () {
        $(this).closest('.cart_item').remove();
    });

    $('.cart_item__restore').click(function () {
        $(this).closest('.cart_item').removeClass('deleted');
    });


    $('.quantity_minus').click(function () {
        var cart_item = $(this).closest('.cart_item');

        if ($(this).hasClass('delete')) {

            cart_item.addClass('deleted');

        } else {

            var current = cart_item.find('.quantity_input').val();

            if (current > 1) {
                var new_val = +current - 1;
                $(this).removeClass('delete');
                cart_item.find('.quantity_input').val(new_val);
            }
            ;

            if (new_val == 1) {
                $(this).addClass('delete');
            }
            ;

        }
        ;
    });

    $('.quantity_plus').click(function () {
        var cart_item = $(this).closest('.cart_item'),
            max = cart_item.find('.quantity_input').data('max'),
            current = cart_item.find('.quantity_input').val();

        cart_item.find('.quantity_minus').removeClass('delete');

        if (current < max) {
            var new_val = +current + 1;
            cart_item.find('.quantity_input').val(new_val);
        }
        ;
    });


    /*function cart_check() {
      if ( $('.cart_item__checkbox.checked').length ) {
        $('.cart_order__submit').removeClass('no_selected').find('span').text('Оформить');
      } else {
        $('.cart_order__submit').addClass('no_selected').find('span').text('Выбрать');
      };
    }

    cart_check();*/

    $('.check_all').click(function () {
        if ($(this).hasClass('checked')) {
            $('.cart_item__checkbox').removeClass('checked');
        } else {
            $('.cart_item__checkbox').addClass('checked');
        }
        ;
        cart_check();
    });

    $('.checkbox').click(function () {
        if ($(this).hasClass('checked')) {
            $(this).find('input').prop('checked', false);
            $(this).removeClass('checked');
        } else {
            $(this).find('input').prop('checked', true);
            $(this).addClass('checked');
        }
        ;
        cart_check();
    });


    var mobcodes_array = [900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999],
        mask_options = {
            onKeyPress: function (cep, event, currentField, options) {
                var this_val = currentField.val(),
                    this_val_len = this_val.length,
                    code = this_val.substring(4, 7),
                    this_wrap = currentField.closest('.field_wrap');

                this_wrap.removeClass('error valid').find('.field_errorlabel').text('Это обязательное поле');

                if (this_val_len >= 17) {
                    if (mobcodes_array.includes(parseInt(code))) {
                        this_wrap.removeClass('error').addClass('valid');
                    } else {
                        this_wrap.addClass('error').find('.field_errorlabel').text('Некорректный номер');
                    }
                    ;
                }
                ;
            }
        };

    // $(".field_phone").mask('+7 (999) 999-9999', mask_options);

    $('body').on('submit', '.restore_step2 .form_v1', function (e) {
        e.preventDefault();

        let code = $('.restore_step2 form [name=code]').val();
        let passwd = $('.restore_step2 form [name=passwd]').val();
        let passwd2 = $('.restore_step2 form [name=passwd2]').val();

        let res = smsChangePasswd(code, passwd, passwd2);

        if (res.error) {
            $('.restore_step2 .error').html(res.error);
        } else if (res.success) {
            $('.restore_step2 .error').html('');

            $('.restore_step').css('display', 'none');
            $('.restore_step3').fadeIn(200);
        } else {
            $('.restore_step2 .error').html('Ошибка.');
        }

        return false;
    });


    $('body').on('click', '.go_login', function () {
        $('.auth_head .go_signup').removeClass('active');
        $('.auth_head .go_login').addClass('active');
        $('.auth_content').css('display', 'none');
        $('.auth_content_1').css('display', 'block');
    });

    $('.go_signup').click(function () {
        $('.auth_head .go_signup').removeClass('active');
        $('.auth_head .go_login').addClass('active');
        $('.auth_content').css('display', 'none');
        $('.auth_content_2').css('display', 'block');
    });

    // $(".field_phone").mask('+7 (999) 999-9999', mask_options);

    function fix_box() {
        if ($('.fixbox').length && screenw > 750) {

            if (screenw > 1000) {
                var right_pos = '25px';
            } else {
                var right_pos = '20px';
            }
            ;

            var sticky = $('.product_info'),
                sticky_h = sticky.outerHeight(),
                gallery_h = $('.product_gallery').outerHeight();

            if (gallery_h > sticky_h) {

                var fixbox = $('.fixbox'),
                    stop_top = gallery_h - sticky_h - 10;

                if ($('body').hasClass('head_scroll')) {

                    if ($('body').hasClass('scroll_down')) {

                        if (scrolltop > (sticky_h - screenh + 30 + 63)) {

                            sticky.css({
                                'position': 'fixed',
                                'bottom': '30px',
                                'top': 'auto',
                                'right': right_pos
                            });

                        }
                        ;

                        if (scrolltop > (gallery_h - 10 - screenh + 30 + 63)) {

                            sticky.css({
                                'position': 'absolute',
                                'bottom': 'auto',
                                'top': stop_top + 'px',
                                'right': '0'
                            });

                        }
                        ;

                    }
                    ;


                    if ($('body').hasClass('scroll_up')) {

                        if (scrolltop < stop_top) {

                            sticky.css({
                                'position': 'fixed',
                                'bottom': 'auto',
                                'top': '63px',
                                'right': right_pos
                            });

                        } else {

                            sticky.css({
                                'position': 'absolute',
                                'bottom': 'auto',
                                'top': stop_top + 'px',
                                'right': '0'
                            });

                        }
                        ;

                    }
                    ;


                } else {

                    sticky.css({
                        'position': 'relative',
                        'bottom': 'auto',
                        'top': 'auto',
                        'right': 'auto'
                    });

                }
                ;

            } else {

                sticky.css({
                    'position': 'relative',
                    'bottom': 'auto',
                    'top': 'auto',
                    'right': 'auto'
                });

            }
            ;

        }
        ;
    };


    function mob_screenh() {
        inner_height = window.innerHeight + 1;

        $('.mobmenu, .nosize, .look, .modal').css('height', inner_height + 'px');

        if (screenw < 1000) {
            $('.header_container .search_block, .mobcats, .filters_out, .modal').css('height', inner_height + 'px');
        } else {
            $('.filters_out').css('height', 'auto');
        }
        ;

        if (screenw < 750) {
            $('.product_mobmodal .product_menuitem__content').css('height', inner_height + 'px');
        } else {
            $('.product_mobmodal .product_menuitem__content').css('height', 'auto');
        }
        ;
    };

    function delivery_option_select() {
        var status = $('.delivery_option.checked .delivery_option__status strong').text();
        $('.cart_order__submit strong').text(status);
        $('.delivery_option').removeClass('error');
        $('.delivery_opts .cart_order__error').css('display', 'none');
        $('body').removeClass('universal_modal__out');
        $('.modal').css('transform', 'translateX(100%)');
    }

    $('.universal_modal__in').on('click', function () {
        close_all_popups();
        $('body').addClass('universal_modal__out');

        modal_name = $(this).data('modalname');

        if ($(this).hasClass('not_from_mapmodal')) {

            last_map_modal = 0;

        } else {

            if (modal_name != 'cityselect') {
                if (modal_name == 'instore' || modal_name == 'courier' || modal_name == 'shoppickup' || modal_name == 'pickuppoint' || modal_name == 'postoffice') {
                    last_map_modal = $('.universal_modal__' + modal_name);
                } else {
                    last_map_modal = 0;
                }
                ;
            }
            ;

        }
        ;

        $('.universal_modal__' + modal_name).css('transform', 'translateX(0)');
    });

    window.close_all_popups = function (type) {
        $('body').removeClass('filters_show reviews_out faq_out mobsizes_out hints_out universal_modal__out');
        $('.modal').css('transform', 'translateX(100%)');
        $('.share, .mobadd_back, .mobadd').fadeOut(200);

        if (modal_name == 'cityselect' && last_map_modal && last_map_modal != 0) {
            $('body').addClass('universal_modal__out');
            last_map_modal.css('transform', 'translateX(0)');
            modal_name = 0;
        }
        ;
    };


    $('body').on('click', '.modal_close', function () {
        close_all_popups();
    });


    $('.product_mob__chosensize').click(function () {
        $('body').addClass('mobsizes_out');
    });

    // $('.product_container .product_mob__actions .button').click(function () {
    //     $(this).addClass('active');
    // });


    $('.instore_store').click(function () {
        var address = $(this).data('address'),
            worktime = $(this).data('worktime'),
            phone = $(this).data('phone'),
            this_title = $(this).find('strong').text();

        $('.instore .store_title').text(this_title);
        $('.instore .store_address').text(address);
        $('.instore .store_worktime').text(worktime);
        $('.instore .store_phone').text(phone);

        $('.instore_store').removeClass('active');
        $(this).addClass('active');
    });

    $('.instore_button').click(function () {
        $('.instore').addClass('store_view');
    });

    $('.instore_pickother').click(function () {
        $('.instore').removeClass('store_view');
    });


    if (screenw < 750) {
        $('.m_customscroll').mCustomScrollbar({
            verticalScroll: true,
            horizontalScroll: false,
            documentTouchScroll: true,
            documentTouchScroll: false
        });

        $('.product_reviews .product_menuitem__title').click(function () {
            $('body').addClass('reviews_out');
        });

        $('.product_faq .product_menuitem__title').click(function () {
            $('body').addClass('faq_out');
        });
    }
    ;


    $('.cities_list li').click(function () {
        $('.submenu_city span, .mob_contacts__city span').text($(this).text());
    });

    $('.citypop .cityform_input').bind('input propertychange', function () {
        if ($('.cityform_input').val().length > 2) {
            searchCityHeader($('.cityform_input').val());
        } else {
            $('.cities_list').fadeIn(200);
            $('.cities_found').css('display', 'none');
        }
    });

    $('.citypop .cityform_button').click(function () {
        if ($('.cityform_input').val().length > 2) {
            $('.cities_list').css('display', 'none');
            $('.cities_found').fadeIn(200);
        }
        ;
    });

    $('.product_share').click(function () {
        close_all_popups();
        $('.share_back, .share').fadeIn(200);
        $('.share_link__input').val(window.location.href);

        let shareVkUrl = "https://vk.com/share.php?url=" + window.location.href + "&utm_source=share2";
        let shareWa = "https://api.whatsapp.com/send?text=" + window.location.href + "&utm_source=share2";
        let shareTg = "https://t.me/share/url?url=" + window.location.href + "&utm_source=share2";
        let shareOk = "https://connect.ok.ru/offer?url=" + window.location.href + "&utm_source=share2";

        $('.share_tg').attr('href', shareTg);
        $('.share_vk').attr('href', shareVkUrl);
        $('.share_wa').attr('href', shareWa);
        $('.share_ok').attr('href', shareOk);
    });

    $('.share_link__copy').click(function () {
        var link = $('.share_link__input').val();
        navigator.clipboard.writeText(link);
    });


    $('.item_w_hint__title').click(function () {
        var item_w_hint = $(this).closest('.item_w_hint');
        if (item_w_hint.hasClass('active')) {
            $('.item_w_hint').removeClass('active');
            close_all_popups();
        } else {
            $('body').addClass('hints_out');
            $('.item_w_hint').removeClass('active');
            item_w_hint.addClass('active');
        }
        ;
    });

    $('.hint_close, .hints_close').click(function () {
        $(this).closest('.item_w_hint').removeClass('active');
        close_all_popups();
    });

    $(document).click(function (e) {
        that = e.target;
        if ($(that).closest(".item_w_hint").length < 1) {
            $('.item_w_hint').removeClass('active');
        }
    });

    $('body').on('click', '.product_mini__tablein, .product_sizes_tablein, .product_measure', function () {
        close_all_popups();
        $('.look').css('right', '-100vw');
        $('body').addClass('sizetable_out');
    });


    $('.podeli').click(function () {
        close_all_popups();
        $('body').addClass('partspay_out');
    });


    $('body').on('click', '.acc_item__open', function () {
        var acc_item = $(this).closest('.acc_item');
        if (acc_item.hasClass('active')) {
            acc_item.find('.acc_item__out').css('display', 'none');
            acc_item.removeClass('active');
        } else {
            acc_item.find('.acc_item__out').fadeIn(200);
            acc_item.addClass('active');
        }
        ;

        fix_box();
    });


    $('.categories_item').each(function () {
        if ($(this).hasClass('last')) {
            $(this).attr('data-rightpos', Math.round($(this).offset().left + $(this).outerWidth()));
        } else {
            $(this).attr('data-rightpos', Math.round($(this).offset().left + $(this).outerWidth() + $(this).next().outerWidth()));
        }
        ;
    });

    $('.categories_prev').click(function () {
        if (!$(this).hasClass('disabled')) {
            var prev = $('.categories_item.active').prev();
            $('.categories_item').removeClass('active');
            prev.addClass('active');
            cats_recalc();
        }
        ;
    });

    $('.categories_next').click(function () {
        if (!$(this).hasClass('disabled')) {
            var next = $('.categories_item.active').next();
            $('.categories_item').removeClass('active');
            next.addClass('active');
            cats_recalc();
        }
        ;
    });

    $('.categories_item').hover(function () {
        if ($('.subcats.subcats_' + $(this).data('catid')).length) {
            $('.categories_item').removeClass('active');
            $(this).addClass('active');
            cats_recalc();
        }
    });

    $('.subcat_item').click(function () {
        $('.subcat_item').removeClass('active');
        $(this).addClass('active');
    });

    function cats_recalc() {
        var active = $('.categories_item.active');

        $('.categories_prev, .categories_next').removeClass('disabled');

        if (active.hasClass('all')) {
            $('.categories_prev').addClass('disabled');
        }
        ;

        if (active.hasClass('last')) {
            $('.categories_next').addClass('disabled');
        }
        ;

        var catid = active.data('catid');
        $('.subcats').css('display', 'none');
        $('.subcats_' + catid).css('display', 'flex');

        var this_right = active.data('rightpos');
        var diff = over_w - this_right;

        if (!active.hasClass('last')) {
            if (diff < 1) {
                $('.categories_list').css('left', diff + 'px');
            } else {
                $('.categories_list').css('left', '0');
            }
            ;
        }
        ;

    };


    function is_high_resolution_screen() {
        return window.devicePixelRatio > 1;
    }

    function picture_img() {
        $('picture img').each(function () {
            var img1x = $(this).data('img1x'),
                img2x = $(this).data('img2x');

            if (isSafari) {

                var imgmob = $(this).data('imgmob'),
                    imgtab = $(this).data('imgtab');

                if (screenw < 750) {
                    if (imgmob) {
                        $(this).attr('src', imgmob);
                    } else {
                        $(this).attr('src', img2x);
                    }
                }
                ;

                if (screenw > 750 && screenw < 1000) {
                    if (imgtab) {
                        $(this).attr('src', imgtab);
                    } else {
                        $(this).attr('src', img2x);
                    }
                }
                ;

                if (screenw > 1000) {
                    $(this).attr('src', img2x);
                }
                ;

            } else {

                if (is_high_resolution_screen()) {
                    $(this).attr('src', img2x);
                } else {
                    $(this).attr('src', img1x);
                }
                ;

            }
            ;
        });
    };

    picture_img();


    $('.product_mobcart').click(function (e) {
        e.preventDefault();
        close_all_popups();
        $('.mobadd_sizes .product_size').remove();
        $('.mobadd').attr('data-product', $(this).closest('.product').attr('data-product'));
        $(this).closest('.product').find('.product_size').clone().appendTo('.mobadd_sizes');
        $('.mobadd_back, .mobadd').fadeIn(200);
        product_sizes();
    });

    $('body').on('click', '.mobadd_sizes__report, .product_mini__sizereport', function () {
        var sizes = $(this).closest('.mobadd').find('.sizes');
        nosize_report(sizes);
    });


    function product_sizes() {

        $('.product_block ').on('click', '.sizes .product_size', function (e) {
            e.preventDefault();
            var sizes = $(this).closest('.sizes');

            $(sizes).find('.product_size').removeClass('active');

            if (!$(this).hasClass('disabled')) {

                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                } else {
                    $(this).addClass('active');
                }

                sizes.addClass('loading');
                setTimeout(function () {
                    sizes.removeClass('loading');
                }, 500);

                var chosen_sizes = '',
                    active_len = 0;

                sizes.find('.product_size.active').each(function () {
                    active_len = active_len + 1;
                    var this_text = $(this).text();
                    if (chosen_sizes == '') {
                        chosen_sizes = this_text;
                    } else {
                        if (active_len > 2) {
                            chosen_sizes = chosen_sizes + '...';
                        } else {
                            chosen_sizes = chosen_sizes + ', ' + this_text;
                        }
                        ;
                    }
                    ;
                });

                $('.product_mob__chosensize span').text(chosen_sizes);

            } else {

                nosize_report(sizes);

            }
            ;
        });
    };

    product_sizes();


    function nosize_report(parent_element) {
        close_all_popups();
        $('.nosize_size').remove();

        parent_element.find('.product_size').each(function () {
            var title = $(this).data('title'),
                subtitle = $(this).data('subtitle');
            $('.nosize_sizes').append('<button class="nosize_size"><div class="nosize_sizebox"><div class="nosize_sizewrap"><div class="nosize_size__title">' + title + '</div><div class="nosize_size__subtitle">' + subtitle + '</div></div></div></button>')
        });

        $('body').addClass('size_out');

        $('.mobadd_back, .mobadd').css('display', 'none');

        customscroll();

        $('.nosize_size').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
            ;

            var report_sizes = '';
            $('.nosize_size.active').each(function () {
                var this_text = $(this).text();
                if (report_sizes == '') {
                    report_sizes = this_text;
                } else {
                    report_sizes = report_sizes + ', ' + this_text;
                }
                ;
            });
            $('.form_v1__in').val(report_sizes);
        });
    };


    $(document).on('click', '.complete_look', function () {
        let data = $(this).parents('[data-set]').attr('data-set');

        // Косякнул где то, и надо offerId заменить - что бы картинки были товара
        let productContainer = $('.product_detail_page .product_block[data-product]'),
            dataProduct;

        if ($(this).closest('.product[data-product]').length) {
            dataProduct = $(this).closest('.product[data-product]').attr('data-product');
        } else if (productContainer.length) {
            dataProduct = productContainer.attr('data-product');
        }


        if (dataProduct.length) {
            dataProduct = JSON.parse(dataProduct);
        }

        if (dataProduct) {
            data = JSON.parse(data);
            let offerId = null;

            console.log($(this));
            console.log(dataProduct.selectedColorOfferId);

            if (!offerId) {
                offerId = dataProduct.selectedColorOfferId;
            }

            if ($('.product_container .product_color__item.active').length && !offerId) {
                offerId = $('.product_container .product_color__item.active').attr('data-offer-id');
            }

            if (!offerId) {
                offerId = dataProduct.activeOfferId;
            }

            if (offerId) {
                data.offerId = offerId;
            }

            data = JSON.stringify(data);
        }

        ///////////////////////

        let params = new URLSearchParams();
        params.append('data', data);

        $('.universal_modal__look_1').html('');

        $.ajax({
            type: 'GET',
            url: '/local/ajax/getLookside.php?'+String(params),
            dataType: 'html',
            success: function (response) {
                $('.universal_modal__look_1').html(response);

                setTimeout(function(){
                    $('.look_1 .look_slider').slick('refresh');
                    
                    $('.look_body .customscroll').mCustomScrollbar({
                        verticalScroll: true,
                        horizontalScroll: false,
                        documentTouchScroll: true,
                        documentTouchScroll: false
                    });
                }, 100);
                
            },
        });

        // $('.universal_modal__look_1').load('/local/ajax/getLookside.php?' + String(params), function () {
        //     $('.look_1 .look_slider').slick('refresh');
        // });
    });

    $('.look_back, .look_close').click(function () {
        // $('.look').css('right', '-100vw');
    });


    /**
     * -> custom.js
     */

    $('.categories_mobin').click(function () {
        window.filtersContainerCssZIndex = $('body').find('.filters').css('z-index');
        $('body').find('.filters').css('z-index', 999999);
        $('body').addClass('mobcats_show');

        window.collectionTriggerElements.forEach(function (item, i, arr) {
            $(item).find('span').first().trigger('click');
        });
    });

    $('.mobcats_head__close, .mobcats_back').click(function () {
        $('body').find('.filters').css('z-index', window.filtersContainerCssZIndex);

        hide_mobcats();
    });

    $('.mobcats_lvl1li > span').click(function () {
        if ($(this).parent().hasClass('w_sub')) {

            if ($(this).parent().hasClass('active')) {
                $('.mobcats_lvl1li').removeClass('active');
                $('body').removeClass('mobcats_lvl2');
                $(this).addClass('has_children');
            } else {
                $('.mobcats_lvl1li').removeClass('active');
                $('body').addClass('mobcats_lvl2');
                $(this).parent().addClass('active');
                $(this).removeClass('has_children');
            }
            ;

        } else {

            hide_mobcats();

        }
        ;
        customscroll();
    });

    $('.mobcats_lvl2li > span').click(function () {
        if ($(this).parent().hasClass('w_sub')) {

            if ($(this).parent().hasClass('active')) {
                $('.mobcats_lvl2li').removeClass('active');
                $('body').removeClass('mobcats_lvl3');
                $(this).addClass('has_children');
            } else {
                $('.mobcats_lvl2li').removeClass('active');
                $('body').addClass('mobcats_lvl3');
                $(this).parent().addClass('active');
                $(this).removeClass('has_children');
            }
            ;

        } else {

            hide_mobcats();

        }
        ;
        customscroll();
    });

    $('.mobcats_lvl3li > span').click(function () {
        hide_mobcats();
    });


    function hide_mobcats() {
        $('body').removeClass('mobcats_show mobcats_lvl2 mobcats_lvl3');
        $('.mobcats_lvl1li, .mobcats_lvl2li, .mobcats_lvl3li').removeClass('active');
    }


    $('.cards_cols span').click(function () {
        $('.cards_cols span').removeClass('active');
        $(this).addClass('active');
        var type = $(this).data('type');
        $('.catalog_items').removeClass('by1 by2 by3 by4').addClass(type);
        $('.products_slider').slick('refresh');
        $('.product_slider').slick('refresh');
    });

    function bycols() {
        var cards_cols_active = $('.cards_cols .active');

        if (screenw < 750) {
            if (cards_cols_active.data('type') == 'by3' || cards_cols_active.data('type') == 'by4' || cards_cols_active.data('type') == 'by2') {
                $('.cards_cols .by1').addClass('active');
                cards_cols_active.removeClass('active');
            }
        }
        ;

        if (screenw > 750 && screenw < 1000) {
            if (cards_cols_active.data('type') == 'by1' || cards_cols_active.data('type') == 'by4') {
                $('.cards_cols .by3').addClass('active');
                cards_cols_active.removeClass('active');
            }
        }
        ;

        if (screenw > 1000) {
            if (cards_cols_active.data('type') == 'by1' || cards_cols_active.data('type') == 'by2') {
                $('.cards_cols .by4').addClass('active');
                cards_cols_active.removeClass('active');
            }
        }
        ;

    };

    function catalog_cats() {
        // if (screenw < 1000) {
        //     $('.catalog_cats-mobile').removeClass('hiddenpro');
        // } else {
        //     $('.catalog_cats-desktop').removeClass('hiddenpro');
        // }

        // let list = [];
        //
        // function collectTriggerElements(sectionId, list)
        // {
        //     let element = '[data-section-id="'+sectionId+'"]';
        //
        //     if ( $(element).length)
        //     {
        //         list.push($(element));
        //
        //         if (parseInt($(element).data('parent-section-id')) > 0) {
        //             collectTriggerElements($(element).data('parent-section-id'), list);
        //         }
        //     }
        // }
        //
        // collectTriggerElements(sectionId, list);
        //
        // list.forEach(function(item, i, arr) {
        //     item.find('.has_children').trigger('click');
        // });

        // let parentId = $('[data-section-id="'+sectionId+'"]').data('section-id');
        //
        // if (parentId) {
        //     list.append($('[data-section-id="'+parentId+'"] .has_children'));
        // }
    }


    $('.footer_col').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        ;
    });


    $('.filters_in').click(function () {
        if ($('body').hasClass('filters_show')) {
            $('body').removeClass('filters_show');
        } else {
            $('body').addClass('filters_show');
        }
        ;
        filters_customscroll_main();
    });

    function filter_item_remove() {
        $(document).on('click', '.filters_applied__remove', function () {
            let controlId = $(this).parent().attr('data-item-control-id');
            $('.filters_item [data-control-id=' + controlId + ']').removeClass('active');
            $(this).parent().remove();
            filters_recalc();
        });
    };

    filter_item_remove();

    function filters_recalc() {
        if ($('.filters_applied .filters_applied__item').length) {
            $('.filters_in').addClass('active');
            $('.filters_reset').css('display', 'block');
        } else {
            $('.filters_in').removeClass('active');
            $('.filters_reset').css('display', 'none');
        }
    };


    $('.filters_item__label').click(function () {
        if (screenw < 1000) {

            var filters_item = $(this).closest('.filters_item'),
                this_text = $(this).text();

            $('.filters_item').removeClass('active').css('display', 'none');
            filters_item.addClass('active').css('display', 'block');

            if (this_text == 'Цена') {
                $('.filters_mobhead__title').text('Фильтр');
            } else {
                $('.filters_mobhead__title').text(this_text);
            }
            ;

            $('.filters .button_apply').css('display', 'block');
            $('.filters .show_results').css('display', 'none');
        }
    });


    $('.button_apply').click(function () {
        var filters_item = $('.filters_item.active');
        var chosen = '';
        filters_item.find('li.active').each(function () {
            var this_text = $(this).find('span').text();
            if (chosen == '') {
                chosen = this_text;
            } else {
                chosen = chosen + ', ' + this_text;
            }
            ;
        });

        filters_item.find('.filters_item__chosen').text(chosen);

        $('.filters_item').removeClass('active').css('display', 'block');

        $('.filters .button_apply').css('display', 'none');
        $('.filters .show_results').css('display', 'block');

        $('.filters_mobhead__title').text('Фильтр');
        filters_customscroll_main();
    });

    $('.filters_reset, .filters_mobhead__reset').click(function () {
        $('.filters .button_apply').css('display', 'none');
        $('.filters .show_results').css('display', 'block');

        $('.filters_applied__item').remove();
        $('.filters_reset').css('display', 'none');
        $('.filters_in, .filters_item, .filters_item li').removeClass('active');
        $('.filters_mobhead__title').text('Фильтр');
        $('.filters_item').css('display', 'block');
        $('.filters_item:not(".filters_item__sort") .filters_item__chosen').text('');
        $('.filters_item:not(".filters_item__sort") .filters_item__out li').removeClass('active');
        filters_customscroll_main();
    });

    $('.filters_item__out li:not(".disabled")').click(function () {
        var filters_item = $(this).closest('.filters_item'),
            this_text = $(this).text();

        if (filters_item.hasClass('filters_item__sort')) {

            filters_item.find('li').removeClass('active');
            $(this).addClass('active');

        } else {

            if ($(this).hasClass('active')) {
                $('.filters_applied__item[data-text="' + this_text + '"]').remove();
                $(this).removeClass('active');
            } else {
                if (!$('.filters_applied__item[data-text="' + this_text + '"]').length) {
                    $('.filters_applied').append('<button class="filters_applied__item" data-item-control-id="' + $(this).attr('data-control-id') + '" data-text="' + this_text + '"><span>' + this_text + '</span><img src="/local/templates/main/img/close.svg" alt="" class="filters_applied__remove"></button>');
                    $('.filters_reset').appendTo('.filters_applied');
                    filter_item_remove();
                }
                ;
                $(this).addClass('active');
            }
            ;

        }
        ;

        if (screenw < 1000) {

            if (filters_item.find('li.active').length) {
                filters_item.find('.button_apply').css('display', 'block');
            } else {
                filters_item.find('.button_apply').css('display', 'none');
            }
            ;

            $('.filters_item__sort .button_apply').css('display', 'block');

        }
        ;

        filters_recalc();
    });

    function filters_customscroll_main() {
        if (screenw < 1000) {
            $('.filters .filters_customscroll_main').mCustomScrollbar({
                verticalScroll: true,
                horizontalScroll: false,
                documentTouchScroll: true,
                documentTouchScroll: false
            });
        }
    };


    function retail_price_slider() {
        if ($('.pricerange_slider').length) {
            if ($('.ui-slider').length) {
                $(".pricerange_slider").slider("destroy");
            }
            ;
            $('.pricerange_slider_wrap').each(function () {
                var pricerange_w = $('.pricerange_slider').outerWidth(),
                    data_min = parseInt($(this).attr('data-min')),
                    data_max = parseInt($(this).attr('data-max')),
                    data_min_html = parseInt($(this).attr('data-min-html')),
                    data_max_html = parseInt($(this).attr('data-max-html'));

                $(this).find('.pricerange_slider').slider({
                    step: 1,
                    range: true,
                    min: data_min,
                    max: data_max,
                    values: [data_min_html, data_max_html],
                    create: function (event, ui) {
                        $('.ui-slider-range').append('<div></div>');
                        $('.ui-slider-handle').each(function (index) {
                            $(this).addClass('index_' + index).append('<div></div>');
                        });
                        $('.ui-slider-handle.index_0 div').text(data_min_html);
                        $('.ui-slider-handle.index_1 div').text(data_max_html);
                    },
                    slide: function (event, ui) {
                        $('.filters_item__price [data-min-html]').attr('data-min-html', ui.values[0]);
                        $('.filters_item__price [data-max-html]').attr('data-max-html', ui.values[1]);
                        $('.ui-slider-handle.index_0 div').text(ui.values[0]);
                        $('.ui-slider-handle.index_1 div').text(ui.values[1]);
                    }
                });
            });
        }
        ;
    };


    $('.submenu_close').click(function () {
        $('body').removeClass('submenu_show search_active');
        $('.submenu').css('display', 'none');
        $('.header_menu li').removeClass('active');
    });

    $('.search_close, .search_closeicon').click(function () {
        $('body').removeClass('submenu_show search_active');
        $('.search_form__input').attr('placeholder', 'Поиск...');
    });


    $('.submenu_nav li span').mouseenter(function () {
        var this_el = $(this).parent();
        var cat = this_el.data('cat');
        $('.submenu_nav li').removeClass('active');
        this_el.addClass('active');
        $('.submenu_out').css('display', 'none');
        $('.submenu_out_' + cat).fadeIn(200);
        $('.submenu_out_' + cat + ' .slider').slick('refresh');
    });


    $('.search_form').on('click', function () {
        if (screenw < 1100) {
            $('.search_form__input').attr('placeholder', 'Нажмите для поиска');
        }
        $('.header_menu li').removeClass('active');
        $('.submenu').css('display', 'none');
        $('body').removeClass('submenu_show mobmenu_active').addClass('search_active');
    });


    $('.header_menu li').hover(function () {
        $('.header_menu li').removeClass('active');
        $(this).addClass('active');
        var submenu = $(this).data('submenu');
        $('.submenu').css('display', 'none');
        $('.submenu_' + submenu).css('display', 'block');
        $('body').removeClass('search_active').addClass('submenu_show');
        $('.submenu_' + submenu).find('.slider').slick('refresh');
    });

    $('.submenu_close, .header_submenu_close').hover(function () {
        $('body').removeClass('submenu_show search_active');
        $('.search_form__input').attr('placeholder', 'Поиск...');
        $('.submenu').css('display', 'none');
        $('.header_menu li').removeClass('active');
    });


    $('.header_action__close').click(function () {
        $('.header_action').remove();
        $('body').removeClass('action_active');

        closeHeaderAction();
    });


    function customscroll() {
        $('.customscroll').mCustomScrollbar({
            verticalScroll: true,
            horizontalScroll: false,
            documentTouchScroll: true,
            documentTouchScroll: false
        });
    };


    $('.mobmenu_lvl1li').click(function (e) {
        that = e.target;

        if (!$(that).hasClass('mobmenu_lvl2') && !$(that).hasClass('mobmenu_lvl2li')) {

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $('.mobmenu_lvl1li.active').removeClass('active');
                $(this).addClass('active');
            }
            ;

            customscroll();
        }
        ;
    });


    $('.sizetable_item__out td').on("mouseenter", function (index) {
        if (!$(this).parent().hasClass('last')) {
            var this_index = $(this).index() + 1;
            $(this).closest('table').find('td:nth-child(' + this_index + '), th:nth-child(' + this_index + ')').addClass('hover');
            $(this).parent().addClass('hover');
        }
        ;
    });

    $('.sizetable_item__out td').on("mouseleave", function (index) {
        $(this).closest('table').find('td, th').removeClass('hover');
        $(this).parent().removeClass('hover');
    });


    $('.product_menuitem__title').click(function () {
        var product_menuitem = $(this).closest('.product_menuitem');
        if (product_menuitem.hasClass('active')) {
            product_menuitem.removeClass('active');
        } else {
            product_menuitem.addClass('active');
        }
        ;

        setTimeout(function () {
            fix_box();
        }, 100);
    });


    $('.product_color__item').click(function () {
        $(this).parent().find('.product_color__item').removeClass('active');

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        ;
    });


    $('.mobmenu_in').click(function () {
        if ($(this).hasClass('active')) {
            $('body').removeClass('mobmenu_active');
            $('.mobmenu_lvl1li').removeClass('active');
            $(this).removeClass('active');
        } else {
            $('body').addClass('mobmenu_active');
            customscroll();
            $(this).addClass('active');
        }
        ;
    });
    if (screenw > 750) {
        $('.nomob_customscroll').mCustomScrollbar({
            verticalScroll: true,
            horizontalScroll: false,
            documentTouchScroll: true,
            documentTouchScroll: false
        });
    }
    ;

    function head_scroll() {
        if (scrolltop > 10) {
            $('body').addClass('head_scroll');
        } else {
            $('body').removeClass('head_scroll');
        }
        ;
    };


    if ($('.hometop_slider').length) {
        var slide_1_duration = $('.hometop_slider .slide_1').attr('data-duration');

        var hometop_slider = $('.hometop_slider').slick({
            fade: true,
            autoplay: true,
            autoplaySpeed: slide_1_duration,
            dots: false,
            arrows: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            pauseOnFocus: false,
            responsive: [ 
              {
                breakpoint: 1100,
                settings: { 
                  touchMove: false
                }
              }
            ]
        });

        if ($('.hometop_slider .slide_1 .video').length) {
            hometop_slider_video(1);
        } else {
            change_duration(1, slide_1_duration, slide_1_duration);
        };

        // $('.hometop_slider').on('touchstart', e => {
        //     $('.hometop_slider').slick('slickPlay');
        // });

        $('.hometop_slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            var new_index = +nextSlide + 1;

            $('.hometop_count span').text('0' + new_index);

            if ($('.hometop_slider .slide_' + new_index + ' .video').length) {

                $('.hometop_slider video').remove();
                hometop_slider_video(new_index);

            } else {

                var duration = $('.hometop_slider .slide_' + new_index).attr('data-duration');
                change_duration(new_index, duration, duration);
            }
            ;

        });

        $('.hometop_nav__item').click(function () {
            var this_index = parseInt($(this).data('index')) - 1;
            $('.hometop_slider').slick('slickGoTo', this_index);
        });
    }
    ;

    function hometop_slider_video(new_index) {
        var video_el = $('.hometop_slider .slide_' + new_index + ' .video'),
            video = video_el.data('video'),
            videomob = video_el.data('videomob');

        if (screenw < 750) {
            var videosrc = videomob;
        } else {
            var videosrc = video;
        }
        ;

        video_el.append('<video autoplay="true" loop="true" preload="" muted="true"><source src="' + videosrc + '" type="video/mp4"></video>');

        setTimeout(function () {
            var duration = $('.hometop_slider .slide_' + new_index + ' video')[0].duration * 1000;
            change_duration(new_index, duration - 800, duration);
        }, 200);

    };


    function change_duration(new_index, slider_duration, line_duration) {
        $('.hometop_nav__item').removeClass('active');
        $('.hometop_nav__item div').css('animation', 'none');

        $('.hometop_nav__item[data-index="' + new_index + '"]').addClass('active');
        if (screenw < 1000) {
            $('.hometop_nav__item[data-index="' + new_index + '"] div').css('animation', 'line_fill2 ' + line_duration + 'ms linear');
        } else {
            $('.hometop_nav__item[data-index="' + new_index + '"] div').css('animation', 'line_fill ' + line_duration + 'ms linear');
        }
        ;

        $('.hometop_slider').slick('slickSetOption', 'autoplaySpeed', slider_duration);
    };

    if ($('.homecats_slider').length) {
        $('.homecats_slider').slick({
            dots: false,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: $('.homecats_container .custom_prev'),
            nextArrow: $('.homecats_container .custom_next'),
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
    ;

    try {
        if ($('.product.v2').length) {
            $('.product_slider').each(function () {
                $(this).slick({
                    dots: false,
                    arrows: false,
                    infinite: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
            });

            $('.product_slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                var product = $(slick.$slides.get(currentSlide)).closest('.product'),
                    new_index = +nextSlide + 1;
                product.find('.product_nav__item').removeClass('active')
                product.find('.product_nav__item[data-index="' + new_index + '"]').addClass('active');
            });
        }
        ;
    } catch (err) {
        console.log(err);
    }


    $('.product_nav__item').hover(function () {
        var product = $(this).closest('.product'),
            index = $(this).data('index');
        if (product.hasClass('v2')) {
            product.find('.product_slider').slick('slickGoTo', index - 1);
        } else {
            product.find('.product_nav ul:not(hiddenpro) .product_nav__item').removeClass('active');
            $(this).addClass('active');
            product.find('.product_gal__img').removeClass('active');
            product.find('.product_gal__img' + index).addClass('active');
        }
    });


    if ($('.products_sliderwrap').length) {
        $('.products_sliderwrap').each(function () {
            var sliderwrap = $(this);
            sliderwrap.find('.products_slider').slick({
                dots: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,
                pauseOnHover: false,
                pauseOnFocus: false,
                prevArrow: sliderwrap.find('.custom_prev'),
                nextArrow: sliderwrap.find('.custom_next'),
                responsive: [
                    {
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 750,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            });
        });

        $('.products_slider').on('touchstart', e => {
            $('.products_slider').slick('slickPlay');
        });
    }
    ;


    function product_gallery__slider() {
        if ($('.product_gallery__slider').length) {

            if (screenw < 750) {

                if (!$('.product_gallery__slider.slick-initialized').length) {

                    $('.product_gallery__slider').slick({
                        dots: false,
                        arrows: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: false
                    });

                    $('.product_gallery__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                        $('.product_gallery__navitem').removeClass('active');
                        $('.product_gallery__navitem[data-index="' + nextSlide + '"]').addClass('active');
                    });

                    $('.product_gallery__navitem').click(function () {
                        var this_index = $(this).data('index');
                        $('.product_gallery__slider').slick('slickGoTo', this_index);
                    });

                }
                ;

            } else {

                if ($('.product_gallery__slider.slick-initialized').length) {
                    $('.product_gallery__slider').slick('unslick');
                }
                ;

            }
            ;
        }
        ;
    };

    product_gallery__slider();


    if ($('.clook_sliderwrap').length) {
        $('.clook_sliderwrap').each(function () {
            var sliderwrap = $(this);
            sliderwrap.find('.clook_slider').slick({
                dots: false,
                arrows: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,
                pauseOnHover: false,
                pauseOnFocus: false,
                prevArrow: sliderwrap.find('.custom_prev'),
                nextArrow: sliderwrap.find('.custom_next'),
                responsive: [
                    {
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            });
        });

        $('.clook_slider').on('touchstart', e => {
            $('.clook_slider').slick('slickPlay');
        });
    }
    ;


    if ($('.sugg_sliderwrap').length) {
        $('.sugg_sliderwrap').each(function () {
            var sliderwrap = $(this);
            sliderwrap.find('.slider').slick({
                dots: false,
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                prevArrow: sliderwrap.find('.custom_prev'),
                nextArrow: sliderwrap.find('.custom_next'),
                responsive: [
                    {
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });

        $('.sugg_slider').on('touchstart', e => {
            $('.sugg_slider').slick('slickPlay');
        });
    }
    ;


    if ($('.look_sliderwrap').length) {
        $('.look_sliderwrap').each(function () {
            var look_sliderwrap = $(this);
            look_sliderwrap.find('.look_slider').slick({
                dots: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: look_sliderwrap.find('.custom_prev'),
                nextArrow: look_sliderwrap.find('.custom_next'),
                responsive: [
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 2
                        }
                    }, {
                        breakpoint: 750,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    }
    ;


    $('body').on('click', '.product_variants li', function () {
        var product = $(this).closest('.product');
        product.find('.product_variants li').removeClass('active');
        $(this).addClass('active');
    });


    function instore_map() {
        if ($('#instore_map').length) {
            var default_zoom = $('#instore_map').data('zoom'),
                default_center = $('#instore_map').data('center'),
                mob_zoom = $('#instore_mobmap').data('zoom'),
                mob_center = $('#instore_mobmap').data('center');

            $.getScript('https://api-maps.yandex.ru/2.1/?apikey=ac21023c-43ed-4386-ad7f-a089e622d710&lang=ru_RU').done(function (script, textStatus) {
                ymaps.ready(function () {

                    var myMap = new ymaps.Map("instore_map", {
                        center: default_center,
                        zoom: default_zoom,
                        controls: ['zoomControl', 'fullscreenControl']
                    });

                    myMap.behaviors.disable('scrollZoom');


                    var myMap2 = new ymaps.Map("instore_mobmap", {
                        center: mob_center,
                        zoom: mob_zoom,
                        controls: ['zoomControl', 'fullscreenControl']
                    });

                    myMap2.behaviors.disable('scrollZoom');


                    $('.map_data li').each(function () {
                        var coos = $(this).data('coos'),
                            this_title = $(this).data('title');

                        myMap.geoObjects.add(new ymaps.Placemark(coos, {
                                balloonContentBody: this_title,
                                iconCaption: this_title
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/local/templates/main/img/marker.svg',
                                iconImageSize: [40, 65],
                                iconImageOffset: [-20, -65]
                            }
                        ));

                        myMap2.geoObjects.add(new ymaps.Placemark(coos, {
                                balloonContentBody: this_title,
                                iconCaption: this_title
                            }, {
                                iconLayout: 'default#image',
                                iconImageHref: '/local/templates/main/img/marker.svg',
                                iconImageSize: [40, 65],
                                iconImageOffset: [-20, -65]
                            }
                        ));
                    });

                });
            });
        }
        ;
    };


    var footer_body_pos = $('.footer_col:first').offset().top;

    function mob_footer_isvisible() {
        if (screenw < 750) {
            if (scrolltop > (footer_body_pos - screenh + 100)) {
                $('body').addClass('footer_visible');
            } else {
                $('body').removeClass('footer_visible');
            }
            ;
        }
        ;
    };

    $(document).ready(function () {
        screenw = $(window).width();
        screenh = $(window).height();

        mob_screenh();
        head_scroll();
        customscroll();
        bycols();
        fix_box();
        mob_footer_isvisible();
        catalog_cats();

        $('body').addClass('loaded');

    });


    $(window).on('resize', function () {
        screenw = $(window).width();
        screenh = $(window).height();
        over_w = $('.categories_over').outerWidth();

        mob_screenh();
        head_scroll();
        fix_box();

        if (last_screenw != screenw) {
            customscroll();
            picture_img();
        }
        ;

        product_gallery__slider();
        bycols();
        mob_footer_isvisible();

        last_screenw = screenw;
    });


    $(window).scroll(function () {
        scrolltop = $(window).scrollTop();

        head_scroll();
        mob_screenh();
        fix_box();
        mob_footer_isvisible();

        if (last_scrolltop > scrolltop) {
            $('body').addClass('scroll_up').removeClass('scroll_down');
        } else {
            $('body').addClass('scroll_down').removeClass('scroll_up');
        }
        ;

        last_scrolltop = scrolltop;
    });


    if (document.readyState === 'loading') {
    } else {
        pageload_var = 1;
        on_load();
    }
    ;

    $(window).on('load', function () {
        if (pageload_var == 0) {
            on_load();
        }
        ;
    });

    function on_load() {

        over_w = $('.categories_over').outerWidth();

        customscroll();
        retail_price_slider();
        instore_map();

    };

    $('body').on('click', '.form_agreement_v2', function () {
        var form = $(this).closest('form'),
            unchecked_ag_len = 0;

        if ($(this).hasClass('checked')) {

            form.find('.form_agreement_v2')

            form.find('.agreement_cover').css('display', 'block');
            $(this).removeClass('checked');
        } else {
            if (form.find('.form_agreement_v2:not(".checked")').length) {

            }
            $(this).removeClass('error').addClass('checked');
        }
        ;
    });


    $('body').on('click', '.agreement_cover', function () {
        $(this).closest('form').find(".form_agreement_v2:not(.checked)").addClass('error');

        if (!$(this).closest('form').find(".form_agreement_v2:not(.checked)").length) {
            $(this).closest('form').find('.agreement_cover').css('display', 'none');
            $(this).closest('form').find('[type=submit]').click();
        }
        // if (!$(this).closest('form').find(".form_agreement_v2").hasClass('checked')) {
        //     $(this).closest('form').find(".form_agreement_v2").addClass('error');
        // } else {
        // }
    });

    $('.news_nav__item').click(function () {
        var out = $(this).data('out');
        $('.news_nav__item').removeClass('active');
        $(this).addClass('active');
        $('.news_out').css('display', 'none');
        $('.news_out_' + out).fadeIn(200);
    });

    /**
     * CABINET
     */

    $('.account_head__goback').click(function() {
        $('body').removeClass('order_details');
        $('.order').removeClass('active');
    });

    $('body').on('click', '.order_showdetails svg', function () {
        var order = $(this).closest('.order');

        $('body').addClass('order_details');

        if (order.hasClass('active')) {
            $('.order').removeClass('active');
        } else {
            $('.order').removeClass('active');
            $(this).closest('.order').addClass('active');
        }
    });

    $('.field_editicon').click(function() {
        $(this).closest('.field_wrap').find('.field').focus();
    });



    var account_over_w = $('.account_nav__over').outerWidth();

    $('.account_nav__item').each(function() {
        if ( $(this).hasClass('last') ) {
            $(this).attr('data-rightpos',Math.round( $(this).offset().left + $(this).outerWidth() ));
        } else {
            $(this).attr('data-rightpos',Math.round( $(this).offset().left + $(this).outerWidth() + $(this).next().outerWidth() ));
        };
    });

    $('.account_nav__prev').click(function() {
        if ( !$(this).hasClass('disabled') ){
            var prev = $('.account_nav__item.active').prev();
            $('.account_nav__item').removeClass('active');
            prev.addClass('active');
            accountnav_recalc();
        };
    });

    $('.account_nav__next').click(function() {
        if ( !$(this).hasClass('disabled') ){
            var next = $('.account_nav__item.active').next();
            $('.account_nav__item').removeClass('active');
            next.addClass('active');
            accountnav_recalc();
        };
    });

    $('.account_nav__item').click(function() {
        $('.account_nav__item').removeClass('active');
        $(this).addClass('active');
        accountnav_recalc();
    });

    function accountnav_recalc() {
        if ( screenw < 750 ) {

            var active = $('.account_nav__item.active');

            $('.account_nav__prev, .account_nav__next').removeClass('disabled');

            if ( active.hasClass('first') ) {
                $('.account_nav__prev').addClass('disabled');
            };

            if ( active.hasClass('last') ) {
                $('.account_nav__next').addClass('disabled');
            };

            var this_right = active.data('rightpos'),
                diff = account_over_w - this_right;

            if ( active.hasClass('last') ) {
                diff = diff + 100;
                $('.account_nav__list').css({
                    'left':'auto',
                    'right':'0'
                });
            } else {
                if ( diff < 1 ) {
                    $('.account_nav__list').css({
                        'left': diff+'px',
                        'right':'auto'
                    });
                } else {
                    $('.account_nav__list').css({
                        'left':'0',
                        'right':'auto'
                    });
                };
            };

        } else {

            $('.account_nav__list').css('left','0');

        };
    };

    accountnav_recalc();

});
