;

function loadOrderDetail(orderContainer, orderId) {
    $.ajax({
        type: 'GET',
        url: '/cabinet/orders/detail.php?ID='+orderId,
        dataType: 'html',
        success: function (data) {
            orderContainer.find('.order_content').append(data);
        }
    });
}

function oneClick(productCode, offerId, data) {
    let requestData = {};;

    requestData.data = {
        'formData': data,
        'offerId': offerId,
        'productCode': productCode
    };
    requestData.entity = 'oneclick';
    requestData.action = 'buy';

    let res = null;

    $.ajax({
        type: 'POST',
        url: '/api/index.php',
        data: requestData,
        success: function (response){
            res = response;
        },
        async:false
    });

    return res;
}
function   buyInstore(productCode, offerId, storeAddress, data)
{
    let requestData = {};

    requestData.data   = {
        'formData': data,
        'offerId': offerId,
		'storeAddress': storeAddress,
        'productCode': productCode
    };
    requestData.entity = 'instore';
    requestData.action = 'buy';

    $.post('/api/index.php', requestData, function(response){
        console.log(response + ' (instore)');
		window.location.href = '/cart/?ORDER_ID='+response;
    });
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function closeHeaderAction()
{
    $.post('/api/index.php', {
        'entity': 'projectSettings',
        'action': 'closeHeaderAction',
    });
}

function aceptCookies()
{
    $.post('/api/index.php', {
        'entity': 'user',
        'action': 'aceptCookies',
    });
}

function smsChangePasswd(code, passwd, passwd2)
{
    let requestData = {};

    requestData.data   = {
        'code': code,
        'passwd': passwd,
        'passwd2': passwd2,
    };

    requestData.entity = 'user';
    requestData.action = 'changepasswd';

    let res;

    $.ajax({
        type: 'POST',
        url: '/api/index.php',
        data: requestData,
        success: function (response){
            res = response;
        },
        async:false
    });

    return res;
}

function searchCityHeader(value)
{
    let requestData = {};

    requestData.data   = {'value': value};
    requestData.entity = 'regionality';
    requestData.action = 'searchCity';

    $.post('/api/index.php', requestData, function(response){
            let res = '';

            if (response.cities.length > 0) {
                $('.cities_list').css('display','none');
                $('.cities_found').fadeIn(200);
                $('.cities_found').html(null);
                for (const city of response.cities) {
                    res += "<li data-location-id='"+city.ID+"'><strong>" + city.NAME_RU + "</strong>";
                    if (city.PARENT_ID && response.parents[city.PARENT_ID]) {
                        res += "<p>" + response.parents[city.PARENT_ID].NAME_RU + "</p>";
                    }
                    res += "<li>";
                }
            } else {
                $('.cities_found').css('display','none');
                $('.cities_list').fadeIn(200);
            }

            $('.cities_found').html(res);

            console.log(res);
    });
}


function requestOnRemove()
{
    let requestData = {};

    requestData.entity = 'user';
    requestData.action = 'requestOnRemove';

    $.post('/api/index.php', requestData, function(response){
        location.reload();
    });
}


function selectCity(locationId)
{
    let requestData = {};

    requestData.data   = {'locationId': locationId};
    requestData.entity = 'regionality';
    requestData.action = 'selectCity';

    $.post('/api/index.php', requestData, function(response){
        location.reload();
    });
}

function addToFavorites(productId)
{
    let requestData = {};

    requestData.data   = {'productId': productId};
    requestData.entity = 'favorites';
    requestData.action = 'add';

    $.post('/api/index.php', requestData, function(response){
        if (response.success) {
            $('.header_links__fav').addClass('active');
        }
    });
}

function clearFavoritesList()
{
    let requestData = {};

    requestData.entity = 'favorites';
    requestData.action = 'deleteAll';

    $.post('/api/index.php', requestData, function(response){
        location.reload();
    });
}
function deleteFromFavorites(productId)
{
    let requestData = {};

    requestData.data   = {'productId': productId};
    requestData.entity = 'favorites';
    requestData.action = 'delete';

    $.post('/api/index.php', requestData, function(response){
        if (response.count === 0) {
            $('.header_links__fav').removeClass('active');
        }
    });
}

function forgotpasswd(phone)
{
    let requestData = {};

    requestData.data   = {'phone': phone};
    requestData.entity = 'sms';
    requestData.action = 'forgotpasswd';

    $.post('/api/index.php', requestData, function(response){
        console.log(response);
    });
}

function forgotpasswdAgain()
{
    let requestData = {};

    requestData.entity = 'sms';
    requestData.action = 'forgotpasswdAgain';

    $.post('/api/index.php', requestData, function(response){
        console.log(response);
    });
}

function basketAddProduct(data)
{
    let requestData = {};

    requestData.data   = data;
    requestData.entity = 'basket';
    requestData.action = 'addProduct';

    let res;

    $.ajax({
        type: 'POST',
        url: '/api/index.php',
        data: requestData,
        success: function (response){
            res = response;

            let dashmailData = {
                "productId": requestData.data['id'],
                "quantity": requestData.data['quantity'],
                "price": res.price,
            };

            if (res.price) {
                let dashmailData = {
                    "productId": requestData.data['id'],
                    "quantity": requestData.data['quantity'],
                    "price": res.price,
                };

                let dashmailRes = dashamail("cart.addProduct", dashmailData);
            }

            if (res.gtmAddToCartEvent && res.gtmAddToCartEvent.ecommerce) {
                window.dataLayer.push({
                    'event': 'add_to_cart',
                    'ecommerce': res.gtmAddToCartEvent.ecommerce
                });
            }


        },
        async:false
    });

    if (res && res.success) {
        $('#alert_tocart').fadeIn(200);
        $('.header_links__cart, .header_links__cart1').addClass('active');

        setTimeout(function() {
            $('#alert_tocart').fadeOut(200);
        }, 2000);

        ym(50233879,'reachGoal','clickBtnAddCart');
    }

    return res;
}

function searchUser(data)
{
    let requestData = {};

    requestData.data   = {'value': data};
    requestData.entity = 'user';
    requestData.action = 'search';

    let res;

    $.ajax({
        type: 'POST',
        url: '/api/index.php',
        data: requestData,
        success: function (response){
            res = response;
        },
        async:false
    });

    return res;
}

function formStep1Validation()
{
    let loginVal = $('.restore_step1 form [name=USER_LOGIN]').val();
    let res = searchUser(loginVal+'');

    if (res.error) {
        $('.restore_step1 .error').html(res.error);

        return false;
    } else if (res.success) {
        $('.restore_step1 .error').html('');

        return  true;
    } else {    
        $('.restore_step1 .error').html('Ошибка.');

        return false;
    }
}

function catalogSearchAjax(){
    console.log('test log 2');
    if ($('.search_block .search_products div').find('.product').length) {
        setTimeout(function (){
            $('.search_block .search_products div').find('.product').each(function () {
                if ($(this).find('.product_gal__img').length) {
                    if (!$(this).find('.product_slider').hasClass('slick-initialized')) {
                        $(this).find('.product_slider').slick({dots: false, arrows: false, infinite: false, slidesToShow: 1, slidesToScroll: 1});
                    }
                }
            });
        }, 200);
    }
}


function submitCatalogSearchForm(e, searchPage=true) {
    e.preventDefault();

    if (searchPage) {
        let q = $('.search_block .search_products .search-page [name="q"]').val();

        if (q.length > 3) {
            location.href = '/search/?q='+q+'&how=r';
        }
    } else {
        $('.search_block .search_products .search-page [type=submit]').click();
    }
}

function gotToSearchPage(e) {
    let q = $('.search_block .search_products .search-page [name="q"]').val();

    if (q.length > 3) {
        location.href = '/search/?q='+q+'&how=r';
    }
}


var thread = null;
var delayMs = 500;

var searchFilterRe = new RegExp('\\/catalog\\/.+\\/filter\\/(.+\\/?)\\/apply');
var saveFilters = ['sale-is', 'new-is'];

$(function () {
    $('body').on('click', '.orders_container .orders_more', function(e){
        e.preventDefault();

        var targetContainer = $('.orders_content'),          //  Контейнер, в котором хранятся элементы
            url =  $('.orders_more').attr('data-url');    //  URL, из которого будем брать элементы

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    //  Удаляем старую навигацию
                    $('.orders_more').remove();

                    var elements = $(data).find('.orders_content .order'),  //  Ищем элементы
                        pagination = $(data).find('.orders_more');//  Ищем навигацию

                    // let mobcats_back = $(targetContainer).find('.mobcats_back');
                    // $(targetContainer).find('.mobcats_back').remove();
                    //
                    targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                    $('.orders_container .orders_morewrap').append(pagination); //  добавляем навигацию следом
                    // targetContainer.append(mobcats_back);
                }
            })
        }
    });

    $('body').on('click', '.order .order_showdetails', function(e) {
        e.preventDefault();
        let orderContainer = $(this).parent('.order'),
            orderId        = orderContainer.data('order-id');

        loadOrderDetail(orderContainer, orderId);
    });

    $('body').on('click', '.profile_block .profile_delete', function(e){
        e.preventDefault();

        requestOnRemove();
    });

    $('body').on('click', '.categories a, .mobcats_body a', function(e){
        e.preventDefault();
        let href        = $(this).attr('href');
            uri         = location.href,
            uriMatchRes = uri.match(searchFilterRe),
            addFilters  = '';

        if (uriMatchRes && uriMatchRes.length && uriMatchRes[1]) {
            let filters = uriMatchRes[1].split('/');

            for (key in filters) {
                for (key2 in saveFilters) {
                    if (filters[key].search(saveFilters[key2]) === 0) {
                        addFilters += '/'+filters[key];
                    }
                }
            }
        }

        if (addFilters.length) {
            href += 'filter' + addFilters + '/apply/';
        }

        location.href = href;
    });

    // Here write custom code.
    $('#acept-cookie-button').on('click', function(e){
        e.preventDefault();
        aceptCookies();
        $('.alert_cookie').remove();
    });

    $('.product_detail_page [data-modalname="oneclick"]').on("click", function(e){
        e.preventDefault;

        try {
            let productData = JSON.parse($('.product_block').attr('data-product'));

            if (!productData.productCode) {
                throw new Error('Product code');
            }

            let res;

            $.ajax({
                type: 'GET',
                url: '/catalog/product/'+productData.productCode+'/?ajax=Y&oneclick=Y&offerId='+productData.activeOfferId,
                success: function (response){
                    res = response;
                },
                async:false
            });

            let x = $('.universal_modal__oneclick .product_mini');

            if (x.length) {
                x.remove();
            }

            $(res).insertBefore($('.universal_modal__oneclick .form_v1'));
        } catch (Error) {
            console.log(Error);
        }
    });


    $('body').on('click', '.action_container .newsdetail_more', function(e){
        e.preventDefault();

        var targetContainer = $('.catalog_items'),          //  Контейнер, в котором хранятся элементы
            url =  $('.newsdetail_more').attr('data-url');    //  URL, из которого будем брать элементы

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    //  Удаляем старую навигацию
                    $('.newsdetail_more').remove();

                    var elements = $(data).find('.catalog_items .product'),  //  Ищем элементы
                        pagination = $(data).find('.newsdetail_more');//  Ищем навигацию

                    let mobcats_back = $(targetContainer).find('.mobcats_back');
                    $(targetContainer).find('.mobcats_back').remove();

                    targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                    targetContainer.after(pagination); //  добавляем навигацию следом
                    targetContainer.append(mobcats_back);

                    $(targetContainer).find('.product').each(function () {
                        if ($(this).find('.product_gal__img').length) {
                            if (!$(this).find('.product_slider').hasClass('slick-initialized')) {
                                $(this).find('.product_slider').slick({dots: false, arrows: false, infinite: false, slidesToShow: 1, slidesToScroll: 1});
                            }
                        }
                    });
                }
            })
        }
    });

    $('body').on('click', '.news_container .news_more', function(e){
        e.preventDefault();

        let news_out_num = $('.news_container .news_out:visible').hasClass('news_out_1') ? 1 : 2;
        let url             =  $('.news_out_'+news_out_num+' .pagenav_button_more').attr('data-url');    //  URL, из которого будем брать элементы
        let targetContainer = $('.news_out_'+news_out_num+' .news_items:visible');          //  Контейнер, в котором хранятся элементы


        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    //  Удаляем старую навигацию
                    $('.news_out_'+news_out_num+' .pagenav_button_more').remove();

                    var elements = $(data).find('.news_out_'+news_out_num+' .news_items .news_item'),  //  Ищем элементы
                        pagination = $(data).find('.news_out_'+news_out_num+' .pagenav_button_more');//  Ищем навигацию

                    targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                    targetContainer.parent().find('.button_wrap').append(pagination); //  добавляем навигацию следом

                    /*$(targetContainer).find('.news_item').each(function () {
                        // if ($(this).find('.product_gal__img').length) {
                        //     if (!$(this).find('.product_slider').hasClass('slick-initialized')) {
                        //         $(this).find('.product_slider').slick({dots: false, arrows: false, infinite: false, slidesToShow: 1, slidesToScroll: 1});
                        //     }
                        // }
                    });*/
                }
            })
        }
    });

    $('body').on('click', '.favorites_container .favorites_more', function(e){


        e.preventDefault();

        var targetContainer = $('.catalog_items'),          //  Контейнер, в котором хранятся элементы
            url =  $('.favorites_more').attr('data-url');    //  URL, из которого будем брать элементы

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
                success: function(data){

                    //  Удаляем старую навигацию
                    $('.favorites_more').remove();

                    var elements = $(data).find('.catalog_items .product'),  //  Ищем элементы
                        pagination = $(data).find('.favorites_more');//  Ищем навигацию

                    targetContainer.append(elements);   //  Добавляем посты в конец контейнера
                    targetContainer.after(pagination); //  добавляем навигацию следом

                    $(targetContainer).find('.product').each(function () {
                        if ($(this).find('.product_gal__img').length) {
                            if (!$(this).find('.product_slider').hasClass('slick-initialized')) {
                                $(this).find('.product_slider').slick({dots: false, arrows: false, infinite: false, slidesToShow: 1, slidesToScroll: 1});
                            }
                        }
                    });
                }
            })
        }
    });

    $('body').on('click', '.favorites_container .favorites_reset', function(e){
        e.preventDefault();

        clearFavoritesList();
    });

    $(document).on('click', '[data-product-id] .product_mini__fav, .product_fav, .product_tofav', function(e){
        e.preventDefault();

        let productId = $(this).closest('[data-product-id]').attr('data-product-id');

        if (!productId) {
            return;
        }

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            deleteFromFavorites(productId);
        } else {
            $(this).addClass('active');
            addToFavorites(productId);

            if ($(this).parents('.favorites_recently').length) {
                location.reload();
            }
        }
    });

    // $('.search_block .search_products div[id]').on('DOMSubtreeModified', catalogSearchAjax);

    $('body.search_results .search_bar__close').on('click', function(e){
        e.preventDefault();

        $('.search_form__input').val('');
    });

    $(".header_search .search_form__loupe1").on('click', gotToSearchPage);
    $('.header_search .search_form').on('submit', submitCatalogSearchForm);

    $(".header_search .search_form__input").on('input propertychange', function(e){
        e.preventDefault;
        if (e.keyCode != 8) {
            clearTimeout(thread);
            var target = $(this);
            thread = setTimeout(function() {
                let text = $(target).val();

                if (text.length > 3) {
                    $('.search_block .search_products .search-page [name="q"]').val(text);
                    submitCatalogSearchForm(e, false);
                }
            }, delayMs);
        }
    });

    $('body').on('click', '.restorepasswdagain', function(e){
        $('.restore_nocode').css('display','block');
        $('.restore_nocode2').css('display','none');
        $('.restore_nocode strong').text(60);

        forgotpasswdAgain();

        setInterval(function() {
            var current_sec = parseInt($('.restore_nocode strong').text());
            new_sec = current_sec - 1;
            if ( new_sec < 0 ) {
                $('.restore_nocode').css('display','none');
                $('.restore_nocode2').css('display','block');
            } else {
                $('.restore_nocode strong').text(new_sec);
            };
        }, 1000);
    });

    $('body').on('submit', '.restore_step1 form', function(e){
        e.preventDefault();

        if (!formStep1Validation()) {
            return false;
        }

        if ($(this).hasClass('phone')) {

            $('.restore_step').css('display', 'none');
            $('.restore_step2').fadeIn(200);
            setInterval(function () {
                var current_sec = parseInt($('.restore_nocode strong').text());
                new_sec = current_sec - 1;
                if (new_sec < 0) {
                    $('.restore_nocode').css('display', 'none');
                    $('.restore_nocode2').css('display', 'block');
                } else {
                    $('.restore_nocode strong').text(new_sec);
                }
                ;
            }, 1000);

            let phoneVal = $('.form_v1.phone [name=USER_LOGIN]').val();

            if (phoneVal) {
                $('.restore_step3 form').addClass('phone');
                forgotpasswd(phoneVal);
            }

            return false;
        } else {
            var form = $(this);
            var actionUrl = form.attr('action') + '&ajax=yes';

            $.ajax({
                type: "POST",
                url: actionUrl,
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    if (data === 'ok') {
                        location.reload();
                    } else {
                        let form = $(data).find('.restore_step.restore_step1').html();
                        if (form.length) {
                            $('.restore_step.restore_step1').html('Мы отрпавили вам на электронную почту ссылку для сброса пароля.');
                            // $('.restore_step.restore_step1').html(form);
                        } else {
                            alert('Ошибка');
                        }
                    }

                }
            });
        }
    });

    $('body').on('submit', '.restore_step2 .form_v1', function(e){
        e.preventDefault();

        var form = $(this);
        var actionUrl = form.attr('action') + '&ajax=yes';

        console.log(form);

        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                if (data === 'ok') {
                    location.reload();
                } else {
                    let form = $(data).find('.restore_step.restore_step2').html();

                    if (form.length) {
                        $('.restore_step.restore_step2').html(form);
                    } else {
                        alert('Ошибка');
                    }
                }

            }
        });

    });

    $('body').on('submit', '.form_v1.login_form[name!=regform]', function(e){
        e.preventDefault();

        var form = $(this);
        var actionUrl = form.attr('action') + '&ajax=yes';

        $.ajax({
            type: "POST",
            url: actionUrl,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {

                if (data === 'ok') {


                    let email = $(form).find('[name=USER_LOGIN]').val();

                    if (email) {
                        dashamail("identify", {
                            "operation": "Authorization",
                            "identificator": {
                                "provider": "email",
                                "identity": email,
                            }
                        });

                        window.dataLayer.push({
                            'event': 'login',
                            'method': 'email',
                        });
                    }

                    location.reload();
                } else {
                    let form = $(data).find('.auth_content.auth_content_1').html();

                    if ((form !== undefined) && form.length) {
                        $('.auth_content.auth_content_1').html(form);
                    } else {
                        location.reload();
                    }
                }

            }
        });
    });

    $('.form_v1 [name=USER_LOGIN]').on('focusout', function(){
        let emailVal = $(this).val();

        console.log(emailVal);

        if (!isEmail(emailVal)) {
            $(this).parents('.form_v1').addClass('phone');
        } else {
            $(this).parents('.form_v1').removeClass('phone');
        }
    });

    $('.product').on('click', '.bitrix_basket_add_product', function (e) {
        e.preventDefault();

        let productContainer = $(this).parents('[data-product]')
        if (!productContainer.attr('data-product')) {
            return;
        }

        basketAddProduct(JSON.parse(productContainer.attr('data-product')));
    });

    $(document).on('click', '.product_mini__actions .product_mini__tocart', function(e) {
        e.preventDefault();

        let productContainer = $(this).parents('[data-product]')
        if (!productContainer.attr('data-product')) {
            return;
        }

        basketAddProduct(JSON.parse(productContainer.attr('data-product')));
    });

    $('body').on('click', '.product_size', function (e) {
        e.preventDefault();

        if ($(this).closest('.product_detail_page')) {
            return;
        }

        if (!$(this).hasClass('disabled')) {
            let productContainer = $(this).parents('[data-product]')
            if (!productContainer.attr('data-product')) {
                return;
            }

            let productData = JSON.parse(productContainer.attr('data-product'));

            let offerId = $(this).attr('data-offer-id');

            if (offerId) {
                productData.id = offerId;
            }

            var sizes = $(this).closest('.sizes');

            sizes.addClass('loading');

            let res = basketAddProduct(productData);

            setTimeout(function () {
                sizes.removeClass('loading');
            }, 500);
        }
    });

    const arrayColumn = (array, column) => {
        return array.map(item => item[column]);
    };

    $('body').on('click', '.product .product_variants li', function (e) {
        e.preventDefault();

        if (!$(this).hasClass('disabled')) {
            let productContainer = $(this).parents('[data-sizes]')
            if (!productContainer.attr('data-sizes')) {
                return;
            }

            let productSizes   = JSON.parse(productContainer.attr('data-sizes')),
                selectedColor  = $(this).attr('data-color'),
                sizes          = [];

            try {
                let allSizes     = JSON.parse(productContainer.attr('data-all-sizes'));

                for (size in allSizes) {
                    sizes.push(allSizes[size]);
                }
            } catch (e) {
                for (color in productSizes) {
                    for (size in productSizes[color]) {
                        if (sizes.indexOf(size) === -1) {
                            sizes.push(size);
                        }
                    }
                }
            }

            let htmlSizes = '',
                offerId = null;

            for (key in sizes) {
                if (productSizes[selectedColor][sizes[key]]) {
                    offerId = productSizes[selectedColor][sizes[key]];
                    htmlSizes += '<a class="product_size" data-offer-id="'+offerId+'">'+sizes[key]+'</a>';
                } else {
                    htmlSizes += '<a class="product_size disabled">'+sizes[key]+'</a>';
                }
            }

            productContainer.find('.product_sizes').html(htmlSizes);

            // Теперь изменяем картикни
            // $(productContainer).find('.product_gal__img[data-offer-id]').each(function () {
            //     if ($(this).attr('data-offer-id') === offerId) {
            //         $(this).removeCl
            //     }
            // })
            $(productContainer).find('.product_gal[data-offer-id='+offerId+'], .product_slider[data-offer-id='+offerId+'], .product_nav ul[data-offer-id='+offerId+']').removeClass('hiddenpro');
            $(productContainer).find('.product_gal[data-offer-id!='+offerId+'], .product_slider[data-offer-id!='+offerId+'], .product_nav ul[data-offer-id!='+offerId+']').addClass('hiddenpro');

            try {
                $(productContainer).find('.product_gal[data-offer-id='+offerId+'], .product_slider[data-offer-id='+offerId+']').slick('refresh');
            } catch (e) {
            }

            let productData = $(productContainer).attr('data-product');
            productData = JSON.parse(productData);
            productData.selectedColorOfferId = offerId;
            $(productContainer).attr('data-product', JSON.stringify(productData));

            if (productData) {
                let detailPageUrl = $(productContainer).attr('data-detail-page-url') + '?offerId='+productData.selectedColorOfferId;
                $(productContainer).find('.detail-page-url').attr('href', detailPageUrl);
            }

            let prices = $(productContainer).data('prices');
            let tags   = $(productContainer).data('tags');

            let priceKey = parseInt(offerId);

            $(productContainer).find('.product_price').html(prices[priceKey]['BASE']);
            $(productContainer).find('.product_priceold').html(prices[priceKey]['OLD'] || '');

            let priceDiscounted = prices[priceKey]['DISCOUNTED'] || '';
            
            if (priceDiscounted) {
                if (!$(productContainer).find('.product_price_discounted').length) {
                    $(productContainer).find('.product_prices').append('<span class="product_price_discounted"></span>');
                }

                $(productContainer).find('.product_price_discounted').html(priceDiscounted);
            } else {
                if ($(productContainer).find('.product_price_discounted').length) {
                    $(productContainer).find('.product_price_discounted').remove();
                }
            }

            let saleTag = null;
            if (tags && tags[offerId] && tags[offerId]['SALE'] && (saleTag = tags[offerId]['SALE'])) {
                if (!$(productContainer).find('.product_tags').find('.sale_name').length) {
                    $(productContainer).find('.product_tags').append('<li class="sale_name">'+saleTag+'</li>');
                } else {
                    $(productContainer).find('.product_tags').find('.sale_name').html(saleTag);
                }
            } else {
                $(productContainer).find('.product_tags').find('.sale_name').remove();
            }

            if (prices[priceKey]['DIFF_PERCENT']) {
                if (prices[priceKey] && prices[priceKey]['DISCOUNTED']) {
                    if ($(productContainer).find('.product_tags').find('.price_diff_percent').length) {
                        $(productContainer).find('.product_tags').find('.price_diff_percent').html(' -' + prices[priceKey]['DIFF_PERCENT'] + '%');
                    } else {
                        $(productContainer).find('.product_tags').append('<li class="price_diff_percent">-' + prices[priceKey]['DIFF_PERCENT'] + '%</li>');
                    }
                } else {
                    if ($(productContainer).find('.product_tags').find('.price_diff_percent').length) {
                        $(productContainer).find('.product_tags').find('.price_diff_percent').html('-' + prices[priceKey]['DIFF_PERCENT'] + '%');
                    } else {
                        $(productContainer).find('.product_tags').append('<li class="price_diff_percent">-' + prices[priceKey]['DIFF_PERCENT'] + '%</li>');
                    }
                }
            } else if ($(productContainer).find('.product_tags').find('.price_diff_percent').length) {
                $(productContainer).find('.product_tags').find('.price_diff_percent').remove();
            }

            // if (offerId) {
            //     productData.id = offerId;
            // }

            // basketAddProduct(productData);

            let dataSetData = $(this).closest('[data-set-data]').attr('data-set-data'),
                setData     = undefined;

            if (dataSetData) {
                dataSetData = JSON.parse(dataSetData);
                setData = dataSetData[selectedColor];

                if (setData) {
                    const ids = arrayColumn(setData.productIds, 'ID');
                    setData = JSON.stringify({'offerId': setData.offerId, 'setId': setData.setId, 'productIds': ids});

                    $(this).closest('[data-set-data]').attr('data-set', setData);
                } else {
                    $(this).closest('[data-set-data]').attr('data-set', JSON.stringify([]));
                }
            }

            if (setData) {
                if ($(productContainer).find('.complete_look').hasClass('hiddenpro')) {
                    $(productContainer).find('.complete_look').removeClass('hiddenpro');
                } else if (!$(productContainer).find('.complete_look').length) {

                    // console.log('==============');
                    // console.log(productContainer);
                    // console.log($(productContainer).parents('.look_content'));
                    // console.log(!$(productContainer).parents('.look_content'));
                    // console.log(!$(productContainer).parents('.look_content').length);

                    if(!$(productContainer).parents('.look_content').length) {
                        $(productContainer).append('<button style="width: 100%; margin: 30px 0 0;" type="button" class="button button_tr complete_look universal_modal__in" data-modalname="look_1" data-lookid="1">Смотреть Весь образ</button>\n');
                    }
                }
            } else {
                $(productContainer).find('.complete_look').addClass('hiddenpro');
            }
        }
    });

    // $('.product .product_title').on('click', function (e) {
    //     e.preventDefault();
    //
    //     let productContainer = $(this).parents('[data-product]'),
    //         productData = JSON.parse(productContainer.attr('data-product'));
    //     if (productData) {
    //         location.href = $(this).find('a').attr('href') + '?offerId='+productData.selectedColorOfferId;
    //     }
    // });

    $('.cart_container .button.auth_form').on('click', function(e){
        e.preventDefault();
        $('[data-modalname=auth]').trigger('click');
    });

    $('.header_links__profile .auth_form, .mob_v_open_auth_form').on('click', function(e){
        e.preventDefault();
        $('[data-modalname=auth]').trigger('click');
    });

    $('body').on('click', '.form_v1.login_form[name=regform] [type=submit]', function(e) {
        $('.form_v1.login_form[name=regform]').submit();
    });

    $('body').on('submit', '.form_v1.login_form[name=regform]', function(e) {
        e.preventDefault();

        let emailValue = $('.form_v1.login_form[name=regform] .field_email').val();
        $('.form_v1.login_form[name=regform] .orig_field_email').val(emailValue);

        var form = $(this);
        var actionUrl = form.attr('action');

        var fd = new FormData();

        $('.form_v1.login_form[name=regform] input').each(function(){
            let inputName = $(this).attr('name');
            if (inputName) {
                fd.append(inputName, $(this).val());
            }

            fd.append('ajax', 'yes');
        });

        $.ajax({
            type: "POST",
            url: actionUrl,
            data: fd,
            processData: false,
            contentType: false,
            success: function(data)
            {
                console.log(data);

                if (data === 'ok') {
                    location.reload();
                } else {
                    let form = $(data).find('.auth_content.auth_content_2').html();

                    if (form.length) {
                        $('.auth_content.auth_content_2').html(form);
                    } else {
                        alert('Ошибка');
                    }

                    $(".field_phone").inputmask('+7 (999) 999-9999');
                }

            }
        });
    });

});

;

$(function(){

    $('body').on('click', '[data-modalname="sizetable"]', function(e) {
        /*
         * Получаем обмены текущего изделия
         */
        let productId = null;

        productId = $(this).parents('[data-product-id]').data('product-id');

        if (!productId) {
            return;
        }

        $.ajax({
            type: 'GET',
            url: '/local/ajax/getProductSizeTable.php?productId='+productId,
            dataType: 'html',
            success: function (response) {
                if ($('.universal_modal__sizetable').find('.product-size-table'))
                {
                    $('.universal_modal__sizetable').find('.product-size-table').remove();
                }

                $('.universal_modal__sizetable .sizetable_item.first').after(response);
            },
        });
    });

    $('body').on('click', '.form_v1.form_oneclick .button.button_black', function(e){
        e.preventDefault();

        if (!$('.product_mini__size.active').length) {
            console.log('Не выбран оффер (oneclick)');
            return;
        }

        let offerId = $('.product_mini__size.active').data('offer-id');
        let productCode = $('.product_mini').data('product')['productCode'];

        if (!offerId) {
            console.log('Fatal (oneclick)');
            return;
        }

        let data = $(this).closest('.form_oneclick').serializeArray();

        let res = oneClick(productCode, offerId, data);

        if (res && res.success) {
            $('.form_v1.form_oneclick .resMessage').remove();
            $('.form_v1.form_oneclick').prepend('<p class="resMessage">Заказ в 1 клик оформлен<br><br></p>');
            setTimeout(close_all_popups, 2000);
        } else if (res && res.message) {
            $('.form_v1.form_oneclick .resMessage').remove();
            $('.form_v1.form_oneclick').prepend('<p class="resMessage">'+res.message+'<br><br></p>');
        } else {
            close_all_popups();
        }
    });

    $('body').on('click', '.form_v1.form_instock .button.button_black', function(e){
        e.preventDefault();

        if (!$('.instore_offer.product_mini__size.active').length) {
            console.log('Не выбран оффер (form_instock)');
            return;
        }

        let offerId = $('.instore_offer.product_mini__size.active').data('offer-id');
        let productCode = $('.instore_product.product_mini').data('product')['productCode'];
		  let storeAddress = $('.instore_storeinfo .store_address').html();

        if (!offerId) {
            console.log('Fatal (form_instock)');
            return;
        }
		instore_form =  $(this).closest('.form_instock');
		$(instore_form).find('input[name="instock_name"]').removeClass('error');
		$(instore_form).find('input[name="instock_phone"]').removeClass('error');
		if($(instore_form).find('input[name="instock_name"]').val()=='')
		{$(instore_form).find('input[name="instock_name"]').addClass('error');}
		else if($(instore_form).find('input[name="instock_phone"]').val()=='')
		{$(instore_form).find('input[name="instock_phone"]').addClass('error');}
	else if($(instore_form).find('input[name="instock_email"]').val()=='')
		{$(instore_form).find('input[name="instock_email"]').addClass('error');}
		 else
		 {
 

        let data = $(this).closest('.form_instock').serializeArray();

   buyInstore(productCode, offerId, storeAddress, data);

        close_all_popups();
		 }
    });



    $('body').on('click', '.product_mini__tablein, .product_sizes_tablein, .product_measure', function () {
        close_all_popups();
        $('.look').css('right', '-100vw');
        $('body').addClass('sizetable_out');
    });

    $('body').on('click', '.product_mini .product_mini__sizes .product_mini__size:not(.disabled)', function (e) {
        let productContainer = $(this).parents('[data-product]');

        let productData = $(productContainer).attr('data-product');
        productData = JSON.parse(productData);
        productData.id = $(this).attr('data-offer-id');
        $(productContainer).attr('data-product', JSON.stringify(productData));
    });

    $('body').on('click', '.product_mini__colorlist li', function (e) {
        let productContainer = $(this).parents('[data-product]');

        let productData = $(productContainer).attr('data-product');
        productData = JSON.parse(productData);
        productData.id = $(this).attr('data-offer-id');
        $(productContainer).attr('data-product', JSON.stringify(productData));


        var product_mini = $(this).closest('.product_mini'),
            color = $(this).find('div').data('color'),
            allSizes = $(product_mini).attr('data-product-all-sizes'),
            sizes = $(this).find('div').data('color-offer'),
            prices = $(this).find('div').data('prices'),
            this_text = $(this).find('span').text();

        product_mini.find('.product_mini__colorlist li').removeClass('active');
        $(this).addClass('active');

        product_mini.find('.product_mini__colorchosen span').css('background', '#' + color);
        product_mini.find('.product_mini__colorchosen i').text(this_text);

        $(this).closest('.product_mini__color').removeClass('active');

        let sizesHtml = '';

        allSizes = JSON.parse(allSizes);

        console.log('LLALALAALLA');
        console.log(productData.id);

        let offerId = null;
        for (let size in allSizes) {
            let classes = '';

            size = allSizes[size];

            if (sizes[size]) {

                if (!offerId) {
                    offerId = sizes[size];
                }

                if (sizes[size] == productData.id) {
                    classes += ' active'; // ????????
                }
            } else {
                classes += ' disabled';
            }

            sizesHtml += '<button class="product_mini__size '+classes+'" data-offer-id="'+sizes[size]+'">'+size+'</button>';
        }

        $(product_mini).find('.product_mini__price').html(prices.price);

        let printOldPrice = prices.oldprice;

        if (!prices.oldpriceNumber) {
            printOldPrice = '';
        }

        $(product_mini).find('.product_mini__priceold').html(printOldPrice);

        $(product_mini).find('.product_mini__info[data-info-offer='+offerId+']').removeClass('hiddenpro');
        $(product_mini).find('.product_mini__info[data-info-offer!='+offerId+']').addClass('hiddenpro');

        $(product_mini).find('.back_img[data-color="'+this_text+'"]').removeClass('hiddenpro');
        $(product_mini).find('.back_img[data-color!="'+this_text+'"]').addClass('hiddenpro');

        let linkDetailPageUri = $(product_mini).find('.product_mini__title a').attr('data-href') + '?offerId=' + offerId;
        $(product_mini).find('.product_mini__title a').attr('href', linkDetailPageUri);

        linkDetailPageUri = $(product_mini).find('.product_mini__imgbox a').attr('data-href') + '?offerId=' + offerId;
        $(product_mini).find('.product_mini__imgbox a').attr('href', linkDetailPageUri);

        $(this).closest('.product_mini').find('.product_mini__sizes').html(sizesHtml);

        let offerIds = [];
        $('.look.modal .look_content [data-product]').each(function(){
            let productData = $(this).attr('data-product');
            productData = JSON.parse(productData);
            offerIds.push(parseInt(productData.id));
        });


        $.ajax({
            type: 'POST',
            url: '/local/ajax/calcTotalSum.php',
            dataType: 'json',
            data: {'offerIds': offerIds, 'formated': true},
            success: function (data) {
                console.log(data);
                $('.look_prices .look_price').html(data.printPrice);
                $('.look_prices .look_priceold').html(data.printOldprice || '');
            },
        });
    });

    $('body').on('click', '.product_mini__colorchosen', function () {
        $(this).closest('.product_mini__color').addClass('active');
    });

    $(document).click(function (e) {
        that = e.target;
        if ($(that).closest(".product_mini__color").length < 1 || $(that).hasClass('product_mini__color')) {
            $('.product_mini__color').removeClass('active');
        }
    });

    $('body').on('click', '.product_mini__size:not(.disabled)', function () {
        $(this).closest('.product_mini').find('.product_mini__size').removeClass('active');
        $(this).addClass('active');
    });

});

$(function (){
    $(".field_phone").inputmask('+7 (999) 999-9999');

    $('.future_link').each(function (number, item){
        let futureLink = $(item),
            href       = futureLink.data('href'),
            text       = futureLink.text();

        $(futureLink).replaceWith(function(){
            return $("<a href='"+href+"'>"+text+"</a>", {html: $(this).html()});
        });
    });
});

