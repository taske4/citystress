<?php

$_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../..';

require($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');

$feed = new Citystress\Export(
    $_SERVER['DOCUMENT_ROOT'] . '/bitrix/catalog_export/webmaster.xml',
    ['ACTIVE' => 'Y'],
    ['ACTIVE' => 'Y'],
    ['ACTIVE' => 'Y']
);

$feed->export();
