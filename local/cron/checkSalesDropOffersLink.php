<?php

//require(__DIR__."/../bitrix/modules/main/include/prolog_before.php");

require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');

$sales = \Bitrix\Iblock\Iblock::wakeUp(12)->getEntityDataClass()::query()
    ->setSelect(['ID', 'DATE_TO_' => 'DATE_TO'])
    ->fetchAll();

echo '<pre>';

$nowDateTime = new \DateTime();

$oldSales = [];
$logicFilter2 =  $logicFilter = [
    'LOGIC' => 'OR',
];

foreach ($sales as $sale) {
    $saleDateTo = \DateTime::createFromFormat('Y-m-d H:i:s', $sale['DATE_TO_VALUE']);

   if ($saleDateTo < $nowDateTime) {
       $oldSales[] = $sale['ID'];
       $logicFilter[] = ['OFFERS_SALES_VALUE' => $sale['ID']];
       $logicFilter2[] = ['LINK_SALE_VALUE' => $sale['ID']];
   }
}

if (!count($oldSales)) {
    die;
}

$filter2 = $filter = [];

if (count($oldSales) > 1) {
    $filter[] = $logicFilter;
    $filter2[] = $logicFilter2;
} else {
    $filter['OFFERS_SALES_VALUE'] = $oldSales[0];
    $filter2['LINK_SALE_VALUE']   = $oldSales[0];
}

$products = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_IBLOCK_ID)->getEntityDataClass()::query()
    ->setSelect(['ID', 'OFFERS_SALES_' => 'OFFERS_SALES'])
    ->setFilter($filter)
    ->fetchAll();

foreach (array_column($products, 'ID') as $cml2link) {
    $offersFilter = array_merge(['CML2_LINK_VALUE' => $cml2link,], $filter2);

    $offers = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass()::query()
        ->setSelect(['ID', 'CML2_LINK_' => 'CML2_LINK', 'LINK_SALE_' => 'LINK_SALE'])
        ->setFilter($offersFilter)
        ->fetchAll();

    foreach($offers as $offer) {
        \CIBlockElement::SetPropertyValuesEx($offer['ID'], CATALOG_OFFERS_IBLOCK_ID, ['LINK_SALE' => null]);
    }


    $offers = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass()::query()
        ->setSelect(['ID', 'CML2_LINK_' => 'CML2_LINK', 'LINK_SALE_' => 'LINK_SALE'])
        ->setFilter(['CML2_LINK_VALUE' => $cml2link, '!LINK_SALE_VALUE' => null])
        ->fetchAll();

    $productOffersSales = null;
    foreach($offers as $offer) {
        $saleId = $offer['LINK_SALE_VALUE'];
        $productOffersSales[$saleId] = $saleId;
    }

    \CIBlockElement::SetPropertyValuesEx($cml2link, CATALOG_IBLOCK_ID, ['OFFERS_SALES' => $productOffersSales], false);
}