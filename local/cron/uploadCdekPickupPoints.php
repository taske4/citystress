<?php

$_SERVER['DOCUMENT_ROOT'] = __DIR__.'/../..';

require($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

$log = new \Monolog\Logger('CdekUploadPvz');
$log->pushHandler(new \Monolog\Handler\StreamHandler($_SERVER['DOCUMENT_ROOT'].'/local/logs/cdekUploadPvz.log'), Monolog\Logger::DEBUG);

\Citystress\Connector\tc\cdek\Adapter::savePvz();
