<?php

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');

global $APPLICATION;

$APPLICATION->RestartBuffer();


$offerIds = $_REQUEST['offerIds'];
$formated = $_REQUEST['formated'];

if (!$offerIds) {
    die;
}

$offers = \Citystress\Entity\Offer::getList(['ID' => $offerIds]);

$result = [
    'price'    => null,
    'oldprice' => null,
    'printPrice'    => null,
    'printOldprice' => null,
];
foreach($offers as $offer) {
    $result['price'] += (float)$offer['PRICES']['base'];
    $result['oldprice'] += ((float)$offer['PRICES']['old'] ?: (float)$offer['PRICES']['base']);
}

if ($result['oldprice'] <= $result['price']) {
    $result['oldprice'] = null;
}

if ($formated) {
    $result['printPrice'] = FormatCurrency(
        $result['price'],
        'RUB'
    );

    if ($result['oldprice']) {
        $result['printOldprice'] = FormatCurrency(
            $result['oldprice'],
            'RUB'
        );
    } else {
        $result['printOldprice'] = null;
    }
}


echo json_encode($result);