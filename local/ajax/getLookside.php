<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION;

$APPLICATION->RestartBuffer();

$data = $_REQUEST['data'];
$data = json_decode($data, true);

//$baseProductId = $GLOBALS['lookSideFilter']['ID'] = $data['offerId'];

// ###################

\Bitrix\Main\Loader::includeModule('iblock');

$offer = \Citystress\Entity\Offer::getById($data['offerId']);
$set   = \Citystress\Entity\Sets::getById($data['setId']);

$morePhoto = $offer['PROPERTIES']['MORE_PHOTO'];

$GLOBALS['lookSideFilter']['ID'] = $data['productIds'];

$setOffers = \Citystress\Entity\Offer::getList(['ID' => $set['PROPERTIES']['OFFERS_LINK']]);

$offersLink = [];

foreach ($setOffers as $setOffer) {
    if ($setOffer['ACTIVE'] !== 'Y') {
        $setOffer['ID'] = $setOffer['VARIANTS'][0]['ID'];
    }

    $offersLink[] = $setOffer['ID'];
}


?>

<div class="look_head modal_head">
    <span>Изделия в образе</span>
    <svg class="look_close modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20"
         xmlns="http://www.w3.org/2000/svg">
        <g fill="#242424" opacity=".25">
            <rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)"
                  width="26.6202"/>
            <rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)"
                  width="26.6202"/>
        </g>
    </svg>
</div>

<div class="look_body modal_body">
    <div class="customscroll">
        <div class="look_top">
            <div class="look_title"><?=$set['NAME']?></div>
            <div class="look_prices">
                <div class="look_priceold"></div>
                <div class="look_price"></div>
            </div>
        </div>

        <div class="look_content">

            <div class="look_sliderwrap">
                <div class="custom_slarrows">
                    <div class="custom_slarrow3 custom_prev"><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/arrow3.svg" alt=""></div>
                    <div class="custom_slarrow3 custom_next"><img src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/arrow3.svg" alt=""></div>
                </div>
                <div class="look_slider">
                    <?
                        foreach($morePhoto as $photoId) {
                            $photoPath = CFile::GetPath($photoId);
                    ?>
                            <div class="slide">
                                <div class="look_slide">
                                    <div class="back_img">
                                        <img src="<?=$photoPath?>" alt="">
<!--                                        <picture>-->
<!--                                            <source type="image/jpg" media="(max-width: 750px)" srcset="--><?php //=$photoPath?><!--">-->
<!--                                            <source type="image/jpg" media="(min-width: 751px)"-->
<!--                                                    srcset="--><?php //=$photoPath?><!-- 1x, --><?php //=$photoPath?><!-- 2x">-->
<!--                                        </picture>-->
                                    </div>
                                </div>
                            </div>
                    <? } ?>
                </div>
            </div>



            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "lookside",
                array(
                    "ACTION_VARIABLE" => "action",
                    "ADD_PICT_PROP" => "-",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BACKGROUND_IMAGE" => "-",
                    "BASKET_URL" => "/personal/basket.php",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COMPATIBLE_MODE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                    "DETAIL_URL" => "",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_COMPARE" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "FILTER_NAME" => "lookSideFilter",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => "6",
                    "IBLOCK_TYPE" => "catalog",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LABEL_PROP" => array(),
                    "LAZY_LOAD" => "N",
                    "LINE_ELEMENT_COUNT" => "3",
                    "LOAD_ON_SCROLL" => "N",
                    "MESSAGE_404" => "",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_BTN_LAZY_LOAD" => "Показать ещё",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "OFFERS_FIELD_CODE" => array("", ""),
                    "OFFERS_LIMIT" => "0",
                    "OFFERS_SORT_FIELD" => "sort",
                    "OFFERS_SORT_FIELD2" => "id",
                    "OFFERS_SORT_ORDER" => "asc",
                    "OFFERS_SORT_ORDER2" => "desc",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Товары",
                    "PAGE_ELEMENT_COUNT" => "18",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => array('Розничная', 'oldprice'),
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_DISPLAY_MODE" => "N",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "PROPERTY_CODE_MOBILE" => array('MORE_PHOTO'),
                    "RCM_TYPE" => "personal",
                    "SECTION_CODE" => "",
                    "SECTION_CODE_PATH" => "",
                    "SECTION_ID" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("", ""),
                    "SEF_MODE" => "Y",
                    "SEF_RULE" => "/catalog/product/#ELEMENT_CODE#/",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N",
                    "SHOW_ALL_WO_SECTION" => "N",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_FROM_SECTION" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "Y",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "PRE_ACTIVE_OFFER" => $offersLink,
                )
            ); ?>

            <div class="look_result">
                <span>Итого</span>
                <div class="look_prices">
                    <div class="look_priceold">18 999 ₽</div>
                    <div class="look_price">14 997 ₽</div>
                </div>
            </div>

        </div>

    </div>
</div>

<script>
    $(function () {
        if ($('.look_sliderwrap').length) {
            $('.look_sliderwrap').each(function () {
                var look_sliderwrap = $(this);
                look_sliderwrap.find('.look_slider').slick({
                    dots: false,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    prevArrow: look_sliderwrap.find('.custom_prev'),
                    nextArrow: look_sliderwrap.find('.custom_next'),
                    responsive: [
                        {
                            breakpoint: 1000,
                            settings: {
                                slidesToShow: 2
                            }
                        }, {
                            breakpoint: 750,
                            settings: {
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            });
        }
        ;

        // $('.look_back, .look_close').click(function () {
        //     $('body').removeClass('look_out');
        //     $('.look').css('right', '-100vw');
        // });


        $('.look_item__colorlist li').click(function () {
            var look_item = $(this).closest('.look_item'),
                color = $(this).find('div').data('color'),
                this_text = $(this).find('span').text();

            look_item.find('.look_item__colorlist li').removeClass('active');
            $(this).addClass('active');

            look_item.find('.look_item__colorchosen span').css('background', '#' + color);
            look_item.find('.look_item__colorchosen i').text(this_text);
            $('.look_item__colorlist').css('display', 'none');
            $('.look_item__colorchosen').css('display', 'inline-block');
        });

        $('.look_item__colorchosen').click(function () {
            $(this).css('display', 'none');
            $(this).closest('.look_item').find('.look_item__colorlist').css('display', 'block');
        });

        $('.look_item__size').click(function () {
            $(this).closest('.look_item').find('.look_item__size').removeClass('active');
            $(this).addClass('active');
        });

        $(document).click(function (e) {
            that = e.target;
            if ($(that).closest(".look_item__color").length < 1 || $(that).hasClass('look_item__color')) {
                $('.look_item__colorlist').css('display', 'none');
                $('.look_item__colorchosen').css('display', 'inline-block');
            }
        });
    });


</script>