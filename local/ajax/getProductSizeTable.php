<?php

use Bitrix\Highloadblock as HL;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');

global $APPLICATION;

$APPLICATION->RestartBuffer();

$productId = $_REQUEST['productId'];

$offerSelectPropsForTable = [
    'SHIRINA_IZDELIYA_PO_TALII_'                     => 'SHIRINA_IZDELIYA_PO_TALII',
    'SHIRINA_IZDELIYA_PO_BEDRAM_'                    => 'SHIRINA_IZDELIYA_PO_BEDRAM',
    'DLINA_PO_VNUTRENNEMU_SHVU_'                     => 'DLINA_PO_VNUTRENNEMU_SHVU',
    'SHIRINA_SHTANINY_PO_NIZU_'                      => 'SHIRINA_SHTANINY_PO_NIZU',
    'VYSOTA_IZDELIYA_OT_PLECHEVOY_TOCHKI_DO_NIZA_'   => 'VYSOTA_IZDELIYA_OT_PLECHEVOY_TOCHKI_DO_NIZA',
    'VYSOTA_IZDELIYA_PO_TSENTRU_'                    => 'VYSOTA_IZDELIYA_PO_TSENTRU',
    'VYSOTA_PO_TSENTRU_PEREDA_'                      => 'VYSOTA_PO_TSENTRU_PEREDA',
    'VYSOTA_PO_TSENTRU_SPINKI_'                      => 'VYSOTA_PO_TSENTRU_SPINKI',
    'DLINA_BOKOVOGO_SHVA_'                           => 'DLINA_BOKOVOGO_SHVA',
    'DLINA_IZDELIYA_PO_SPINKE_'                      => 'DLINA_IZDELIYA_PO_SPINKE',
    'DLINA_IZDELIYA_PO_SPINKE_DO_TALII_'             => 'DLINA_IZDELIYA_PO_SPINKE_DO_TALII',
    'DLINA_IZDELIYA_SZADI_'                          => 'DLINA_IZDELIYA_SZADI',
    'DLINA_IZDELIYA_SPEREDI_'                        => 'DLINA_IZDELIYA_SPEREDI',
    'DLINA_IZDELIYA_SPEREDI_OT_PLECHEVOGO_SHVA_'     => 'DLINA_IZDELIYA_SPEREDI_OT_PLECHEVOGO_SHVA',
    'DLINA_PLECHA_'                                  => 'DLINA_PLECHA',
    'DLINA_PLECHA_S_RUKAVOM_'                        => 'DLINA_PLECHA_S_RUKAVOM',
    'DLINA_PO_NARUZHNOMU_SHVU_'                      => 'DLINA_PO_NARUZHNOMU_SHVU',
    'DLINA_POYASA_'                                  => 'DLINA_POYASA',
    'DLINA_RUKAVA_OT_GORLOVINY_'                     => 'DLINA_RUKAVA_OT_GORLOVINY',
    'DLINA_RUKAVA_OT_PLECHA_'                        => 'DLINA_RUKAVA_OT_PLECHA',
    'PLECHEVOY_DIAMETR_'                             => 'PLECHEVOY_DIAMETR',
    'SHIRINA_BRETELEY_PO_PLECHU_'                    => 'SHIRINA_BRETELEY_PO_PLECHU',
    'SHIRINA_IZDELIYA_PO_VERKHNEMU_KRAYU_TALIYA_'    => 'SHIRINA_IZDELIYA_PO_VERKHNEMU_KRAYU_TALIYA',
    'SHIRINA_IZDELIYA_PO_GRUDI_'                     => 'SHIRINA_IZDELIYA_PO_GRUDI',
    'SHIRINA_IZDELIYA_PO_NIZU_'                      => 'SHIRINA_IZDELIYA_PO_NIZU',
    'SHIRINA_IZDELIYA_PO_TALII_V_RASTYANUTOM_VIDE_'  => 'SHIRINA_IZDELIYA_PO_TALII_V_RASTYANUTOM_VIDE',
    'SHIRINA_IZDELIYA_PO_TALII_V_SOBRANNOM_VIDE_'    => 'SHIRINA_IZDELIYA_PO_TALII_V_SOBRANNOM_VIDE',
    'SHIRINA_RUKAVA_'                                => 'SHIRINA_RUKAVA',
    'SHIRINA_IZDELIYA_PO_TALII_V_RYASTYANUTOM_VIDE_' => 'SHIRINA_IZDELIYA_PO_TALII_V_RYASTYANUTOM_VIDE',
    'SHIRINA_RUKAVA_POD_PROYMOY_'                    => 'SHIRINA_RUKAVA_POD_PROYMOY',
    'SHIRINA_SHTANINY_PO_NIZU_REZINKA_'              => 'SHIRINA_SHTANINY_PO_NIZU_REZINKA',
];

$props = \Bitrix\Iblock\PropertyTable::query()
    ->setSelect(['NAME', 'CODE'])
    ->setFilter(['CODE' => $offerSelectPropsForTable])
    ->fetchAll();

$propsCodeWithName = [];

foreach($props as $prop) {
    $propsCodeWithName[$prop['CODE']] = $prop['NAME'];
}

$offerSelect = array_merge([
    'CML2_LINK_' => 'CML2_LINK',
    'SIZE_'      => 'SIZE'
], $offerSelectPropsForTable);

$offers = \Citystress\Entity\Offer::getEntity()::query()
    ->setSelect($offerSelect)
    ->setFilter([
        'CML2_LINK_VALUE' => $productId,
        'ACTIVE'          => 'Y',
        '!SIZE_VALUE'     => null,
    ])
    ->fetchAll();

$thead = $tableBody = [];

$sizes = HL\HighloadBlockTable::getById(HL_SIZES)->fetch();
$sizes = HL\HighloadBlockTable::compileEntity($sizes);
$sizes_data_class = $sizes->getDataClass();
$sizes = $sizes_data_class::getList(array(
    "select" => ['UF_NAME', 'UF_SORT'],
))->fetchAll();

$sortedOffersBySize = [];

$sizesColumnName = array_column($sizes, 'UF_NAME');

foreach($offers as $offer) {
    $index = array_search($offer['SIZE_VALUE'], $sizesColumnName);

    if ($index === false) {
        continue;
    }

    $sizeSort = $sizes[$index]['UF_SORT'];

    if (!$sizeSort) {
        $sizeSort = 1000;
    }

    if (array_search($offer['SIZE_VALUE'], $thead) !== false) {
        continue;
    }

    if ($sortedOffersBySize[$sizeSort.'_'.$offer['SIZE_VALUE']]) {
        continue;
    }

    $sortedOffersBySize[$sizeSort.'_'.$offer['SIZE_VALUE']] = $offer;
}

ksort($sortedOffersBySize);

foreach($sortedOffersBySize as $offer) {
    if (array_search($offer['SIZE_VALUE'], $thead) !== false) {
        continue;
    }

    $thead[] = $offer['SIZE_VALUE'];

    foreach($offerSelectPropsForTable as $prop) {
        /*
         * Добавляем первым элементом NAME, нужно для простой отрисовки <table>
         */
        if (!$tableBody[$propsCodeWithName[$prop]]) {
            $tableBody[$propsCodeWithName[$prop]][] = $propsCodeWithName[$prop];
        }

        if ($offerPropValue = $offer[$prop.'_VALUE']) {
            $offerPropValue = number_format($offer[$prop . '_VALUE'], 1);
            $offerPropValue = ($offerPropValue == intval($offerPropValue)) ? intval($offerPropValue) : $offerPropValue;
        }

        $offerPropValue = $offerPropValue ?: '-';

        $tableBody[$propsCodeWithName[$prop]][] = $offerPropValue ?: '-';
    }
}

/*
 * Исключаем пустые строки
 */

foreach($tableBody as $name => $row) {
    $removeRow  = true;
    $firstValue = true;

    foreach($row as $value) {

        if ($firstValue) { // Первый элемент это названия строки (выше есть комент)
            $firstValue = false;
            continue;
        }

        if ($value !== '-') {
            $removeRow = false;
            break;
        }
    }

    if ($removeRow) {
        unset($tableBody[$name]);
    }
}

if (!$tableBody) {
    die;
}

?>

<div class="sizetable_item acc_item product-size-table">
    <div class="sizetable_item__title acc_item__open">Обмеры изделия</div>
    <div class="sizetable_item__out acc_item__out">
        <table class="measurements">
            <thead>
            <th></th>
            <? foreach($thead as $size) { ?>
                <th><?=$size?></th>
            <? } ?>
            </thead>
            <tbody>
            <? foreach($tableBody as $row) { ?>
            <tr>
                <? foreach ($row as $value) { ?>
                <td><?= $value?></td>
                <? } ?>
            </tr>
            <? } ?>
            <tr class="last">
                <td colspan="<?=count((array)$thead) +1?>"><span>*Все размеры указаны в см</span></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>