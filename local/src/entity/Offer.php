<?php

namespace Citystress\Entity;

class Offer
{
    private int $id;
    private $fillVariants;
    public array $offer;

    const PRICE_ID = 3;
    const OLD_PRICE_ID = 2;

    public function __construct(int $id, $fillVariants=true)
    {
        $this->id = $id;
        $this->fillVariants = $fillVariants;
        $this->fillData();
    }

    private function fillData()
    {
        $res = \CIBlockElement::GetList(
            [],
            ['ID' => $this->id],
            false, false,
            []
        );

        $value = $res->GetNextElement();

        if (!$value) {
            throw new \Exception('offer not found, id: '.$this->id);
        }

        $this->offer = $fields = (array)$value->GetFields();
        $props = $value->GetProperties();

        $this->setProps($props);
        $this->fillCatalog();
        $this->setVariants();
    }

    private function fillCatalog()
    {
        $this->offer['PRICES']['base'] = $this->getPrice();
        $this->offer['PRICES']['old']  = $this->getOldprice();
    }

    private function getPrice()
    {
        return \Bitrix\Catalog\PriceTable::query()
            ->setSelect(['PRICE'])
            ->setFilter([
                'CATALOG_GROUP_ID' => self::PRICE_ID,
                'PRODUCT_ID'       => $this->id,
            ])
            ->fetch()['PRICE'] ?: null;
    }

    private function getOldprice()
    {
        return \Bitrix\Catalog\PriceTable::query()
            ->setSelect(['PRICE'])
            ->setFilter([
                'CATALOG_GROUP_ID' => self::OLD_PRICE_ID,
                'PRODUCT_ID'       => $this->id,
            ])
            ->fetch()['PRICE'] ?: null;
    }

    private function setProps($props)
    {
        foreach($props as $name => $value) {
            $this->offer['PROPERTIES'][$name] = $value['VALUE'];
        }
    }

    private function setVariants()
    {
        if (!$this->fillVariants) {
            return;
        }

        $color    = $this->offer['PROPERTIES']['COLOR'];
        $cml2link = $this->offer['PROPERTIES']['CML2_LINK'];

        if (!$color || !$cml2link) {
            throw new \Exception('Торговое предложение не привязано к товару, или не задан цвет. Id: '.$this->offer['ID']);
        }

        $res = self::getEntity()::query()
            ->setSelect(['ID', 'CML2_LINK_' => 'CML2_LINK', 'COLOR_' => 'COLOR'])
            ->setFilter(['CML2_LINK_VALUE' => $cml2link, 'COLOR_VALUE' => $color, 'ACTIVE' => 'Y'])
            ->fetchAll();

        foreach($res as $value) {
            $this->offer['VARIANTS'][] = [
                'ID'        => $value['ID'],
                'CML2_LINK' => $value['CML2_LINK_VALUE'],
            ];
        }
    }

    public static function getById(int $id) {
        return (new self($id))->offer;
    }

    public static function getList(array $filter)
    {
        $res = \CIBlockElement::GetList(
            [],
            $filter,
            false, false,
            ['IBLOCK_ID', 'ID']
        );

        $offers = [];

        while($value = $res->GetNext()) {
            $offers[] = (new self($value['ID']))->offer;
        }

        return $offers;
    }

    public static function getEntity()
    {
        return \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass();
    }
}