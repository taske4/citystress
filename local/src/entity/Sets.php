<?php

namespace Citystress\Entity;

class Sets
{
    private int $id;
    private array $data;

    public function __construct(int $id)
    {
        $this->id = $id;
        $this->fillData();
    }

    private function fillData()
    {
        $res = \CIBlockElement::GetList(
            [],
            ['ID' => $this->id],
            false, false,
            []
        );

        $value = $res->GetNextElement();

        if ($value === false) {
            throw new \Exception('set not found, id: '.$this->id);
        }

        $this->data = $fields = (array)$value->GetFields();
        $props = $value->GetProperties();

        $this->setProps($props);
    }

    private function setProps($props)
    {
        foreach($props as $name => $value) {
            $this->data['PROPERTIES'][$name] = $value['VALUE'];
        }
    }

    public static function getById(int $id) {
        return (new self($id))->data;
    }

    public static function getList(array $filter)
    {
        $res = \CIBlockElement::GetList(
            [],
            $filter,
            false, false,
            ['IBLOCK_ID', 'ID']
        );

        $offers = [];

        while($value = $res->GetNext()) {
            $offers[] = (new self($value['ID']))->data;
        }

        return $offers;
    }

    public static function getEntity()
    {
        return \Bitrix\Iblock\Iblock::wakeUp(CATALOG_SETS_IBLOCK_ID)->getEntityDataClass();
    }
}