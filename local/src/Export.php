<?php

namespace Citystress;

/*
 * Example
 *
    $feed = new Export(
      $_SERVER['DOCUMENT_ROOT'].'/local/new_rees.yml',
      ['ACTIVE' => 'Y'],
      ['ACTIVE' => 'Y', 'ID' => [37237]],
      ['ACTIVE' => 'Y'],
    );
    $feed->export();
 */
class Export
{
    const PRICE_ID = 3;
    const FILE_PATH_TO_SAVE = '/local/feed-test.yml';
    const PRODUCT_LIMIT = 0;
    const SUBDOMAIN = '';
    const COMPANY_NAME = 'CityStress'; // название кампании
    const PROTOCOL = 'https://';
    const COMPANY_URL = 'citystress.ru'; // url кампании
    const PLATFORM_NAME = '1C-Bitrix'; // url кампании

    private string $filePathToSave;
    private array $sectionFilter;
    private array $productFilter;
    private array $offersFilter;
    private string $xml;
    private array $sectionsIds;
    private array $cities;

    private bool $regionality = false;

    public function __construct(
        string $filePathToSave,
        array  $sectionFilter,
        array  $productFilter,
        array  $offersFilter
    )
    {
        $this->filePathToSave = $filePathToSave;
        $this->sectionFilter = $sectionFilter;
        $this->productFilter = $productFilter;
        $this->offersFilter = $offersFilter;

        $this->xml = '';

        $this->addHeader();
    }

    private function readFeed($path)
    {
        $handle = fopen($path, "r");

        while (!feof($handle)) {
            yield trim(fgets($handle));
        }

        fclose($handle);
    }

    public function copyFeedByRegionality(string $dir, string $file)
    {
        if (!file_exists($this->filePathToSave)) {
            return false;
        }

        $feed = file_get_contents($this->filePathToSave);

        foreach ($this->cities as $city) {
            if (!$city['SUBDOMAIN_VALUE']) {
                continue;
            }

            $newRegionFeed = $dir . $city['SUBDOMAIN_VALUE'] . '_' . $file;

            if (file_put_contents($newRegionFeed, '') !== false) {
                $str = str_replace('https://zapovednik96.ru', 'https://' . $city['SUBDOMAIN_VALUE'] . '.zapovednik96.ru', $feed);
                file_put_contents($newRegionFeed, $str, FILE_APPEND);
            }
        }
    }

    public function withRegionality()
    {
        $this->regionality = true;
        $this->cities = City::getRegionalityList();
    }

    private function addHeader()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml .= '<yml_catalog date="' . date('Y-m-d\TH:i') . '">' . "\n";
        $xml .= '<shop>' . "\n";
        $xml .= '<name>' . self::COMPANY_NAME . '</name>' . "\n";
        $xml .= '<company>' . self::COMPANY_NAME . '</company>' . "\n";
        $xml .= '<url>' . self::PROTOCOL . self::SUBDOMAIN . self::COMPANY_URL . '</url>' . "\n";
        $xml .= '<platform>' . self::PLATFORM_NAME . '</platform>' . "\n";
        $xml .= '<currencies>' . "\n";
        $xml .= '<currency id="RUB" rate="1"/>' . "\n";
        $xml .= '</currencies>' . "\n";

        $this->xml .= $xml;
    }

    private function addFooter()
    {
        $xml = '</shop>' . "\n";
        $xml .= '</yml_catalog>';
        $this->xml .= $xml;

    }

    private function addSections()
    {
        $xml = '<categories>' . "\n";

        $res = \CIBlockSection::GetList(
            [],
            $this->sectionFilter,
            false,
            ['ID', 'IBLOCK_SECTION_ID', 'NAME']
        );

        $ids = [];

        while ($cat = $res->fetch()) {
            $ids[$cat['ID']] = $cat['ID'];
            $xml .= '<category id="' . $cat['ID'] . '" ' . ($cat['IBLOCK_SECTION_ID'] ? 'parentId="' .
                    $cat['IBLOCK_SECTION_ID'] . '"' : '') . '>' . htmlspecialchars($cat['NAME']) . '</category>' . "\n";
        }

        $xml .= '</categories>' . "\n";

        $this->xml .= $xml;
        $this->sectionsIds = $ids;
    }

    private function productPriceCalc(int $productId)
    {
        // цена товара
        $price = \Bitrix\Catalog\PriceTable::getList([
            "select" => ["*"],
            "filter" => [
                'CATALOG_GROUP_ID' => self::PRICE_ID,
                "=PRODUCT_ID" => $productId,
            ]
        ])->fetchAll();

        $basePrice = $price[0]['PRICE'];

        $oldPrice = \Bitrix\Catalog\PriceTable::query()
            ->where('PRODUCT_ID', $productId)
            ->where('CATALOG_GROUP_ID', 2)
            ->addSelect('*')
            ->setLimit(1)
            ->fetch()['PRICE'];

        return [
            'base' => $basePrice,
            'oldPrice' => $oldPrice ?: null,
        ];
    }

    /*
     * Проверям есть ли раздел товара в списке выгружаемых разделов, если нет итерируем родительские разделы
     */
    private function checkProductOnSection(int $productId, int $productSectionId): bool
    {
        if (!$this->sectionsIds[$productSectionId]) {
            $db_groups = \CIBlockElement::GetElementGroups($productId, true, ['ID', 'NAME']);

            while ($ar_group = $db_groups->Fetch()) {
                if ($this->sectionsIds[$ar_group['ID']]) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

    private function getLocationsData($props)
    {
        if ($this->regionality) {
            $totalQuantity = 0;

            $xml = '<locations>';

            foreach ($this->cities as $city) {
                $xmlId1c = strtoupper($city['XML_ID_1C_VALUE']);
                $amount = $props['PROPERTY_STORE_' . $xmlId1c . '_VALUE'] ?: $props['STORE_' . $xmlId1c]['VALUE'];

                if ($amount) {
                    $totalQuantity += $amount;
                    $xml .= '<location id="' . $city['ID'] . '"><stock_quantity>' . $amount . '</stock_quantity></location>';
                }
            }
            $xml .= '</locations>';

            return [
                'totalQuantity' => $totalQuantity,
                'locations' => $xml,
            ];
        }

        return null;
    }

    private function addProducts()
    {
        $xml = '<offers>' . "\n";

        $products = \CIBlockElement::GetList(
            ['SORT' => 'ASC'],
            $this->productFilter,
            false, false,
            [
                'IBLOCK_ID', 'ID', 'XML_ID', 'NAME', 'ACTIVE', 'XML_ID', 'IBLOCK_SECTION_ID', 'DETAIL_TEXT',
                'PREVIEW_PICTURE', 'CATALOG_AVAILABLE', 'PREVIEW_TEXT', 'DETAIL_PAGE_URL',
            ]
        );

        $skipedCount = $offersCount = $productsCount = 0;

        $baseUri   = self::PROTOCOL . self::SUBDOMAIN . self::COMPANY_URL;

        while ($productRes = $products->GetNextElement()) {
            $productsCount++;


            $fields = $productRes->GetFields();

            if ((!$fields['IBLOCK_SECTION_ID']) || !$this->checkProductOnSection($fields['ID'], $fields['IBLOCK_SECTION_ID'])) {
                unset($fields);
                continue;
            }

            /*
             * Получаем свойства товара
             */
            $props = [];

            foreach ($productRes->GetProperties() as $prop) {
                $props['PROPERTY_' . $prop['CODE'] . '_VALUE'] = $prop['VALUE'];
            }

            $product = array_merge($fields, $props);
            unset ($fields);

            /*
             * Формируем offers
             */

            # required fields
            $manufacturer = $vendor = str_replace('&', '&amp;', $product['PROPERTY_BREND_VALUE'] ?? 'Свой');
            $country = $product['PROPERTY_COUNTRY_VALUE'] ?: 'Россия';
            $catId = $product['IBLOCK_SECTION_ID'];
            $available = $product['CATALOG_AVAILABLE'] === 'Y' ? 'true' : 'false';
            $barcode = $product['PROPERTY_CML2_BAR_CODE_VALUE'] ?? '0000000000000';

            $sectionEntity = \Bitrix\Iblock\Model\Section::compileEntityByIblock(CATALOG_IBLOCK_ID);
            $sectionRes = $sectionEntity::query()
                ->setSelect(['UF_OFFERS_WEIGHT'])
                ->setFilter(['ID' => $catId])
                ->fetch();

            $weight = $sectionRes['UF_OFFERS_WEIGHT'];

            if (!$weight) {
                $parentsSections = \CIBlockSection::GetNavChain(CATALOG_IBLOCK_ID, $catId, ['ID']);
                while($parentSection = $parentsSections->GetNext()) {
                    $sectionRes = $sectionEntity::query()
                        ->setSelect(['UF_OFFERS_WEIGHT'])
                        ->setFilter(['ID' => $parentSection['ID']])
                        ->fetch();
                    if ($sectionRes['UF_OFFERS_WEIGHT']) {
                        $weight = $sectionRes['UF_OFFERS_WEIGHT'];
                        break;
                    }
                }
            }

//            $desc = mb_strimwidth($this->cleanText($product['DETAIL_TEXT']), 0, 250); // описание товара, обрезанное до заданного количества символов
//            if (!$desc) {
//                $desc = mb_strimwidth($this->cleanText($product['PREVIEW_TEXT']), 0, 250); // описание товара, обрезанное до заданного количества символов
//            }

            $offersCollection = [];

            $offers = \CIBlockElement::GetList(
                ['SORT' => 'ASC'],
                array_merge(['PROPERTY_CML2_LINK' => $product['ID']], $this->offersFilter),
                false, false,
                [
                    'IBLOCK_ID', 'ID', 'XML_ID', 'NAME', 'ACTIVE', 'XML_ID', 'IBLOCK_SECTION_ID', 'DETAIL_TEXT',
                    'PREVIEW_PICTURE', 'CATALOG_AVAILABLE', 'PREVIEW_TEXT', 'DETAIL_PAGE_URL',
                ]
            );

            $productData = [
                'VENDOR' => $vendor,
                'CATEGORY_ID' => $catId,
                'COUNTRY' => $country,
            ];

            if (!$offers) {
                $skipedCount++;
                continue;
            } else {
                while ($offer = $offers->GetNextElement()){
                    $offersCount++;
                    $offer =  array_merge($offer->GetFields(), $offer->GetProperties());

                    $price = $this->productPriceCalc($offer['ID']);

                    /*echo '<div style="display: flex;">';
                    echo '<div style="width: 50%;"><pre>';
                    var_dump($offer->toArray());
                    echo '</pre></div>';*/

                    $available = $offer['CATALOG_AVAILABLE'] ? 'true' : 'false'; // Надо понять есть ли фиды
                    // где
                    // это

                    $offerDesc = $offer['DETAIL_TEXT'] ?: $offer['PREVIEW_TEXT'] ?: null;
                    $desc = null;

                    if (!$offerDesc) {
                        $offerDescAr = [
                            'Артикул: '                      => $offer['CML2_ARTICLE']['VALUE'],
                            'Цвет: '                         => $offer['COLOR']['VALUE'],
                            'На модели размер: '             => $offer['SIZE']['VALUE'],
                            'Рост модели на фото, см: '      => $offer['HEIGHT_MODEL_ON_PHOTO']['VALUE'],
                            'Параметры модели на фото, см: ' => $offer['PARAMS_MODEL_ON_PHOTO']['VALUE'],
                            ''                               => $offer['SEO_WORDS']['VALUE'],
                            'Состав: '                       => $product['PROPERTY_SOSTAV_VALUE'],
                        ];

                        $offerDesc = '';

                        foreach($offerDescAr as $text => $value) {
                            if (!$value) {
                                continue;
                            }

                            if (is_array($value)) {
                                $value = implode(', ', $value);
                            }

                            $offerDesc .= $text . $value . PHP_EOL;
                        }
                    }

                    $desc      = $offerDesc;
                    $offerName = str_replace($offer['CML2_ARTICLE']['VALUE'], ' ', $offer['NAME']);
                    $offerName = str_replace(['(', ')'], ['', ''], $offerName);
                    $offerName = trim($offerName);
                    $picture   = (int)$offer['MORE_PHOTO']['VALUE'][0];

                    if (!$picture) {
                        continue;
                    }

                    $fileRes = \Bitrix\Main\FileTable::query()
                        ->setSelect(['SUBDIR', 'FILE_NAME'])
                        ->setFilter(['ID' => $picture])
                        ->fetch();

                    $picture = $baseUri . '/upload/' . $fileRes['SUBDIR'] . '/' . $fileRes['FILE_NAME'];

                    $offersCollection[] = [
                        'ID'              => $offer['ID'],
                        'NAME'            => $offerName,
                        'AVAILABLE'       => $available,
                        'PRICE'           => $price['base'],
                        'OLD_PRICE'       => $price['oldPrice'],
                        'WEIGHT'          => $offer['CATALOG_WEIGHT'] ?: $weight ?: 250,
                        'DETAIL_PAGE_URL' => $baseUri . '/catalog/product/' . $product['CODE'] . '/?offerId=' .
                            $offer['ID'],
                        'BARCODE'         => $offer['CML2_BAR_CODE']['VALUE'] ?: '0000000000000',
                        'PREVIEW_PICTURE' => $product['PREVIEW_PICTURE'],
                        'PICTURE'         => $picture,
                    ];

                    /*echo '<div style="width: 50%;"><pre>';
                    var_dump($offersCollection);
                    echo '</pre></div>';
                    echo '</div>';
                    die;*/
                }
            }

            foreach ($offersCollection as $offer) {
                $xml .= '<offer id="' . $offer['ID'] . '" available="' . $offer['AVAILABLE'] . '">' . "\n";
                $xml .= '<name>' . str_replace('&', '&amp;', $offer['NAME']) . '</name>' . "\n";
                $xml .= '<url>' . $offer['DETAIL_PAGE_URL'] . '</url>' .
                    "\n";
                $xml .= '<price>' . $offer['PRICE'] . '</price>' . "\n";

                if ($offer['OLD_PRICE']) {
                    $xml .= '<oldprice>' . $offer['OLD_PRICE'] . '</oldprice>' . "\n";
                }

                $xml .= '<currencyId>RUB</currencyId>' . "\n";
                $xml .= '<picture>' . $offer['PICTURE'] . '</picture>' . "\n";
                $xml .= '<categoryId>' . $productData['CATEGORY_ID'] . '</categoryId>' . "\n";
                $xml .= ($desc) ? '<description><![CDATA[' . $desc . ']]></description>' . "\n" : '<description></description>' . "\n";
                $xml .= '<vendor>CityStress</vendor>' . "\n";
                $xml .= '<manufacturer>CityStress</manufacturer>' . "\n";
                $xml .= '<country_of_origin>Россия</country_of_origin>' . "\n";
                $xml .= '<weight>' . $offer['WEIGHT'] . '</weight>' . "\n";

                if ($offer['BARCODE'] !== null) {
                    $xml .= '<barcode>' . $offer['BARCODE'] . '</barcode>' . "\n";
                }

                $xml .= '</offer>' . "\n";
            }
        }

        $xml .= '</offers>' . "\n";

        $this->xml .= $xml;


        var_dump($productsCount);
        var_dump($offersCount);
        var_dump($skipedCount);
    }

    private function addLocations()
    {
        $xml = '<locations>';
        foreach ($this->cities as $city) {
            $xml .= '<location id="' . $city['ID'] . '" type="city" name="' . $city['NAME'] . '" />';
        }
        $xml .= '</locations>';

        $this->xml .= $xml;
    }

    public function export()
    {
        $this->addSections();

        if ($this->regionality) {
            $this->addLocations();
        }

        $this->addProducts();
        $this->addFooter();
        $this->write();
    }

    private function write()
    {
        file_put_contents($this->filePathToSave, $this->xml);
    }

    private function cleanText($text)
    {
        $text = strip_tags($text);
        $text = htmlspecialcharsbx($text);
        $text = trim($text);
        $text = preg_replace('/[\x01-\x08\x0B-\x0C\x0E-\x1F]/', "", $text);

        return $text;
    }
}
