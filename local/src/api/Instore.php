<?php

namespace Citystress\Api;
use Bitrix\Main\Mail\Event;
use Bitrix\Main;
use Bitrix\Main\Application;
use Bitrix\Main\UserTable;
\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::includeModule("catalog");

class Instore
{
    public function buy(array $data)
    {
		$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

		$siteId = \Bitrix\Main\Context::getCurrent()->getSite();
		 $params = [];
		   $offerId     = $data['offerId'];
        $productCode = $data['productCode'];
		 $storeAddress = $data['storeAddress'];

        foreach($data['formData'] as $row) {
           $params[strtoupper($row['name'])] = $row['value'];
        }
 
		global $USER;
 
    $emailUser = \CUser::GetList($by="", $order="", array('=EMAIL' => $params['INSTOCK_EMAIL']));
    while($arUser = $emailUser->Fetch())
        $USER_ID = $arUser["ID"];
             
    if(empty($USER_ID)){ // Если не нашлось email-а в БД, созданим нового юзвера
         
        $NEW_LOGIN = $params['INSTOCK_EMAIL'];
        $NEW_EMAIL = $params['INSTOCK_EMAIL'];
        $NEW_NAME = '';
        $NEW_LAST_NAME = "";
 
        if(strlen($_POST["FIO"]) > 0)
        {
            $arNames = explode(" ", $params['INSTOCK_NAME']);
            $NEW_NAME = $arNames[1];
            $NEW_LAST_NAME = $arNames[0];
        }
 
        $pos = strpos($NEW_LOGIN, "@");
        if ($pos !== false)
            $NEW_LOGIN = substr($NEW_LOGIN, 0, $pos);
 
        if (strlen($NEW_LOGIN) > 47)
            $NEW_LOGIN = substr($NEW_LOGIN, 0, 47);
 
        if (strlen($NEW_LOGIN) < 3)
            $NEW_LOGIN .= "___";
 
        $dbUserLogin = \CUser::GetByLogin($NEW_LOGIN);
        if ($arUserLogin = $dbUserLogin->Fetch())
        {
            $newLoginTmp = $NEW_LOGIN;
            $uind = 0;
            do
            {
                $uind++;
                if ($uind == 10)
                {
                    $NEW_LOGIN = $arUserResult["USER_EMAIL"];
                    $newLoginTmp = $NEW_LOGIN;
                }
                elseif ($uind > 10)
                {
                    $NEW_LOGIN = "buyer".time().GetRandomCode(2);
                    $newLoginTmp = $NEW_LOGIN;
                    break;
                }
                else
                {
                    $newLoginTmp = $NEW_LOGIN.$uind;
                }
                $dbUserLogin = \CUser::GetByLogin($newLoginTmp);
            }
            while ($arUserLogin = $dbUserLogin->Fetch());
            $NEW_LOGIN = $newLoginTmp;
        }
 
        $GROUP_ID = array(3,4);
        $def_group = \COption::GetOptionString("main", "new_user_registration_def_group", "");
        if($def_group!="")
        {
            $GROUP_ID = explode(",", $def_group);
            $arPolicy = $USER->GetGroupPolicy($GROUP_ID);
        }
        else
        {
            $arPolicy = $USER->GetGroupPolicy(array());
        }
 
        $password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
        if($password_min_length <= 0)
            $password_min_length = 6;
        $password_chars = array(
            "abcdefghijklnmopqrstuvwxyz",
            "ABCDEFGHIJKLNMOPQRSTUVWXYZ",
            "0123456789",
        );
        if($arPolicy["PASSWORD_PUNCTUATION"] === "Y")
            $password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
        $NEW_PASSWORD = $NEW_PASSWORD_CONFIRM = randString($password_min_length+2, $password_chars);
 
        $user = new \CUser;
        $arAuthResult = $user->Add(Array(
            "LOGIN" => $NEW_LOGIN,
            "NAME" => $NEW_NAME,
			'PHONE_NUMBER' =>  $params['INSTOCK_PHONE'],
            "LAST_NAME" => $NEW_LAST_NAME,
            "PASSWORD" => $NEW_PASSWORD,
            "CONFIRM_PASSWORD" => $NEW_PASSWORD_CONFIRM,
            "EMAIL" => $NEW_EMAIL,
            "GROUP_ID" => $GROUP_ID,
            "ACTIVE" => "Y",
            "LID" => $siteId,
            )
        );
 
        if (IntVal($arAuthResult) <= 0)
        {
            $arResult["ERROR"][] = GetMessage("STOF_ERROR_REG").((strlen($user->LAST_ERROR) > 0) ? ": ".$user->LAST_ERROR : "" );
        }
        else
        {
            $USER->Authorize($arAuthResult);
        }
    } else {
        $USER->Authorize($USER_ID); // авторизуем, если новый
    }
	$basket = \Bitrix\Sale\Basket::create($siteId);

 

    $item = $basket->createItem('catalog',$offerId);

    $item->setFields(array( // стандартный вариант, цена берется из каталога

        'QUANTITY' => 1,

        'PRODUCT_PROVIDER_CLASS' => \Bitrix\Catalog\Product\Basket::getDefaultProviderName(),

        'LID' => $siteId,

    ));

    
$currency = \Bitrix\Currency\CurrencyManager::getBaseCurrency();
$deliveryId = 5;
  if($order = \Bitrix\Sale\Order::create($siteId,$USER->GetID(),$currency))

    {        

        $order->setPersonTypeId(1);    

        

        $order->setBasket($basket);        

    

        $basketSum = $order->getPrice();

    

        /* shipment */

        

        $shipmentCollection = $order->getShipmentCollection();

        $shipment = $shipmentCollection->createItem();

        $service = \Bitrix\Sale\Delivery\Services\Manager::getById($deliveryId);

        $delivery = $service['NAME'];

        $shipment->setFields(array(

            'DELIVERY_ID' => $service['ID'],

            'DELIVERY_NAME' => $service['NAME'],

        ));

        $shipmentItemCollection = $shipment->getShipmentItemCollection();

        foreach ($basket as $item)

        {

            $shipmentItem = $shipmentItemCollection->createItem($item);

            $shipmentItem->setQuantity($item->getQuantity());

        }
		$propertyCollection = $order->getPropertyCollection();

        $propertyCodeToId = array();

        foreach($propertyCollection as $propertyValue)
		{
            $propertyCodeToId[$propertyValue->getField('CODE')] = $propertyValue->getField('ORDER_PROPS_ID');
		}
            
 
        $propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['NAME']);

        $propertyValue->setValue($params['INSTOCK_NAME']);

  
  $propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['PHONE']);

        $propertyValue->setValue($params['INSTOCK_PHONE']);

    

       $propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['MAIL']);

        $propertyValue->setValue($params['INSTOCK_EMAIL']);	 
		
	 $propertyValue=$propertyCollection->getItemByOrderPropertyId($propertyCodeToId['DELIVERY_ADDRESS']);

        $propertyValue->setValue($storeAddress);
	
		
		
		$paymentCollection = $order->getPaymentCollection();

        $payment = $paymentCollection->createItem();

        $payment->setField('SUM', $order->getPrice());

        $paySystemService = \Bitrix\Sale\PaySystem\Manager::getObjectById(4);

        $payment->setFields(array(

            'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),

            'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),

        ));        
	$order->doFinalAction(true);

        $result = $order->save();

        if($result->isSuccess())

        {

            $orderId = $order->getId();

        

            //$order = Order::load($orderId);

    

            $accountNumber = $order->getField('ACCOUNT_NUMBER');

return $orderId;
        }

        else

        {

            $arErrors[] = "Ошибка создания заказа: ".implode(", ",$result->getErrorMessages());

        }

}
  
}
}