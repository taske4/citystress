<?php

namespace Citystress\Api;

class Regionality
{
    public static function selectCity(array $data)
    {
        if (!$data['locationId']) {
            throw new \InvalidArgumentException('LocationId is required.');
        }

        if (regionality()->setCity($data['locationId'])) {
            return ['success' => true];
        } else {
            throw new \Exception('Ошибка по городу');
        }
    }

    public static function searchCity(array $data)
    {
        \Bitrix\Main\Loader::includeModule('sale');

        $value = (string)$data['value'];
        if (strlen($value) < 3) {
            throw new \InvalidArgumentException('Минимум 3 символа.');
        }

        $cities = \Bitrix\Sale\Location\LocationTable::query()
            ->setSelect(['NAME_RU' => 'NAME.NAME', 'ID', 'PARENT_ID', 'LANG_ID' => 'NAME.LANGUAGE_ID',])
            ->setFilter([
                'NAME.NAME' => '%'.$value.'%',
                'TYPE_ID'   => 5,
                '=LANG_ID' => 'ru',
            ])->fetchAll();

//        foreach($cities as $city) {
//            $city['PARENT_ID']
//        }

        $parents    = array_column($cities, 'PARENT_ID');
        $parentsIds = array_filter($parents, static function($var){return $var !== null;} );

        $res = \Bitrix\Sale\Location\LocationTable::query()
            ->setSelect(['NAME_RU' => 'NAME.NAME', 'ID', 'LANG_ID' => 'NAME.LANGUAGE_ID'])
            ->setFilter([
                '=ID' => $parentsIds,
                '=LANG_ID' => 'ru',
            ])->fetchAll();

        $parents = [];
        foreach($res as $item)  {
            $parents[$item['ID']] = $item;
        }

        return [
            'cities'  => $cities,
            'parents' => $parents,
        ];
    }
}