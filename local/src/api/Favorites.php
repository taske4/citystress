<?php

namespace Citystress\Api;

class Favorites
{
    public static function add(array $data)
    {
        global $USER;
        \Bitrix\Main\Loader::includeModule('iblock');

        if (!$data['productId']) {
            throw new \InvalidArgumentException('ProductId is required.');
        }

        $userId = (int)$USER->GetID();


        $res = \Bitrix\Iblock\Elements\ElementCatalogTable::query()
            ->setFilter([
                '=ID' => $data['productId'],
            ])
            ->fetch();

        if (!$res) {
            throw new \InvalidArgumentException('product not found');
        }

        $res = \Citystress\Favorites::getInstance()->add($userId, $data['productId']);

        return ['success' => (bool)$res];
    }

    public static function delete(array $data)
    {
        global $USER;

        if (!$data['productId']) {
            throw new \InvalidArgumentException('ProductId is required.');
        }

        $userId = (int)$USER->GetID();

        \Citystress\Favorites::getInstance()->remove($userId, [(int)$data['productId']]);

        return ['count' => count(\Citystress\Favorites::getInstance()->get($userId))];
    }

    public static function deleteAll()
    {
        global $USER;

        $userId = (int)$USER->GetID();


        $ids = \Citystress\Favorites::getInstance()->get($userId);
        $res = \Citystress\Favorites::getInstance()->remove($userId, $ids);

        return ['success' => $res];
    }
}