<?php

namespace Citystress\Api;

use Bitrix\Main\UserTable;

class User
{
    public static function requestOnRemove()
    {
        global $USER;

        if (!\Bitrix\Main\Application::getInstance()->getSession()->get("request_on_remove_user")) {
            $res = \Bitrix\Main\Mail\Event::send([
                "EVENT_NAME" => "REQUEST_ON_REMOVE_USER",
                "LID"        => "s1",
                "C_FIELDS"   => [
                    "USER_ID" => $USER->GetID(),
                    "EMAIL"   => $USER->GetEmail(),
                ],
            ]);

            if ($res->isSuccess()) {
                \Bitrix\Main\Application::getInstance()->getSession()->set("request_on_remove_user", true);

                return ['success' => true];
            } else {
                return ['success' => false];
            }
        }

        return ['success' => false];
    }

    public static function aceptCookies()
    {
        $_SESSION['acept_cookies'] = true;
    }

    public static function search(array $data)
    {
        global $USER;

        if (!$data['value']) {
            throw new \InvalidArgumentException('Введите почту или номер телефона.');
        }

        $res = UserTable::query()
            ->setSelect(['ID'])
            ->setFilter([
                [
                    'LOGIC' => 'OR',
                    [ '=EMAIL' =>  $data['value'] ],
                    [ '=PERSONAL_PHONE' => $data['value'] ]
                ]
            ])
            ->fetch();

        if ($res) {
            return ['success' => true];
        } else {
            throw new \Exception('Пользователь не найден');
        }
    }
    public static function changepasswd(array $data)
    {
        global $USER;

        if (!$data['code']) {
            throw new \InvalidArgumentException('code');
        }

        if (!$data['passwd']) {
            throw new \InvalidArgumentException('passwd');
        }

        if ($data['passwd'] !== $data['passwd2']) {
            throw new \Exception('Неверное подтверждение пароля.');
        }


        $sms = new \Citystress\Sms();

        if (($userId = $sms->checkSmsCode($data['code']))) {
            $user = new \CUser;
            $fields = Array(
                "PASSWORD"          => $data['passwd'],
                "CONFIRM_PASSWORD"  => $data['passwd2'],
            );
            $user->Update($userId, $fields);

            if ($user->LAST_ERROR) {
               throw new \Exception($user->LAST_ERROR);
            }

            return ['success' => true];
        } else {
            throw new \Exception('Неверный код');
        }
    }
}