<?php

namespace Citystress\Api;

use Citystress\Entity\Offer;
use Error;
use ErrorException;
use Exception;

class Basket
{
    public static function addProduct(array $data)
    {
        if (!$data['id']) {
            throw new \InvalidArgumentException('id');
        }

        if (!$data['quantity']) {
            throw new \InvalidArgumentException('quantity');
        }

        \Bitrix\Main\Loader::includeModule("catalog");

        $r = \Bitrix\Catalog\Product\Basket::addProduct([
            'PRODUCT_ID' => $data['id'],
            'QUANTITY' => $data['quantity'],
        ]);


        if (!$r->isSuccess()) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/basket_product_add_error.txt', $r->getErrorMessages().PHP_EOL, FILE_APPEND);
        }

        try {
            $gtmAddToCartEvent = $price = null;

            $price = \Bitrix\Catalog\PriceTable::query()
                ->setSelect(['PRICE'])
                ->setFilter(['=PRODUCT_ID' => (int)$data['id'], 'CATALOG_GROUP_ID' => 3])
                ->fetch()['PRICE'];

            $offer = Offer::getEntity()::query()
                ->setSelect([
                    'NAME', 'CML2_LINK', 'SIZE'
                ])
                ->setFilter([
                    '=ID' => $data['id'],
                ])->fetchObject();


            $cml2link  = $offer->getCml2Link()->getValue();

            $product = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_IBLOCK_ID)->getEntityDataClass()::query()
                ->setSelect(['IBLOCK_SECTION'])
                ->setFilter(['=ID' => $cml2link])
                ->fetchObject();

            $sectionName = trim($product->getIblockSection()->getName());

            $gtmAddToCartEvent = [
                'ecommerce' => [
                    'items' => [
                        'item_id' => $data['id'],
                        'item_name' => $offer->getName(),
                        'item_category' => $sectionName,
                        'quantity' => (int)$data['quantity'],
                        'variant' => $offer->getSize()->getValue(),
                        'price' => (float)$price,
                    ],
                    'value' => (float)$price,
                    'currency' => 'RUB',
                ],
            ];
        } catch (Error | ErrorException | Exception $e) {
            //
        }

        return [
            'success' => true,
            'price'   => $price,
            'gtmAddToCartEvent' => $gtmAddToCartEvent,
        ];
    }
}