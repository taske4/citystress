<?php

namespace Citystress\Api;
use Bitrix\Main\Mail\Event;

class Oneclick
{
    public static function buy(array $data)
    {
        $mailParams = [];

        foreach($data['formData'] as $row) {
            $mailParams[strtoupper($row['name'])] = $row['value'];
        }

        $mailParamsTmp = array_filter($mailParams, fn($value) => !is_null($value) && $value !== '');
        $diff = array_diff($mailParams, $mailParamsTmp);

        if (count($diff)) {
            return [
                'success' => false,
                'message' => 'Пожалуйста укажите ваше имя, телефон, и выберите цвет размер товара',
            ];
        }

        $offerId     = $data['offerId'];
        $productCode = $data['productCode'];

        $mailParams['OFFER_PAGE'] = "/catalog/product/$productCode/?offerId=$offerId";

        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/test.oneclick.email.json', json_encode($mailParams));

        $eventFields = array(
            "EVENT_NAME" => "ONECLICK", // Тип почтового события
            "LID" => "s1", // Сайт (может быть другим)
            "C_FIELDS" => $mailParams,
        );

        $res = Event::send($eventFields);

        return ['success' => $res->isSuccess()];
//        if (!$data['locationId']) {
//            throw new \InvalidArgumentException('LocationId is required.');
//        }
//
//        if (regionality()->setCity($data['locationId'])) {
//            return ['success' => true];
//        } else {
//            throw new \Exception('Ошибка по городу');
//        }
    }
}