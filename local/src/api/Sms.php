<?php

namespace Citystress\Api;

use Bitrix\Main\UserTable;

class Sms
{
    public static function forgotpasswd(array $data)
    {
        if (!$data['phone']) {
            throw new \InvalidArgumentException('phone');
        }

        if(!($phone = formatToCustomFormat($data['phone'])) || (strlen($phone) !== 11)) {
            throw new \InvalidArgumentException('Укажите номер телефона правильно.');
        }

        $user = UserTable::query()
            ->setSelect(['ID', 'LOGIN'])
            ->setFilter([
                '=PERSONAL_PHONE' => $phone
            ])
            ->fetch();

        if (!$user) {
            throw new \Exception('Нет пользователя с таким номером');
        }

        $sms = new \Citystress\Sms();
        $sms->forgotpasswd($phone);

        $_SESSION['USER_ID'] = $user['ID'];

        return ['success' => true];
    }

    public static function forgotpasswdAgain()
    {
        $sms = new \Citystress\Sms();

        if ($sms->forgotpasswdSendAgain()) {
            return ['success' => true];
        } else {
            throw new \Exception('Не удалось повторно отправить смс код');
        }
    }
}