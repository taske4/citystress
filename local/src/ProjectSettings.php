<?php

namespace Citystress;

use Bitrix\Main\Loader;
use Citystress\Entity\Offer;
use Citystress\Helper\Offers;

class ProjectSettings
{
    public static ProjectSettings $instance;

    private function __construct()
    {
        Loader::includeModule("iblock");
    }

    private function getProp(string $propName)
    {
        return \Bitrix\Iblock\Iblock::wakeUp(PROJECT_SETTINGS_IBLOCK_ID)->getEntityDataClass()::query()
            ->setSelect([$propName.'_' => $propName])
            ->setFilter([
                'CODE' => PROJECT_SETTINGS_ELEMENT_CODE,
            ])
            ->setLimit(1)
            ->fetch()[$propName.'_VALUE'];
    }

    private function getMultiplyProp(string $propName) {
        $res = \Bitrix\Iblock\Iblock::wakeUp(PROJECT_SETTINGS_IBLOCK_ID)->getEntityDataClass()::query()
            ->setSelect([$propName.'_' => $propName])
            ->setFilter([
                'CODE' => PROJECT_SETTINGS_ELEMENT_CODE,
            ])
            ->fetchAll();

        return array_column($res, $propName.'_VALUE');
    }

    public function getBlockNewProducts()
    {
        return $this->getBlockProducts('BLOCK_NEW_PRODUCTS');
    }

    public function getBlockHitProducts()
    {
        return $this->getBlockProducts('BLOCK_HIT_PRODUCTS');
    }

    private function getBlockProducts($propName): array
    {
        $values = $this->getMultiplyProp($propName);

        $offersIds = array_map(function($value){
            return (int)$value;
        }, $values);

        $offers = Offer::getEntity()::query()
            ->setSelect(['ID', 'CML2_LINK_' => 'CML2_LINK'])
            ->setFilter(['ID' => $offersIds, 'ACTIVE' => 'Y'])
            ->fetchAll();

        usort($offers, function ($a, $b) use ($offersIds) {
            return array_search($a['ID'], $offersIds) - array_search($b['ID'], $offersIds);
        });

        $result = [];

        foreach($offers as $offer) {
            $result[$offer['CML2_LINK_VALUE']] = $offer['ID'];
        }

        return $result;
    }

    public function getHomeBanner()
    {
        $result = [];

        foreach (['DESKTOP', 'TABLET', 'MOBILE'] as $propPrefix) {
            $value               = $this->getProp('CONTENT_HOME_BANNER_'.$propPrefix);
            $result[$propPrefix] = \CFile::GetPath($value);
        }

        $result['LINK'] = $this->getProp('CONTENT_HOME_BANNER_LINK');

        return $result;
    }

    public function getFooterDesc()
    {
        if ($desc = $this->getProp('CONTENT_FOOTER_DESCRIPTION')) {
            return unserialize($desc)['TEXT'] ?? null;
        }

        return null;
    }

    public function isShowHeaderAction()
    {
        global $APPLICATION;
        return ($APPLICATION->GetCurPage(false) === '/') && ($_SESSION['user']['showHeaderAction'] !== false);
    }
    public function printHeaderAction()
    {
        if (!$this->isShowHeaderAction()) {
            return;
        }

        $fullText  = $this->getProp('NOTIFY_TEXT');
        $shortText = $this->getProp('NOTIFY_SHORT_TEXT');
        $link      = $this->getProp('NOTIFY_LINK');

        $search = [
            '%FULL_TEXT%',
            '%SHORT_TEXT%',
            '%LINK%',
        ];

        $replace = [
            $fullText,
            $shortText,
            $link,
        ];

        echo str_replace($search, $replace, '
            <div class="header_action">
                <div class="header_action__close">
                    <svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#AAA">
                            <rect height="1.66376" rx=".831881"
                                  transform="matrix(.7071 .707114 -.7071 .707114 1.17676 .000061)" width="26.6202"/>
                            <rect height="1.66376" rx=".831881"
                                  transform="matrix(.707099 -.707114 .7071 .707114 0 18.8235)" width="26.6202"/>
                        </g>
                    </svg>
                </div>
                <a href="%LINK%">
                    <span class="header_action__full">%FULL_TEXT%</span>
                    <span class="header_action__short">%SHORT_TEXT%</span>
                </a>
                <svg class="header_action__arrow" width="17" height="9" viewBox="0 0 17 9" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.5487 1L16 4.5M16 4.5L12.5487 8M16 4.5H1" stroke="white" stroke-linecap="round"
                          stroke-linejoin="round"/>
                </svg>
            </div>
        ');
    }

    public static function getInstance()
    {
        return self::$instance ?? self::$instance = new self();
    }
}