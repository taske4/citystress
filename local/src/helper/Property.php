<?php

namespace Citystress\Helper;

class Property
{
    public static function enumGetValueId($propertyCode, $propertyValue, $iblockId = CATALOG_IBLOCK_ID)
    {
        $propertyId = \Bitrix\Iblock\PropertyTable::query()
            ->addSelect('ID')
            ->setFilter(['IBLOCK_ID' => $iblockId, 'CODE' => $propertyCode])
            ->fetch()['ID'] ?: null;

        if (is_null($propertyId)) {
            return false;
        }

        return $isOfferPropValue = \Bitrix\Iblock\PropertyEnumerationTable::query()
            ->setSelect(['ID'])
            ->setFilter(['PROPERTY_ID' => $propertyId, 'VALUE' => $propertyValue])
            ->fetch()['ID'];
    }

    public static function getPropertyId(string $propCode, int $iblockId=CATALOG_IBLOCK_ID)
    {
        return $propId = \Bitrix\Iblock\PropertyTable::query()
            ->addSelect('ID')
            ->setFilter(['IBLOCK_ID' => $iblockId, 'CODE' => $propCode])
            ->fetch()['ID'] ?: null;
    }

    public static function getPropertyValues(array $propertyValues, string $propCode, int $iblockId=CATALOG_IBLOCK_ID)
    {
        $propId = \Bitrix\Iblock\PropertyTable::query()
            ->addSelect('ID')
            ->setFilter(['IBLOCK_ID' => $iblockId, 'CODE' => $propCode])
            ->fetch()['ID'] ?: null;

        return $propertyValues[$propId] ?: null;
    }

    public static function getPropertyValue(array $propertyValues, string $propCode, int $iblockId=CATALOG_IBLOCK_ID)
    {
        $propId = \Bitrix\Iblock\PropertyTable::query()
            ->addSelect('ID')
            ->setFilter(['IBLOCK_ID' => $iblockId, 'CODE' => $propCode])
            ->fetch()['ID'] ?: null;

        return $propertyValues[$propId][0]['VALUE'] ?? $propertyValues[$propId]['n0']['VALUE'] ??
            array_shift($propertyValues[$propId])['VALUE'] ?? null;
    }
}