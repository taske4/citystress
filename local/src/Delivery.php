<?php

namespace Citystress;

use Bitrix\Catalog\StoreProductTable;
use Bitrix\Catalog\StoreTable;

class Delivery
{
    public function calcDeliveryCdek($shopId=null)
    {
        $result = [];

        $date = new \DateTime();
        $regionality = new Regionality();

        $locationId = $regionality->getCurrentCity()['LOCATION_ID'];

        $where = $this->whereProducts([], $locationId, $shopId);

        $cityHasShops = $regionality->cityHasShops();

        if ($where['allProductsOnStore']) {
            $days = 0;
        } else {
            $days = 3;
        }

        /*
         * Складываем там все, и отдаем
         */

        if ($days) {
            $date->add(new \DateInterval("P${days}D"));
        }

        $result['days'] = $days;
        $result['date'] = $date;

        return $result;
    }

    public function calcDeliveryStress($shopId=null)
    {
        $result = [];

        $date = new \DateTime();
        $regionality = new Regionality();

        $locationId = $regionality->getCurrentCity()['LOCATION_ID'];

        $where = $this->whereProducts([], $locationId, $shopId);
 
        // Логика рассчета сроков в екб
        if ((int)$locationId === LOCATION_ID_EKB) {
            if ($where['allProductsInShop']) {
                $days = 0;
            } else {
                $days = 3;
            }
        } elseif ($regionality->cityHasShops()) { // Логика рассчета в города с магазинами cityStress
            if ($where['allProductsInShop']) {
                $days = 0;
            } else if ($where['allProductsOnStore']) {
                $days = 5;
            } else {
                $days = 8;
            }
        } else { // Логики для городов без наших магазинов - нет
            return false;
        }

        /*
         * Складываем там все, и отдаем
         */

        if ($days) {
            $date->add(new \DateInterval("P${days}D"));
        }

        $result['days'] = $days;
        $result['date'] = $date;

        return $result;
    }

    public function whereProducts($productIds=[], $locationId=null, $shopId=null)
    {
        if (!$productIds) {
            \Bitrix\Main\Loader::includeModule('sale');

            $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());

            foreach ($basket as $item) {
                $productIds[] = $item->getProductId();
            }
        }

        if (count($productIds)) {
          //  throw new \Exception('Для рассчета сроков нужен хотя бы один товар.');
       

        if (!$locationId) {
            $locationId = (new Regionality())->getCurrentCity()['LOCATION_ID'];
        }

        /*
         * На складе EKB
         */
        $res = StoreProductTable::query()
            ->setSelect(['AMOUNT', 'PRODUCT_ID'])
            ->setFilter([
                'STORE_ID'   => EKB_STORE_ID,
                'PRODUCT_ID' => $productIds,
            ])
            ->fetchAll();

        $onEkbStore = StoreProductTable::query()
            ->setSelect(['AMOUNT', 'PRODUCT_ID'])
            ->setFilter([
                'STORE_ID'   => EKB_STORE_ID,
                'PRODUCT_ID' => $productIds,
            ])
            ->fetchAll();

        /*
         * В магазинах города
         */

        $shops = \Bitrix\Iblock\Iblock::wakeUp(SHOPS)->getEntityDataClass()::query()
            ->setSelect([
                'ID',
                'XML_ID',
                'PVZ_LOCATION_'  => 'PVZ_LOCATION',
            ])
            ->setFilter([
                'PVZ_LOCATION_VALUE' => (string)$locationId
            ])
            ->fetchAll();

        $shopXmlIds = array_column($shops, 'XML_ID');

        /*
         * Если есть магазины идем дальше
         */
        if ($shopXmlIds) {
            $stores = StoreTable::query()
                ->setSelect(['ID', 'XML_ID'])
                ->setFilter([
                    'XML_ID' => $shopXmlIds,
                ])
                ->fetchAll();

            /*
             * Определяем нужны ли данные по наличию в конкретном магазине
             */
            $onStore_id = false;

            if ($shopId) {
                if (($index = array_search($shopId, array_column($shops, 'ID'))) !== false) {
                    if ($shopXmlId = $shops[$index]['XML_ID']) {
                        if (($index = array_search($shopXmlId, array_column($stores, 'XML_ID'))) !== false) {
              $onStore_id = $stores[$index]['ID'];
							    $onShop = StoreProductTable::query()
            ->setSelect(['AMOUNT', 'PRODUCT_ID'])
            ->setFilter([
                'STORE_ID'   => $onStore_id ,
                'PRODUCT_ID' => $productIds,
            ])
            ->fetchAll();
							
                        }
                    }
                }
            }
            /*
             * end
             */
            $inShops = StoreProductTable::query()
                ->setSelect(['AMOUNT', 'PRODUCT_ID', 'STORE_ID'])
                ->setFilter([
                    'STORE_ID'   => array_column($stores, 'ID'),
                    'PRODUCT_ID' => $productIds,
                ])
                ->fetchAll();
        }

        $result = [
            'products'           => [],
            'allProductsInShop'  => true,
            'allProductsInShops' => true,
            'allProductsOnStore' => true,
        ];
 
        foreach($productIds as $productId) {
            $data = [
                'ekb' => 0,
                'inShops' => 0,
            ];

            if ($shopId) {
                $data['inShop'] = 0;
            }


            if (($index = array_search($productId, array_column($onEkbStore ?: [], 'PRODUCT_ID'))) !== false) {
                $data['ekb'] = (int)$onEkbStore[$index]['AMOUNT'];

                if (!$data['ekb']) {
                    $result['allProductsOnStore'] = false;
                }
            }

            if (($index = array_search($productId, array_column($inShops ?: [], 'PRODUCT_ID'))) !== false) {
                $data['inShops'] = (int)$inShops[$index]['AMOUNT'];

                if (!$data['inShops']) {
                    $result['allProductsInShops'] = false;
                }
            }
 
            if ($onStore_id && (($index = array_search($productId, array_column( $onShop ?: [], 'PRODUCT_ID'))) !== false)) {
                $data['inShop'] = (int)$onShop[$index]['AMOUNT'];
                if (!$data['inShop']) {
                    $result['allProductsInShop'] = false;
                }
            }

            $result['products'][$productId] = $data;
        }

        return $result;
		}
		else
		{
			 return false;
		}
    }
} 