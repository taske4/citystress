<?php

function user()
{
    global $USER;
    return $USER;
}

function formatToCustomFormat($phoneNumber)
{
    // Удалить все символы, кроме цифр
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    // Если номер начинается с 8, заменить на 7 (формат России)
    if (substr($phoneNumber, 0, 1) === '8') {
        $phoneNumber = '7' . substr($phoneNumber, 1);
    }

    // Если номер не начинается с 7, добавить 7 в начало (предполагаем формат России)
    if (substr($phoneNumber, 0, 1) !== '7') {
        $phoneNumber = '7' . $phoneNumber;
    }

    // Если длина номера больше 11 символов, обрезать до 11 символов
    if (strlen($phoneNumber) > 11) {
        $phoneNumber = substr($phoneNumber, 0, 11);
    }

    // Форматировать номер в заданный формат 79648557290
    $formattedNumber = preg_replace('/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/', '$1$2$3$4$5', $phoneNumber);

    return $formattedNumber;
}

