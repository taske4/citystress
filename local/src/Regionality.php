<?php

namespace Citystress;

use \Bitrix\Main\Service\GeoIp;

class Regionality
{
    public function setCity(int $locationId)
    {
        \Bitrix\Main\Loader::includeModule('sale');

        $location = \Bitrix\Sale\Location\LocationTable::query()
            ->setSelect([
                'NAME_RU' => 'NAME.NAME',
                'LANG_ID' => 'NAME.LANGUAGE_ID',
            ])
            ->setFilter([
                '=ID' => $locationId,
                '=LANG_ID' => 'ru',
            ])
            ->fetch();

        if ($location) {
            $_SESSION['CITY'] = [
                'LOCATION_ID' => $locationId,
                'NAME' => $location['NAME_RU'],
            ];

            $coords = $this->getcoords($location['NAME_RU']);

            $_SESSION['LOCATION_LONGITUDE'] = $coords['lon'];
            $_SESSION['LOCATION_LATITUDE'] = $coords['lat'];
            return true;
        }

        return false;
    }

    public function cityHasShops()
    {
        $locationId = $this->getCurrentCity()['LOCATION_ID'];

        return (bool)\Bitrix\Iblock\Iblock::wakeUp(SHOPS)->getEntityDataClass()::query()
            ->setSelect([
                'ID',
                'XML_ID',
                'PVZ_LOCATION_'  => 'PVZ_LOCATION',
            ])
            ->setFilter([
                'PVZ_LOCATION_VALUE' => (string)$locationId
            ])
            ->setLimit(1)
            ->fetch();
    }

    public function setDefaultCity()
    {
        $_SESSION['CITY'] = [
            'LOCATION_ID' => 2203,
            'NAME'        => 'Екатеринбург',
        ];
	//	$coords =	$this->getcoords('Екатеринбург');
			$_SESSION['LOCATION_LONGITUDE'] = '60.6054911';
			$_SESSION['LOCATION_LATITUDE'] = '56.8385216';
    }
    public function getCurrentCity()
    {
        if (!$_SESSION['CITY']) {
            $this->detectCity();
        }

        return $_SESSION['CITY'];
    }
    public function getCitiesList()
    {
        \Bitrix\Main\Loader::includeModule('iblock');

        $entity = \Bitrix\Iblock\Iblock::wakeUp(CITIES_IBLOCK_ID)->getEntityDataClass();
        $cities = $entity::query()
            ->setSelect([
                'NAME',
                'LOCATION_ID_' => 'LOCATION_ID',
            ])
            ->setFilter([
                'ACTIVE' => 'Y',
                '!LOCATION_ID_VALUE' => null,
            ])
            ->fetchAll();

//        $locations = array_column($cities, 'LOCATION_ID_VALUE');

        return $cities;
    }

    public function detectCity()
    {
        try {
            if (!$_SESSION['CITY']) {
                $ipAddress = GeoIp\Manager::getRealIp();

                if ($ipAddress) {
                    $locationId = null;
                    $result     = GeoIp\Manager::getDataResult($ipAddress, "ru");
                    $cityName   = $result->getGeoData()->cityName;
                    $locationId = $result->getGeoData()->locationId;

                     if (!$locationId && $cityName) {
                         \Bitrix\Main\Loader::includeModule('sale');

                         $locationId = \Bitrix\Sale\Location\LocationTable::query()
                             ->setSelect(['ID', 'NAME_RU' => 'NAME.NAME'])
                             ->setFilter(['NAME_RU' => $cityName])
                             ->fetch()['ID'];
                     }

                    if (!$locationId) {
                        throw new \Exception('Похоже запросы кончились');
                    } else {
                        $this->setCity($locationId);
                    }
                }
            }
        } catch (\Error | \Exception $e) {
            $this->setDefaultCity();
        }
    }
	   public function getcoords($city)
    {
		global $APPLICATION;
		if(!preg_match('/(jpg)|(media)|(xml)|(yml)|(php)|(txt)|(png)|(css)|(JPG)/',$APPLICATION->GetCurPage()) && !defined("ERROR_404") && !preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT'])){
		  file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/dadata.log', $APPLICATION->GetCurPage().' '.$_SERVER['HTTP_USER_AGENT'] . PHP_EOL, FILE_APPEND);
		$dadata = new \Dadata\DadataClient(dadata_token,dadata_secret_key);
		$result = $dadata->suggest("address",$city);
		if($result[0]["data"]['geo_lat']!=NULL){
		return ['lat'=> $result[0]["data"]['geo_lat'], 'lon' => $result[0]["data"]['geo_lon']];
		}
		else
		{
			return ['lat'=> $result[1]["data"]['geo_lat'], 'lon' => $result[1]["data"]['geo_lon']];
		}
		}
			
	}
	 public function getclues($query)
    {
		global $APPLICATION;

		$dadata = new \Dadata\DadataClient(dadata_token,dadata_secret_key);
		$result = $dadata->suggest("address",$query,5,["locations"=>[["city"=>$_SESSION['CITY']['NAME']]]]);
		foreach($result as $res)
		{
			$result_array[] = ['full_adress' => $res['unrestricted_value'], 'street'=> $res["data"]['street'], 'house' => $res["data"]['house'],'type'=>$res["data"]["settlement_with_type"],"street_type"=>$res["data"]["street_type"],'house_type'=>$res["data"]["house_type"]];
		}
		return $result_array;
	}
	 public function getcity($query)
    {

	 	$dadata = new \Dadata\DadataClient(dadata_token,dadata_secret_key);
		$result = $dadata->suggest("address",$query,1);
 
		return $result[0]['data']["city"]; 
	}

}