<?php

namespace Citystress;

use Bitrix\Main\UserTable;

class Sms
{
    public function forgotpasswd(string $phone)
    {
        $code = $_SESSION['SMS_CODE'] = $this->genCode();
        \Citystress\Connector\Smsc::send($phone, 'Код восстановления пароля для сайта citystress: ' . $code);
    }

    public function forgotpasswdSendAgain() {
        if ($_SESSION['SMS_CODE'] && $_SESSION['USER_ID']) {
            $res = UserTable::query()
                ->addSelect('PERSONAL_PHONE')
                ->addFilter('=ID', $_SESSION['USER_ID'])
                ->fetch();

            if ($res && $res['PERSONAL_PHONE']) {
                \Citystress\Connector\Smsc::send($res['PERSONAL_PHONE'], 'Код восстановления пароля для сайта citystress: '.$_SESSION['SMS_CODE']);
                return true;
            }
        }

        return false;
    }

    protected function genCode()
    {
        return rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    }

    public function checkSmsCode($code)
    {
        if ($code === $_SESSION['SMS_CODE']) {
            $login = $_SESSION['USER_ID'];
            unset($_SESSION['USER_ID']);
            unset($_SESSION['SMS_CODE']);
            return $login;
        } else {
            return false;
        }
    }
}