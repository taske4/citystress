<?php


namespace Citystress\Connector\tc\cdek;

use Bitrix\Main\ArgumentNullException;
use Citystress\Delivery;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use TransportCompanies\BaseAdapter;

class Adapter extends \Citystress\Connector\tc\BaseAdapter
{
    private const TC_TYPE_PVZ_ID = '1481'; // TYPE_PVZ - CDEK - ID
    public const TC_TYPE_PVZ_VALUE = 'Cdek'; // TYPE_PVZ
    private const TARIFF_PVZ_CODE = '136'; // ПВЗ Посылка склад-склад до 30 кг
    private const TARIFF_COURIER_CODE = '137'; // Посылка склад-дверь до 30кг

    public static function getZpvdPvz(int $locationId, int $serviceId, $type = self::TC_TYPE_PVZ_VALUE)
    {
        return self::getPickupPoints($type, $locationId, $serviceId);
    }

    public static function getLocationByCityName($city)
    {
        return  self::getLocationByCity($city);
    }

    public static function savePvz()
    {
        global $log;

        try {
            $exceptionCodes = [
                0 => 'Нет пвз'
            ];

            $arPvz    = self::getTcPvz();
            $arZpvPvz = self::getPickupPointsForSave(self::TC_TYPE_PVZ_VALUE);
            $arDeliveryInfo = array();

            if ((!is_array($arPvz)) || (count($arPvz) === 0))
                throw new \Exception($exceptionCodes[0]);

            $limit = 1000;
            foreach ($arPvz as $pvz) {
                if (!$limit--) {
                    break;
                }

                $cityCode = $pvz['location']['city_code'];

                if(isset($arDeliveryInfo[$cityCode])) {
                    $deliveryInfo = $arDeliveryInfo[$cityCode];
                } else {
                    $deliveryInfo = Connector::calculate(
                        self::TARIFF_PVZ_CODE,
                        self::FROM_PLACE,
                        $cityCode,
                        '',
                        '',
                        self::DEFAULT_WEIGHT
                    );

                    if (isset($deliveryInfo['errors']) && self::DEBUGGING) {
                        $log->alert('Ошибка - '.$deliveryInfo['errors']['message'].' адрес - '.$pvz['location']['address_full']);
                    }

                    $arDeliveryInfo[$cityCode] = $deliveryInfo;
                }

                $locationId = self::getLocationByCity($pvz['location']['city'], $pvz['location']['region']);

                if (!$locationId || self::isRC($locationId)) {
                    if(self::DEBUGGING)
                        $unknownPvzCount++;

                    continue;
                }

                \CModule::IncludeModule('iblock');
                $el = new \CIBlockElement;

                $PROP = self::getDefaultProp(
                    $pvz['code'],
                    $locationId,
                    $pvz['location']['latitude'],
                    $pvz['location']['longitude'],
                    self::TC_TYPE_PVZ_ID,
                    $pvz['phones'][0]['number'],
                    $deliveryInfo['total_sum'] ?: '',
                    $deliveryInfo['calendar_min'] ?: '',
                    $pvz['work_time']
                );

                $PROP['CDEK_POSTCODE'] = $pvz['location']['postal_code'];

                $previewText  = '';
                $previewText .= $pvz['location']['address_full'].PHP_EOL;

                if (count($pvz['phones']) > 0 ) {
                    foreach($pvz['phones'] as $arPhone) {
                        $previewText .= PHP_EOL.'тел.'.$arPhone['number'].PHP_EOL;
                    }
                } elseif($pvz['phones'][0]['number']) {
                    $previewText .= PHP_EOL.'тел.'.$pvz['phones'][0]['number'].PHP_EOL;
                }

                foreach ($pvz['work_time_list'] as $arItem) {
                    $previewText.=PHP_EOL.self::formatDay($arItem['day']).PHP_EOL;
                    $previewText.='С '.self::formatTime($arItem['time']).PHP_EOL;
                }

                $arLoadProductArray = Array(
                    "NAME"              => $pvz['location']['address_full'],
                    "ACTIVE_FROM"       => date('d.m.Y H:i:s'),
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID"         => self::ZPV_STORE_IBLOCK_ID,
                    "ACTIVE"            => "Y",
                    "PREVIEW_TEXT"      => $previewText,
                    "PROPERTY_VALUES"   => $PROP,
                );

                // update
                if ($arZpvPvz[$pvz['code']]) {
                    if($el->Update($arZpvPvz[$pvz['code']]['ID'], $arLoadProductArray)) {
                        if(self::DEBUGGING) {
                            $log->alert('Элемент с ID ' . $arZpvPvz[$pvz['code']]['ID'] . ' Обновлен');
                            $updatedPvzCount++;
                        }
                    } elseif(self::DEBUGGING) {
                        $log->error('Элемент с ID '.$arZpvPvz[$pvz['code']]['ID'].' Не обновлен: '.$el->LAST_ERROR);
                    }

                    unset($arZpvPvz[$pvz['code']]);
                } else {
                    if($newElement = $el->Add($arLoadProductArray)) {
                        if(self::DEBUGGING) {
                            $log->alert('ID Нового элемента: ' . $newElement);
                            $addedPvzCount++;
                        }
                    } elseif(self::DEBUGGING) {
                        $log->error('Элемент не создан: '.$el->LAST_ERROR);
                    }
                }
            }

            foreach ($arZpvPvz as $arItem) {
                \CIBlockElement::Delete($arItem['ID']);
                if(self::DEBUGGING)
                    $removedPvzCount++;
            }

        } catch (\Exception $e) {
            var_dump($e->getMessage());
            if(self::DEBUGGING)
                $log->critical($e->getMessage());
        } finally {
            if(self::DEBUGGING) {
                $log->debug('Обновлено      pvz: ' . $updatedPvzCount);
                $log->debug('Добавлено      pvz: ' . $addedPvzCount);
                $log->debug('Удалено        pvz: ' . $removedPvzCount);
                $log->debug('Неопределенных pvz: ' . $unknownPvzCount);
                $log->debug('Всего API ТК   pvz: ' . count($arPvz));
                $log->close();
            }
        }
    }

    private static function formatDay($day)
    {
        $days = array( 1 => "Понедельник" , "Вторник" , "Среда" , "Четверг" , "Пятница" , "Суббота" , "Воскресенье" );
        return $days[$day];
    }

    private static function formatTime($time)
    {
        return str_replace('/',' До ', $time);
    }

    private static function getPostalCode($locationId)
    {
        $res = \Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array(
                'EXTERNAL.SERVICE.CODE' => array('ZIP_LOWER', 'ZIP'),
                '=ID' => $locationId
            ),
            'select' => array(
                'EXTERNAL.*',
                'EXTERNAL.SERVICE.CODE'
            )
        ));

        return $res->fetch()['SALE_LOCATION_LOCATION_EXTERNAL_XML_ID'];
    }

    protected static function getTcPvz()
    {
        return Connector::getPvz(array(
            'type' => self::DELIVERY_TYPE,
            'country_code' => self::COUNTRY_CODE
        ));
    }

    /**
     * @throws ArgumentNullException
     */
    public static function getPvzDeliveryInfo(\stdClass $params): array
    {
        $pvzId = (int)$params->pvzId;
        $weight = (int)$params->weight;

        if (!$pvzId)
            throw new ArgumentNullException('pvzId');
        if (!$weight)
            throw new ArgumentNullException('weight');
        $postalCode = self::getPvzPropertyValue(self::PROP_ZIP, $pvzId);
        $pvzAddress = self::getPvzName($pvzId);
        $arRes = array();
        if ($postalCode) {
            try {
                $arRes = Connector::calculate(
                    self::TARIFF_PVZ_CODE,
                    self::FROM_PLACE,
                    '',
                    $postalCode,
                    '',
                    $weight
                );
            } catch (\ErrorException $e) {
                $arRes = Connector::calculate(
                    self::TARIFF_PVZ_CODE,
                    self::FROM_PLACE,
                    '',
                    '',
                    $pvzAddress,
                    $weight
                );
            }
        }

        $stressMore = (new Delivery())->calcDeliveryCdek($pvzId);

        $arRes['calendar_min'] += $stressMore['days'];
        $arRes['calendar_max'] += $stressMore['days'];


        return self::getData($arRes['calendar_min'], $arRes['calendar_max'], $arRes['delivery_sum'], $pvzId);
    }

    /**
     * @throws ArgumentNullException
     */
    public static function getCourierDeliveryInfo(\stdClass $params): array
    {
        $locationId = (int) $params->locationId;
        $street     = (string) $params->street;
        $house      = (string) $params->house;
        $weight     = (int) $params->weight;

        if(!$locationId)
            throw new ArgumentNullException('locationId');
        if(!$street)
            throw new ArgumentNullException('street');
        if(!$house)
            throw new ArgumentNullException('house');
        if(!$weight)
            throw new ArgumentNullException('weight');

        $city = self::getCityByLocation($locationId);

        $address = implode(', ', array($city, $street, $house));

        $arRes = array();
        if ($city) {
            $arRes = Connector::calculate(
                self::TARIFF_COURIER_CODE,
                self::FROM_PLACE,
                '',
                '',
                $address,
                $weight
            );
        }

        $stressMore = (new Delivery())->calcDeliveryCdek();

        $arRes['calendar_min'] += $stressMore['days'];
        $arRes['calendar_max'] += $stressMore['days'];

        return self::getData($arRes['calendar_min'], $arRes['calendar_max'], $arRes['delivery_sum']);
    }

    public static function testPvzLocation()
    {
        $arPvz = self::getTcPvz();
        $log = new Logger('name');
        $log->pushHandler(new StreamHandler($_SERVER['DOCUMENT_ROOT'].self::LOG_PATH.'cdek/log.txt', Logger::DEBUG));
        $log->alert('///////////////////// Начало отладки местоположения /////////////////////');

        foreach ($arPvz as $pvz) {

            $formatCityName = self::formatLocationName($pvz['location']['city']);
            $formatRegionName = self::formatRegionName($pvz['location']['region']);
            $locationId = self::getLocationByCity($pvz['location']['city'], $pvz['location']['region']);

            if(!$locationId)
                $log->debug('; '.$pvz['location']['city'] .'; '. $formatCityName.'; '.$pvz['location']['region'] .'; '. $formatRegionName.'; ');

        }

        $log->alert('///////////////////// Конец отладки местоположения /////////////////////');
        $log->close();
    }
}
