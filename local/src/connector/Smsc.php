<?php

namespace Citystress\Connector;

class Smsc
{
    const LOGIN = 'soloviovaLI';
    const PASSWD = 'SoloLI2022URaL';

    public static function send(string $phones, string $message)
    {
        $uri = 'https://smsc.ru/sys/send.php?login='.self::LOGIN.'&psw='.self::PASSWD.'&phones='.$phones.'&mes='.$message;
        $res = file_get_contents($uri);
        if (strpos($res, 'OK -') !== false) {
            return true;
        } elseif (strpos($res, 'ERROR = 9') !== false) {
            throw new \Exception('Пожалуйста попробуйте повторить через минуту.');
        } else {
            throw new \Exception('Попробуйте позже, приносим свои извенения.');
        }
    }
}