<?php


namespace Citystress;

use Bitrix\Main\Loader;


use \Bitrix\Highloadblock as HL;

class Favorites
{
    const HL_FAVORITES = 4;

    public static Favorites $instance;

    private $dataClass;

    private function __construct()
    {
        Loader::includeModule("highloadblock");

        $hlblock = HL\HighloadBlockTable::getById(self::HL_FAVORITES)->fetch();
        $entity  = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->dataClass = $entity->getDataClass();
    }

    public static function getInstance()
    {
        return self::$instance ?? self::$instance = new self();
    }

    public function remove($userId, array $productId)
    {
        if (!$userId) {

            foreach($productId as $id) {
                $index = array_search($id, $_SESSION['FAVORITES']);

                if ($index !== false) {
                    unset($_SESSION['FAVORITES'][$index]);
                }
            }

           return true;
        }

        $res = $this->dataClass::query()
            ->setFilter([
                'UF_USER_ID' => $userId,
                'UF_PRODUCT' => $productId
            ])
            ->setSelect(['ID'])
            ->fetchAll();

        foreach($res as $row) {
            $this->dataClass::delete($row['ID']);
        }

        return true;
    }

    public function add($userId, int $productId)
    {
        if (!$userId) {
            $_SESSION['FAVORITES'][] = $productId;
            return true;
        }

        $res = $this->dataClass::add([
            'UF_USER_ID' => $userId,
            'UF_PRODUCT' => $productId,
        ]);

        return $res;
    }

    public function get($userId)
    {
        if (!$userId) {
            return $_SESSION['FAVORITES'];
        }

        $res = $this->dataClass::getList(array(
            "select" => ['UF_PRODUCT'],
            "filter" => [
                'UF_USER_ID' => $userId
            ],
        ))->fetchAll();

        return array_column($res, 'UF_PRODUCT');
    }
}