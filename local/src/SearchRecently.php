<?php

namespace Citystress;

use Bitrix\Main\Loader;


use \Bitrix\Highloadblock as HL;

class SearchRecently
{
    public static SearchRecently $instance;

    private $dataClass;

    const LIMIT = 6;
    const SESSION_NAME = 'SEARCH_RECENTLY';

    private function __construct()
    {
        Loader::includeModule("highloadblock");

        $hlblock = HL\HighloadBlockTable::getById(HL_SEARCH_RECENTLY)->fetch();
        $entity  = HL\HighloadBlockTable::compileEntity($hlblock);
        $this->dataClass = $entity->getDataClass();
    }

    public static function getInstance()
    {
        return self::$instance ?? self::$instance = new self();
    }
    public function get()
    {
        if ($GLOBALS['USER']->IsAuthorized()) {
            $res = $this->dataClass::query()
                ->setOrder(['ID' => 'DESC'])
                ->setSelect([
                    'UF_TEXT',
                ])
                ->setFilter([
                    'UF_USER_ID' => $GLOBALS['USER']->GetID(),
                ])
                ->fetchAll();

            return array_column($res, 'UF_TEXT');
        } else {
            return $_SESSION[self::SESSION_NAME] ?? [];
        }
    }

    /*
     * Может и write лучше, или я не знаю)
     */
    public function record(String $text)
    {
        if ($GLOBALS['USER']->IsAuthorized()) {

            $res = $this->getList();

            if (count($res) === self::LIMIT) {
                $this->dataClass::delete($res[0]['ID']);
            }

            $this->dataClass::add([
                'UF_USER_ID' => $GLOBALS['USER']->GetID(),
                'UF_TEXT'    => $text,
            ]);
        } else {
            if (count($_SESSION[self::SESSION_NAME] ?: []) > 5) {
                array_shift($_SESSION[self::SESSION_NAME]);
            }

            $_SESSION[self::SESSION_NAME][] = $text;
        }
    }

    private function getList()
    {
        return $this->dataClass::query()
            ->setOrder(['ID' => 'ASC'])
            ->setSelect(['ID'])
            ->setFilter(['UF_USER_ID' => $GLOBALS['USER']->GetID()])
            ->fetchAll();
    }

    public function merge()
    {
        if (!$GLOBALS['USER']->IsAuthorized()) {
            return false;
        }

        $searchRecently = $_SESSION[self::SESSION_NAME];

        if (is_array($searchRecently) && count($searchRecently)) {
            $hasList = $this->getList();

            $totalCount = count($hasList) + count($searchRecently);

            if ($totalCount > self::LIMIT) {

                $i = $totalCount - self::LIMIT;
                foreach(array_column($hasList, 'ID') as $rowId) {
                    if ($i--) {
                        $this->dataClass::delete($rowId);
                    } else {
                        break;
                    }
                }
            }

            foreach($searchRecently as $text) {
                $this->record($text);
            }
        }

        unset($_SESSION[self::SESSION_NAME]);

        return true;
    }
}