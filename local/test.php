<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");

//require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/dev2fun.imagecompress/console/optimize.php';
//require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/dev2fun.imagecompress/console/convert.php';

global $USER;

$USER->Authorize(1);
die;