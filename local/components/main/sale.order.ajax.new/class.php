<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use
    Bitrix\Main\Config\Option,
    Bitrix\Main\Localization\Loc,
    Bitrix\Sale,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Sale\Result,
    Bitrix\Sale\Delivery,
    Bitrix\Catalog\Model\Product,
    Bitrix\Main\Loader,
    Bitrix\Highloadblock as HL,
    Bitrix\Catalog\StoreProductTable,
    Bitrix\Main\Entity;

Loader::includeModule("highloadblock");

/**
 * @var $APPLICATION CMain
 * @var $USER CUser
 */

CBitrixComponent::includeComponentClass('bitrix:sale.order.ajax');

class SaleOrderAjaxNew extends SaleOrderAjax
{

    protected function changeQuantityAction()
    {

        $productID = $this->request->get('productID');
        $quantity = $this->request->get('basketItemQuantity');
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());//$this->order->getBasket();

        $item = false;
        foreach ($basket as $basketItem) {
            if ($basketItem->getField('PRODUCT_ID') == $productID) {
                $item = $basketItem;
                break;
            }
        }

        if ($item) {
            if ($quantity > 0) {
                $item->setField('QUANTITY', $quantity);
            } else {
                $item->delete();
            }

            $basket->save();
        }

        if ($productID == 0) {
            foreach ($basket as $basketItem) {
                $basketItem->delete();
            }
            $basket->save();
        }

        $this->refreshOrderAjaxAction();

    }

    protected function addCouponAction()
    {
        $coupon = $this->request->get('coupon');
        $couponinfo = \Bitrix\Sale\DiscountCouponsManager::getData($coupon, true);
        if ($couponinfo['ACTIVE'] == "Y") {
            \Bitrix\Sale\DiscountCouponsManager::add($coupon);
        }
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        $Discounts = \Bitrix\Sale\Discount::loadByBasket($basket);
        $basket->refreshData(['PRICE', 'COUPONS']);
        $Discounts->calculate();
        $result = $Discounts->getApplyResult();

        $basket->save();

        $this->refreshOrderAjaxAction();

    }

    protected function deleteCouponAction()
    {
        $coupon = $this->request->get('coupon');
        $couponinfo = \Bitrix\Sale\DiscountCouponsManager::getData($coupon, true);
        if ($couponinfo['ACTIVE'] == "Y") {
            \Bitrix\Sale\DiscountCouponsManager::delete($coupon);
        }
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        $Discounts = \Bitrix\Sale\Discount::loadByBasket($basket);
        $basket->refreshData(['PRICE', 'COUPONS']);
        $Discounts->calculate();
        $result = $Discounts->getApplyResult();

        $basket->save();

        $this->refreshOrderAjaxAction();

    }

    protected function initBasket(\Bitrix\Sale\Order $order)
    {


        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());

        $update = false;
        foreach ($basket as $basketItem) {
            if ($basketItem->getField('DELAY') == 'Y' || intval($basketItem->getQuantity()) == 0) {
                $basketItem->delete();
                $update = true;
            }
        }
        if ($update)
            $basket->save();


        parent::initBasket($order);

    }

    protected function obtainPropertiesForIbElements()
    {
        CModule::IncludeModule("catalog");
        parent::obtainPropertiesForIbElements();
        $hlbl = HL_COLORS;
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();


        foreach ($this->arResult["GRID"]["ROWS"] as $key => $item) {
            $intElementID = $item['data']["PRODUCT_ID"]; // ID предложения
            $mxResult = CCatalogSku::GetProductInfo(
                $intElementID
            );
            if (is_array($mxResult)) {
                $iblock_id = $arParams['IBLOCK_OFFERS_ID'];
            }

            $arSelect = array("IBLOCK_ID", "ID", "PROPERTY_ARTICLE");
            $arFilter = array("IBLOCK_ID" => $iblock_id, "ID" => $item['data']["PRODUCT_ID"], "ACTIVE" => "Y");
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arProps = $ob->GetProperties();
                $this->arResult["GRID"]["ROWS"][$key]['data']['ARTICLE'] = $arProps["CML2_ARTICLE"]['VALUE'];
                $this->arResult["GRID"]["ROWS"][$key]['data']['RAZMER'] = $arProps["RAZMER"]['VALUE'];
                $this->arResult["GRID"]["ROWS"][$key]['data']['TSVET'] = $arProps["TSVET"]['VALUE'];

                if($linkSale = $arProps['LINK_SALE']['VALUE']) {
                    $sale   = \Bitrix\Iblock\Iblock::wakeUp(SALES_IBLOCK_ID)->getEntityDataClass()::query()
                        ->setSelect(['NAME', 'PERCENT_' => 'PERCENT', 'COUPON_' => 'COUPON'])
                        ->setFilter(['ID' => $linkSale])
                        ->fetch();

                    $this->arResult["GRID"]["ROWS"][$key]['data']['SALE'] = $sale;
                }


                if (isset($arProps["MORE_PHOTO"]['VALUE'][0])) {
                    $path = CFile::ResizeImageGet($arProps["MORE_PHOTO"]['VALUE'][0], array('width' => 190, 'height' => 230), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    $mini_path = CFile::ResizeImageGet($arProps["MORE_PHOTO"]['VALUE'][0], array('width' => 32, 'height' => 32), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    $this->arResult["GRID"]["ROWS"][$key]['data']['PHOTO'] = $path['src'];
                    $this->arResult['JS_DATA']["MINI_IMG"][$item['data']["PRODUCT_ID"]] = $mini_path['src'];
                }
                $rsData = $entity_data_class::getList(array(
                    "select" => array("*"),
                    "order" => array("ID" => "ASC"),
                    "filter" => array("UF_XML_ID" => $arProps["TSVET"]['VALUE'])
                ));

                while ($arData = $rsData->Fetch()) {
                    $this->arResult["GRID"]["ROWS"][$key]['data']['HASH'] = $arData["UF_HASH"];

                }

            }
            $mxResult = CCatalogSku::GetProductInfo(
                $item['data']["PRODUCT_ID"]
            );

            $this->arResult["GRID"]["ROWS"][$key]['data']['ELEMENT_ID'] = $mxResult['ID'];

            $ar_res = CCatalogProduct::GetByID($item['data']["PRODUCT_ID"]);

            $this->arResult["GRID"]["ROWS"][$key]['data']['AVAILABLE'] = $ar_res['QUANTITY'];


        }
        global $USER;
        if ($USER->IsAuthorized()) {
            $user_id = $USER->GetID();
            $rsUser = CUser::GetByID($user_id);
            $arUser = $rsUser->Fetch();
            $this->arResult['JS_DATA']['USER'] =
                [
                    'NAME' => $arUser['NAME'],
                    'MAIL' => $arUser['EMAIL'],
                    'PHONE' => $arUser['PERSONAL_PHONE']
                ];

        }
        $region = regionality()->getCurrentCity();
        $this->arResult['JS_DATA']['LOCATION_ID'] = $region["LOCATION_ID"];
        $this->arResult['JS_DATA']['LOCATION_NAME'] = $region["NAME"];

        if (!$_SESSION['LOCATION_LONGITUDE']) {
            $coords = regionality()->getcoords($_SESSION['CITY']['NAME']);
            $_SESSION['LOCATION_LONGITUDE'] = $coords['lon'];
            $_SESSION['LOCATION_LATITUDE'] = $coords['lat'];
        }
        $this->arResult['JS_DATA']['LOCATION_LONGITUDE'] = $_SESSION['LOCATION_LONGITUDE'];
        $this->arResult['JS_DATA']['LOCATION_LATITUDE'] = $_SESSION['LOCATION_LATITUDE'];


    }

    protected function obtainDelivery()
    {
        parent::obtainDelivery();
        if (!empty($this->arDeliveryServiceAll)) {
            $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());

            foreach ($basket as $item) {
                $productIds[] = $item->getProductId();
            }

            foreach ($this->arDeliveryServiceAll as $deliveryObj) {
                $config = $deliveryObj->getConfig()["MAIN"]["ITEMS"];
                $this->arResult["DELIVERY"][$deliveryObj->getId()]['TYPE'] = $config['TYPE']["VALUE"];
                $this->arResult["DELIVERY"][$deliveryObj->getId()]['DEFAULT_PRICE'] = $config["PRICE"]["VALUE"];
                $shops = \Citystress\Connector\tc\cdek\Adapter::getZpvdPvz($_SESSION["CITY"]["LOCATION_ID"], 1, 'CityStress');
                $ekb_shops = \Citystress\Connector\tc\cdek\Adapter::getZpvdPvz(2203, 1, 'CityStress');
                $products = [];

                foreach ($shops as $shop) {

                    foreach ($productIds as $key => $id) {

                        if (!in_array($id, $products)) {
                            $rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList(array(
                                'filter' => array('=PRODUCT_ID' => $id, '=STORE.XML_ID' => $shop['XML_ID']),
                                'select' => array('AMOUNT', 'STORE_ID', 'XML_ID' => 'STORE.XML_ID'),
                            ));
                            while ($arStoreProduct = $rsStoreProduct->fetch()) {


                                if ($arStoreProduct['AMOUNT'] > 0) {


                                    $products[] = $id;

                                }
                            }
                        }
                    }

                }

                if (count($productIds ?: []) > count($products ?: [])) {
                    foreach ($ekb_shops as $ekb_shop) {

                        foreach ($productIds as $key => $id) {

                            if (!in_array($id, $products)) {
                                $rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList(array(
                                    'filter' => array('=PRODUCT_ID' => $id, '=STORE.XML_ID' => $ekb_shop['XML_ID']),
                                    'select' => array('AMOUNT', 'STORE_ID', 'XML_ID' => 'STORE.XML_ID'),
                                ));
                                while ($arStoreProduct = $rsStoreProduct->fetch()) {


                                    if ($arStoreProduct['AMOUNT'] > 0) {


                                        $products[] = $id;

                                    }
                                }
                            }
                        }

                    }
                    foreach ($productIds as $key => $id) {
                        if (!in_array($id, $products)) {
                            $onEkbStore = StoreProductTable::query()
                                ->setSelect(['AMOUNT', 'PRODUCT_ID'])
                                ->setFilter([
                                    'STORE_ID' => EKB_STORE_ID,
                                    'PRODUCT_ID' => $id,
                                ])
                                ->fetch();
                            if ($onEkbStore['AMOUNT'] > 0) {

                                $products[] = $id;
                            }

                        }
                    }

                }

                if (count($products ?: []) == count($productIds ?: [])) {
                    $message = "<span>доступны</span> <strong>все товары</strong>";
                } else {
                    $message = "<span>доступно</span> <strong>" . count($products ?: []) . " из " . count($productIds ?: []) . "</strong>";
                }
                if ($config['TYPE']["VALUE"] == 'courier') {

                    $this->arResult["DELIVERY"][$deliveryObj->getId()]["AVAIL_PRODUCTS"] = $products;
                    $this->arResult["DELIVERY"][$deliveryObj->getId()]["BASKET_COUNT"] = count($productIds ?: []);
                    $this->arResult["DELIVERY"][$deliveryObj->getId()]["AVAIL_MESSAGE"] = $message;


                }
                if ($config['TYPE']["VALUE"] == 'pickuppoint') {
                    $points = \Citystress\Connector\tc\cdek\Adapter::getZpvdPvz($_SESSION["CITY"]["LOCATION_ID"], 1);
                    foreach ($points as $point) {

                        $this->arResult["DELIVERY"][$deliveryObj->getId()]['PICKUP_POINTS'][] =
                            [
                                'ID' => $point["ID"],
                                "TITLE" => $point["TYPE"],
                                "ADDRESS" => $point['NAME'],
                                "PICKUP_POINT" => $point['PICKUP_POINT'],
                                "COORDS" => $point['COORDS'],
                                "INFO" => $point['PREVIEW_TEXT'],
                                "PVZ_DELIVERY_TIME" => $point['"PVZ_DELIVERY_TIME'],
                                "PHONE" => $point['PHONE'],
                                "SCHEDULE" => $point['SCHEDULE'],
                            ];
                    }
                    $this->arResult["DELIVERY"][$deliveryObj->getId()]["AVAIL_PRODUCTS"] = $products;
                    $this->arResult["DELIVERY"][$deliveryObj->getId()]["BASKET_COUNT"] = count($productIds ?: []);
                    $this->arResult["DELIVERY"][$deliveryObj->getId()]["AVAIL_MESSAGE"] = $message;
                } elseif ($config['TYPE']["VALUE"] == 'shoppickup') {
                    $points = \Citystress\Connector\tc\cdek\Adapter::getZpvdPvz($_SESSION["CITY"]["LOCATION_ID"], 1, 'CityStress');

                    foreach ($points as $point) {

                        $products = [];
                        foreach ($productIds as $key => $id) {
                            $rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList(array(
                                'filter' => array('=PRODUCT_ID' => $id, '=STORE.XML_ID' => $point['XML_ID']),
                                'select' => array('AMOUNT', 'STORE_ID', 'XML_ID' => 'STORE.XML_ID'),
                            ));
                            while ($arStoreProduct = $rsStoreProduct->fetch()) {

                                if ($arStoreProduct['AMOUNT'] > 0) {

                                    $products[] = $id;
                                }
                            }
                        }
                        if (count($products ?: []) == count($productIds ?: [])) {
                            $message = '<div class="shoppickup_item__instock all_items">В наличии ' . count($productIds ?: []) . ' из ' . count($productIds ?: []) . ' товаров</div>';
                        } elseif (count($products ?: []) > 0) {
                            $message = '<div class="shoppickup_item__instock some_items">В наличии ' . count($products ?: []) . ' из ' . count($productIds ?: []) . ' товаров</div>';
                        } else {
                            $message = '<div class="shoppickup_item__instock no_items">В наличии 0 товаров</div>';
                        }
                        $title = $point["TYPE"];
                        if ($point["SHOP_NAME"] != '') $title = $point["SHOP_NAME"];

                        $this->arResult["DELIVERY"][$deliveryObj->getId()]['SHOP_POINTS'][] =
                            [
                                'ID' => $point["ID"],
                                "TITLE" => $title,
                                "SHOP_NAME" => $point["SHOP_NAME"],
                                "ADDRESS" => $point['NAME'],
                                "PICKUP_POINT" => $point['PICKUP_POINT'],
                                "COORDS" => $point['COORDS'],
                                "INFO" => $point['PREVIEW_TEXT'],
                                "PVZ_DELIVERY_TIME" => $point['"PVZ_DELIVERY_TIME'],
                                "PHONE" => $point['PHONE'],
                                "SCHEDULE" => $point['SCHEDULE'],
                                'AMOUNT' => $amount,
                                "AVAIL_PRODUCTS" => $products,
                                "BASKET_COUNT" => count($productIds ?: []),
                                "AVAIL_MESSAGE" => $message,
                            ];
                    }
                    if ($this->arResult["DELIVERY"][$deliveryObj->getId()]['SHOP_POINTS'] == NULL) unset($this->arResult["DELIVERY"][$deliveryObj->getId()]);

                }

            }
        }

    }
}
 