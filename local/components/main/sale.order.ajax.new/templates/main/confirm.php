<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y")
{
	$APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
}
$info = []; 
$dbRes = \Bitrix\Sale\PropertyValueCollection::getList([
	'select' => ['*'],
	'filter' => [
		'=ORDER_ID' => $arResult["ORDER"]["ACCOUNT_NUMBER"], 
	]
]);
while ($item = $dbRes->fetch())
{
	
	$info[$item['CODE']] = $item["VALUE"];
 
}
$dbRes = \Bitrix\Sale\ShipmentCollection::getList(
	[
		'select' => ['*'],
		'filter' => [
			'=ORDER_ID' => $arResult["ORDER"]["ACCOUNT_NUMBER"], 
		]
	]
);
while ($item = $dbRes->fetch())
{
 
	$delivery_name = $item["DELIVERY_NAME"];
}
 
?>

<? if (!empty($arResult["ORDER"])): ?>
<div class="success_container container">
  <div class="success_block block">

    <div class="success_ordernum">Заказ <?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?> создан</div>

    <div class="success_title">Спасибо за заказ!</div>

      <? if ($arResult['ORDER']['ACCOUNT_NUMBER'] && $arResult['ORDER']['PRICE']) { ?>
		  <?
		  $orderId = $arResult['ORDER']['ACCOUNT_NUMBER'];
		  if ($orderId) {
			  $order = \Bitrix\Sale\Order::load((int)$orderId);
              $gtmPurchesEvent = $dashmailBasket = [];

			  foreach($order->getBasket() as $basketItem) {
				  $dashmailBasket[] = [
                      'productId' => $basketItem->getProductId(),
                      'quantity'  => $basketItem->getQuantity(),
                      'price'     => $basketItem->getPrice(),
                  ];
				  $gtmPurchesEvent[] = [
					  'item_id' => $basketItem->getProductId(),
					  'item_name' => $basketItem->getField('NAME'),
					  'quantity'  => $basketItem->getQuantity(),
					  'price' => $basketItem->getPrice(),
				  ];
			  }

			  $deliveryPrice = $order->getDeliveryPrice();
		  }
		  ?>
          <script>
              (window.b24order = window.b24order || []).push({
                  id: "<?=$arResult['ORDER']['ACCOUNT_NUMBER']?>",
                  sum: "<?=$arResult['ORDER']['PRICE']?>"
              });

			  try {
                  dashamail("async", {
                      "operation": "OrderCreate",
                      "data": {
                          "order": {
                              "orderId": "<?=$arResult['ORDER']['ACCOUNT_NUMBER']?>",
                              "totalPrice": "<?=$arResult['ORDER']['PRICE']?>",
                              "status": "created",
                              "lines": <?=json_encode($dashmailBasket)?>,
                          }
                      }
                  });

				  window.dataLayer.push({
					  'event': 'purchase',
					  'transaction_id': "<?=$arResult['ORDER']['ACCOUNT_NUMBER']?>",
					  'affiliation': 'Онлайн магазин',
					  'value': <?=$arResult['ORDER']['PRICE']?>,
					  'currency': 'RUB',
					  'shipping': <?=$deliveryPrice?>,
					  'items': <?=json_encode($gtmPurchesEvent)?>,
				  });
			  } catch (err) {
				  console.log(err.message);
			  }
          </script>
      <? } ?>
      
    <div class="success_flex">

      <div class="success_left">

        <div class="success_item success_paid">
		<?foreach ($arResult["PAYMENT"] as $payment)
			{
		 
				if ($payment["PAID"] == 'Y')
				{?>
          <div class="success_item__head">
            <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="m13.8007 18.8999h-9.59966c-1.32546 0-2.39996-1.0745-2.4-2.3999l-.00026-8.99984c-.00004-1.32551 1.07449-2.40006 2.4-2.40006h14.39942c1.3255 0 2.4001 1.07389 2.4001 2.39941v4.20059m-3.6 4.7828 1.5086 1.5086 3.2914-3.2916m-19.79998-5.39995h17.99998" stroke="#242424" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
            <strong>Оплачено <?=$payment['SUM']?> ₽</strong>
          </div>
				<?}
			}?>
          <div class="success_paid__message">Заказ выдается по коду из SMS на номер телефона покупателя <svg fill="none" height="14" viewBox="0 0 14 14" width="14" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><clipPath id="a"><path d="m0 0h14v14h-14z" transform="matrix(-1 0 0 1 14 0)"/></clipPath><g clip-path="url(#a)" fill="#242424"><path clip-rule="evenodd" d="m7 0c3.866 0 7 3.13401 7 7 0 3.866-3.134 7-7 7-3.86599 0-7.00000002-3.134-7.00000002-7 0-3.86599 3.13401002-7 7.00000002-7zm5.6 7c0 3.0928-2.5072 5.6-5.6 5.6-3.09279 0-5.6-2.5072-5.6-5.6 0-3.09279 2.50721-5.6 5.6-5.6 3.0928 0 5.6 2.50721 5.6 5.6z" fill-rule="evenodd"/><path d="m6.62384 7.90833c0-.28889.0571-.52777.17132-.71666.11918-.18889.2905-.39723.51396-.625.16388-.16667.28306-.30556.35755-.41667.07945-.11667.11918-.24722.11918-.39167 0-.20555-.07449-.36666-.22347-.48333-.14401-.12222-.33767-.18333-.581-.18333-.2334 0-.44196.05555-.6257.16666-.17877.10556-.33023.25556-.45438.45l-.9013-.59166c.20857-.35556.48914-.63056.84171-.825.35755-.19445.77964-.29167 1.2663-.29167.57107 0 1.02793.13611 1.37057.40833.34761.27223.52142.65.52142 1.13334 0 .22777-.03476.42777-.10428.6-.06456.17222-.1465.31944-.24581.44166-.09435.11667-.2185.25278-.37244.40834-.18374.18333-.31782.33889-.40224.46666-.08442.12223-.12663.27223-.12663.45zm.5661 2.09167c-.2036 0-.37492-.07222-.51396-.21667-.13408-.15-.20112-.33055-.20112-.54166s.06704-.38611.20112-.525c.13408-.14445.3054-.21667.51396-.21667.20857 0 .37989.07222.51397.21667.13408.13889.20112.31389.20112.525s-.06952.39166-.20857.54166c-.13408.14445-.30292.21667-.50652.21667z"/></g></svg></div>
        </div>

        <div class="success_item success_delivery">
          <div class="success_item__head">
            <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><g stroke="#242424" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"><path d="m4.22222 5h15.55558l2.2222 3.5v3.5h-20v-3.5z"/><path d="m10 12h10v7c0 .5523-.4477 1-1 1h-8c-.5523 0-1-.4477-1-1z"/><path d="m4 12v8"/></g></svg>
            <strong><?=$delivery_name?></strong>
          </div>
          <p><?=$info["DELIVERY_ADDRESS"]?></p>
        </div>

        <!--div class="success_item success_date">
          <div class="success_item__head">
            <svg fill="none" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><g stroke="#242424" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"><path d="m4 6.77778c0-.98184.79594-1.77778 1.77778-1.77778h12.44442c.9819 0 1.7778.79594 1.7778 1.77778v12.44442c0 .9819-.7959 1.7778-1.7778 1.7778h-12.44442c-.98184 0-1.77778-.7959-1.77778-1.7778z"/><path d="m4 10h16"/><path d="m16 3v4"/><path d="m8 3v4"/></g></svg>
            <strong>Заберите завтра, 23 июня, после получения SMS о готовности</strong>
          </div>
          <p>Хранение заказа до 28 июня включительно</p>
        </div-->

      </div>

      <div class="success_right">
        <p>Уведомление о статусе заказа вы получите в SMS на <?=$info["PHONE"]?> и <?=$info["MAIL"]?>.</p>
        <p>Электронный чек придет на почту <?=$info["MAIL"]?>.</p>
        <p>Информация о заказе доступна на странице проверки статуса заказа.</p>
      </div>

    </div>

  </div>
</div>

	<?
	if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y')
	{
		if (!empty($arResult["PAYMENT"]))
		{
			foreach ($arResult["PAYMENT"] as $payment)
			{
				if ($payment["PAID"] != 'Y')
				{
					if (!empty($arResult['PAY_SYSTEM_LIST'])
						&& array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
					)
					{
						$arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];

						if (empty($arPaySystem["ERROR"]))
						{
							?>
							<br /><br />

							<table class="sale_order_full_table">
								<tr>
									<td class="ps_logo">
										<div class="pay_name"><?=Loc::getMessage("SOA_PAY") ?></div>
										
										<div class="paysystem_name"><?=$arPaySystem["NAME"] ?></div>
										<br/>
									</td>
								</tr>
								<tr>
									<td>
										<? if ($arPaySystem["ACTION_FILE"] <> '' && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y"): ?>
											<?
											$orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
											$paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
											?>
											<script>
												window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
											</script>
                                        <br>
										<?=Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber))?>
                                        <br><br>
										<? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']): ?>
										<br/>
											<?=Loc::getMessage("SOA_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&pdf=1&DOWNLOAD=Y"))?>
										<? endif ?>
										<? else: ?>
											<?=$arPaySystem["BUFFERED_OUTPUT"]?>
										<? endif ?>
									</td>
								</tr>
							</table>

							<?
						}
						else
						{
							?>
							<span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
							<?
						}
					}
					else
					{
						?>
						<span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
						<?
					}
				}
			}
		}
	}
	else
	{
		?>
		<br /><strong><?=$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']?></strong>
		<?
	}
	?>

<? else: ?>

	<b><?=Loc::getMessage("SOA_ERROR_ORDER")?></b>
	<br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST", ["#ORDER_ID#" => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"])])?>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>

<? endif ?>