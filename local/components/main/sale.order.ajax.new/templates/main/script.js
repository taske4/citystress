const podeliCustom = new PodeliWidgetCustom();


BX.saleOrderAjaxExt = { // bad solution, actually, a singleton at the page

    BXCallAllowed: false,

    options: {},
    indexCache: {},
    controls: {},
    products: [],
    paymentSystem: false,
    modes: {},
    properties: {},

    // called once, on component load
    init: function (options) {

        var ctx = this;
        modal_name = '';
        this.options = options;

        window.submitFormProxy = BX.proxy(function () {
            ctx.submitFormProxy.apply(ctx, arguments);
        }, this);
        BX.ready(function () {
            ctx.renderOrder();


        });


    },

    initPodeliWidget: function()
    {
        BX.ready(function () {
            //Инициализируем виджет для страницы Оформления заказа
            podeliCustom.init({
                widgetContainerNodeSelector:
                    "#js-payment",
                type: "checkout", //catalog || item || checkout
                priceNodeSelector: ".cart_order__total strong",
                containerNodeSelector: "body",
                insertMethod: "append",
            });
            const payNode = document.querySelector(".paySystemPodeli");

            function startInteval() {
                window.podeliInteval = setInterval(function () {
                    if (payNode.classList.contains("checked")) {
                        podeliCustom.add();
                    }
                }, 3000);
            }
            startInteval();
            BX.addCustomEvent("onAjaxSuccess", function (data, method) {
                if (payNode.classList.contains("checked")) {
                    if (window.podeliInteval) {
                        clearInterval(window.podeliInteval);
                    }
                    podeliCustom.add();
                } else {
                    if (window.podeliInteval) {
                        clearInterval(window.podeliInteval);
                    }
                    startInteval();
                }
            });
        });
    },

    renderOrder: function (data) {

        this.renderBasket();
        this.renderBuyer(data);
        this.renderDeliveryServices();
        this.renderPaymentSystems();
        this.renderCoupon();
        this.renderTotal();
        this.addListeners();

        this.initPodeliWidget();
    },
    renderBasket: function (products) {
        let htmlItems = {},
            html = '',
            countProduct = 0,
            basketItems = {},
            rows = this.options.result.GRID.ROWS,
            saleTagValue = '';
        TSVET = '';
        RAZMER = '';
        HASH = '';

        products = products || this.products;

        if (products.length > 0) {
            products.forEach(function (item, i, products) {
                let row = rows[item];
                basketItems[i] = row;
            });
        } else
            basketItems = this.options.result.GRID.ROWS;

        html = '';
        discount_tag = '';
        basket_discount_tag = '';
        old_price = '';
        del = '';
        let gtmBeginCheckoutEvent = {
            ecommerce: {
                currency: 'RUB',
                value: 0,
                items: [

                ]
            }
        };

        for (let i in basketItems) {
            let item = basketItems[i];

            if (item.data.TSVET != null) TSVET = item.data.TSVET;
            if (item.data.RAZMER != null) RAZMER = item.data.RAZMER;
            if (item.data.HASH != null) HASH = item.data.HASH;
            if (item.data.QUANTITY == 1) del = 'delete';
            if (item.data.DISCOUNT_PRICE_PERCENT != '0') {
                basket_discount_tag = '<div class="cart_item__tag desktop_hide" style="background: #FE7779; color: #FFF;">-' + item.data.DISCOUNT_PRICE_PERCENT_FORMATED + '</div>';
                discount_tag = '<div class="cart_item__tag mob_only" style="background: #FE7779; color: #FFF;">-' + item.data.DISCOUNT_PRICE_PERCENT_FORMATED + '</div>';
                old_price = '<div class="cart_item__oldprice">' + item.data.SUM_BASE_FORMATED + '</div>';
            } else {
                discount_tag = basket_discount_tag = discount_tag = '';
                old_price = '';
            }

            if (item.data.SALE) {
                basket_discount_tag += '<div class="cart_item__tag desktop_hide" style="background: #fff; color: #222;">'+item.data.SALE.NAME+'</div>';
                discount_tag += '<div class="cart_item__tag mob_only" style="background: #fff; color: #222;">'+item.data.SALE.NAME+'</div>';
            }

            gtmBeginCheckoutEvent.ecommerce.value += (+item.data.PRICE);
            gtmBeginCheckoutEvent.ecommerce.items.push({
                item_id: item.data.PRODUCT_ID,
                item_name: item.data.NAME,
                quantity: +item.data.QUANTITY,
                price: +item.data.PRICE,
            });

            html += '\ <div class="cart_item">' +
                '<div class="cart_item__content">' +
                '<div class="cart_item__img">' +
                '<div class="back_img">' +
                '<picture>' +
                '<source type="image/jpg" srcset="' + item.data.PHOTO + '">' +
                '<img data-img1x="' + item.data.PHOTO + '" data-img2x="' + item.data.PHOTO + '" src="/img/blank.png" alt="">' +
                '</picture></div></div>' +
                '<div class="cart_item__top">' +
                '<div class="cart_item__tags mob_only">' + discount_tag + '</div>' +
                '<div class="cart_item__titlewrap">' +
                '<div class="cart_item__title"><a href="' + item.data.DETAIL_PAGE_URL + '?offerId=' + item.data.PRODUCT_ID + '">' + item.data.NAME + '</a></div>' +
                '<div class="cart_item__article">' + item.data.ARTICLE + '</div>' +
                '</div>' +
                '<div class="basket_tags">' +
                basket_discount_tag +
                '</div>' +
                '<div class="cart_item__prices">' +
                '<div class="cart_item__price">' + item.data.SUM + '</div>' + old_price + ' </div>' +
                '</div>' +
                '<div class="cart_item__body">' +
                '<div class="cart_item__variant"><span style="background:' + HASH + ';"></span> ' + TSVET + '<em>/</em>' + RAZMER + '</div>' +
                '<div class="cart_item__tags">' + discount_tag + '</div>' +
                '<div class="cart_item__error">' +
                '<span>Товар недоступен в данном способе доставки</span></div>            </div>            <div class="cart_item__bottom"  data-product-id="' + item.data.ELEMENT_ID + '">              <div class="quantity" data-product-id="' + item.data.PRODUCT_ID + '" data-quantity="' + item.data.QUANTITY + '" data-max-quantity="' + item.data.AVAILABLE + '" ><div class="quantity_minus ' + del + '">                  <svg fill="none" height="2" viewBox="0 0 9 2" width="9" xmlns="http://www.w3.org/2000/svg"><path d="m.48.720122h8.32v.579998h-8.32z" fill="#242424"/></svg>                  <svg fill="none" height="12" viewBox="0 0 12 12" width="12" xmlns="http://www.w3.org/2000/svg"><path d="m3 9 6-6m0 6-6-6" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>                </div>                <input class="quantity_input" type="number" min="1" value="' + item.data.QUANTITY + '" data-max="' + item.data.AVAILABLE + '">                <div class="quantity_plus">                  <svg fill="none" height="10" viewBox="0 0 9 10" width="9" xmlns="http://www.w3.org/2000/svg"><path d="m8.8 5.30012h-3.86v3.76h-.62v-3.76h-3.84v-.58h3.84v-3.779999h.62v3.779999h3.86z" fill="#242424"/></svg>                </div>              </div>              <span class="product_fav">                 <svg fill="none" height="15" viewBox="0 0 16 15" width="16" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="m8 2.24296c.00309.00353.00303.00358.00303.00358l.0051-.00465c.00484-.00439.01286-.01161.02396-.02135.02219-.01949.05661-.04905.10234-.08638.09157-.07474.22783-.18006.40147-.2977.349-.23645.83983-.5164 1.41532-.70164 1.12918-.363466 2.58788-.37135 4.01108 1.09592l.0044.00455.0034.00338.0025.00251c.0028.00273.0076.0077.0144.01484.0137.0143.0351.03727.0627.06851.0552.06258.1346.15783.2253.28261.1821.25055.4055.61418.5725 1.06614.3281.88796.4497 2.13808-.4482 3.60505l-.0368.06012-.0119.0461c-.001.0023-.0023.00509-.0039.00839-.0127.02696-.0424.08477-.1028.17974-.1214.19072-.3595.52046-.8139 1.03791-.854.97262-2.4532 2.58731-5.43 5.16271-2.97678-2.5754-4.57596-4.19009-5.43-5.16271-.45436-.51745-.69251-.84719-.81386-1.03791-.06044-.09497-.09018-.15278-.10286-.17974-.00156-.0033-.00283-.00609-.00386-.00839l-.01188-.0461-.0368-.06012c-.897931-1.46697-.776381-2.71709-.44828-3.60505.167-.45196.3904-.81559.57253-1.06614.0907-.12478.17009-.22003.22531-.28261.02757-.03124.049-.05421.06265-.06851.00683-.00714.0117-.01211.01441-.01484l.00229-.0023.00368-.00359.00441-.00455c1.4232-1.46727 2.88186-1.459386 4.01104-1.09592.57549.18524 1.06632.46519 1.41532.70164.17364.11764.3099.22296.40147.2977.04573.03733.08015.06689.10234.08638.0111.00974.01912.01696.02396.02135l.0051.00465s-.00006-.00005.00303-.00358zm0-1.16037c-.0116-.00796-.02335-.01598-.03525-.02404-.39631-.268496-.96454-.594474-1.644-.813184-1.37984-.444149-3.21325-.436565-4.92366 1.324354l-.007.00702c-.00651.00657-.01526.01552-.02604.0268-.02154.02255-.05122.05446-.08729.09535-.07208.08168-.17014.19969-.280083.35093-.219154.30149-.489785.74091-.693784 1.29301-.410695 1.11148-.5402 2.65801.503789 4.3907.007751.01921.017088.04068.02836.06464.032688.06948.082795.1623.160304.28411.154524.24286.424614.6113.899634 1.15227.94223 1.07305 2.71163 2.84845 6.0384 5.69815l.05466.0673c.00399-.0034.00798-.0068.01196-.0102.00399.0034.00797.0068.01196.0102l.05466-.0673c3.32678-2.8497 5.09618-4.6251 6.03838-5.69815.475-.54097.7451-.90941.8997-1.15227.0775-.12181.1276-.21463.1603-.28411.0112-.02396.0206-.04543.0283-.06463 1.044-1.7327.9145-3.27923.5038-4.39071-.204-.5521-.4746-.99152-.6938-1.29301-.1099-.15124-.208-.26925-.2801-.35093-.036-.04089-.0657-.0728-.0873-.09535-.0107-.01128-.0195-.02023-.026-.0268l-.007-.00702c-1.7104-1.760919-3.5438-1.768503-4.92365-1.324354-.67946.21871-1.24769.544688-1.644.813184-.01189.00806-.02365.01608-.03525.02404z" fill="#242424" fill-rule="evenodd"/></svg>  <svg class="fav_active" fill="none" height="15" viewBox="0 0 16 15" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m7.96475 1.05855.03525.02404.03525-.02404c.39631-.268496.96454-.594474 1.644-.813184 1.37985-.444149 3.21325-.436565 4.92365 1.324354l.007.00702c.0065.00657.0153.01552.026.0268.0216.02255.0513.05446.0873.09535.0721.08168.1702.19969.2801.35093.2192.30149.4898.74091.6938 1.29301.4107 1.11148.5402 2.65801-.5038 4.39071-.0077.0192-.0171.04067-.0283.06463-.0327.06948-.0828.1623-.1603.28411-.1546.24286-.4247.6113-.8997 1.15227-.9422 1.07305-2.7116 2.84845-6.03838 5.69815l-.05466.0673-.01196-.0102-.01196.0102-.05466-.0673c-3.32677-2.8497-5.09617-4.6251-6.0384-5.69815-.47502-.54097-.74511-.90941-.899634-1.15227-.077509-.12181-.127616-.21463-.160304-.28411-.011272-.02396-.020609-.04543-.02836-.06464-1.043989-1.73269-.914484-3.27922-.503789-4.3907.203999-.5521.47463-.99152.693784-1.29301.109943-.15124.208003-.26925.280083-.35093.03607-.04089.06575-.0728.08729-.09535.01078-.01128.01953-.02023.02604-.0268l.007-.00702c1.71041-1.760919 3.54382-1.768503 4.92366-1.324354.67946.21871 1.24769.544688 1.644.813184z" fill="#242424"/></svg>                               </span>            </div>            <div class="cart_item__restore">Вернуть</div>            <svg class="cart_item__remove" fill="none" height="18" viewBox="0 0 18 18" width="18" xmlns="http://www.w3.org/2000/svg"><g fill="#242424"><rect height="1.49739" rx=".748693" transform="matrix(.7071 .707114 -.7071 .707114 1.05859 .000122)" width="23.9582"/><rect height="1.49739" rx=".748693" transform="matrix(.707099 -.707114 .7071 .707114 0 16.9412)" width="23.9582"/></g></svg>          </div>        </div>';


        }
        html += this.objectJoin(htmlItems);

        BX('js-order-basket').innerHTML = html;

        if (gtmBeginCheckoutEvent) {
            window.dataLayer.push({
                'event': 'begin_checkout',
                ecommerce: gtmBeginCheckoutEvent.ecommerce,
            });
        }

    },
    renderTotal: function () {	//3

        console.log('renderTotal');
        console.log(total.DISCOUNT_PRICE);

        total = this.options.result.TOTAL;
        discount = '';
        if (total.DISCOUNT_PRICE != '0') {
            discount = '<li class="cart_order__sale"><span>Скидка</span> <strong>-' + total.DISCOUNT_PRICE_FORMATED + '</strong></li>'
        }

        html = '<ul class="cart_order__calc"><li><span>' + total.BASKET_POSITIONS + ' товара на сумму</span> <strong>' + total.PRICE_WITHOUT_DISCOUNT + '</strong></li>' + discount + '<li><span>Доставка</span> <strong>' + total.DELIVERY_PRICE_FORMATED + '</strong></li></ul><div class="cart_order__total"><span>Итого</span> <strong>' + total.ORDER_TOTAL_PRICE_FORMATED + '</strong> </div>';
        BX('js-total_calc').innerHTML = html;
    },
    renderBuyer: function (data) { //2
        auth = this.options.result.IS_AUTHORIZED;
        name = '';
        phone = '';
        mail = '';
        html = '';
        form_html = '<div class="buyerdata modal universal_modal__buyerdata"><div class="buyerdata_head modal_head"><span>Данные получателя</span><svg class="buyerdata_close modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg></div><div class="buyerdata_body modal_body"><div class="customscroll">';

        if (auth) {
            if (data) {
                name = data.NAME;
                mail = data.MAIL;
                phone = data.PHONE;
            } else {

                user = this.options.result.USER;
                if ($('#ORDER_PROP_1').val() != '') {
                    name = $('#ORDER_PROP_1').val();
                } else {
                    name = user.NAME;
                }
                if ($('#ORDER_PROP_3').val() != '') {
                    mail = $('#ORDER_PROP_3').val();
                } else {
                    mail = user.MAIL;
                }
                if ($('#ORDER_PROP_2').val() != '') {
                    phone = $('#ORDER_PROP_2').val();
                } else {
                    phone = user.PHONE;
                }
            }

            html += '<div class="buyer_change universal_modal__in" data-modalname="buyerdata" style="display:block;">Изменить</div><div class="buyer_title cart_order__titles">Получатель</div>';
            html += '<div class="buyer_email"><span>' + mail + '</span></div><ul class="buyer_namewrap"><li class="buyer_name">' + name + '</li><li class="buyer_phone">' + phone + '</li></ul>';
            form_html += '<form class="form_v1 basket_login_form"><div class="fields"><div class="field_wrap"><input type="text" class="field field_email required" name="order_email" value="' + mail + '" placeholder="Введите Email"></div><div class="fields_twocol"><div class="field_wrap"><input type="text" class="field field_name required" value="' + name + '"  name="order_name"  placeholder="Ваше имя"></div><div class="field_wrap"><input type="text" class="field field_phone required" value="' + phone + '"  name="order_phone"   placeholder="+7 (999) 999-99-99"></div></div><div class="button_wrap"><button type="submit" class="button button_black"><span>Сохранить</span></button></div></div></form></div></div></div>';
        } else {
            display = '';
            none = '';
            if ($('#ORDER_PROP_1').val() != '') {
                name = $('#ORDER_PROP_1').val();
                display = 'style="display: block;"';
                none = 'style="display: none;"';
            }
            if ($('#ORDER_PROP_2').val() != '') {
                phone = $('#ORDER_PROP_2').val();
            }
            if ($('#ORDER_PROP_3').val() != '') {
                mail = $('#ORDER_PROP_3').val();
            }

            html = '<div class="buyer_change universal_modal__in" data-modalname="buyerdata" ' + display + '>Изменить</div><svg class="buyer_add universal_modal__in" ' + none + ' data-modalname="buyerdata" fill="none" height="21" viewBox="0 0 21 21" width="21" xmlns="http://www.w3.org/2000/svg"><g fill="#242424"><rect height="21" rx=".5" width="1" x="10"/><rect height="21" rx=".5" transform="matrix(0 -1 1 0 -11 11)" width=".999999" y="11"/></g></svg><div class="buyer_title cart_order__titles">Получатель</div><div class="buyer_email"><span>' + mail + '</span></div><ul class="buyer_namewrap"><li class="buyer_name">' + name + '</li><li class="buyer_phone">' + phone + '</li></ul>';
            if (name == '') {
                html += '<div class="buyer_hint buyer_login"><span data-modalname="auth" class="universal_modal__in go_login auth_form">Войдите</span> или <span data-modalname="auth" class="universal_modal__in go_signup">зарегистрируйтесь</span>, чтобы увидеть отложенные товары из корзины и применить скидку по дисконтной карте.</div>'
            }
            form_html += '<div class="buyerdata_hint"><span data-modalname="auth" class="universal_modal__in basket_auth_form">Войдите</span> или <span data-modalname="auth" class="universal_modal__in go_signup">зарегистрируйтесь</span>, чтобы увидеть отложенные товары из корзины и применить скидку по дисконтной карте.</div><form class="form_v1 basket_login_form"><div class="fields"><div class="field_wrap"><input type="text" class="field field_email required"  value="' + mail + '"  name="order_email" placeholder="Введите Email"></div><div class="fields_twocol"><div class="field_wrap"><input type="text" class="field field_name required" name="order_name"  value="' + name + '"  placeholder="Ваше имя"></div><div class="field_wrap"><input type="text" class="field field_phone required" name="order_phone" value="' + phone + '" placeholder="+7 (999) 999-99-99"></div></div><div class="button_wrap"><button type="submit" class="button button_black"><span>Сохранить</span></button></div></div></form></div></div></div>';
        }

        BX('js-order-buyer').innerHTML = html;
        BX('js-buyerdata-modal').innerHTML = form_html;
    },
    renderCoupon: function () {
        coupon_entered = $("#ENTERED_COUPON").val();
        var applied = '',
            coupon = '',
            name = '';
        current_coupons = this.options.result.COUPON_LIST;

        if (current_coupons.length != 0) {
            for (let i in current_coupons) {
                let item = current_coupons[i];

                if (item.JS_STATUS == "APPLIED") {
                    applied = 'opened applied';
                    coupon = item.COUPON;
                    name = item.DISCOUNT_NAME;

                }
            }

        }
        if (applied == '' && coupon_entered != '') {
            applied = 'opened error';
        }
        html = '<div class="cart_codes__box promocode ' + applied + '"><svg class="cart_codes__show" fill="none" height="21" viewBox="0 0 21 21" width="21" xmlns="http://www.w3.org/2000/svg"><g fill="#242424"><rect height="21" rx=".5" width="1" x="10" y=".000122"/><rect height="21" rx=".5" transform="matrix(0 -1 1 0 -11.0001 11.0001)" width=".999999" y="11.0001"/></g></svg><div class="cart_codes__title cart_order__titles">Промокод</div><div class="cart_codes__form"><input type="text" class="cart_codes__input" value="' + coupon + '" placeholder="Промокод"><span class="cart_codes__apply" id="coupon_add">Применить</span><span class="cart_codes__change">Изменить</span></div><div class="cart_codes__error">Промокод не найден</div><div class="cart_codes__success"><div class="cart_codes__successtitle">Промокод активирован</div><div class="cart_codes__successtext">' + name + '</div></div></div>';
        BX('js-promocode').innerHTML = html;
    },
    renderDeliveryServices: function () {

        let error = '', htmlItems = {};
        html = '<div class="delivery_opts__title cart_order__titles">Способ доставки</div><div class="cart_order__error" style="display: none;">Выберите способ доставки, чтобы оформить заказ</div>';
        free_delivery_block = '';
        html_form = '';
        go_back = '';
        select_button = '';

        delivery_id = $('#DELIVERY_ID').val();


        for (let i in this.options.result.DELIVERY) {

            let item = this.options.result.DELIVERY[i];
            delivery_address = '';
            shop_name = '';
            delivery_form = '';
            period = '';
            delivery_avail = '';
            price = '';
            item_worktime = '';

            if (typeof item === 'undefined' || typeof item.PRICE === 'undefined') {
                continue;
            }

            if (item.TYPE == 'courier') {
                street = '';
                house = '';
                app = '';
                values = '';
                if ($('#COURIER_ADDRESS').val()) {

                    values = $('#COURIER_ADDRESS').val().split(';');

                    street = values[0];
                    house = values[1];
                    app = values[2];
                    delivery_address = $('#ORDER_PROP_4').val();
                }
                delivery_form = '<form class="form_v1"><div class="fields"><div class="field_wrap"><input type="hidden" name="delivery" value="' + item.ID + '"><input type="text" class="field field_street required" name="street" value="' + street + '" placeholder="Улица*"><div id="city_chose"></div></div><div class="fields_twocol"><div class="field_wrap"><input type="text" class="field field_bld required" name="house"  value="' + house + '" placeholder="Дом*"></div><div class="field_wrap"><input type="text" class="field field_apt" name="app" value="' + app + '"  placeholder="Квартира"> </div></div><div class="field_wrap"><input type="text" class="field field_comment" name="comment"  placeholder="Комментарий курьеру"><div class="field_afterhint">Подъезд, домофон, этаж. Мы сохраним эти данные для следующих заказов</div></div><div class="button_wrap"><button type="submit" class="button button_black"><span>Выбрать адрес</span></button></div></div></form>';
            }
            if (item.TYPE == 'pickuppoint') {
                pickuppoint_id = $('#ORDER_PROP_5').val();
                go_back = '<div class="mappoints_goback"><svg fill="none" height="13" viewBox="0 0 25 13" width="25" xmlns="http://www.w3.org/2000/svg"><path d="m7.67377 2.25-4.13194 4.25m0 0 4.13194 4.25m-4.13194-4.25h17.95817" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg><span>Назад к списку</span></div>';
                delivery_form = '<form class="form_v1"><div class="fields"><div class="field_wrap"><input type="text" class="field point_search" data-points="' + item.TYPE + '_item" placeholder="Поиск пункта по адресу"></div></div></form>';
                for (let j in item.PICKUP_POINTS) {
                    let point = item.PICKUP_POINTS[j];
                    checked_point = '';

                    if (point.ID == pickuppoint_id) {
                        checked_point = 'checked';
                        delivery_address = point.ADDRESS;
                    }
                    delivery_form += '<div class="pickuppoint_item optionview_item mappoints_data__item ' + checked_point + '" data-coos="[' + point.COORDS[0] + ',' + point.COORDS[1] + ']" data-point-id="' + point.ID + '" data-delivery-id="' + item.ID + '" data-address="' + point.ADDRESS + '"><div class="pickuppoint_item__title">' + point.TITLE + '</div><div class="pickuppoint_item__address">' + point.ADDRESS + '</div><div class="pickuppoint_item__options"></div><div class="pickuppoint_item__full"><div class="pickuppoint_item__charstitle">Условия доставки</div><div class="pickuppoint_item__charstitle">Время работы пункта самовывоза</div><ul class="pickuppoint_item__chars"><li><strong>' + point.SCHEDULE + '</li></ul><div class="pickuppoint_item__charstitle">Срок хранения</div><ul class="pickuppoint_item__chars"><li><strong>2 дня</strong><span>После указанного срока товары будут возвращены на склад</span></li></ul><div class="pickuppoint_item__charstitle">Телефон</div><ul class="pickuppoint_item__chars"><li><strong>' + point.PHONE + '</strong></li></ul><div class="pickuppoint_item__hint">Когда заказ поступит в пункт самовывоза, вам придет SMS. Пожалуйста, приходите за заказом только после получения SMS. Самое комфортное время посещения: 13:00 - 18:00</div></div></div>';
                }
                select_button = '<button class="button button_black select_pickuppoint"><span>Заберу здесь</span></button>';
            }
            if (item.TYPE == 'shoppickup') {
                shoppickup_id = $('#ORDER_PROP_6').val();
                go_back = '<div class="mappoints_goback"><svg fill="none" height="13" viewBox="0 0 25 13" width="25" xmlns="http://www.w3.org/2000/svg"><path d="m7.67377 2.25-4.13194 4.25m0 0 4.13194 4.25m-4.13194-4.25h17.95817" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg><span>Назад к списку</span></div>';
                delivery_form = '';
                for (let j in item.SHOP_POINTS) {
                    let point = item.SHOP_POINTS[j];
                    checked_point = '';

                    if (point.ID == shoppickup_id) {
                        checked_point = 'checked';
                        delivery_address = point.ADDRESS;
                        shop_name = '<li class="new">' + point.TITLE + '</li>';
                    }
                    worktime = '';

                    avaible = point.AVAIL_MESSAGE + '<div class="shoppickup_item__items">';
                    var AVAIL_PRODUCTS = point.AVAIL_PRODUCTS.map(function (e) {
                        return e.toString()
                    });
                    $.each(this.options.result.MINI_IMG, function (index, value) {
                        disabled = '';
                        if ($.inArray(index, AVAIL_PRODUCTS) == -1) disabled = 'disabled';
                        avaible += '<div class="shoppickup_item__item ' + disabled + '" style="background-image: url(\'' + value + '\');"></div>';
                    });
                    avaible += '</div>';
                    if (point.SCHEDULE != '' && point.SCHEDULE != 'null' && point.SCHEDULE != null) {
                        worktime = '<div class="shoppickup_item__worktime">Режим работы: ' + point.SCHEDULE + '</div>';
                        item_worktime = '<li class="new">Режим работы: ' + point.SCHEDULE + '</li>';
                    }
                    delivery_form += '<div class="shoppickup_item optionview_item mappoints_data__item ' + checked_point + '" data-coos="[' + point.COORDS[0] + ',' + point.COORDS[1] + ']" data-point-id="' + point.ID + '" data-delivery-id="' + item.ID + '" data-address="' + point.ADDRESS + '"><div class="shoppickup_item__title">' + point.TITLE + '</div><div class="shoppickup_item__address">' + point.ADDRESS + '</div><div class="shoppickup_item__options"><div class="shoppickup_item__option"><img src="/img/shoppickup_icon2.svg" alt=""></div></div><div class="shoppickup_item__full">' + worktime + '<div class="shoppickup_item__charstitle">Телефон</div><ul class="shoppickup_item__chars"><li><strong>' + point.PHONE + '</strong></li></ul></div>' + avaible + '<div class="button_wrap"><button class="button button_black select_shop"><span>Выбрать магазин</span></button></div></div>';
                }
            }
            checked = '';
            option_view = '';
            if (item.ID == delivery_id) {
                checked = 'checked';
                if (item.TYPE == 'pickuppoint') {
                    option_view = 'option_view';
                }
            }

            if (checked != '') {
                if (item.PRICE != 0) {
                    price = '<li>Стоимость доставки — ' + item.PRICE_FORMATED + '</li>';
                }

                if (item.PERIOD_TEXT) {
                    period = '<li class="new">' + item.PERIOD_TEXT + '</li>';
                }
            }
            if (item.TYPE == 'pickuppoint' || item.TYPE == 'courier') {
                delivery_avail = '<div class="delivery_option__items">';
                var AVAIL_PRODUCTS = item.AVAIL_PRODUCTS.map(function (e) {
                    return e.toString()
                });

                $.each(this.options.result.MINI_IMG, function (index, value) {
                    disabled = '';

                    if ($.inArray(index, AVAIL_PRODUCTS) == -1) disabled = 'disabled';
                    delivery_avail += '<div class="delivery_option__item ' + disabled + '" style="background-image: url(\'' + value + '\');"></div>';
                });
                delivery_avail += '</div>';
                delivery_avail += '<div class="delivery_option__status">' + item.AVAIL_MESSAGE + '</div>';
            }
            html += '<div class="delivery_option delivery_option__' + item.TYPE + ' universal_modal__in ' + checked + '" data-modalname="' + item.TYPE + '"><div class="delivery_option__change">Изменить</div><div class="delivery_option__title">' + item.NAME + '</div><div class="delivery_option__address">' + delivery_address + '</div><ul class="delivery_option__list"><li>' + item.DESCRIPTION + '</li>' + price + period + shop_name + item_worktime + '</ul>' + delivery_avail + '</div>';
            html_form += '<div class="' + item.TYPE + ' mappoints_box modal universal_modal__' + item.TYPE + ' ' + option_view + '"><div class="mappoints_head modal_head"><span class="mappoints_head__title">' + item.NAME + '</span><svg class="mappoints_mobclose modal_close modal_closeicon" fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".25"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17676 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg></div><div class="mappoints_close modal_close"><svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><g fill="#242424" opacity=".75"><rect height="1.66376" rx=".831881" transform="matrix(.7071 .707114 -.7071 .707114 1.17578 0)" width="26.6202"/><rect height="1.66376" rx=".831881" transform="matrix(.707099 -.707114 .7071 .707114 0 18.8237)" width="26.6202"/></g></svg></div><div class="mappoints_body"><div class="customscroll"><div class="mappoints_inside"><div class="mappoints_title">' + item.NAME + '</div>' + go_back + '<div class="mappoints_city universal_modal__in" data-modalname="cityselect"><div class="mappoints_city__chose">Город доставки</div><span>' + this.options.result.LOCATION_NAME + '</span><svg fill="none" height="16" viewBox="0 0 15 16" width="15" xmlns="http://www.w3.org/2000/svg"><path d="m3 6.5 4.5 4 4.5-4" opacity=".35" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg></div><div data-zoom="10" data-center="[' + this.options.result.LOCATION_LATITUDE + ',' + this.options.result.LOCATION_LONGITUDE + ']" class="mappoints_mobmap_in"></div>' + delivery_form + '</div></div>' + select_button + '</div><div data-zoom="12" data-center="[' + this.options.result.LOCATION_LATITUDE + ',' + this.options.result.LOCATION_LONGITUDE + ']" class="mappoints_map_in"></div><div class="mappoints_map"></div></div>';
        }
        total = this.options.result.TOTAL.ORDER_PRICE;
        courier_price = 10000;
        courier_active = '';

        pickup_active = '';
        pickup_price = 5000;
        percent = total / 100;

        if (total < 5000) {
            delivery_name = '<div class="freedelivery_title">До бесплатной доставки <span>ПВЗ</span><br> осталось ' + (pickup_price - total).toFixed() + ' ₽</div>';

        } else if (total < 10000) {
            delivery_name = '<div class="freedelivery_title">До бесплатной доставки <span>курьером</span><br> осталось ' + (courier_price - total).toFixed() + ' ₽</div>';
            pickup_active = 'active';

        } else {
            percent = 100;
            delivery_name = '<div class="freedelivery_title">Доставка <span>ПВЗ</span> и <span>курьером</span><br> 0 ₽</div>';
            courier_active = 'active';
            pickup_active = 'active';
        }

        free_delivery_block = delivery_name + '<div class="freedelivery_linebox"><div class="freedelivery_line"><div class="freedelivery_mark freedelivery_mark1 ' + pickup_active + '"><span>в пункт выдачи</span><div class="freedelivery_dot"></div><strong>от 5 000 ₽</strong></div><div class="freedelivery_mark freedelivery_mark2 ' + courier_active + '"><div class="freedelivery_dot"></div><span>курьером</span><strong>от 10 000 ₽</strong></div><div class="freedelivery_fill" style="width: ' + percent + '%;"></div></div></div>';
        BX('js-delivery-modal').innerHTML = html_form;
        BX('js-delivery').innerHTML = html;
        BX('js-delivery_block').innerHTML = free_delivery_block;

    },
    renderPaymentSystems: function () {

        let error = '', htmlItems = {}, html = '';
        pay_options = this.options.result.PAY_SYSTEM;

        html = '<div class="payment_opts__title cart_order__titles">Способ оплаты</div>';
        for (let i in pay_options) {
            let item = pay_options[i];
            let checked = '';
            if (item.PAY_SYSTEM_ID == $('#PAY_SYSTEM_ID').val()) {
                checked = 'checked';
                this.paymentSystem = item.ID;
            }

            let customClass = '';

            if (item.PAY_SYSTEM_ID == 6) {
                customClass = 'paySystemPodeli';
            }

            html += '<div class="payment_option ' + customClass + ' ' + checked + '" data-value="' + item.PAY_SYSTEM_ID + '"><div class="payment_option__icon"></div><div class="payment_option__title">' + item.NAME + '</div><div class="payment_option__text">' + item.DESCRIPTION + '</div></div>';
        }
        html += '<div class="payment_opts__hint">* Вернем деньги, если товары не подойдут</div></div>';

        BX('js-payment').innerHTML = html;
    },
    addListeners: function () { //4
        last_map_modal = 0;
        let ctx = this,
            form = BX('js-order-basket');


        let quantityMinus = BX.findChild(form, {class: 'quantity_minus'}, true, true);
        quantityMinus.forEach(function (minus) {
            BX.bind(BX(minus), 'click', function (event) {
                ctx.onChangeQuantity(this, '-1');
                event.preventDefault();
            });
        });

        let quantityAdd = BX.findChild(form, {class: 'quantity_plus'}, true, true);
        quantityAdd.forEach(function (add) {
            BX.bind(BX(add), 'click', function (event) {
                ctx.onChangeQuantity(this, '+1');
                event.preventDefault();
            });
        });

        let inputAdd = BX.findChild(form, {class: 'quantity_input'}, true, true);
        inputAdd.forEach(function (input) {
            BX.bind(BX(input), 'change', function (event) {
                ctx.onChangeQuantity(this, 'set');
                event.preventDefault();
            });
        });
        let itemDelete = BX.findChild(form, {class: 'delete'}, true, true);
        itemDelete.forEach(function (deleteBtn) {
            BX.bind(BX(deleteBtn), 'click', function (event) {
                ctx.onChangeQuantity(this, '0');
                event.preventDefault();
            });

        });

        BX.bind(BX('coupon_add'), 'click', function (event) {
            ctx.onCouponAdd(this);
            event.preventDefault();
        });
        $('.basket_login_form').submit(function (e) {
            e.preventDefault();
            var form = $(this),
                empty_fields = 0;
            data = new Object();

            form.find('.required').each(function () {
                if ($(this).val() == '') {
                    empty_fields = 1;
                    $(this).addClass('error');
                }
                ;
            });

            if (empty_fields == 0) {
                var email = form.find('.field_email').val(),
                    name = form.find('.field_name').val(),
                    phone = form.find('.field_phone').val();

                $('.buyer .buyer_email span').text(email);
                $('.buyer .buyer_name').text(name);
                $('.buyer .buyer_phone').text(phone);
                $('.buyer').removeClass('error');
                data.NAME = name;
                data.MAIL = email;
                data.PHONE = phone;
                $('#ORDER_PROP_1').val(name);
                $('#ORDER_PROP_2').val(phone);
                $('#ORDER_PROP_3').val(email);
                ctx.renderOrder(data);

                $('.buyer .buyer_login, .buyer .buyer_add').css('display', 'none');
                $('.buyer .buyer_register, .buyer .buyer_change').fadeIn(200);

                close_all_popups();
            }
            ;

            return false;
        });

        $('body').on('click', '.universal_modal__in', function () {

            close_all_popups();
            $('body').addClass('universal_modal__out');

            modal_name = $(this).data('modalname');
            if ($(this).hasClass('not_from_mapmodal')) {

                last_map_modal = 0;

            } else {

                if (modal_name != 'cityselect') {
                    if (modal_name == 'instore' || modal_name == 'courier' || modal_name == 'shoppickup' || modal_name == 'pickuppoint' || modal_name == 'postoffice') {
                        last_map_modal = $('.universal_modal__' + modal_name);
                    } else {
                        last_map_modal = 0;
                    }
                    ;
                }
                ;

            }
            ;
            $(last_map_modal).css('transform', 'translateX(100%)');
            $('.universal_modal__' + modal_name).css('transform', 'translateX(0)');
            $('.universal_modal__' + modal_name).css('-webkit-transform', 'translateX(0)');
        });
        if (!$.isFunction(close_all_popups)) {
            function close_all_popups(type) {
                $('body').removeClass('filters_show reviews_out faq_out mobsizes_out hints_out universal_modal__out');
                $('.modal').css('transform', 'translateX(100%)');
                $('.share, .mobadd_back, .mobadd').fadeOut(200);

                if (modal_name == 'cityselect' && last_map_modal && last_map_modal != 0) {
                    $('body').addClass('universal_modal__out');
                    last_map_modal.css('transform', 'translateX(0)');
                    modal_name = 0;
                }
                ;
            };
        }
        $('.cart_codes__show').click(function () {
            $(this).closest('.cart_codes__box').addClass('opened');
        });

        $('.cart_codes__change, .cart_codes__remove').click(function () {
            $(this).closest('.cart_codes__box').removeClass('applied').find('.cart_codes__input').val('');
        });

        $('.cart_codes__input').bind('input propertychange', function () {
            $(this).closest('.cart_codes__box').removeClass('error');
        });

        $('.cart_codes__apply').click(function () {
            var codesbox = $(this).closest('.cart_codes__box');
            if (codesbox.find('.cart_codes__input').val() == '') {
                codesbox.addClass('error');
            } else {
                codesbox.removeClass('error');
            }
            ;
        });
        $('.payment_option').click(function () {

            $('.payment_option').removeClass('checked');
            $(this).addClass('checked');
            if ($(this).hasClass('payment_option__podeli')) {
                $('.payment_opts .podeli').fadeIn(200);
            } else {
                $('.payment_opts .podeli').css('display', 'none');
            }
            ;
            $('#PAY_SYSTEM_ID').val($(this).data('value'));
            ctx.requestOrder();
        });
        $(".quantity_input").bind('keyup mouseup', function () {
            if ($(this).val() <= 0) {
                $(this).val(1);
            }
        });
        $('.cart_form').submit(function (e) {
            e.preventDefault();
            var form = $(this);
            mail = $('.basket_login_form').find('.field_email').val();
            name = $('.basket_login_form').find('.field_name').val();
            phone = $('.basket_login_form').find('.field_phone').val();
            if (!$('.delivery_option.checked').length) {
                $('.delivery_opts .cart_order__error').fadeIn(200);
                $('.delivery_option').addClass('error');

                window.scrollTo({
                    top: $('.delivery_opts .cart_order__error').offset().top - 150,
                    left: 0,
                    behavior: 'smooth'
                });

            } else if (mail == '' || name == '' || phone == '') {
                $('.buyer').addClass('error');
                window.scrollTo({
                    top: $('.buyer').offset().top - 150,
                    left: 0,
                    behavior: 'smooth'
                });
            } else {

                ctx.requestOrder('saveOrderAjax');
            }

        });
        $('.courier .form_v1').submit(function (e) {
            e.preventDefault();
            var form = $(this),
                empty_fields = 0;

            $('.courier .form_v1 .required').each(function () {
                if ($(this).val() == '') {
                    empty_fields = 1;
                    $(this).addClass('error');
                }
                ;
            });

            if (empty_fields == 0) {

                var street = form.find('.field_street').val(),
                    bld = form.find('.field_bld').val(),
                    apt = form.find('.field_apt').val(),
                    comment = form.find('.field_comment').val();
                city = ctx.options.result.LOCATION_NAME;
                data = {
                    query: street + ', ' + bld

                };
                console.log(1111);
                $.ajax({
                    type: 'POST',
                    dataType: 'html',
                    url: '/local/ajax/get_clue.php',
                    data: data,
                    success: function (data) {

                        if (data) {
                            console.log(data);
                            $('.delivery_option__courier .new').remove();

                            if (comment != '') {
                                $('.delivery_option__courier ul').append('<li class="new">Комментарий курьеру: ' + comment + '</li>');
                            }
                            ;

                            $('.delivery_option__courier .delivery_option__address').text(street + ', ' + bld + ', ' + apt);

                            $('.delivery_option').removeClass('checked');
                            $('.delivery_option__courier').addClass('checked');
                            delivery_option_select();
                            $('#ORDER_PROP_4').val(street + ', дом ' + bld + ', ' + apt);
                            $('#COURIER_ADDRESS').val(street + ';' + bld + ';' + apt);
                            $('#ORDER_PROP_5').val('');
                            $('#ORDER_PROP_6').val('');
                            $('#ORDER_PROP_7').val('');
                            $('#ORDER_DESCRIPTION').val(comment);
                            $('#DELIVERY_ID').val(form.find('[name="delivery"]').val());
                            ctx.requestOrder();

                            return false;
                        } else {
                            form.find('.field_street').addClass('error');
                            form.find('.field_field_apt').addClass('error');
                        }
                    }
                });
            }

        });

        function delivery_option_select() {
            var status = $('.delivery_option.checked .delivery_option__status strong').text();
            $('.cart_order__submit strong').text(status);
            $('.delivery_option').removeClass('error');
            $('.delivery_opts .cart_order__error').css('display', 'none');
            $('body').removeClass('universal_modal__out');
            $('.modal').css('transform', 'translateX(100%)');
        }

        $('.select_pickuppoint').click(function () {
            var pickuppoint_item = $('.pickuppoint_item.checked'),
                item__title = pickuppoint_item.find('.pickuppoint_item__title').text(),
                item__address = pickuppoint_item.find('.pickuppoint_item__address').text(),
                item__date = pickuppoint_item.find('.pickuppoint_item__date').text();

            $('.delivery_option__pickuppoint .new').remove();

            $('.delivery_option__pickuppoint .delivery_option__address').text(item__address);
            $('.delivery_option__pickuppoint ul').append('<li class="new">' + item__title + '</li><li class="new">' + item__date + '</li>');

            $('.delivery_option').removeClass('checked');
            $('.delivery_option__pickuppoint').addClass('checked');
            $('#ORDER_PROP_5').val(pickuppoint_item.data('point-id'));
            $('#ORDER_PROP_4').val('');
            $('#COURIER_ADDRESS').val('');
            $('#ORDER_PROP_6').val('');
            $('#ORDER_PROP_7').val(item__address);
            $('#ORDER_DESCRIPTION').val('');
            $('#DELIVERY_ID').val(pickuppoint_item.data('delivery-id'));
            delivery_option_select();

            ctx.requestOrder();
        });
        $('.select_shop').click(function () {
            var shoppickup_item = $('.shoppickup_item.checked'),
                item__title = shoppickup_item.find('.shoppickup_item__title').text(),
                item__address = shoppickup_item.find('.shoppickup_item__address').text(),
                item__date = shoppickup_item.find('.pickuppoint_item__date').text(),
                item__worktime = shoppickup_item.find('.shoppickup_item__worktime').text();

            $('.delivery_option__shoppickup .metros, .delivery_option__shoppickup .new').remove();

            $('.delivery_option__shoppickup .delivery_option__address').text(item__address);
            $('.delivery_option__shoppickup ul').append('<li class="new">' + item__title + '</li><li class="new">' + item__date + '</li><li class="new">' + item__worktime + '</li>');
            shoppickup_item.find('.metros').clone().insertAfter('.delivery_option__shoppickup ul');
            $('#ORDER_PROP_6').val(shoppickup_item.data('point-id'));
            $('#ORDER_PROP_7').val(item__address);
            $('#ORDER_PROP_5').val('');
            $('#ORDER_PROP_4').val('');
            $('#COURIER_ADDRESS').val('');
            $('#ORDER_DESCRIPTION').val('');
            $('#DELIVERY_ID').val(shoppickup_item.data('delivery-id'));
            $('.delivery_option').removeClass('checked');
            $('.delivery_option__shoppickup').addClass('checked');
            delivery_option_select();
            ctx.requestOrder();
        });

        $('.mappoints_box').each(function (index) {
            var this_index = index + 1;
            $(this).find('.mappoints_data__item').each(function (index) {
                var data_item_index = index + 1;
                $(this).attr('data-markerid', 'marker_' + this_index + '_' + data_item_index);
            });
        });
        $(document).ready(function () {
            $(".field_phone").inputmask('+7 (999) 999-9999');
            var mappage_var = 0,
                mapscreenw = $(window).width();


            $('.mappoints_box').each(function (index) {
                var this_index = index + 1;
                $(this).find('.mappoints_data__item').each(function (index) {
                    var data_item_index = index + 1;
                    $(this).attr('data-markerid', 'marker_' + this_index + '_' + data_item_index);
                });
            });


            function map_position() {
                $('.mappoints_box').each(function () {
                    var mappoints_map_in = $(this).find('.mappoints_map_in'),
                        mappoints_mobmap_in = $(this).find('.mappoints_mobmap_in');

                    if (mapscreenw < 750) {
                        $(this).find('.mappoints_map').appendTo(mappoints_mobmap_in);
                    } else {
                        $(this).find('.mappoints_map').appendTo(mappoints_map_in);
                    }
                    ;
                });
            };

            map_position();

            $(window).on('resize', function () {
                mapscreenw = $(window).width();
                map_position();
            });
            maps();
        });
        $(document).mouseup(function (e) {
            var div = $("#city_chose");
            if (!div.is(e.target)
                && div.has(e.target).length === 0) {
                $('#city_chose').html('');
            }
        });
        $('#city_chose').on('click', '.address_option', function () {
            street = $(this).data('street');
            house = $(this).data('house');
            $('.field_street').val(street);
            $('.field_bld').val(house);
            $('#city_chose').html('');
        });
        $('.field_street').keyup(function () {
            query = $(this).val();
            data = {
                city: ctx.options.result.LOCATION_NAME,
                query: query

            };
            console.log(22222);
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: '/local/ajax/get_clue.php',
                data: data,
                success: function (data) {

                    if (data && query != '') {
                        $('#city_chose').html(data);
                    } else {
                        $('#city_chose').html('');
                    }
                }
            });
        });

        $('.point_search').keyup(function () {
            points = $(this).data('points');
            search = $(this).val().toLowerCase();

            $("." + points).each(function (index) {
                $(this).show();
                item_search = $(this).data('address').toLowerCase();
                if (search != '') {
                    if (item_search.indexOf(search) == -1) {
                        $(this).hide();
                    }
                }

            });
        });
    },
    onChangeQuantity: function (element, mode) {
        let item = element,
            rootItem = item.closest('.quantity'),
            productID = rootItem.dataset.productId,
            quantity = rootItem.dataset.quantity,
            maxQuantity = +rootItem.dataset.maxQuantity,
            input = BX.findChildren(rootItem, {tag: 'input'}, true);

        if (mode === 'set') {
            quantity = +input[0].value;
            if (maxQuantity < quantity)
                quantity = maxQuantity;
        } else if (mode === '+1') {
            quantity++;
            if (maxQuantity < quantity)
                quantity = maxQuantity;
        } else if (mode === '-1') {
            quantity--;
        } else if (mode === 'del') {
            quantity = 0;
        }

        this.requestOrder('changeQuantity', {productID: productID, quantity: quantity});
    },
    onCouponAdd: function (element) {
        let item = $('.cart_codes__input').val();
        $("#ENTERED_COUPON").val(item);
        this.requestOrder('addCoupon', {coupon: item});
    },
    requestOrder: function (action, actionData, onFinish) { //1
        var
            ctx = this,
            data,
            check_required = false,
            delivery_properties_required = {},
            property_required = false,
            check_phone = false,
            error = false,
            fields = '';
        data_buyer = new Object();


        action = (typeof action !== 'undefined') ? action : 'refreshOrderAjax';

        data = this.getData(action, actionData);

        var form = $('.basket_login_form'),
            email = form.find('.field_email').val(),
            name = form.find('.field_name').val(),
            phone = form.find('.field_phone').val();
        data_buyer.NAME = name;
        data_buyer.MAIL = email;
        data_buyer.PHONE = phone;
        if (action == 'saveOrderAjax') {
            for (var i in data.order) {
                data[i] = data.order[i];
            }
            delete (data.order);

        }

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ctx.options.ajaxUrl,
            data: data,
        }).done(function (result, textStatus, jqXHR) {

            var redirecting = false;
            if (typeof result.redirect !== 'undefined' && result.redirect && (result.redirect.length > 0)) {
                document.location.href = result.redirect;
                redirecting = true;
            }
            if (typeof result.order !== 'undefined' && result.order.REDIRECT_URL && result.order.REDIRECT_URL.length && result.order.ID) {
                document.location.href = result.order.REDIRECT_URL;
                redirecting = true;
            }

            if (!redirecting) {

                var
                    hasErrors = false;

                if (typeof result.order !== 'undefined') {
                    if (result.order.ERROR && ctx.objectSize(result.order.ERROR) > 0) {
                        ctx.options.result.ERROR = result.order.ERROR;
                        hasErrors = true;
                    }
                    if (result.order.WARNING && ctx.objectSize(result.order.WARNING) > 0) {
                        ctx.options.result.WARNING = result.order.WARNING;
                    } else if (hasErrors) {
                        ctx.options.result.WARNING = [];
                    }

                    for (var field in result.order) {
                        if (result.order[field]) {
                            if (field === 'GRID') {
                                if (result.order['GRID']['ROWS'] !== null) {
                                    ctx.options.result['GRID'] = result.order['GRID'];
                                }
                            } else {
                                ctx.options.result[field] = result.order[field];
                            }
                        }
                    }

                }

                switch (action) {
                    case 'changeQuantity':
                        ctx.renderOrder(data_buyer);
                    //ctx.countBasketItems();
                    case 'saveOrderAjax':
                    case 'refreshOrderAjax':
                        if (result.error) {
                            ctx.auxErrors = result.error;
                        }
                        ctx.renderOrder(data_buyer);
                        break;

                    case 'addCoupon':
                        if (typeof result.order != 'undefined') {
                            ctx.renderOrder(data_buyer);
                        } else {
                            ctx.requestOrder(data_buyer);
                        }
                        break;
                    case 'removeCoupon':
                        if (result.order) {
                            ctx.renderOrder(data_buyer);
                        }

                        break;

                }
            }

            if (typeof onFinish === 'function') {
                onFinish(result);
            }

            if (!redirecting) {
                //ctx.hideWaitingSpinner();
            }
        });
    },
    objectJoin: function (obj) {//1
        var result = '', i;
        for (i in obj) {
            result += obj[i];
        }
        return result;
    },

    objectSize: function (obj) {//1
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    },
    getData: function (action, actionData) {
        ctx = this;

        var data = {
            order: this.getAllFormData(),
            sessid: BX.bitrix_sessid(),
            via_ajax: 'Y',
            SITE_ID: this.options.siteID,
            action: action,
            signedParamsString: this.options.signedParamsString
        };

        if (action === 'addCoupon') {
            data.coupon = actionData.coupon;

        }

        if (action === 'changeQuantity') {
            data.productID = actionData.productID;
            data.basketItemQuantity = actionData.quantity;
        }

        if (action === 'saveOrderAjax') {
            data.save = 'Y';
        }

        data.DELIVERY_ID = data.order.DELIVERY_ID;
        data.PAY_SYSTEM_ID = data.order.PAY_SYSTEM_ID;
        data.PERSON_TYPE = data.order.PERSON_TYPE;
        data.PERSON_TYPE_OLD = data.order.PERSON_TYPE_OLD;
        data.SITE_ID = 's1';


        return data;
    },
    getAllFormData: function () {//1
        var form = BX('bx-soa-order-form'),
            prepared = BX.ajax.prepareForm(form),
            i;

        for (i in prepared.data)
            if (prepared.data.hasOwnProperty(i) && i == '')
                delete prepared.data[i];

        return !!prepared && prepared.data ? prepared.data : {};
    },
    objectJoin: function (obj) {//1
        var result = '', i;
        for (i in obj) {
            result += obj[i];
        }
        return result;
    },
    cleanUp: function () {

        for (var k in this.properties) {
            if (this.properties.hasOwnProperty(k)) {
                if (typeof this.properties[k].input != 'undefined') {
                    BX.unbindAll(this.properties[k].input);
                    this.properties[k].input = null;
                }

                if (typeof this.properties[k].control != 'undefined')
                    BX.unbindAll(this.properties[k].control);
            }
        }

        this.properties = {};
    },

    addPropertyDesc: function (desc) {
        this.properties[desc.id] = desc.attributes;
        this.properties[desc.id].id = desc.id;
    },

    // called each time form refreshes
    initDeferredControl: function () {
        var ctx = this,
            k,
            row,
            input,
            locPropId,
            m,
            control,
            code,
            townInputFlag,
            adapter;

        // first, init all controls
        if (typeof window.BX.locationsDeferred != 'undefined') {

            this.BXCallAllowed = false;

            for (k in window.BX.locationsDeferred) {

                window.BX.locationsDeferred[k].call(this);
                window.BX.locationsDeferred[k] = null;
                delete (window.BX.locationsDeferred[k]);

                this.properties[k].control = window.BX.locationSelectors[k];
                delete (window.BX.locationSelectors[k]);
            }
        }

        for (k in this.properties) {

            // zip input handling
            if (this.properties[k].isZip) {
                row = this.controls.scope.querySelector('[data-property-id-row="' + k + '"]');
                if (BX.type.isElementNode(row)) {

                    input = row.querySelector('input[type="text"]');
                    if (BX.type.isElementNode(input)) {
                        this.properties[k].input = input;

                        // set value for the first "location" property met
                        locPropId = false;
                        for (m in this.properties) {
                            if (this.properties[m].type == 'LOCATION') {
                                locPropId = m;
                                break;
                            }
                        }

                        if (locPropId !== false) {
                            BX.bindDebouncedChange(input, function (value) {

                                var zipChangedNode = BX('ZIP_PROPERTY_CHANGED');
                                zipChangedNode && (zipChangedNode.value = 'Y');

                                input = null;
                                row = null;

                                if (BX.type.isNotEmptyString(value) && /^\s*\d+\s*$/.test(value) && value.length > 3) {

                                    ctx.getLocationsByZip(value, function (locationsData) {
                                        ctx.properties[locPropId].control.setValueByLocationIds(locationsData);
                                    }, function () {
                                        try {
                                            // ctx.properties[locPropId].control.clearSelected();
                                        } catch (e) {
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }

            // location handling, town property, etc...
            if (this.properties[k].type == 'LOCATION') {

                if (typeof this.properties[k].control != 'undefined') {

                    control = this.properties[k].control; // reference to sale.location.selector.*
                    code = control.getSysCode();

                    // we have town property (alternative location)
                    if (typeof this.properties[k].altLocationPropId != 'undefined') {
                        if (code == 'sls') // for sale.location.selector.search
                        {
                            // replace default boring "nothing found" label for popup with "-bx-popup-set-mode-add-loc" inside
                            control.replaceTemplate('nothing-found', this.options.messages.notFoundPrompt);
                        }

                        if (code == 'slst')  // for sale.location.selector.steps
                        {
                            (function (k, control) {

                                // control can have "select other location" option
                                control.setOption('pseudoValues', ['other']);

                                // insert "other location" option to popup
                                control.bindEvent('control-before-display-page', function (adapter) {

                                    control = null;

                                    var parentValue = adapter.getParentValue();

                                    // you can choose "other" location only if parentNode is not root and is selectable
                                    if (parentValue == this.getOption('rootNodeValue') || !this.checkCanSelectItem(parentValue))
                                        return;

                                    var controlInApater = adapter.getControl();

                                    if (typeof controlInApater.vars.cache.nodes['other'] == 'undefined') {
                                        controlInApater.fillCache([{
                                            CODE: 'other',
                                            DISPLAY: ctx.options.messages.otherLocation,
                                            IS_PARENT: false,
                                            VALUE: 'other'
                                        }], {
                                            modifyOrigin: true,
                                            modifyOriginPosition: 'prepend'
                                        });
                                    }
                                });

                                townInputFlag = BX('LOCATION_ALT_PROP_DISPLAY_MANUAL[' + parseInt(k) + ']');

                                control.bindEvent('after-select-real-value', function () {

                                    // some location chosen
                                    if (BX.type.isDomNode(townInputFlag))
                                        townInputFlag.value = '0';
                                });
                                control.bindEvent('after-select-pseudo-value', function () {

                                    // option "other location" chosen
                                    if (BX.type.isDomNode(townInputFlag))
                                        townInputFlag.value = '1';
                                });

                                // when user click at default location or call .setValueByLocation*()
                                control.bindEvent('before-set-value', function () {
                                    if (BX.type.isDomNode(townInputFlag))
                                        townInputFlag.value = '0';
                                });

                                // restore "other location" label on the last control
                                if (BX.type.isDomNode(townInputFlag) && townInputFlag.value == '1') {

                                    // a little hack: set "other location" text display
                                    adapter = control.getAdapterAtPosition(control.getStackSize() - 1);

                                    if (typeof adapter != 'undefined' && adapter !== null)
                                        adapter.setValuePair('other', ctx.options.messages.otherLocation);
                                }

                            })(k, control);
                        }
                    }
                }
            }
        }

        this.BXCallAllowed = true;

        //set location initialized flag and refresh region & property actual content
        if (BX.Sale.OrderAjaxComponent)
            BX.Sale.OrderAjaxComponent.locationsCompletion();
    },

    checkMode: function (propId, mode) {

        //if(typeof this.modes[propId] == 'undefined')
        //	this.modes[propId] = {};

        //if(typeof this.modes[propId] != 'undefined' && this.modes[propId][mode])
        //	return true;

        if (mode == 'altLocationChoosen') {

            if (this.checkAbility(propId, 'canHaveAltLocation')) {

                var input = this.getInputByPropId(this.properties[propId].altLocationPropId);
                var altPropId = this.properties[propId].altLocationPropId;

                if (input !== false && input.value.length > 0 && !input.disabled && this.properties[altPropId].valueSource != 'default') {

                    //this.modes[propId][mode] = true;
                    return true;
                }
            }
        }

        return false;
    },

    checkAbility: function (propId, ability) {

        if (typeof this.properties[propId] == 'undefined')
            this.properties[propId] = {};

        if (typeof this.properties[propId].abilities == 'undefined')
            this.properties[propId].abilities = {};

        if (typeof this.properties[propId].abilities != 'undefined' && this.properties[propId].abilities[ability])
            return true;

        if (ability == 'canHaveAltLocation') {

            if (this.properties[propId].type == 'LOCATION') {

                // try to find corresponding alternate location prop
                if (typeof this.properties[propId].altLocationPropId != 'undefined' && typeof this.properties[this.properties[propId].altLocationPropId]) {

                    var altLocPropId = this.properties[propId].altLocationPropId;

                    if (typeof this.properties[propId].control != 'undefined' && this.properties[propId].control.getSysCode() == 'slst') {

                        if (this.getInputByPropId(altLocPropId) !== false) {
                            this.properties[propId].abilities[ability] = true;
                            return true;
                        }
                    }
                }
            }

        }

        return false;
    },

    getInputByPropId: function (propId) {
        if (typeof this.properties[propId].input != 'undefined')
            return this.properties[propId].input;

        var row = this.getRowByPropId(propId);
        if (BX.type.isElementNode(row)) {
            var input = row.querySelector('input[type="text"]');
            if (BX.type.isElementNode(input)) {
                this.properties[propId].input = input;
                return input;
            }
        }

        return false;
    },

    getRowByPropId: function (propId) {

        if (typeof this.properties[propId].row != 'undefined')
            return this.properties[propId].row;

        var row = this.controls.scope.querySelector('[data-property-id-row="' + propId + '"]');
        if (BX.type.isElementNode(row)) {
            this.properties[propId].row = row;
            return row;
        }

        return false;
    },

    getAltLocPropByRealLocProp: function (propId) {
        if (typeof this.properties[propId].altLocationPropId != 'undefined')
            return this.properties[this.properties[propId].altLocationPropId];

        return false;
    },

    toggleProperty: function (propId, way, dontModifyRow) {

        var prop = this.properties[propId];

        if (typeof prop.row == 'undefined')
            prop.row = this.getRowByPropId(propId);

        if (typeof prop.input == 'undefined')
            prop.input = this.getInputByPropId(propId);

        if (!way) {
            if (!dontModifyRow)
                BX.hide(prop.row);
            prop.input.disabled = true;
        } else {
            if (!dontModifyRow)
                BX.show(prop.row);
            prop.input.disabled = false;
        }
    },

    submitFormProxy: function (item, control) {
        var propId = false;
        for (var k in this.properties) {
            if (typeof this.properties[k].control != 'undefined' && this.properties[k].control == control) {
                propId = k;
                break;
            }
        }

        // turning LOCATION_ALT_PROP_DISPLAY_MANUAL on\off

        if (item != 'other') {

            if (this.BXCallAllowed) {

                this.BXCallAllowed = false;
                setTimeout(function () {
                    BX.Sale.OrderAjaxComponent.sendRequest()
                }, 20);
            }

        }
    },

    getPreviousAdapterSelectedNode: function (control, adapter) {

        var index = adapter.getIndex();
        var prevAdapter = control.getAdapterAtPosition(index - 1);

        if (typeof prevAdapter !== 'undefined' && prevAdapter != null) {
            var prevValue = prevAdapter.getControl().getValue();

            if (typeof prevValue != 'undefined') {
                var node = control.getNodeByValue(prevValue);

                if (typeof node != 'undefined')
                    return node;

                return false;
            }
        }

        return false;
    },
    getLocationsByZip: function (value, successCallback, notFoundCallback) {
        if (typeof this.indexCache[value] != 'undefined') {
            successCallback.apply(this, [this.indexCache[value]]);
            return;
        }

        var ctx = this;

        BX.ajax({
            url: this.options.source,
            method: 'post',
            dataType: 'json',
            async: true,
            processData: true,
            emulateOnload: true,
            start: true,
            data: {'ACT': 'GET_LOCS_BY_ZIP', 'ZIP': value},
            //cache: true,
            onsuccess: function (result) {
                if (result.result) {
                    ctx.indexCache[value] = result.data;
                    successCallback.apply(ctx, [result.data]);
                } else {
                    notFoundCallback.call(ctx);
                }
            },
            onfailure: function (type, e) {
                // on error do nothing
            }
        });
    }
};