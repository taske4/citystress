<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $USER;
use Bitrix\Main\Localization\Loc;
?>

<div class="cart_container container">
  <div class="cart_block block">
    
    <div class="cart_emptybox">
      <div class="cart_empty">
        <img src="<?= SITE_MAIN_TEMPLATE_PATH?>/img/bag.svg" alt="">
        <div class="cart_empty__title"><?=Loc::getMessage("SBB_EMPTY_BASKET_TITLE")?></div>
		<?if(!$USER->isAuthorized()){?>
        <div class="cart_empty__text"><?=Loc::getMessage("SBB_EMPTY_BASKET_DESCRIPTION_NON_AUTH")?></div>
		<?}else{?>
		 <div class="cart_empty__text"><?=Loc::getMessage("SBB_EMPTY_BASKET_DESCRIPTION")?></div>
		<?}?>
		
        <div class="cart_empty__actions">
		<?if(!$USER->isAuthorized()){?>
          <button class="button button_black auth_form">
            <span><?=Loc::getMessage("SBB_EMPTY_BASKET_LOGIN")?></span>
          </button>
		<?}?>
          <button class="button button_black">
            <a href="/catalog/odezhda/">
              <span><?=Loc::getMessage("SBB_EMPTY_BASKET_CATALOG")?></span></button>
            </a>
          </button>
        </div>
      </div>
    </div>
  
  </div>
</div>