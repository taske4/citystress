<?

namespace lib;

use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request,
    Bitrix\Main\Server;

class Helper
{
    public static function VarDump($data) {
        global $USER;

        $req= Context::getCurrent()->getRequest();

        if ($USER->IsAdmin() || ($req->getQuery('mode') === '41row5')) {
            foreach ($data as $key => $value) {
                echo "<details><summary><strong>${key}</strong></summary><pre>";
                if (is_array($value) && !empty($value)) {
                    self::VarDump($value);
                } else if (empty($value)) {
                    echo '<span style="color:red;">';
                    var_dump($value);
                    echo '</span>';
                } else {
                    var_dump($value);
                }
                echo "</pre></details>";
            }
        }
    }

    public static function ConvertCharset($str, $charset='UTF-8'): String
    {
        global $APPLICATION;
        return $APPLICATION->ConvertCharset($str, mb_detect_encoding($str), $charset);
    }

    public static function monthReplaceRu(string $string) {
        $monthsLangTranslate = [
            'January'   => 'Января',
            'February'  => 'Февраля',
            'March'     => 'Марта',
            'April'     => 'Апреля',
            'May'       => 'Мая',
            'June'      => 'Июня',
            'July'      => 'Июля',
            'August'    => 'Авруста',
            'September' => 'Сентября',
            'October'   => 'Октября',
            'November'  => 'Ноября',
            'December'  => 'Декабря',
        ];

        return str_replace(array_keys($monthsLangTranslate), $monthsLangTranslate, $string);
    }

    public static function elementActivityModify(\DateTime $activeFrom, \DateTime $activeTo)
    {
        $salesPeriod = '';

        if ($activeFrom->format('Y') === $activeTo->format('Y')) {
            $salesPeriod = 'с ' . $activeFrom->format('d F') . ' по ' . $activeTo->format('d F Y') . 'г.';

            if ($activeFrom->format('m') === $activeTo->format('m')) {
                $salesPeriod = 'с ' . $activeFrom->format('d') . ' по ' . $activeTo->format('d F Y') . 'г.';
            }
        } else {
            $salesPeriod = 'с ' . $activeFrom->format('d F Y') . 'г по ' . $activeTo->format('d F Y') . 'г.';
        }

        return self::monthReplaceRu($salesPeriod);
    }

}