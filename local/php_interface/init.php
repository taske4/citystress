<?

\Bitrix\Main\Config\Option::set("catalog", "DEFAULT_SKIP_SOURCE_CHECK", "Y");

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/constants.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/shorts.php';

if (file_get_contents($_SERVER['DOCUMENT_ROOT'] . COMPOSER_AUTOLOAD_FILE)) {
    require_once $_SERVER['DOCUMENT_ROOT'] . COMPOSER_AUTOLOAD_FILE;
} else {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/autoload.php';
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/event.php';

function formatToCustomFormat($phoneNumber)
{
    // Удалить все символы, кроме цифр
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    // Если номер начинается с 8, заменить на 7 (формат России)
    if (substr($phoneNumber, 0, 1) === '8') {
        $phoneNumber = '7' . substr($phoneNumber, 1);
    }

    // Если номер не начинается с 7, добавить 7 в начало (предполагаем формат России)
    if (substr($phoneNumber, 0, 1) !== '7') {
        $phoneNumber = '7' . $phoneNumber;
    }

    // Если длина номера больше 11 символов, обрезать до 11 символов
    if (strlen($phoneNumber) > 11) {
        $phoneNumber = substr($phoneNumber, 0, 11);
    }

    // Форматировать номер в заданный формат 79648557290
    $formattedNumber = preg_replace('/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/', '$1$2$3$4$5', $phoneNumber);

    return $formattedNumber;
}
function vardump($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

/*
 * ОТключаем jivo для карточки
 */
global $APPLICATION;
if (preg_match('/\/catalog\/product\/.+/', $APPLICATION->GetCurPage(false))) {
    define('PRODUCT_CARD', true);
}