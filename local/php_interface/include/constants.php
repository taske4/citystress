<?

define('SITE_MAIN_TEMPLATE_PATH', '/local/templates/main');
define('COMPOSER_AUTOLOAD_FILE',  '/local/vendor/autoload.php');

define('SUBSCRIBE_IBLOCK_ID', 10);
define('HL_SEARCH_RECENTLY', 7);
define('HL_COLORS', 5);
define('HL_SIZES', 3);
define('PROJECT_SETTINGS_IBLOCK_ID', 5);
define('PROJECT_SETTINGS_ELEMENT_CODE', 'SETTINGS');

define('SALES_IBLOCK_ID', 11);
define('CATALOG_IBLOCK_ID', 6);
define('CATALOG_OFFERS_IBLOCK_ID', 7);
define('CATALOG_SETS_IBLOCK_ID', 13);
define('CITIES_IBLOCK_ID', 2);

define('MICROMARKUP_IBLOCK_ID', 12); // изменено для stage


define('dadata_token', '3dfb19e52bd1201d48a0a5b66a8495c5a61dd9dd');
define('dadata_secret_key', "9b7664b0fe7c8b8a715f268c3bbf4d134cf32e00");

define('SHOPS', 8);

if (isset($_GET['type'], $_GET['mode']) && $_GET['type'] === 'catalog' && $_GET['mode'] === 'import') {
    define('CATALOG_IMPORT_1C', true);
} else {
    define('CATALOG_IMPORT_1C', false);
}

define('OLD_PRICE_GROUP_ID', 2);

define('EKB_STORE_ID', 32);

define('LOCATION_ID_EKB', 2203);
