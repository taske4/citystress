<?php

function regionality() {
    global $regionality;

    if (!$regionality) {
        $regionality = new \Citystress\Regionality();
    }

    return $regionality;
}
function plural($n, $form1, $form2, $form3) {
	return in_array($n % 10, array(2,3,4)) && !in_array($n % 100, array(11,12,13,14)) ? $form2 : ($n % 10 == 1 ? $form1 : $form3);
}

function mb_ucfirst($text) {
    return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
}

function search_key($searchKey, array $arr, array &$result)
{
    // Если в массиве есть элемент с ключем $searchKey, то ложим в результат
    if (isset($arr[$searchKey])) {
        $result[] = $arr[$searchKey];
    }
    // Обходим все элементы массива в цикле
    foreach ($arr as $key => $param) {
        // Если эллемент массива есть массив, то вызываем рекурсивно эту функцию
        if (is_array($param)) {
            search_key($searchKey, $param, $result);
        }
    }
}