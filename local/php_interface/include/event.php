<?

use Bitrix\Main\Event;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Iblock\ElementTable;

$eventManager = \Bitrix\Main\EventManager::getInstance();

/*
 * Когда клиент изменяет данные пользователя
 */
$eventManager->addEventHandler('main', 'OnBeforeUserUpdate', function(&$arFields){
    global $USER;

    $oldEmail = null;

    $checkSubscribe = false;
    if ($_REQUEST['SUBSCRIBE_ON_STRESS']) {
        $checkSubscribe = true;
    }

    $oldUserData = \Bitrix\Main\UserTable::query()
        ->setSelect(['EMAIL'])
        ->setFilter(['=ID' => $arFields['ID']])
        ->fetch();

    $arFields["EMAIL"] = trim($arFields["EMAIL"]);

    if ($oldUserData['EMAIL'] !== $arFields["EMAIL"]) {
        $oldEmail = $oldUserData['EMAIL'];

        global $APPLICATION;


        /*$emailValid = "/^[A-Z0-9._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i";

        if (!preg_match($emailValid, $arFields["EMAIL"])) {
            $APPLICATION->ThrowException("Неверный email.");
            return false;
        } else {*/
            $emailExist = \Bitrix\Main\UserTable::query()
                ->setSelect(["ID"])
                ->setFilter(["=LOGIN" => $arFields["EMAIL"]])
                ->fetch();

            if ($emailExist) {
                $APPLICATION->ThrowException("Пользователь с таким email уже существует.");
                return false;
            }
        //}

        $arFields["LOGIN"] = $arFields["EMAIL"];

        \Bitrix\Main\Application::getInstance()->getSession()->set("is_user_change_email", true);

        $checkSubscribe = true;
    }

    if ($checkSubscribe) {
        $searchValue = $oldEmail ?: $arFields['EMAIL'];

        $subscribeEntity = \Bitrix\Iblock\Iblock::wakeUp(SUBSCRIBE_IBLOCK_ID)->getEntityDataClass();
        $subscribeFound = $subscribeEntity::query()
            ->setSelect(['ID', 'EMAIL_' => 'EMAIL'])
            ->setFilter(['=EMAIL_VALUE' => $searchValue])
            ->fetch();

        if ($subscribeFound && $oldEmail) {
            (new \CIBlockElement)->Update($subscribeFound['ID'], [
                'NAME'      => 'Новая подписка (Профиль пользвателя -> изменил email)',
                'IBLOCK_ID' => SUBSCRIBE_IBLOCK_ID,
                'PROPERTY_VALUES' => [
                    '132' => $arFields['EMAIL'], // EMAIL
                ]
            ]);
        } elseif (!$subscribeFound && ($_REQUEST['SUBSCRIBE_ON_STRESS'] === 'Y')) {
            (new \CIBlockElement)->Add([
                'NAME'      => 'Новая подписка (Профиль пользвателя)',
                'IBLOCK_ID' => SUBSCRIBE_IBLOCK_ID,
                'PROPERTY_VALUES' => [
                    '132' => $arFields['EMAIL'], // EMAIL
                ]
            ]);
        } elseif ($subscribeFound && ($_REQUEST['SUBSCRIBE_ON_STRESS'] === 'N')) {
            \CIBlockElement::Delete($subscribeFound['ID']);
        }
    }
});

$eventManager->addEventHandler('main', 'OnAfterUserUpdate', function($arFields) {
    if (\Bitrix\Main\Application::getInstance()->getSession()->get("is_user_change_email")) {
        \Bitrix\Main\Mail\Event::send([
            "EVENT_NAME" => "USER_CHANGE_EMAIL",
            "LID"        => "s1",
            "C_FIELDS"   => [
                "EMAIL" => $arFields["EMAIL"],
            ],
        ]);
    }
});

// Модификация обмена товарами
$eventManager->addEventHandler('iblock', 'OnBeforeIBlockElementAdd', ['CatalogImport1c', 'onElementBeforeAdd']);
$eventManager->addEventHandler('iblock', 'OnAfterIBlockElementAdd', ['CatalogImport1c', 'onOffer']);
$eventManager->addEventHandler('iblock', 'OnAfterIBlockElementUpdate', ['CatalogImport1c', 'onOffer']);
$eventManager->addEventHandler('iblock', 'OnBeforeIBlockElementUpdate', ['CatalogImport1c', 'onOffer_DoNotUpdate']);

// Привязка набора
$eventManager->addEventHandler('iblock', 'OnAfterIBlockElementUpdate', ['Offers', 'onLinkSet']);
// Модификация Наборов
$eventManager->addEventHandler('iblock', 'OnBeforeIBlockElementUpdate', ['Sets', 'onLinkOffers']);
// Привязка ацкии
$eventManager->addEventHandler('iblock', 'OnBeforeIBlockElementUpdate', ['Offers', 'onSaleLink']);



$eventManager->addEventHandler('catalog', 'OnCatalogStoreAdd', ['CatalogStore', 'onStoreUpdate']);
$eventManager->addEventHandler('catalog', 'OnBeforeCatalogStoreDelete', ['CatalogStore', 'onStoreDelete']);

$eventManager->addEventHandler('main', 'OnPageStart', function(){
    regionality()->detectCity();
});


function linkOldPrice($id, $fields, $delete=false){
    if (intval($fields['CATALOG_GROUP_ID']) === OLD_PRICE_GROUP_ID) {

        if (CATALOG_IMPORT_1C) {
            return false;
        }

        $offerId = $fields['PRODUCT_ID'];
        $price   = $fields['PRICE'];

        $res = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass()::getByPrimary($offerId,
            [
                'select' => [
                    'CML2_LINK',
                    'COLOR',
                    'ACTIVE',
                ],
            ]
        )->fetchObject();

        $active = $res->getActive();
        $cml2link = $res->getCml2Link()->getValue();
        $color = $res->getColor()->getValue();

        if ((intval($price)) > 0 && $active && $cml2link) {
            $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => CATALOG_IBLOCK_ID, "CODE" => "SALE"));
            
            while ($enum_fields = $property_enums->GetNext()) {
                /* Если значение равно "Да" */
                if ($enum_fields["VALUE"] == "Да") {
                    $arProperty = array(
                        "SALE" => $enum_fields["ID"],
                    );

                    if ($enum_fields['ID']) {
                        CIBlockElement::SetPropertyValuesEx($cml2link, CATALOG_IBLOCK_ID, $arProperty);
                    }
                }
            }
        }

        $sku = [];

        if (strlen($color)) {
            if ($cml2link) {
                $sku = array_keys(\CCatalogSKU::getOffersList(
                    $cml2link,
                    6,
                    ['PROPERTY_COLOR' => $color],
                    ['ID'],
                    ['ID'],
                )[$cml2link]);
            }
        }

        foreach($sku as $skuId) {
            if ($offerId == $skuId) {
                continue;
            }

            try {
                $offerPriceRow = \Bitrix\Catalog\PriceTable::query()
                    ->setSelect(['ID', 'PRICE'])
                    ->setFilter([
                        'PRODUCT_ID' => $skuId,
                        'CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID,
                    ])
                    ->fetch();
            } catch (Error | Exception $e) {
                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/onPriceAddException.json', json_encode($e->getMessage()));
            }

            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/offerPriceRow.json', json_encode($offerPriceRow));

            if ($offerPriceRow) {
                if (intval($offerPriceRow['PRICE']) != intval($price)) {
                    if ($delete) {
                        \Bitrix\Catalog\PriceTable::delete($offerPriceRow['ID']);
                    } else {
                        \Bitrix\Catalog\PriceTable::update(
                            $offerPriceRow['ID'],
                            [
                                'PRICE' => $price,
                                'PRICE_SCALE' => $price,
                            ]
                        );
                    }
                }
            } else {
                $res = \Bitrix\Catalog\PriceTable::add([
                    'PRODUCT_ID'       => $skuId,
                    'CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID,
                    'PRICE'            => $price,
                    'CURRENCY'         => 'RUB',
                    'PRICE_SCALE'      => $price,
                ]);

                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/PriceTableAdd.json', json_encode($res->getErrorMessages()));
            }
        }
    }
}

$eventManager->addEventHandler(
    'catalog',
    'OnBeforePriceUpdate',
    'linkOldPrice',
);
$eventManager->addEventHandler(
    'catalog',
    'OnPriceAdd',
    'linkOldPrice',
);
$eventManager->addEventHandler(
    'catalog',
    'OnBeforePriceDelete',
    function($id) {
        $arPrice = \CPrice::GetByID($id);
        if ($arPrice["CATALOG_GROUP_ID"] == OLD_PRICE_GROUP_ID){
            return false;
        }
    }
);
$eventManager->addEventHandler(
    'catalog',
    'OnProductPriceDelete',
    function ($id, $arr) {
        $delete = true;
        foreach ($arr as $ar) {
            $arPrice = \CPrice::GetByID($ar);
            if ($arPrice["CATALOG_GROUP_ID"] == OLD_PRICE_GROUP_ID) {
                $delete = false;
                break;
            }
        }

        if ($delete) {
            linkOldPrice(0, [
                'PRODUCT_ID' => $id,
                'CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID,
                'PRICE' => 0,
            ], true);
        }
    }
);
$eventManager->addEventHandler(
    'catalog',
    'OnProductPriceDelete',
    function ($offerId, $exceptionPriceIds) {
        $exceptionOldPrice = \Bitrix\Catalog\PriceTable::query()
            ->setFilter(['=ID' => $exceptionPriceIds, '=CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID])
            ->fetchAll();

        if ($exceptionOldPrice) {
            return true;
        }

        $offersEntity = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass();

        $offer = $offersEntity::query()
            ->setSelect(['CML2_LINK_' => 'CML2_LINK'])
            ->setFilter([
                '=ID'    => $offerId,
                'ACTIVE' => 'Y',
            ])
            ->fetch();

        if (!$offer) {
            return true;
        }

        $cml2link = $offer['CML2_LINK_VALUE'];

        $offers = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass()::query()
            ->setSelect(['ID', 'CML2_LINK_' => 'CML2_LINK'])
            ->setFilter([
                'ACTIVE'          => 'Y',
                'CML2_LINK_VALUE' => $cml2link,
            ])
            ->fetchAll();

        $offersIds = array_column($offers, 'ID');

        if (count($offersIds)) {
            $offersHasOldPrice = \Bitrix\Catalog\PriceTable::query()
                ->setSelect(['ID'])
                ->setFilter([
                    '=PRODUCT_ID'       => $offersIds,
                    '=CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID,
                    '>PRICE'            => 0,
                ])
                ->fetchAll();

            if (!$offersHasOldPrice) {
                CIBlockElement::SetPropertyValuesEx($cml2link, CATALOG_IBLOCK_ID, ['SALE' => '']);
            }
        }
    }
);

Loader::includeModule("highloadblock");

$log = new \Monolog\Logger('1cexchange');
$log->pushHandler(new \Monolog\Handler\StreamHandler($_SERVER['DOCUMENT_ROOT'].'/local/logs/catalog_import.log'), Monolog\Logger::INFO);

$eventManager->addEventHandler('main', 'OnAfterUserAuthorize', function($arFields){
    $userId = $arFields['user_fields']['ID'];

    /**
     * Мерджим Избранное
     */

    if (
        $ids = \Citystress\Favorites::getInstance()->get(null)
    ) {
        \Citystress\Favorites::getInstance()->remove(null, $ids);

        foreach ($ids as $id) {
            \Citystress\Favorites::getInstance()->add($userId, $id);
        }
    }

    /**
     * Мерджим результаты поиска
     */
    \Citystress\SearchRecently::getInstance()->merge();
});

class CatalogImport1c {
    /*
public function handlerOnPrice(Event $event) {
        $result = new \Bitrix\Main\Entity\EventResult;
        $data = $event->getParameter("fields");

        $price = $data['PRICE'];

        $res = \Bitrix\Catalog\PriceTable::query()
            ->setSelect([
                'PRICE'
            ])
            ->setFilter([
                'CATALOG_GROUP_ID' => $data['CATALOG_GROUP_ID'],
                'PRODUCT_ID'       => $data['PRODUCT_ID'],
            ])->fetch();

        if (($res) && ($price < $res['PRICE'])) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/onPriceDebug.json', PHP_EOL.'OLD', FILE_APPEND);

            $oldPrice = $res['PRICE'];
            $res = \Bitrix\Catalog\PriceTable::query()
                ->setSelect([
                    'ID'
                ])
                ->setFilter([
                    'CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID,
                    'PRODUCT_ID'       => $data['PRODUCT_ID'],
                ])->fetch();

            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/onPriceDebug.json', PHP_EOL.'findOld: '.$res, FILE_APPEND);

            if ($res) {
                \Bitrix\Catalog\PriceTable::update($res['ID'], [
                    'PRICE'       => $oldPrice,
                    'PRICE_SCALE' => $oldPrice,
                ]);
            } else {
                $res = \Bitrix\Catalog\PriceTable::add([
                    'PRODUCT_ID'       => $data['PRODUCT_ID'],
                    'CATALOG_GROUP_ID' => OLD_PRICE_GROUP_ID,
                    'PRICE'            => $oldPrice,
                    'PRICE_SCALE'      => $oldPrice,
                    'CURRENCY'         => 'RUB'
                ]);

                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/onPriceDebug.json', PHP_EOL.'addRes: '.$res->isSuccess().PHP_EOL.json_encode($res->getErrorMessages()), FILE_APPEND);
            }
        } else {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/onPriceDebug.json', PHP_EOL.'NOOOO OLD', FILE_APPEND);
        }

        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/logs/onPriceDebug.json', PHP_EOL.'OK, '.PHP_EOL.json_encode($data), FILE_APPEND);
    }
*/

    public static function onElementBeforeAdd(&$arFields) {

        if (
            CATALOG_IMPORT_1C &&
            in_array(
                intval($arFields['IBLOCK_ID']),
                [CATALOG_IBLOCK_ID, CATALOG_OFFERS_IBLOCK_ID]
            )
        ) {
            $arFields['ACTIVE'] = 'N';
        }
    }

    public static function onOffer_DoNotUpdate(&$arFields)
    {
        if (!CATALOG_IMPORT_1C) {
            return true;
        }

        unset($arFields['NAME']);
        unset($arFields['IBLOCK_SECTION']);
        unset($arFields['PREVIEW_PICTURE']);
        unset($arFields['DETAIL_PICTURE']);
        unset($arFields['PREVIEW_TEXT']);
        unset($arFields['DETAIL_TEXT']);

        if (intval($arFields['IBLOCK_ID']) !== CATALOG_OFFERS_IBLOCK_ID) {
            return true;
        }

        foreach (['FILES', 'MORE_PHOTO'] as $propCode) {
            $propRes = \Bitrix\Iblock\PropertyTable::query()
                ->addSelect('ID')
                ->setFilter([
                    'IBLOCK_ID' => CATALOG_OFFERS_IBLOCK_ID,
                    'CODE' => $propCode,
                ])
                ->fetch();

            if ($propRes['ID'] && $arFields['PROPERTY_VALUES'][$propRes['ID']]) {
                unset($arFields['PROPERTY_VALUES'][$propRes['ID']]);
            }
        }

        $offer = \Bitrix\Iblock\Elements\ElementTradeoffersTable::query()
            ->setSelect(['MORE_PHOTO_' => 'MORE_PHOTO', 'ACTIVE'])
            ->setFilter([
                '=ID'      => $arFields['ID'],
            ])
            ->fetch();

        $catalogQuantity = \Bitrix\Catalog\ProductTable::query()
            ->setSelect(['QUANTITY'])
            ->setFilter(['ID' => $arFields['ID']])
            ->setLimit(1)
            ->fetch()['QUANTITY'];

        if (
            ($catalogQuantity < 10)
            ||
            !isset($offer['MORE_PHOTO_VALUE'])
        ) {
            $arFields['ACTIVE'] = 'N';
        }

    }

    public static function onOffer(&$params) {
        global $APPLICATION;
        global $log;

        if ((intval($params['IBLOCK_ID']) !== CATALOG_OFFERS_IBLOCK_ID) ) { //|| !CATALOG_IMPORT_1C) {
            return true;
        }

        $logFile = $_SERVER['DOCUMENT_ROOT'].'/local/logs/onOffer.json';
        $logFileData = [];

        /**
         * Линковка =======================
         */

        $res = \Bitrix\Iblock\Iblock::wakeUp($params['IBLOCK_ID'])->getEntityDataClass()::getByPrimary($params['ID'],
            [
                'select' => [
                    'CML2_LINK',
                    'MORE_PHOTO',
                    'COLOR',
                ],
            ]
        )->fetchObject();

        $pictures = [];
        $cml2link = $res->getCml2Link()->getValue();
        $color = $res->getColor()->getValue();

        $logFileData['paramsId'] = $params['ID'];
        $logFileData['cml2link'] = $cml2link;

        /*
         * Деактивируем/Активируем товар
         */
        $offers = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_OFFERS_IBLOCK_ID)->getEntityDataClass()::query()
            ->setSelect(['ACTIVE', 'CML2_LINK_' => 'CML2_LINK'])
            ->setFilter(['CML2_LINK_VALUE' => $cml2link])
            ->fetchAll();

        $hasActiveOffers = false;

        foreach($offers as $offer) {
            if ($offer['ACTIVE'] === 'Y') {
                $hasActiveOffers = true;
                break;
            }
        }

        $ibp = new \CIBlockElement();
        $ibp->Update($cml2link, ['ACTIVE' => ($hasActiveOffers ? 'Y' : 'N')]);
        /*
         * #############################
         */


        if (strlen($color)) {
            if ($cml2link) {
                $getOffersListResult = \CCatalogSKU::getOffersList(
                    $cml2link,
                    6,
                    [],
                    ['ID', 'PROPERTY_COLOR', 'PROPERTY_MORE_PHOTO'],
                    ['ID'],
                )[$cml2link];

                $sku = [];

                $skuMorePhoto = [];
                foreach($getOffersListResult as $offerId => $offer) {
                    if (
                        trim(strtolower($offer['PROPERTY_COLOR_VALUE'])) === trim(strtolower($color))
                    ) {
                        $skuMorePhoto[$offerId] = $offer['PROPERTY_MORE_PHOTO_VALUE'];
                        $sku[] = $offerId;
                    }
                }
            }
            
            


            $element = \Bitrix\Iblock\Iblock::wakeUp($params['IBLOCK_ID'])->getEntityDataClass()::getList([
                'select' => [
                    'ID', 'DESCRIPTION',
                    'MORE_PHOTO.FILE', 'HEIGHT_MODEL_ON_PHOTO', 'PARAMS_MODEL_ON_PHOTO', 'SEO_WORDS', 'SIZE_ON_MODEL',
                    'SALES_FLAG',
                    'SALES_FLAG_INFO',
                    'LINK_SALE'
                ],
                'filter' => [
                    'ID' => $params['ID'],
                ],
            ])->fetchObject();

            unset($sku[array_search($params['ID'], $sku ?: [])]);

            if (is_array($sku) && count($sku)) {
                /**
                 * Собираем данные для линка ?
                 */

                $desc               = unserialize($element->getDescription()->getValue())['TEXT'];
                $heightModelOnPhoto = $element->getHeightModelOnPhoto()->getValue();
                $sizeOnModel        = $element->getSizeOnModel()->getValue();
                $salesFlag          = $element->getSalesFlag()->getValue();
                $salesFlagInfo      = $element->getSalesFlagInfo()->getValue();
                $linkSale           = $element->getLinkSale()->getValue() ?: null;

                $morePhoto = [];

                if (!CATALOG_IMPORT_1C) {
                    foreach ($element->getMorePhoto()->getAll() as $value) {
                        $picId = $value->getFile()->getId();
                        $morePhoto[] = (string)$picId;
                    }
                }

                $paramsModelOnPhoto = [];
                foreach ($element->getParamsModelOnPhoto()->getAll() as $value) {
                    $paramsModelOnPhoto[] = $value->getValue();
                }

                $seoWords = [];
                foreach ($element->getSeoWords()->getAll() as $value) {
                    $seoWords[] = $value->getValue();
                }

                /**
                 * Ну и сетим
                 */
                foreach ($sku as $offerId) {
//                CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['MORE_PHOTO' => []]);

                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['SALES_FLAG' => $salesFlag]);
                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['SALES_FLAG_INFO' => $salesFlagInfo]);
                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['HEIGHT_MODEL_ON_PHOTO' => $heightModelOnPhoto]);
                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['PARAMS_MODEL_ON_PHOTO' => $paramsModelOnPhoto]);
                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['SEO_WORDS' => $seoWords]);
                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['SIZE_ON_MODEL' => $sizeOnModel]);

                    if (count($skuMorePhoto[$offerId]) !== count($morePhoto)) {
                        $morePhotoSkuValues = [];

                        foreach($morePhoto as $picId) {
                            $morePhotoSkuValues[] = \CFile::MakeFileArray(intval($picId));
                        }

                        \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['MORE_PHOTO' => $morePhotoSkuValues]);
                    }

                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['DESCRIPTION' => $desc]);

                    \CIBlockElement::SetPropertyValuesEx($offerId, $params['IBLOCK_ID'], ['LINK_SALE' => $linkSale]);
                }

                if ($cml2link) {
                    $getOffersListResult = \CCatalogSKU::getOffersList(
                        $cml2link,
                        6,
                        ['ACTIVE' => 'Y'],
                        ['ID', 'PROPERTY_LINK_SALE'],
                        ['ID'],
                    )[$cml2link];

                    $productOffersSales = [];

                    foreach($getOffersListResult as $offer) {
                        if ($saleLink = $offer['PROPERTY_LINK_SALE_VALUE']) {
                            $productOffersSales[$saleLink] = $saleLink;
                        }
                    }

                    $productOffersSales = array_unique($productOffersSales);
                }

                if (is_array($productOffersSales) && !count($productOffersSales)) {
                    $productOffersSales = null;
                }

                \CIBlockElement::SetPropertyValuesEx($cml2link, CATALOG_IBLOCK_ID, ['OFFERS_SALES' => $productOffersSales], false);
            }
        }

        try {
            $propValues = $params['PROPERTY_VALUES'];

            foreach (['TSVET', 'RAZMER'] as $propCode) {

                $propRes = \Bitrix\Iblock\PropertyTable::query()
                    ->addSelect('ID')
                    ->setFilter([
                        'IBLOCK_ID' => CATALOG_OFFERS_IBLOCK_ID,
                        'CODE' => $propCode,
                    ])
                    ->fetch();

                $propValue = null;


                if ($propRes['ID']) {
                    $colorPropValue = $propValues[$propRes['ID']];
                    $propValueId = $colorPropValue[0]['VALUE'];


                    if (!$propValueId) {
                        $propValueId = $colorPropValue['n0']['VALUE'];
                    }

                    if (!$propValueId && is_array($colorPropValue)) {
                        $searchValue = [];
                        search_key('VALUE', $colorPropValue, $searchValue);

                        $propValueId = $searchValue[0];

                    }
                }



                if ($propValueId) {
                    $propValue = \Bitrix\Iblock\PropertyEnumerationTable::query()
                        ->setSelect(['VALUE', 'ID'])
                        ->addFilter('ID', $propValueId)
                        ->fetch()['VALUE'];

                    if ($propCode == 'TSVET') {
                        $propValue = mb_ucfirst($propValue);
                    }

                    if (!$propValue) {
                        \Bitrix\Iblock\ElementTable::update($params['ID'], ['ACTIVE' => 'N']);

                        $log->debug('Не задан цвет или размер');

                        $APPLICATION->throwException("Не задан цвет или размер");
                        return false;
                    }
                }

                if ($propCode === 'TSVET') {
                    $hlid = HL_COLORS;
                } elseif ($propCode === 'RAZMER') {
                    $hlid = HL_SIZES;
                }


                if (
                    ($propCode === 'TSVET') && $propValue ||
                    ($propCode === 'RAZMER') && $propValue
                ) {
                    $hlblock = HL\HighloadBlockTable::getById($hlid)->fetch();
                    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();


                    $res = $entity_data_class::getList([
                        'select' => [
                            'ID',
                            'UF_NAME',
                        ],
                        'filter' => [
                            'UF_NAME' => trim($propValue),
                        ]
                    ])->fetch();

                    if (!$res) {
                        $entity_data_class::add([
                            'UF_NAME' => trim($propValue),
                            'UF_XML_ID' => trim($propValue),
                        ]);
                    } else {
                        $propValue = $res['UF_NAME'];
                    }

                    if (($propCode === 'RAZMER') && $propValue) {
                        \CIBlockElement::SetPropertyValuesEx($params['ID'], $params['IBLOCK_ID'], ['SIZE' => $propValue]);
                    } elseif (($propCode === 'TSVET') && $propValue) {
                        \CIBlockElement::SetPropertyValuesEx((int)$params['ID'], $params['IBLOCK_ID'], ['COLOR' =>
                            trim(mb_ucfirst($propValue))]);
                    }

                    \CIBlock::clearIblockTagCache($params['IBLOCK_ID']);
                }
            }
        } catch (\Error | \Exception $e) {
            $log->error($e->getMessage());
        }

        return true;
    }
}

class Offers
{
    public static function onSaleLink(&$arFields)
    {
        if (CATALOG_IMPORT_1C) {
            return true;
        }

        if ((intval($arFields['IBLOCK_ID']) !== CATALOG_OFFERS_IBLOCK_ID) ) {
            return true;
        }

        /*
        $saleLinkPropId    = \Citystress\Helper\Property::getPropertyId('LINK_SALE', CATALOG_OFFERS_IBLOCK_ID);
        $saleLinkProp      = $arFields['PROPERTY_VALUES'][$saleLinkPropId] ?: [];
        $saleLinkPropValue = end($saleLinkProp)['VALUE'];
        */

        /*if ($saleLinkPropValue) {
            $sales = \Bitrix\Iblock\Iblock::wakeUp(SALES_IBLOCK_ID)->getEntityDataClass();
            $linkedSale = $sales::query()
                ->setSelect(['ACTIVE'])
                ->setFilter(['ID' => $saleLinkPropValue])
                ->fetch();

            global $APPLICATION;

            if (!$linkedSale) {
                unset($arFields['PROPERTY_VALUES'][$saleLinkPropId]);
                $APPLICATION->throwException("Акция не найдена");
                return false;
            } elseif ($linkedSale['ACTIVE'] === 'N') {
                unset($arFields['PROPERTY_VALUES'][$saleLinkPropId]);
                $APPLICATION->throwException("Акция не активна");
                return false;
            }
        }*/
    }

    public static function onLinkSet($arFields)
    {
        if (CATALOG_IMPORT_1C) {
            return true;
        }
        
        if ((intval($arFields['IBLOCK_ID']) !== CATALOG_OFFERS_IBLOCK_ID) ) {
            return true;
        }

        $setLinkPropId = \Citystress\Helper\Property::getPropertyId('SET_LINK', CATALOG_OFFERS_IBLOCK_ID);
        $setPropIdValue = $arFields['PROPERTY_VALUES'][$setLinkPropId] ?: [];
        $setLinkValue = end($setPropIdValue)['VALUE'];

        $offer = \Citystress\Entity\Offer::getById((int)$arFields['ID']);

        $variants = $offer['VARIANTS'];

        if (!$variants) {
            return true;
        }

        foreach($variants as $value) {
            \CIBlockElement::SetPropertyValuesEx($value['ID'], $arFields['IBLOCK_ID'], ['SET_LINK' => $setLinkValue]);
        }
    }
}

class Sets {
    public static function onLinkOffers(&$arFields)
    {
        return true;

        if ((intval($arFields['IBLOCK_ID']) !== CATALOG_SETS_IBLOCK_ID) ) {
            return true;
        }

        $setId = $arFields['ID'];

        $setsEntity   = \Bitrix\Iblock\Iblock::wakeUp(CATALOG_SETS_IBLOCK_ID)->getEntityDataClass();

        $setResult = $setsEntity::query()
            ->setSelect(['OFFERS_LINK'])
            ->setFilter(['ID' => $setId])
            ->fetchObject();

        $offerLinkPropId = \Citystress\Helper\Property::getPropertyId('OFFERS_LINK', CATALOG_SETS_IBLOCK_ID);
        $res = $arFields['PROPERTY_VALUES'][$offerLinkPropId];

        $linkedOffers = $offersValues = [];

        foreach($res as $value) {
            $offersValues[] = (int)$value['VALUE'];
        }

        foreach($setResult->getOffersLink()->getAll() as $value) {
            $linkedOffers[] = (int)$value->getValue();
        }

        $offersValues = array_filter($offersValues, function($value){
            return !!$value;
        });

        $diffOffers = array_diff($offersValues, $linkedOffers);

        if ($diffOffers) {
            $offers = \Citystress\Entity\Offer::getList(['ID' => $diffOffers]);

            foreach ($offers as $offer) {
                foreach ($offer['VARIANTS'] as $variantId) {
                    $offersValues[] = (int)$variantId;
                }
            }

            $arFields['PROPERTY_VALUES'][$offerLinkPropId] = null;

            $i = 0;
            foreach ($offersValues as $value) {
                $arFields['PROPERTY_VALUES'][$offerLinkPropId]['n' . $i++]['VALUE'] = $value;
            }
        }
    }
}

class CatalogStore {
public function onStoreUpdate($id, $arFields)
{
 			\CModule::IncludeModule('iblock');
                $el = new \CIBlockElement;
 	$coords = regionality()->getcoords($arFields["ADDRESS"]);
			$PROP = [
            'PVZ_LOCATION' => \Citystress\Connector\tc\cdek\Adapter::getLocationByCityName(regionality()->getcity($arFields["ADDRESS"])),
            'TYPE'         => 1480,
            'PHONE'        => $arFields["PHONE"],
            'SCHEDULE'	   =>  $arFields["SCHEDULE"]];
			$arLoadProductArray = Array(
                    "NAME"              => $arFields["ADDRESS"],
                    "ACTIVE_FROM"       => date('d.m.Y H:i:s'),
                    "IBLOCK_SECTION_ID" => false,
                    "IBLOCK_ID"         => 8,
					'MAP_COORDS'   =>$coords["lat"].', '.$coords["lon"],
                    "ACTIVE"            => "Y",
                    "PREVIEW_TEXT"      => $arFields["DESCRIPTION"],
					"XML_ID" =>  $arFields['XML_ID'],
                    "PROPERTY_VALUES"   => $PROP,
                );
			 $newElement = $el->Add($arLoadProductArray);
}
public function onStoreDelete($id)
{
	$rsStore = \Bitrix\Catalog\StoreTable::getList(array(

    'filter' => array('ACTIVE'>='Y',"ID"=>$id),

));
while($arStore=$rsStore->fetch())

{
	if($arStore['XML_ID']!=''){
	\CModule::IncludeModule('iblock');
    $el = new \CIBlockElement;
	  $arFilter = [
        'IBLOCK_ID' => 8,
        'PROPERTY_TYPE_VALUE' => 'CityStress',
		'XML_ID' => $arStore['XML_ID']
        ];
        $arSelectFields = [
            'IBLOCK_ID',
            'ID',
        ];
        $rsElements = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelectFields);
		if ($arElement = $rsElements->GetNext()) {
			$el->Update($arElement['ID'], ['ACTIVE'=>'N']);
		}
	}
}
}
}