<?
namespace Sale\Handlers\Delivery;
use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;
use Citystress\Delivery;

class PvzHandler extends Base
{
	public static function getClassTitle()
		{
			return 'Настраиваемая доставка Citystress';
		}
		
	public static function getClassDescription()
		{
			return '';
		}
		
	protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
		{
			$result = new CalculationResult();
			$order = $shipment->getCollection()->getOrder();
			$propertyCollection = $order->getPropertyCollection();
    $property = $propertyCollection->getItemByOrderPropertyId(5); 
    $property_address = $propertyCollection->getItemByOrderPropertyId(4); 
	$property_shop = $propertyCollection->getItemByOrderPropertyId(6); 
    $order_price = $order->getPrice()-$order->getDeliveryPrice();
 $basket = $order->getBasket();
 $item = 0;
 	foreach ($basket as $basketItem) {
			 
				$item = $item+$basketItem->getField('QUANTITY');
		 
		}
 
if($this->config["MAIN"]["PRICE"])
{
	if( $order_price>=10000){
		$price=0;
	}
	else
	{
	$price=$this->config["MAIN"]["PRICE"];
	}
	if($this->config["MAIN"]['TYPE']=='courier' &&$property_address->getValue()!='')
	{
		 $address = explode(',',$property_address->getValue());
			$params = (object)  array(
		'locationId'=> $_SESSION["CITY"]["LOCATION_ID"],
        'street'=> $address[1],
        'house'=> $address[2],
        'weight'=> 1.2*$item);
	 $info_courier = \Citystress\Connector\tc\cdek\Adapter::getCourierDeliveryInfo($params);
if($info_courier["DAYS"]!=null){	 
	 $days = 'от '.  $info_courier["DAYS"]. ' '.plural( $info_courier["DAYS"], 'дня', 'дней', 'дней');
}
elseif($info_courier["DELIVERY_MIN"]!=null)
{
	 $days = 'от '.  $info_courier["DELIVERY_MIN"]. ' '.plural( $info_courier["DELIVERY_MIN"], 'дня', 'дней', 'дней');
}
	  if($order_price<10000){
		 	$price = $info_courier['PRICE'];
			if($_SESSION["CITY"]["LOCATION_ID"]==2203) 	$price = $price*1.2;
			
			$result->setDeliveryPrice($price);
		   
	  }
	 
			$result->setPeriodDescription( $days );
	}

}
else
{
 
	if($property->getValue() && $this->config["MAIN"]['TYPE']=='pickuppoint' ){
	$params = (object)  array(
'pvzId' => $property->getValue(),
'weight' => 1.2*$item);

 $info = \Citystress\Connector\tc\cdek\Adapter::getPvzDeliveryInfo($params);
	if( $order_price>=5000){
		$price=0;
	}
	else
	{
		$price = $info['PRICE'];
		
		if($_SESSION["CITY"]["LOCATION_ID"]==2203) 	$price = $price*1.2;
	} 

	if(preg_match('/%DELIVERY_MIN%/',$info["DAYS_INFO"])){
	//$days =  preg_replace(array( '/%DELIVERY_MIN%/','/%DELIVERY_MAX%/'),array($info["DELIVERY_MIN"],$info["DELIVERY_MAX"]),$info["DAYS_INFO"]);
		$days = 'от '. $info["DELIVERY_MIN"]. ' '.plural($info["DELIVERY_MIN"], 'дня', 'дней', 'дней');
	}
	else
	{
		$days = $info["DAYS"]. ' '.plural($info["DAYS"], 'день', 'дня', 'дней');
	}

	}	
			$result->setDeliveryPrice(roundEx($price));
			$result->setPeriodDescription($days);
}
if( $this->config["MAIN"]['TYPE']=='shoppickup' ) 
{
    $stressMore = (new Delivery())->calcDeliveryStress($property_shop->getValue());
	if($stressMore['days']>0){
	$days = 'от '.$stressMore['days']. ' '.plural($stressMore['days'], 'дня', 'дней', 'дней');
	}
	else
	{
		$days = 'Можно забрать сегодня';
	}
 
    $result->setPeriodDescription($days);
}
 	
 
			return $result;
		}
		
	protected function getConfigStructure()
		{
	return array(
				"MAIN" => array(
					"TITLE" => 'Настройка обработчика',
					"DESCRIPTION" => 'Настройка обработчика',
					"ITEMS" => array(
						"TYPE" => array(
									"TYPE" => "STRING",
									"NAME" => 'Тип доставки'
						),
						"PRICE" => array(
									"TYPE" => "NUMBER",
									"MIN" => 0,
									"NAME" => 'Стоимость доставки'
						)
					)
				)
			);
		}
		
	public function isCalculatePriceImmediately()
		{
			return true;
		}
		
	public static function whetherAdminExtraServicesShow()
		{
			return true;
		}
}
?>