<?php

namespace src;

use Bitrix\Main\Loader;

class ProjectSettings
{
    public static ProjectSettings $instance;

    private function __construct()
    {
        Loader::includeModule("iblock");
    }

    private function getProp(string $propName)
    {
        return \Bitrix\Iblock\Iblock::wakeUp(PROJECT_SETTINGS_IBLOCK_ID)->getEntityDataClass()::query()
            ->setSelect([$propName.'_' => $propName])
            ->setFilter([
                'CODE' => PROJECT_SETTINGS_ELEMENT_CODE,
            ])
            ->setLimit(1)
            ->fetch()[$propName.'_VALUE'];
    }

    public function getHomeBannerPath()
    {
        $value = $this->getProp('CONTENT_HOME_BANNER');
        return \CFile::GetPath($value);
    }

    public function getFooterDesc()
    {
        if ($desc = $this->getProp('CONTENT_FOOTER_DESCRIPTION')) {
            return unserialize($desc)['TEXT'] ?? null;
        }

        return null;
    }

    public function getNotify()
    {
        return $this->getProp('NOTIFY');
    }

    public static function getInstance()
    {
        return self::$instance ?? self::$instance = new self();
    }
}