<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Заказ"); ?>

    <div class="orders_container container pagetype2">
        <div class="orders_block block">

            <? require_once $_SERVER['DOCUMENT_ROOT'] . SITE_MAIN_TEMPLATE_PATH . "/include/cabinet-menu.php" ?>
            <script>
                $('.account_nav__item.my_orders').addClass('active');
            </script>


            <div class="account_head">
                <div class="account_head__tohide">
                    <div class="account_head__title">Мои заказы</div>
                </div>
                <div class="account_head__goback">
                    <svg fill="none" height="13" viewBox="0 0 25 13" width="25" xmlns="http://www.w3.org/2000/svg"><path d="m7.67377 2.25-4.13194 4.25m0 0 4.13194 4.25m-4.13194-4.25h17.95817" stroke="#242424" stroke-linecap="round" stroke-linejoin="round"/></svg>
                    <span>Назад к заказам</span>
                </div>
            </div>

            <? $APPLICATION->IncludeComponent(
                "bitrix:sale.personal.order",
                "stress",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ALLOW_INNER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "CUSTOM_SELECT_PROPS" => array(""),
                    "DETAIL_HIDE_USER_INFO" => array("0"),
                    "DISALLOW_CANCEL" => "N",
                    "HISTORIC_STATUSES" => array("GV"),
                    "NAV_TEMPLATE" => "orders-more",
                    "ONLY_INNER_FULL" => "N",
                    "ORDERS_PER_PAGE" => "5",
                    "ORDER_DEFAULT_SORT" => "ID",
                    "PATH_TO_BASKET" => "/personal/cart",
                    "PATH_TO_CATALOG" => "/catalog/",
                    "PATH_TO_PAYMENT" => "/personal/order/payment/",
                    "PROP_1" => array(),
                    "REFRESH_PRICES" => "N",
                    "RESTRICT_CHANGE_PAYSYSTEM" => array("0"),
                    "SAVE_IN_SESSION" => "Y",
                    "SEF_FOLDER" => "/",
                    "SEF_MODE" => "Y",
                    "SEF_URL_TEMPLATES" => array("cancel" => "cancel/#ID#", "detail" => "detail/#ID#", "list" => "index.php"),
                    "SET_TITLE" => "Y",
                    "STATUS_COLOR_DD" => "gray",
                    "STATUS_COLOR_DE" => "gray",
                    "STATUS_COLOR_F" => "gray",
                    "STATUS_COLOR_GV" => "gray",
                    "STATUS_COLOR_N" => "green",
                    "STATUS_COLOR_NR" => "gray",
                    "STATUS_COLOR_OJ" => "gray",
                    "STATUS_COLOR_OP" => "gray",
                    "STATUS_COLOR_OT" => "gray",
                    "STATUS_COLOR_OV" => "gray",
                    "STATUS_COLOR_PSEUDO_CANCELLED" => "red",
                    "STATUS_COLOR_VZ" => "gray",
                )
            ); ?>

        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>