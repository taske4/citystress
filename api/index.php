<?

use Bitrix\Main\Application,
    Bitrix\Main\Context,
    Bitrix\Main\Request,
    Bitrix\Main\Server;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Content-Type: Application/Json');

$result = [];

$entity = $_REQUEST['entity'];
$action = $_REQUEST['action'];
$data   = $_REQUEST['data'];

if ($_GET['test']) {
    var_dump($entity);
    die;
}

$entity = "\Citystress\Api\\" . ucfirst($entity);

try {
    if (
        !class_exists($entity) ||
        !method_exists($entity, $action)
    ) {
        throw new \Exception('Fatal error.');
    }

    $result = $entity::$action($data);

} catch (\Error | \Exception $e) {
    $result['error'] = $e->getMessage() ?? 'Fatal error.';
} finally {
    echo json_encode($result);
    die;
}