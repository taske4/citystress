<?

/**
 * @global $APPLICATION
 */

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
?>

<div class="error404_container container">
    <div class="error404_block block">

        <div class="error404_content">
            <img class="error404_img" src="<?=SITE_MAIN_TEMPLATE_PATH?>/img/error_img.svg" alt="">
            <div class="error404_title">Страница не найдена</div>
            <div class="error404_text">Возможно она была удалена или устарела.<br> Попробуйте перепроверить правильность вводимого адреса</div>
            <div class="error404_actions">
                <button class="button button_black"><a href="/"><span>На главную</span></a></button>
                <button class="button button_black"><a href="/catalog/odezhda/"><span>В каталог</span></a></button>
            </div>
        </div>

    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>