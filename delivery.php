<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords",  '');
$APPLICATION->SetTitle("dostavka i vozvrat citystress.ru");?><div class="container taske-container">
    <div class="block taske-block">
        <p align="center">
            <br>
        </p>
        <p>
            <span style="font-weight: normal; line-height: 160%; font-size: 16px;"> <b>Доставка</b> </span>
        </p>
        <p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Возможные способы доставки: </span>
        </p>
        <p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			• Самовывоз </span>
        </p>
        <p>
        </p>
        <p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Вы можете получить заказ в розничных магазинах CityStress и СТРЕСС. Адреса магазинов можно посмотреть в разделе <i><u><a href="http://vm-1fe44102.na4u.ru/shops/">Магазины</a></u></i>. </span>
        </p>
        <p>
        </p>
        <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
		• Пункт выдачи<br>
		<ul>
		</ul>
		<p>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Через службу доставки в ближайший к вам ПВЗ. Адреса пунктов выдачи можно посмотреть <i><u><a href="http://vm-1fe44102.na4u.ru/shops/">здесь</a></u></i>. </span>
		</p>
		<p>
		</p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
		• Курьер<br>
		<ul>
		</ul>
		<p>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Доставка заказа курьером осуществляется компанией СДЭК на указанный Вами адрес. </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Правила доставки: </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— прием и обработка заказов осуществляется в будние дни с 9 до 18 часов;<br>
 </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— после того, как Вы оформили заказ с доставкой, с Вами свяжется менеджер для подтверждения заказа; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— доставка заказов производится курьерской службой CDEK; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— сбор заказа и доставка до терминала CDEK может производиться до двух рабочих дней, доставка до вашего пункта устанавливается курьерской службой; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— стоимость доставки рассчитывается индивидуально онлайн при оформлении заказа; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— стоимость доставки оплачивается онлайн-платежом на сайте интернет-магазина; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— стоимость заказа Вы можете оплатить на сайте со скидкой 10%, она не распространяется на доставку; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— при принятии заказа вы обязаны осмотреть товар и проверить количество, качество, ассортимент и целостность упаковки; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— при доставке курьерской службой у Вас есть возможность примерить товар (кроме чулочно-носочных изделий и нижнего белья). Время примерки ограничено (не более 15 минут); </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— если вам не подошел товар по цвету, фасону или размеру, вы можете вернуть его, заполнив бланк возврата, который идёт в комплекте с заказом. С условиями возврата можно ознакомиться<a href="https://base.garant.ru/12108380/#block_3000"> </a><i><u><a href="https://base.garant.ru/12108380/#block_3000">здесь</a></u></i>. Возврат денежных средств производится согласно правилам <i><u><a href="http://vm-1fe44102.na4u.ru/delivery.php">возврата денег</a></u></i>. </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— доставка за пределы России оговаривается индивидуально. </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;"> <b>Возврат</b> </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Условия возврата: </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Товар, купленный в магазинах нашей сети и не подошедший по каким-либо причинам (форма, габариты, фасон, расцветка, размер и комплектация), может быть возвращен в течение 14 (четырнадцати) календарных дней, не считая дня покупки (согласно ст. 25, п. 2 Закона РФ «О защите прав потребителей»). </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Товар оформленный в интернет-магазине и не подошедший по каким-либо причинам, может быть обменен или возвращен в любое время до момента доставки и в течение 7 (семи) дней после передачи (согласно ст. 26.1 Закона РФ «О защите прав потребителей»). </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Условия обмена/возврата товара надлежащего качества: </span>
		</p>
		<p>
		</p>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• товар не был в употреблении (новый и не использованный);</li>
 </span>
		</ul>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• сохранены потребительские свойства и товарный вид (этикетки; ярлыки, содержащие характеристики товара; оригинальная упаковка);</li>
 </span>
		</ul>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• заполнен бланк возврата. «<a href="/zayavlenie_vozvrat.docx"><u>Заявление на обмен/возврат</u></a>».</li>
 </span>
		</ul>
		<p>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> Товары, поставляемые в комплекте (например: брючный костюм или рубашки с брошью), необходимо возвращать также в комплекте. </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> При возврате товаров надлежащего качества стоимость доставки и комиссия за перевод платежа не возмещаются. </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> В случае неправильной комплектации заказа или наличии дефекта стоимость обратной доставки Почтой России оплачивает продавец. </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Условия обмена/возврата товара надлежащего качества: </span>
		</p>
		<p>
		</p>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• Недостатки товара, выявленные в момент передачи Покупателю должны быть отражены в письменной форме в акте доставки (за исключением постаматов). В ином случае обмен/возврат товара возможен только в пределах гарантийного срока. Гарантийный срок для одежды и аксессуаров составляет 30 дней с момента получения товара. Для обычной одежды срок гарантии начинает истекать со дня ее получения (или доставки по почте, курьером и т.д.). Для сезонных изделий гарантийный срок исчисляется с момента наступления времени года, для которого они предназначаются;</li>
 </span>
		</ul>
		<p>
		</p>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>•&nbsp;Гарантийный срок действует только при условии, что вы, приобретя вещь, в должной мере за ней ухаживали: </li>
 </span>
		</ul>
		<p>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— следовали размещенным на бирках рекомендациям; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— вовремя просушивали намокшую одежду, при этом исключая перегревание, а тем более прижигание ткани; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— стирали вещи – или, при недопустимости стирки, осуществляли другие разрешенные мероприятия по уходу; </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— особым образом, если это необходимо, обслуживали одежду для занятий спортом и фитнесом. </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Возврату не подлежат: </span>
		</p>
		<p>
		</p>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• Швейные и трикотажные изделия (изделия швейные и трикотажные бельевые, изделия чулочно-носочные);</li>
 </span>
		</ul>
		<p>
		</p>
		<p>
		</p>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• Предметы личной гигиены (заколки для волос и другие аналогичные товары) (п. 2 в ред. Постановления Правительства РФ от 20.10.1998 N 1222);</li>
 </span>
		</ul><p>
		</p>
		<p>
		</p>
		<ul>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			<li>• С полным списком товаров надлежащего качества, не подлежащих возврату (согласно Постановлению Правительства РФ от 19 января 1998 г. N 55), можно ознакомиться здесь: перечень товаров<i> </i><a href="https://www.consultant.ru/document/cons_doc_LAW_17579/c8b966271331ed0f59b793d7144df9b88335c640/"><u>тут</u></a><u>.</u></li>
 </span>
		</ul>
		<p>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Способы возврата: </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Возврат товаров через отделения компании "CDЕК" </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			1. Заполните «<a href="/zayavlenie_vozvrat.docx"><u>Заявление на обмен/возврат</u></a>» </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			2. Приложите товарный или кассовый чек (необязательно). </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			3. Приложите копию Вашего паспорта (его главного разворота). </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			4. Вложите в посылку изделие в фирменной упаковке. </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			5. Сообщить номер отправки возврата любым удобным способом нашему оператору. </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Возврат товара в фирменный магазин «CityStress», «СТРЕСС» </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			Если товар был получен в розничном магазине сети, то Вы можете вернуть заказ полностью или частично. Возврат можно осуществить в магазине в день его получения, сразу же после примерки или в течение 7 календарных дней. </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Возврат оформляется в магазине, в котором заказ был оплачен. Для оформления возврата Вам потребуется: </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— паспорт, </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— товарный или кассовый чек, </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			— заполненный бланк заявления (бланк Вы можете получить у персонала магазина или скачать <i><a href="/zayavlenie_vozvrat.docx"><u>Заявление на возврат/обмен</u></a></i>). </span>
		</p>
		<p>
			 &nbsp;
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 16px;">
			Рассмотрение заявлений о возврате </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
			1. Рассмотрение заявления о возврате или обмене товара ненадлежащего качества производится в течении 10 дней с момента его получения при наличие документов, указанных выше. </span> </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> 2. Рассмотрение заявления о возврате или обмене товара надлежащего качества производится в течении 10 дней с момента его получения. </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> 3. Возврат товара осуществляется в указанные рабочие часы с понедельника по пятницу. </span>
		</p>
		<p>
 <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> 4. Форма подачи заявления о возврате/обмене: на электронную на почту <a href="mailto:info@citystress.ru"><u>info@citystress.ru</u></a>, по согласованию с менеджером интернет-магазина (тел.: + 7 903-570-06-52) при онлайн заказе, по согласованию с розничным магазином (тел. 8 800-700-46-00) при покупке в розничной сети </span>
		</p>
 </span> </span>
    </div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
