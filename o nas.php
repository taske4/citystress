<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords",  '');
$APPLICATION->SetTitle("О нас");?><div class="container taske-container">
    <div class="block taske-block">
<span style="font-weight: normal; line-height: 160%; font-size: 14px;">Современный глобализованный мир заставляет нас соответствовать выдуманным стандартам, которые на самом деле ничего общего с индивидуальностью, красотой и счастьем не имеют. Наш покупатель не подстраивается под мейнстрим, беззаветно следуя основному безликому течению, он свободен в выборе и создает свой уникальный стиль, не пытаясь подогнать его под шаблон. Ведь в моде, как и в искусстве, нет правил, а рамки и ограничения каждый ставит себе сам.
<p style="text-align: center;">
 <img width="322" alt="1.png" src="/upload/medialibrary/7f5/plp1l0xxm71fi3o6gw6upfvapgfs6pzj.png" height="568" title="1.png"><img width="366" alt="2.png" src="/upload/medialibrary/7c2/s7um6gv2hhp2n8whkm5mb03pjrzqejsf.png" height="511" title="2.png">
</p>
 </span> <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> <strong>CityStress</strong> – бренд со своей философией и мировоззрением, предоставляющий свободу выбора, ведь только тебе решать, какой ты хочешь быть. Находясь в местах, где рождаются тренды, мы изучаем историю моды и проникаемся её идеями и духом. Видя их истоки и развитие, команда <strong>CityStress</strong> делится с Вами своим видением, создавая самобытные модели, которые помогут Вам найти баланс между своим внутренним и внешним миром. Наши стилисты-консультанты, опираясь на свои знания и опыт, помогут создать Ваш особенный образ, который позволит чувствовать себя уверенно и отразит Вашу натуру. Стилист не говорит, как надо одеваться, он лишь предлагает интересные варианты и сочетания, которые Вы бы сами вряд ли смогли сразу заметить. </span>
        <p style="text-align: center;">
            <img width="59" alt="3.png" src="/upload/medialibrary/98c/v8fz2ufjrwguv3i4b8f3olezh2dkjv03.png" height="440" title="3.png"><img width="375" alt="4.png" src="/upload/medialibrary/84c/z52qcq3jf4f0dnfnkqbknyafeftf48ke.png" height="573" title="4.png">
        </p>
        <br>
        <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
Наши покупатели вне возраста, но они - молоды. Ты молод – пока не боишься ошибаться и развиваться, пока не ставишь мнение окружающих выше своего и готов менять мир вокруг себя. Ты молод до тех пор, пока не загнал себя в рамки. Наша аудитория – это активные люди, которые проявляют себя в различных сферах жизни и никогда не останавливаются на достигнутом. </span>
        <p style="text-align: center;">
            <img width="514" alt="5.png" src="/upload/medialibrary/1b5/4bal1rmagccr5086s6290dplqs5jt0ai.png" height="676" title="5.png">
        </p>
        <br>
        <span style="font-weight: normal; line-height: 160%; font-size: 14px;"> Хорошая и стильная вещь – это не та вещь, которая стоит дорого, а та, которая подходит тебе и раскрывает твою индивидуальность, подчеркивает твою натуральную красоту. Дорогие вещи ограничивают человека, и он должен думать, в первую очередь, о них, а не о своих чувствах и эмоциях. Человек становится заложником своих вещей. Доступность бренда <strong>CityStress</strong> дает возможность избавиться от скованности и ощутить себя свободной. </span>
        <p style="text-align: center;">
            <img width="424" alt="6.png" src="/upload/medialibrary/b7e/q0w66gwkjdzpk19smhlnsikepv07hsh9.png" height="362" title="6.png"><br>
            <img width="383" alt="7.png" src="/upload/medialibrary/f49/8x9v5of4a1h2rl75zz3gmaujq10oawdp.png" height="238" title="7.png"><br>
            <img width="398" alt="8.png" src="/upload/medialibrary/ff9/1brrfuji92hvuzmf2ksr128i0x6b70sa.png" height="96" title="8.png">
        </p>
        <br>
        <span style="font-weight: normal; line-height: 160%; font-size: 14px;">
В дизайне наших магазинов мы отразили своё видение взаимосвязи природы и футуризма. Вдохновляясь стремлением человека приблизиться обратно к естественному и натуральному, мы использовали в интерьере такие природные мотивы как дерево и эко-трава, что привносит в магазин тепло и уют. Нам также близки идеи минимализма, привлекательность которого скрыта в простоте форм, динамике геометрических линий и отсутствии деталей, отвлекающих от самого главного. Это хорошо отражает потребности современного человека. Если тебе есть, что сказать, ты готова проявлять свою индивидуальность и не быть «одной из», то ты обязательно найдешь модели и образы, которые помогут раскрыть каждую из твоих сторон. И помни: «Будь собой. Еще ни одна копия не стала оригиналом».
<p style="text-align: center;">
 <img width="538" alt="9.jpg" src="/upload/medialibrary/445/zr8i0wz3je7dp6h4cc4rjx0arss5e42e.jpg" height="694" title="9.jpg">
</p>
 </span>
    </div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
